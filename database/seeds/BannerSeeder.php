<?php

use Illuminate\Database\Seeder;
use App\Banner;
use App\File;
use App\EntityImage;
class BannerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=1; $i < 10; $i++) {
            $file = File::inRandomOrder()->first();
            $banner = Banner::create([
                'position'   =>  rand(1,3),
                'start_date' =>  date('Y-m-d'),
                'end_date' =>  date('Y-m-30'),
                'title' =>  "Banner ".$i,
                'status'    =>  2
            ]);
            EntityImage::create([
                'file_id'   =>  $file->id,
                'entity_type'   =>  'App/Banner',
                'entity_id' =>  $banner->id,
                'zone'  =>  'banner'
            ]);
        }
    }
}
