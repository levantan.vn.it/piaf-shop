<?php

use Illuminate\Database\Seeder;
use App\Supplier;
class BrandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=1; $i < 10; $i++) {
            Supplier::create([
                'name'   =>  'Supplier '.$i,
                'email' =>  'emailsupplier'.$i.'@gmail.com',
                'phone' =>  '090929329'.$i,
                'status'    => 1,
                'representative'    =>  'representative',
                'location'    =>  "Location".$i,
                'address' =>  "Address ".$i
            ]);
        }
    }
}
