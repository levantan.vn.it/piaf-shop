<?php

use Illuminate\Database\Seeder;
use App\User;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=1; $i < 10; $i++) {
            User::create([
                'displayname'   =>  'user'.$i,
                'email' =>  'email'.$i.'@gmail.com',
                'password' => bcrypt('123123'),
                'fullname'  =>  "User Test ".$i,
                'phone' =>  '09092929'.$i,
                'birthday'  =>  '1994-08-'.$i,
                'gender'    =>  rand(1,2),
                'verify_phone'  => 1,
                'status'    => 1,
                'business_name' =>  "Company ".$i,
                'tax_invoice'   =>  '09092929'.$i,
                'representative'    =>  'representative',
                'store_name'    =>  "Store name".$i,
                'store_address' =>  "Address ".$i
            ]);
        }
    }
}
