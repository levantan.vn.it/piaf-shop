<?php

use App\Collection;
use Illuminate\Database\Seeder;

class CollectionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=1; $i < 10; $i++) {
            Collection::create([
                'name'   =>  'Winter Sale up to 50%',
                'section' =>  rand(1,3),
                'status'    =>  rand(1,2),
            ]);
        }
    }
}
