<?php

use Illuminate\Database\Seeder;
use App\Product;
use App\File;
use App\EntityImage;
use App\Category;
use App\Supplier;
use App\ProductSKU;
class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public $product_sku;
    public function run(Faker\Generator $faker)
    {
        for ($i=1; $i < 10; $i++) {
            $files = File::inRandomOrder()->limit(3)->get();
            $variant = [
                'title' =>  ["Origin",'Size'],
                'value' =>  [
                    ["USA","KOREA"],
                    ['77p/17kg','8p/17kg','12p/12kg']
                ]
            ];
            $variant = serialize($variant);

            $product = Product::create([
                'name'   =>  $faker->name,
                'category_id' =>  Category::inRandomOrder()->first()->id,
                'supplier_id' =>  Supplier::inRandomOrder()->first()->id,
                'variant'   =>  $variant,
                'description' =>  $faker->text,
                'status'    =>  1
            ]);
            $sku = [];
            $new_variant = unserialize($product->variant);
            $count = count($new_variant['title']);
            $this->product_sku = [];
            foreach ($new_variant['value'][0] as $value) {
                $sku = '#PRO'.$product->id.$value;
                $this->renderSku($sku,$new_variant['value'],$count,1,$sku);
            }
            $product_sku = [];
            foreach ($this->product_sku as $sku) {
                ProductSKU::create([
                    'sku'   =>  $sku,
                    'price' =>  $faker->randomDigit,
                    'qty'   =>  rand(1000,100000),
                    'status'    =>  rand(1,2),
                    'product_id'    =>  $product->id
                ]);
            }
            foreach ($files as $key) {
                EntityImage::create([
                    'file_name'   =>  'assets/img/non_verified.png',
                    'file_original_name'   =>  $key->file_original_name,
                    'file_size'   =>  $key->file_size,
                    'extension'   =>  $key->extension,
                    'type'   =>  $key->type,
                    'entity_type'   =>  'App\Product',
                    'entity_id' =>  $product->id,
                    'zone'  =>  'image'
                ]);
            }

        }
    }

    public function renderSku($sku,$new_variant,$count,$index,$old_sku){
        if($index >= $count){
            $this->product_sku[] = $sku;
            return true;
        }
        $old_sku = $sku;
        foreach ($new_variant[$index] as $value) {
            $sku = $old_sku;
            $sku .= $value;
            $this->renderSku($sku,$new_variant,$count,$index+1,$old_sku);
        }
    }
}
