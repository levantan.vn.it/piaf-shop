<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserSearchTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_search', function (Blueprint $table) {
            $table->unsignedBigInteger('search_id');
            $table->unsignedBigInteger('user_id');
            $table->timestamp('last_search_at')->nullable();
            $table->foreign('search_id')->references('id')->on('searches')->cascadeOndelete();
            $table->foreign('user_id')->references('id')->on('users')->cascadeOndelete();
            $table->primary(['search_id', 'user_id']);
        });


        Schema::table('searches', function (Blueprint $table) {
            $table->dropColumn('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('searches', function (Blueprint $table) {
            $table->bigInteger('user_id')->default(0)->nullable();
        });

        Schema::dropIfExists('user_search');
    }
}
