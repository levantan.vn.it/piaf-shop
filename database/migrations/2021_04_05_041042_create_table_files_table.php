<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('file_original_name',255)->nullable();
            $table->string('file_name',255)->nullable();
            $table->bigInteger('member_id')->default(0)->nullable();
            $table->bigInteger('user_id')->default(0)->nullable();
            $table->integer('file_size')->nullable();
            $table->string('extension',100)->nullable();
            $table->string('type',100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('files');
    }
}
