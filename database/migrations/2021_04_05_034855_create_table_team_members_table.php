<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableTeamMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('team_members', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->date('birthday')->nullable();
            $table->string('phone',20)->nullable();
            $table->string('password')->nullable();
            $table->string('email')->nullable();
            $table->string('address')->nullable();
            $table->string('id_number')->nullable();
            $table->enum('status',[1,2,3])->default(1)->nullable();
            $table->date('date_of_issue')->nullable();
            $table->bigInteger('role_id')->default(0)->nullable();
            $table->string('avatar')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('team_members');
    }
}
