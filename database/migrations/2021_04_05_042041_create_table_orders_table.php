<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('displayname')->nullable();
            $table->string('phone',20)->nullable();
            $table->bigInteger('user_id')->nullable();
            $table->string('delivery_address',255)->nullable();
            $table->double('sub_total')->nullable();
            $table->double('delivery_fee')->nullable();
            $table->double('total')->nullable();
            $table->string('delivery_method',255)->nullable();
            $table->string('delivery_id',255)->nullable();
            $table->string('shipper_name',255)->nullable();
            $table->string('shipper_phone',20)->nullable();
            $table->date('delivery_date')->nullable();
            $table->tinyInteger('status')->default(1)->nullable();
            $table->text('images')->nullable();
            $table->text('id_images')->nullable();
            $table->text('note')->nullable();
            $table->text('reason_cancel')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
