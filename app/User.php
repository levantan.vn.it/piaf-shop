<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use App;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    protected $table = 'tdm_user';
    protected $appends = ['avatar_url'];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'USER_NO', 'TYPE', 'STATUS', 'REG_NAME', 'NICK_NAME', 'ID', 'PASSWD', 'HINT_QST', 'HINT_QST_ANSWER', 'EMAIL', 'HOME_ZIP', 'HOME_ADDR1',
        'HOME_TEL', 'CELLPHONE', 'SMS', 'REAL_NAME', 'SELF_INTRO', 'PIC', 'OPEN', 'REG_DT', 'MOD_DT', 'PASSWORD_CHANGE_DE', 'PASSWORD_INITL_AT', 'SNS_KND_CODE',
        'FLLWR_CO','SNS_ID','ACCML_PNTT','DRMNCY_AT','DELETE_AT','LOGIN_CO','LAST_LOGIN_DT','LINK_URL','TRY_CLICK_DT','REVIEW_CLICK_DT',
        'BIRTHDAY','GENDER','P_CNT','M_CNT','PUSH_GET_YN','SLUG','allow_review','allow_comment','role_id','password','created_at','updated_at',
        'deleted_at','message','hot_fimer_7days','hot_fimer_30days','hot_fimer_90days','hot_fimer_1year','facebook_url','reviews','hot_fimer',
        'verification','verification_max','verification_at','access_token','update_review','update_pic','check_cron','add_firebase_ids',
        'firebase_ids','push_fime','push_follow','push_me','follower','following','level_number','number_point','event_point','history_point','basic_point',
        'fistar_join','update_event','add_point_brithday','level_qa','point_tmp','youtube_url','instagram_url','tiktok_url','street_id',
        'city_id','district_id','ward_id','time_update_pic','permission','add_point_address','external_company','user_agent','ipaddress','pier_role',
        'zodiac','interest_ids','career_id','expense_id','add_point_update_profile','last_update_role_pier'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function deviceFCMs()
    {
        return $this->hasMany('App\UserDeviceFcms', 'user_id');
    }

    /**
     * ------------------------------
     * Notification TO
     * ------------------------------
     */
    /**
     * Specifies the user's FCM token
     *
     * @return array
     */
    public function routeNotificationForFcm()
    {
        return $this->deviceFCMs()->pluck('token')->toArray();
    }


    /**
     * Get all of the payment method for the User
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function payments()
    {
        return $this->hasMany(UserPayment::class, 'user_id');
    }

    /**
     * Get all of the preimary payment method for the User
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function paymentPrimary()
    {
        return $this->hasOne(UserPayment::class, 'user_id')->whereType(1);
    }

    /**
     * Get all of the delivery_books for the User
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function deliveryBooks()
    {
        return $this->hasMany(DeliveryBook::class, 'user_id');
    }

    /**
     * Get all of the primary delivery_books for the User
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function deliveryBookPrimary()
    {
        return $this->hasOne(DeliveryBook::class, 'user_id')->where('delivery_books.type', DeliveryBook::PRIMARY);
    }

    public function genderText()
    {
        if ($this->gender == 1) {
            return __('users.male');
        } elseif ($this->sex == 2) {
            return __('users.female');
        }
        return "";
    }

    public function statusText()
    {
        if ($this->status == 1) {
            return __('users.active');
        } elseif ($this->status == 2) {
            return __('users.deactive');
        } elseif ($this->status == 3) {
            return __('users.block');
        }
        return "";
    }

    public function isActive()
    {
        return intval($this->status) === 1;
    }

    public function isInActive()
    {
        return intval($this->status) === 2;
    }


    public function likeProducts()
    {
        return $this->belongsToMany(Product::class, 'like_products', 'user_no', 'product_id');
    }


    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function transaction()
    {
        return $this->hasMany(Transaction::class);
    }

    public function getAvatarUrlAttribute()
    {
        return my_asset($this->avatar);
    }

    public function carts()
    {
        return $this->hasMany(Cart::class, 'user_id');
    }

    public function searches()
    {
        return $this->belongsToMany(Search::class, 'user_search', 'user_id', 'search_id')->withPivot('last_search_at');
    }

    public function fk_district()
    {
        return $this->hasOne(District::class, 'id', 'district_id');
    }

    public function fk_ward()
    {
        return $this->hasOne(Ward::class, 'id', 'ward_id');
    }

    public function fk_city()
    {
        return $this->hasOne(City::class, 'id', 'city_id');
    }

    public function getFullAddressAttribute()
    {
        $list = [$this->store_address];
        if (!empty($this->fk_ward->name)) {
            $list[] = $this->fk_ward->name;
        }
        if (!empty($this->fk_district->name)) {
            $list[] = $this->fk_district->name;
        }
        if (!empty($this->fk_city->name)) {
            $list[] = $this->fk_city->name;
        }
        return implode(", ", $list);
    }
}
