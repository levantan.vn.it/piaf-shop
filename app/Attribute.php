<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App;

class Attribute extends Model
{
    public function getTranslation($field = '', $lang = false){
        return $this->$field;
    }

    public function attribute_translations(){
      return $this->hasMany(AttributeTranslation::class);
    }

}
