<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    protected $table = 'banners';
    protected $fillable = [
        'id','collection_id', 'position', 'start_date', 'end_date', 'title', 'status', 'created_at','updated_at','images','id_images'
    ];

    const POSITIONS = [
        '홈화면' => 1,
        '카테고리'=>2,
        '검색 페이지'=>3
    ];

    public function collection()
    {
        return $this->belongsTo(Collection::class);
    }
    public function isActive()
    {
        return intval($this->status) === 1;
    }
    public function isInActive()
    {
        return intval($this->status) === 2;
    }

    public function checkStatus() {
        if($this->status == 1) {
            return '스탠바이';
        } else if($this->status == 2){
            return '게시중';
        } else if($this->status == 3){
            return '완료';
        }else {
            return '취소';
        }
    }

    public function changeStatus(){
        if($this->status==1){
            return '비활성';
        }else{
            return'액티브';
        }

    } 
    public function checkPosition(){
        if($this->position ==1){
            return '홈화면';
        } else if($this->position ==2 ){
            return '카테고리';

        } else {
            return '검색 페이지';
        }
    }
}
