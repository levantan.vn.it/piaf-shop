<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeliveryManagement extends Model
{
    protected $table = "delivery_management";
    protected $fillable = [
        'type_delivery',
        'distance_type',
        'delivery_from',
        'delivery_to',
        'price',
        'list_area',
        'interprovincial_type',
        'list_city',
        'created_at',
        'updated_at'
    ];

    public function deliveryCity()
    {
        return $this->belongsToMany(City::class, 'delivery_management_city', 'delivery_id', 'city_id');
    }

    public function deliveryDistrict()
    {
        return $this->belongsToMany(City::class, 'delivery_management_city', 'delivery_id', 'district_id');
    }
}
