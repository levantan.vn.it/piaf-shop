<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeliveryBook extends Model
{
    protected $table = 'delivery_books';

    const PRIMARY = 1;
    const NORMAL = 0;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'location_name',
        'user_id',
        'address',
        'receiver',
        'phone',
        'type',
        'post_code',
        'city_id','district_id','ward_id', 'detailed_address'
    ];

    protected $casts = [
        'type' => 'integer',
    ];
    // protected $appends = ['full_address'];

    public function typeText()
    {
        if ($this->type == 1) {
            return __('users.primary');
        } elseif ($this->type == 0) {
            return __('users.normal');
        }
        return "";
    }

    public function isPrimary()
    {
        return intval($this->type) === static::PRIMARY;
    }
    public function isNormal()
    {
        return intval($this->type) === static::NORMAL;
    }

    public function fk_district()
    {
        return $this->hasOne(District::class, 'id', 'district_id');
    }

    public function fk_ward()
    {
        return $this->hasOne(Ward::class, 'id', 'ward_id');
    }

    public function fk_city()
    {
        return $this->hasOne(City::class, 'id', 'city_id');
    }

    public function getFullAddressAttribute()
    {
        $list = [$this->address];
        if(!empty($this->fk_ward->name)){
            $list[] = $this->fk_ward->name;
        }
        if(!empty($this->fk_district->name)){
            $list[] = $this->fk_district->name;
        }
        if(!empty($this->fk_city->name)){
            $list[] = $this->fk_city->name;
        }
        return implode(", ",$list);
    }
}
