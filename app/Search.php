<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Tymon\JWTAuth\Facades\JWTAuth;

class Search extends Model
{
  //

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'query',
    'user_id',
    'count'
  ];

  public function auth_search()
  {
    return $this->hasOne(UserSearch::class, 'search_id')->where('user_search.user_id', auth('api')->id());
  }
}
