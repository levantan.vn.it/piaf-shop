<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Collection extends Model
{
    protected $table = 'collections';

    protected $fillable = [
        'name','section'
        ,'images',
        'status','id_images',
        'short_description',
        'start_date','end_date',
        'brand_id'
    ];

    public function products(){
    	return $this->belongsToMany(Product::class,'collection_products','collection_id','product_id');
    }

    public function suppliers() {
        return $this->belongsTo(Supplier::class,'brand_id','id');
    }
    public function allProduct(){
        $products = Product::where('status', '1')->get();
        return $products;
    }

    public function allBrands() {
        $brands = Supplier::where('status','1')->get();
        return $brands;
    }

    public function statusText()
    {
        if ($this->status == 1) {
            return __('collections.active');
        }else{
            return __('collections.inactive');
        }
        return "";
    }

    public function sectionText()
    {
        if ($this->section == 1) {
            return __('collections.hot_deal');
        } elseif ($this->section == 2) {
            return __('collections.special_events');
        }elseif ($this->section == 3) {
            return __('collections.end_season_sale');
        }
        return "";
    }
}
