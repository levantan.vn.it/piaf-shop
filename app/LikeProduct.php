<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LikeProduct extends Model
{
    protected $table = 'like_products';
    public $timestamps = false;
    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'id', 'user_no', 'product_id'
    ];

    public function products()
    {
        return $this->belongsTo(Product::class,'product_id','id');
    }
}
