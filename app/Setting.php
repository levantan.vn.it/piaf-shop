<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $table = 'settings';
    protected $fillable = [
        'type',
        'value'
    ];

    const POLICY = 'policy_service';
    const TERMS = 'term_service';

    public function scopeOfType($query, $type){
        return $query->select('value','updated_at')->where('type',$type);
    }
}
