<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class TrimSpace implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $remove = strip_tags($value);
        $new = html_entity_decode($remove);
        $str = str_replace('&nbsp;', ' ', htmlentities($new));
        return (trim($str) !== '');
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return ':attribute '.__('settings.msg_validate_space');
    }
}
