<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class SupplierExport implements FromCollection, WithHeadings,WithMapping,ShouldAutoSize
{

    private $data;

    public function __construct($data){
        $this->data = $data;
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return $this->data;
    }

    /**
     * Set header columns
     *
     * @return array
     */
    public function headings(): array
    {
        return [
            '브랜드 ID',
            '브랜드명',
            'Email',
            '지역',
            '가입일',
            '상품수',
            '상태',
        ];
    }
        /**
     * Mapping data
     *
     * @return array 
     */
    public function map($suppllier): array
    {
        return [
            $suppllier->id,
            $suppllier->name,
            $suppllier->email,
            $suppllier->location,
            $suppllier->created_at->format('Y/m/d'),
            count($suppllier->products),
            $suppllier->statusText(),
        ];
    }
}
