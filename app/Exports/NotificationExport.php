<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class NotificationExport implements FromCollection,WithMapping,ShouldAutoSize,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    private $data;
    public function __construct($data)
    {
        $this->data = $data;
    }
    public function collection()
    {
        return $this->data;
    }

    public function headings():array
    {
        return [
            '알림 ID',
            '타이틀',
            '알림 유형',
            '생성일	',
            '상태',
        ];
    }
    public function map($noti):array
    {
        return [
            $noti->id,
            $noti->title,
            $noti->typeText(),
            date('Y/m/d',strtotime(optional($noti)->created_at)),
            $noti->statusText(),

        ];
    }

}
