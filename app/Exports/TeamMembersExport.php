<?php

namespace App\Exports;

use App\TeamMember;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class TeamMembersExport implements FromView,ShouldAutoSize
{
    protected $members;

    public function __construct($members)
    {
        $this->members = $members;
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        return view('exports.members', [
            'members' => $this->members
        ]);
    }
}
