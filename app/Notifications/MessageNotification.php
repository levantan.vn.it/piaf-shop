<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use App\User;
use App\Message;
use Illuminate\Notifications\DatabaseNotification;
use NotificationChannels\Fcm\FcmChannel;
use NotificationChannels\Fcm\FcmMessage;
use NotificationChannels\Fcm\Resources\AndroidConfig;
use NotificationChannels\Fcm\Resources\AndroidFcmOptions;
use NotificationChannels\Fcm\Resources\AndroidMessagePriority;
use NotificationChannels\Fcm\Resources\AndroidNotification;
use NotificationChannels\Fcm\Resources\ApnsConfig;
use NotificationChannels\Fcm\Resources\ApnsFcmOptions;

class MessageNotification extends Notification implements ShouldQueue
{
    use Queueable;
    private $message;

    /**
     * Create a new notification instance.
     * @param  Message  $message
     */
    public function __construct($message)
    {
        $this->message = $message;
    }

    /**
     * @return string[]
     */
    private function getDataNotification()
    {
        $sender = (object)[];
        if ($this->message->sender_id != config('constant.ID_ADMIN_FIREBASE'))
            $sender = optional($this->message)->sender;
        else
            $sender->fullname = config('constant.NAME_ADMIN_FIREBASE');
        $data = [
            'title' => 'UNKNOWN',
            'content' => 'UNKNOWN',
            'action_name' => 'UNKNOWN',
            'conversation_id' => optional($this->message)->conversation_id,
            'product_id' => optional($this->message)->product_id
        ];
        if ($this->message) {
            $data["title"] = __('message.one_message');
            $data["content"] = optional($sender)->fullname . ': ' . $this->message->message;
            $data["product_id"] = $this->message->product_id;
            $data["action_name"] = 'NEW_MESSAGE';
        }
        return $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        if ($this->message->notify_enabled) {
            return ['database', FcmChannel::class];
        }
        return ['database'];
    }

    public function toFcm($notifiable)
    {
        $notify = DatabaseNotification::where([
            'notifiable_id' => $notifiable->id,
            'notifiable_type' => 'App\User',
            'type' => 'App\Notifications\MessageNotification'
        ])->latest()->first();
        $notify = optional($notify);

        $sender = (object)[];
        $dataContent = $this->getDataNotification();
        if ($this->message->sender_id != config('constant.ID_ADMIN_FIREBASE'))
            $sender = $this->message->sender;
        else {
            $sender->fullname = config('constant.NAME_ADMIN_FIREBASE');
            $sender->avatar = config('constant.AVATAR_ADMIN_FIREBASE');
        }
        return FcmMessage::create()
            ->setData([
                'sender_id' => $this->message->sender_id . "",
                'receiver_id' => $this->message->receiver_id . "",
                'sender_name' => $sender->fullname . "",
                'user_avatar' => $sender->avatar . "",
                'action_name' => $dataContent['action_name'] . "",
                'message' => $this->message->message . "",
                'conversation_id' => $this->message->conversation_id . "",
                'product_id' => $this->message->product_id . "",
                'notification_id' => $notify->id
            ])
            ->setNotification(\NotificationChannels\Fcm\Resources\Notification::create()
                ->setTitle($dataContent['title'])
                ->setBody($dataContent['content']))
            ->setAndroid(
                AndroidConfig::create()
                    ->setFcmOptions(AndroidFcmOptions::create()->setAnalyticsLabel('message_notify_android'))
                    ->setNotification(AndroidNotification::create()->setColor('#f7dc7a')->setClickAction('FLUTTER_NOTIFICATION_CLICK'))
            )->setApns(
                ApnsConfig::create()
                    ->setFcmOptions(ApnsFcmOptions::create()->setAnalyticsLabel('message_notify_ios'))
            );
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return $this->getDataNotification();
    }

    // public function toMail($notifiable)
    // {
    //     $sender = (object)[];
    //     $getDataNotification = $this->getDataNotification();
    //     if ($this->message->sender_id != config('constant.ID_ADMIN_FIREBASE'))
    //         $sender = $this->message->sender;
    //     else {
    //         $sender->fullname = config('constant.NAME_ADMIN_FIREBASE');
    //     }
    //     $receiver = optional($this->message)->receiver;
    //     return (new MailMessage)
    //         ->from(config('constant.CONST_MAILADDR_ADMIN'), config('constant.QUICHEF'))
    //         ->subject('You have a new message from ' . $sender->fullname)
    //         ->greeting('Hi ' . $receiver->fullname . '!')
    //         ->line('You have a new message from ' . $sender->fullname)
    //         ->line($getDataNotification['content'])
    //         ->line('Thank you for using our application!');
    // }
}
