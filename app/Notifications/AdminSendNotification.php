<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Notifications\DatabaseNotification;
use NotificationChannels\Fcm\FcmChannel;
use NotificationChannels\Fcm\FcmMessage;
use NotificationChannels\Fcm\Resources\AndroidConfig;
use NotificationChannels\Fcm\Resources\AndroidFcmOptions;
use NotificationChannels\Fcm\Resources\AndroidNotification;
use NotificationChannels\Fcm\Resources\ApnsConfig;
use NotificationChannels\Fcm\Resources\ApnsFcmOptions;

class AdminSendNotification extends Notification implements ShouldQueue
{
    use Queueable;
    public $title;
    public $content;
    public $image;
    public $message_id;
    public $created_at;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($message_id, $title, $content, $image, $created_at)
    {
        $this->message_id = $message_id;
        $this->title = $title;
        $this->content = $content;
        $this->image = $image;
        $this->created_at = $created_at;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', FcmChannel::class];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Notification User')
            ->line('The introduction to the notification')
            ->line($this->title)
            ->line($this->content)
            ->action('Notification Action', url('/'))
            ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'message_id' => $this->message_id . "",
            'title' =>  $this->title,
            'content' =>  $this->content,
            'images'    =>  $this->image,
            'created_at' => $this->created_at,
            'action_name' => config('constant.ADMIN_ACTION')
        ];
    }
    // public function toDatabase($notifiable)
    // {
    //     return [
    //         'title' =>  $this->title,
    //         'content' =>  $this->content,
    //         'images'    =>  $this->images,
    //     ];
    // }

    public function toFcm($notifiable)
    {
        $notify = DatabaseNotification::where([
            'notifiable_id' => $notifiable->id,
            'notifiable_type' => 'App\User',
            'type' => 'App\Notifications\AdminSendNotification'
        ])->latest()->first();
        $notify = optional($notify);
        $data = [
            'message_id' => $this->message_id . "",
            'title' =>  $this->title,
            'content' =>  $this->content,
            'image'    =>  my_asset($this->image),
            'created_at' => $notify->created_at,
            'action_name' => config('constant.ADMIN_ACTION'),
            'notification_id' => $notify->id
        ];
        return FcmMessage::create()
            ->setData($data)
            ->setNotification(
                \NotificationChannels\Fcm\Resources\Notification::create()
                    ->setTitle($this->title)
                    ->setBody($this->content)
                    ->setImage(my_asset($this->image))
            )
            ->setAndroid(
                AndroidConfig::create()
                    ->setFcmOptions(AndroidFcmOptions::create()->setAnalyticsLabel('analytics'))
                    ->setNotification(AndroidNotification::create()->setColor('#0A0A0A')->setClickAction('FLUTTER_NOTIFICATION_CLICK'))
            )->setApns(
                ApnsConfig::create()
                    ->setFcmOptions(ApnsFcmOptions::create()->setAnalyticsLabel('analytics_ios'))
            );
    }
}
