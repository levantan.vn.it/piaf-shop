<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequestBusiness extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'business_name' => 'required|max:255',
            'tax_invoice' => 'required|min:10',
            'representative' => 'required|max:255',
            'store_name' => 'required|max:255',
            'store_address' => 'required|max:255',
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            if (!empty($validator->errors()->all())) {
            $validator->errors()->add('UserRequestBusiness', 'is-invalid');
            }
        });
    }
    public function attributes()
    {
        return [
            'business_name' => '상호명',
            'tax_invoice' => '등록번호',
            'representative' => '대표명',
            'store_name' => '세금계산서 항목',
            'store_address' => '배송 수령지',
        ];
    }
}
