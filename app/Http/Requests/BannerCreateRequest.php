<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BannerCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string|max:255',
            'id_files'=>'required',
            'position'  => 'required',
            'collection_id' => 'required|exists:collections,id',
            'start_date'=>'required|date',
            'end_date'  => 'required|date|after:start_date',
        ];
    }

    public function attributes(){
        return [
            'end_date' => '종료일',
            'start_date' => '시작일',
            'title' => '표제',
            'position' => '위치 필드는 필수입니다.',
            'collection_id'=> '컬렉션 ID',
            'id_files'=>'배너 이미지',
        ];
    }
}
