<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'shipper_name' => 'required',
            // 'shipper_phone' => 'required|max:12',
            'user_id' => 'required',
            'phone'=> 'required|max:12',
            'displayname'=>'required|max:255',
            'receiver'=>'required|max:255',
            'delivery_address'=>'required|max:255',
            'delivery_fee'=>'required|max:255',
            'delivery_method'=>'required',
            'id'=>'required',
        ];
    }

    public function attributes()
    {
        return [
            'id'=>'상품',
            'user_id' => '사용자 ID',
            'phone'=> '전화',
            'displayname'=>'이름 표시하기',
            'receiver'=>'리시버',
            'delivery_fee'=>'배달비',
            'delivery_method'=>'배달 방법',
            'delivery_address'=>'배송 주소록',
        ];
    }
    
}
