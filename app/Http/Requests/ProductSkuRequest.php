<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Validator;

class ProductSkuRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sku' => 'bail|required|array',
            'weight'    => 'required|array|size:'.count($this->sku),
            'weight.*'     => 'required|min:0|',
            'price'     => 'required|array|size:'.count($this->sku),
            'price.*'     => 'required|min:0|',
            'qty'       =>  'required|array|size:'.count($this->sku),
            'qty.*'     => 'required|integer|min:0|',
            'sale'       =>  'nullable|array|size:'.count($this->sku),
            'sale.*' =>'nullable|integer|max:100'
        ];
    }

    public function attributes()
    {
        return [
            'weight.*'=>'무게',
            'price.*'=>'가격',
            'qty.*'=>'수량',
            'sale.*'=>'판매',
        ];
    }
}
