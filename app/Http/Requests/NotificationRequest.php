<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NotificationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string|max:100',
            'type' => 'required',
            'content' => 'required|string|min:10',
            'id_files' => 'required|string'
        ];
    }
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            if (!empty($validator->errors()->all())) {
            $validator->errors()->add('NotificationRequest', 'is-invalid');
            }
        });
    }

    public function attributes()
    {
        return [
            'title' => __('notifications.noti_title'),
            'type' => __('notifications.type'),
            'content' => __('notifications.noti-content'),
            'id_files' => __('aiz_uploader.image')
        ];
    }
}
