<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InterprovincialRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'inter.*.city_id'=> 'required|array',
            'inter.*.price'=> 'required|integer',
            'airplane.*.city_id'=> 'required|array',
            'airplane.*.price'=> 'required|integer'
        ];
    }

    public function messages()
    {
        return [
            'required' => '이 필드는 비워 둘 수 없습니다.',
        ];
    }
}
