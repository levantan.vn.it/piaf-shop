<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TeamMemmberRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:20',
            'birthday' => 'required|before:today',
            'phone' => 'required|numeric|digits:11|regex:/[0-9]{11}/',
            'email' => 'required|email|unique:team_members',
            'address' => 'required|string',
            'id_number' => 'numeric',
            'date_of_issue' => 'required',
            'role_id' => 'required',
            'id_files' => 'required|string',
        ];
    }
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            if (!empty($validator->errors()->all())) {
            $validator->errors()->add('TeamMemmberRequest', 'is-invalid');
            }
        });
    }

    public function attributes()
    {
        return [
            'name' => __('members.noti-name'),
            'birthday' => __('members.noti-date'),
            'email' => __('members.email'),
            'phone' => __('members.phone'),
            'address' => __('members.noti-address'),
            'id_number' => __('members.noti-id'),
            'date_of_issue' => __('members.noti-date2'),
            'role_id' => __('members.role'),
            'id_files' => __('aiz_uploader.image'),
        ];
    }
}
