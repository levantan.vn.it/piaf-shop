<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Tymon\JWTAuth\Facades\JWTAuth;

class PaymentMethodRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'card_number' => 'bail|required|regex:/^[0-9]{12,19}$/|max:50',
            'card_name' => 'bail|required|string|max:100',
            'expiry_date' => 'bail|required|regex:/^[0-9]{2}\/[0-9]{2}$/',
            'ccv' => 'bail|required|regex:/^[0-9]{3,4}$/',
            'type' => 'bail|required|in:0,1',
        ];
    }
}
