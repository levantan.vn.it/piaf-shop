<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MemberRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:20',
            'birthday' => 'required|before:today',
            'phone' => 'required|numeric|digits:11|regex:/[0-9]{11}/',
            'address' => 'required|string',
            'id_number' => 'numeric',
            'date_of_issue' => 'required',
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            if (!empty($validator->errors()->all())) {
            $validator->errors()->add('MemberRequest', 'is-invalid');
            }
        });
    }

    public function attributes()
    {
        return [
            'name' => __('members.noti-name'),
            'birthday' => __('members.noti-date'),
            'email' => __('members.email'),
            'phone' => __('members.phone'),
            'address' => __('members.noti-address'),
            'id_number' => __('members.noti-id'),
            'date_of_issue' => __('members.noti-date2')
        ];
    }

}
