<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:255',
            'id_files' => 'required',
            // 'id_main_files' => 'required',
            // 'id_images' => 'required',

            'parent_id'=>'nullable'

        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            if (!empty($validator->errors()->all())) {
            $validator->errors()->add('CategoryRequest', 'is-invalid');
            }
        });
    }

    public function attributes()
    {
        return [
            'title' => '표제',
            'id_files' => '이미지',


        ];
    }
}
