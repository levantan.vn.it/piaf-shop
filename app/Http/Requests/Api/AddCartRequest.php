<?php

namespace App\Http\Requests\Api;

use App\ProductSKU;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class AddCartRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_id' => ['bail', 'required', Rule::exists('products', 'id')->whereNull('deleted_at')],
            'qty' => 'bail|required|integer|min:1|max:4294967295',
        ];
    }

    public function messages()
    {
        return [
            'product_id.exists' => __('api_not_found.Product')
        ];
    }
}
