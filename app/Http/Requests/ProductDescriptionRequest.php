<?php

namespace App\Http\Requests;

use App\Rules\TrimSpace;
use Illuminate\Foundation\Http\FormRequest;

class ProductDescriptionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'description' => ['required','string','min:10', new TrimSpace]
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            if (!empty($validator->errors()->all())) {
            $validator->errors()->add('ProductDescriptionRequest', 'is-invalid');
            }
        });
    }

    public function attributes()
    {
        return [
            'description' => __('products.description'),
        ];
    }
}
