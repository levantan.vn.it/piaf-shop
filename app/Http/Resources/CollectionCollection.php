<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class CollectionCollection extends ResourceCollection
{
    public function toArray($request)
    {

        return [
            'data' => $this->collection->map(function($data) {
                $images = get_asset_from_list($data->images);
                return [
                    'id' => $data->id,
                    'name' => $data->name,
                    'section' => $data->section,
                    'images'    =>  $images,
                    'main_image' => my_asset($data->main_image),
                    'end_date'=> $data->end_date,
                    'products' =>  $data->products,
                    'short_description'=> $data->short_description,
                ];
            })
        ];
    }

    public function with($request)
    {
        return [
            'success' => true,
            'status' => 200
        ];
    }
}
