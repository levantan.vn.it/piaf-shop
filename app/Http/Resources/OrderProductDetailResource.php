<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderProductDetailResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return is_null($this->resource) ? [] : [
            'name' => $this->product->name,
            'product_id' => $this->product_id,
            'product_sku' => ProductSkuResource::make($this->productSku),
            'qty' => $this->qty,
            'subtotal' => $this->subtotal,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->product->deleted_at ?? $this->productSku->deleted_at,
            'variant' => $this->parse_variant,
            'photo' => $this->product->photos
        ];
    }

    public function __construct($resource)
    {
        $this->resource = $resource;
        $this->additional([
            'success' => true,
            'status' => 200
        ]);
    }

    public static function collection($resource)
    {
        return parent::collection($resource)->additional([
            'success' => true,
            'status' => 200
        ]);
    }
}
