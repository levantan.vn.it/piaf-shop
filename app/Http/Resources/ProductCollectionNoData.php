<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ProductCollectionNoData extends ResourceCollection
{
    public function toArray($request)
    {

        return $this->collection->map(function ($data) {
            $photo = get_asset_from_list($data->images);
            return [
                'id'    =>  $data->id,
                'name' => $data->name,
                'description' => $data->description,
                'brand_name'  =>  optional($data->supplier)->name,
                'photo' => $photo,
                'price' => $data->productSku->price,
            ];
        });
    }

    public function with($request)
    {
        return [
            'success' => true,
            'status' => 200
        ];
    }
}
