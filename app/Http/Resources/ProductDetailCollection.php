<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductDetailCollection extends JsonResource
{
    public function toArray($request)
    {
        $photo = get_asset_from_list($this->images);
        return [
            'name' => $this->name,
            'description' => $this->description,
            'photo' => $photo,
            'variant' => !empty($this->variant) ? unserialize($this->variant) : '',
            'sku' => $this->skus()->select('id', 'sku', 'price', 'qty')->where('status', 1)->get(),
        ];
    }

    public function with($request)
    {
        return [
            'success' => true,
            'status' => 200
        ];
    }
}
