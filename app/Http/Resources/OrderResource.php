<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return is_null($this->resource) ? [] : [
            'id' => $this->id,
            'displayname' => $this->displayname,
            'phone' => $this->phone,
            'email' => $this->email,
            'sub_total' => $this->sub_total,
            'delivery_fee' => $this->delivery_fee,
            'total' => $this->total,
            'status' => $this->status,
            'user_id' => $this->user_id,
            'count_qty' => $this->OrderProduct->count('qty'),
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'products' => OrderProductDetailResource::collection($this->OrderProduct)->first()
        ];
    }

    public static function collection($resource)
    {
        return parent::collection($resource)->additional([
            'success' => true,
            'status' => 200
        ]);
    }
}
