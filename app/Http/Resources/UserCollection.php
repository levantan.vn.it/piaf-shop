<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Http\Resources\Json\JsonResource;
class UserCollection extends JsonResource
{
    public function toArray($request)
    {
        return [
            'user_no'       => $this->USER_NO,
            'reg_name'      => $this->REG_NAME,
            'status'        => $this->STATUS,
            'email'         => $this->EMAIL,
            'HOME_ADDR1'    => $this->HOME_ADDR1,
            'HOME_TEL'      => $this->HOME_TEL,
            'CELLPHONE'     => $this->CELLPHONE,
            'PIC'           => $this->PIC,
            'BIRTHDAY'      => $this->BIRTHDAY,
            'GENDER'        => $this->GENDER,
            'SLUG'          => $this->SLUG,
            "allow_review"  => $this->allow_review,
            "allow_comment" => $this->allow_comment, 
            "role_id" => $this->role_id, 
            "deleted_at" => $this->deleted_at, 
            "facebook_url" => $this->facebook_url, 
            "update_pic" => $this->update_pic, 
            "follower" => $this->follower, 
            "following" => $this->following, 
            "number_point" => $this->number_point, 
            "message" => $this->message, 
        ];
    }

    public function with($request)
    {
        return [
            'success' => true,
            'status' => 200
        ];
    }
}
