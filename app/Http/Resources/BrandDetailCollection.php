<?php

namespace App\Http\Resources;

use App\Product;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\DB;

class BrandDetailCollection extends JsonResource
{
    public function toArray($request)
    {
        return is_null($this->resource) ? [] : [
            'id' => $this->id,
            'name' => $this->name,
            'status' => $this->status,
            'address' => $this->address,
            'location' => $this->location,
            'email' => $this->email,
            'phone' => $this->phone,
            'representative' => $this->representative,
            'images'  =>  $this->images,
            'products_count' => $this->products_count,
            'orders_count' => $this->orders()->count()
        ];
    }

    public function __construct($resource)
    {
        $this->resource = $resource;
        $this->additional([
            'success' => true,
            'status' => 200
        ]);
    }

    public static function collection($resource)
    {
        return parent::collection($resource)->additional([
            'success' => true,
            'status' => 200
        ]);
    }
}
