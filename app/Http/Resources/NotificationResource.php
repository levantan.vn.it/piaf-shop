<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class NotificationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return empty($this->resource) ? null : [
            'id' => $this->id,
            'title' => $this->data['title'] ?? null,
            'content' => $this->data['content'] ?? null,
            'image' => $this->images ?? $this->data['images'] ?? null,
            'action_name' => $this->data['action_name'] ?? null,
            'order_icon' => $this->order_icon ?? null,
            'is_read' => boolval($this->read_at),
            'body_data' => empty($this->body_data) ? null :  $this->body_data,
            'created_at' => $this->created_at
        ];
    }

    public static function collection($resource)
    {
        return parent::collection($resource)->additional([
            'success' => true,
            'status' => 200
        ]);
    }
}
