<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class OrderCollection extends ResourceCollection
{
    use ApiCollection;

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->resource->map(function ($order) {
                return [
                    'id' => $order->id,
                    'displayname' => $order->displayname,
                    'phone' => $order->phone,
                    'email' => $order->email,
                    'store_name' => $order->store_name,
                    'sub_total' => $order->sub_total,
                    'delivery_fee' => $order->delivery_fee,
                    'total' => $order->total,
                    'status' => $order->status,
                    'user_id' => $order->user_id,
                    'count_qty' => $order->OrderProduct->count('qty'),
                    'created_at' => $order->created_at,
                    'updated_at' => $order->updated_at,
                    'products' => ProductDetailCollection::collection($order->products)->first()
                ];
            })
        ];
    }
}
