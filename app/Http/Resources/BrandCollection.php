<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class BrandCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return [
            'data' => $this->collection->map(function ($data) {
                return [
                    'id' => $data->id,
                    'status' => $data->status,
                    'representative' => $data->representative,
                    'images' =>  $data->images,
                    'id_images' =>  $data->id_images,
                    // 'name' => $data->name,
                    // 'location' => $data->location,
                    // 'main_image' => my_asset($data->main_image),
                    
                ];
            })
        ];
    }

    public function with($request)
    {
        return [
            'success' => true,
            'status' => 200
        ];
    }
}
