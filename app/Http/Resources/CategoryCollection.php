<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class CategoryCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return [
            'data' => $this->collection->map(function($data) {
                return [
                    'id' => $data->id,
                    'title' => $data->title,
                    'type'  =>  $data->type,
                    'parent_id'  =>  $data->parent_id,
                    'image'  =>  my_asset($data->image)
                    // 'banner' => $data->banner,
                    // 'main_image' => my_asset($data->main_image),
                ];
            })
        ];
    }

    public function with($request)
    {
        return [
            'success' => true,
            'status' => 200
        ];
    }
}
