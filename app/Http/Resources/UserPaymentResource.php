<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserPaymentResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return is_null($this->resource) ? [] : [
            'id' => $this->id,
            'card_number' => $this->card_number,
            'card_name' => $this->card_name,
            'expiry_date' => $this->expiry_date,
            'ccv' => $this->ccv,
            'type' => $this->type,
            'card_brand' => $this->getCardBrand(),
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }

    public function __construct($resource)
    {
        $this->resource = $resource;
        $this->additional([
            'success' => true,
            'status' => 200
        ]);
    }

    public static function collection($resource)
    {
        return parent::collection($resource)->additional([
            'success' => true,
            'status' => 200
        ]);
    }
}
