<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductSkuResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return is_null($this->resource) ? [] : [
            'id' => $this->id,
            'product_id' => $this->product_id,
            'sku' => $this->sku,
            'weight' => $this->weight,
            'price' => $this->price,
            'status' => $this->status,
            'variant' => $this->parse_variant ?? ['title' => [], 'value' => []],
            'qty' => $this->qty,
            'sale' => $this->sale ?? 0,
            'sales' => $this->sales ?? 0,
            'deleted_at' => $this->deleted_at
        ];
    }
}
