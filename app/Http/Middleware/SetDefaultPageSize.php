<?php

namespace App\Http\Middleware;

use Closure;

class SetDefaultPageSize
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $pageSize = $request->get('pageSize', null);
        if (intval($pageSize) != $pageSize || $pageSize < 1) {
            $request->merge(['pageSize' => 10]);
        } else {
            $request->merge(['pageSize' => intval($pageSize)]);
        };
        return $next($request);
    }
}
