<?php

namespace App\Http\Middleware;

use Closure;

class RoleMidleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $member = \Auth::user()->role;
        $permis = json_decode($member->permissions);
        
        foreach(($permis ?? []) as $item) {
            $items = strtolower($item);
            // dd($items);
            if(request()->is("admin/$items" )|| request()->is("admin/$items/*") || request()->is("admin/transactions")) {
                return $next($request);
            }
        }
        flash('You do not have permission to access')->error();
        return back();
    }
}