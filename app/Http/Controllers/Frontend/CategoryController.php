<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Category;
use App\Supplier;
use App\Product;
class CategoryController extends Controller
{
    public function index(Request $request)
    {
        try {
            $categories = Category::all();
            $supplier     = Supplier::all();
            $breadcrumbs = [];
            $products =  Product::where('category_id',$request->id_category);
    
            if (!empty($request->supplier)) {
                $supplier_2   = Supplier::where('name','like','%'.$request->supplier.'%')->get();
                $products =  $products->where('supplier_id',$supplier_2[0]['id']);
            }
            
            if (!empty($request->from_price) || !empty($request->to_price)) {
                $from = $request->from_price ? $request->from_price : 0;
                $to = $request->to_price ? $request->to_price : 0;
                $products =  $products->whereBetween('purchase_price',[$from ,$to ]);
            }

            if (!empty($request->sort_price)) {
                $products =  $products->orderBy('purchase_price',$request->sort_price);
            }

            $products =  $products->get();
    
            $breadcrumbs['category'] = Category::find($request->id_category)->title;
            $breadcrumbs['category_id'] = $request->id_category;
        } catch (\Throwable $th) {
            return abort(404);
        }
       
        
        return view('frontend.categories.index',compact('categories','supplier','products','breadcrumbs'));
    }

   
}
