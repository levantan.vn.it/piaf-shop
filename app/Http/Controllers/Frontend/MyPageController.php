<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
class MyPageController extends Controller
{
    /**
     * Show order history page
     */
    public function index()
    {
        $orders = $this->orders();
        return view('frontend.my_page.history_order',compact('orders'));
    }

    /**
     * Show edit profile page
     */
    public function editProfile()
    {  
        // $user_id = Auth::user();
        $user = User::find(20);
        return view('frontend.my_page.edit_profile',compact('user'));
    }

    public function updateProfile(Request $request, $id)
    {
        $birthday = [];
        if (isset($request->day)) {
            $birthday['d'] = $request->day;
        }
        if (isset($request->month)) {
            $birthday['m'] = $request->month;
        }
        if (isset($request->year)) {
            $birthday['Y'] = $request->year;
        }
        $birthday = implode("-",$birthday);
        $birthday = date("Y-m-d",strtotime($birthday));
        $user = User::find($id);
        if($user){
           $user->displayname = $request->displayname;
           $user->email = $request->email;
           $user->phone = $request->phone;
           $user->gender = $request->gender;
           $user->birthday = $birthday;
           $user->update();
           return back();
        }else{
            abort(404);
        }
        
    }

    /**
     * get all orders of user
     */
    public function orders()
    { 
        $user_id = 20;
        $orders = Order::where('user_id',$user_id)->get();
        return $orders;
    }

     /**
     * Show point and total order in sidebar my page
     */
    public function countOrderAndPoint(){
        $orders_total = count($this->orders());
        $point = 10205;
        $data = ['order'=>$orders_total,'point'=>$point];
        return response()->json($data, 200);
    }
}
