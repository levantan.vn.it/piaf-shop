<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\ProductTranslation;
use App\ProductStock;
use App\Category;
use App\Exports\ProductsExport;
use App\Http\Requests\ProductRequest;
use App\Http\Requests\ProductSkuRequest;
use App\Http\Requests\ProductsRequest;
use App\Http\Requests\ProductDescriptionRequest;
use App\Http\Requests\ProductImage;
use Maatwebsite\Excel\Facades\Excel;
use App\Language;
use App\ProductSKU;
use Auth;
use App\SubSubCategory;
use App\Supplier;
use Session;
use ImageOptimizer;
use DB;
use CoreComponentRepository;
use Illuminate\Support\Str;
use Artisan;

class ProductController extends Controller
{
    public $entity2;
    public $product_sku;
    public function __construct()
    {
        $this->entity2 = "Product";
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function admin_products(Request $request){
        $col_name = null;
        $query = null;
        
        $customers = optional();
        
        $products = $this->getData($request, 1);
        $categories = Category::get();
        return view('backend.products.index', compact('products', 'customers', 'col_name', 'query',"categories"));
    }

    
    /**
     * xuất dữ liệu exprort, tìm kiếm
     */
    private function getData($request, $is_paginate = 0){
        // dd($request->all());
        $products = Product::where(function ($query) use ($request) {

            $queryRank = $request->rank_search ?? [];
            $search = $request->search ?? [];
            if ($request->search) {
                $query->where(function ($q) use ($queryRank,$search) {
                    foreach ($queryRank as $index=>$item) {
                        $itemSearch = $search[$index];
                        $q->where($item, 'like',"%". $itemSearch."%");
                    }
                });
            }
        });
        
        if($request->category_1){
            $products = $products->where('category_id',$request->category_1);
        }
        
        $start = $request->start_date;
        $end = $request->end_date;
        $queryBy = $request->date_post ?? 'created_at';
        // if ($request->type_date) {
        //     $products->whereDate($queryBy, '>=', Date('Y-m-d H:i:s', strtotime("-{$request->type_date} days")));
        // }
        
        if (!empty($request->start_date) && !empty($request->end_date)) {
            $products->whereBetween($queryBy, [$start." 00:00:00", $end." 23:59:59"]);
        }
       
        if($request->optradio) {
            $products = $products->where('status',$request->optradio);
        }

        if($request->optradio2) {
            $products = $products->where('status_sale',$request->optradio2);
        }

        $products = $products->latest();
       
        $products = $products->paginate(15);
        return $products;
    }


    public function detail($id){
        $supplier = Supplier::all();
        $categories = Category::all();
        $product = Product::findOrFail($id);

        $data = [
            'product' => $product,
            'categories' => $categories,
            'supplier' => $supplier,
        ];
        return view('backend.products.products.edit',$data );
        // return view('backend.products.detail');
    }

    private function recursiveRemoveNullArray($array){
        $newArr = $array;
        foreach(($newArr ?? []) as $key => $item){
            if(gettype($item) == 'array'){
                $newArr[$key] = $this->recursiveRemoveNullArray($item);
            }else{
                if(!$item){
                    unset($newArr[$key]);
                }
            }
        }
        return $newArr;
    }

    public function delete($id){
        $products = Product::findOrFail($id);
        $products->skus()->delete();
        $result = $products->delete();
        flash(__('products.delete-success'))->success();
        if ($result) {
            return response()->json(true, 200);
        }
        return response()->json(false, 422);
    }

    public function changeStatus2($id) {
        $products = Product::findOrFail($id);
        if($products->status == 1) {
            $products->status = 2;
            flash(__('products.change-status-success'))->success();
        } else {
            $products->status = 1;
            flash(__('products.change-status-success'))->success();
        }
        $result= $products->save();
        // return redirect()->route('products.index');
        if ($result) {
            return response()->json(true, 200);
        }
        return response()->json(false, 422);
    }
    public function toggleStatus($id) {
        $product = Product::findOrFail($id);
        $product->update([
            'status'=> $product->isActive() ? 2 : 1
        ]);
        flash(__('products.change-status-success'))->success();

        return response()->json();
    }



    public function changeStatus($id) {
        $products = Product::findOrFail($id);
        if($products->status == 1) {
            $products->status = 2;
            flash(__('products.change-status-success'))->success();
        } else {
            $products->status = 1;
            flash(__('products.change-status-success'))->success();
        }
        $products->save();
        return back();
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        $supplier = Supplier::all();
        $category = Category::all();
        $data = [
            'supplier' => $supplier,
            'categories' => $category,
        ];
        return view('backend.products.products.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        // dd($request->all());
        $product= new Product;
        $product->name=$request->name;
        $product->category_id=$request->category_id;
        $product->supplier_id=$request->brand_id;
        $product->current_stock     = $request->current_stock;
        $images=[];
        if(!empty($request->photos)){
            $photos = explode(",",$request->photos);
            if(!empty($photos)&& count($photos)){
                foreach($photos as $value){
                    $url=api_asset($value);
                    if(!empty($url)){
                        $images[]=$url;
                    }
                }
            }
        };
        $product->images = implode(",",$images);
        $product->id_images= ltrim($request->photos,"0");
        $thumbnail_img = "";
        if(!empty($request->thumbnail_img)){
            $url=api_asset($request->thumbnail_img);
            if(!empty($url)){
                $thumbnail_img = $url;
            }
        };
        $product->thumbnail_img = $thumbnail_img;
        $product->thumbnail_img_id= $request->thumbnail_img;
        $product->description=$request->description;
        $product->video_provider = $request->video_provider;
        $product->video_link = $request->video_link;
        $product->unit_price = $request->unit_price;
        $product->purchase_price = $request->purchase_price;
        $product->meta_title = $request->meta_title;
        $product->meta_description = $request->meta_description;

        if($request->has('meta_img')){
            $product->meta_img = $request->meta_img;
        } else {
            $product->meta_img = $request->thumbnail_img;
        }
        if($product->meta_title == null) {
            $product->meta_title = $product->name;
        }
        if($product->meta_img == null) {
            $product->meta_img = $product->thumbnail_img_id;
        }

        if($product->meta_description == null) {
            $product->meta_description = strip_tags($product->description);
        }
        if($request->has('colors_active') && $request->has('colors') && count($request->colors) > 0){
            $product->colors = json_encode($request->colors);
        }
        else {
            $colors = array();
            $product->colors = json_encode($colors);
        }

        $choice_options = array();

        if($request->has('choice_no')){
            foreach ($request->choice_no as $key => $no) {
                $str = 'choice_options_'.$no;

                $item['attribute_id'] = $no;

                $data = array();
                foreach (json_decode($request[$str][0]) as $key => $eachValue) {
                    array_push($data, $eachValue->value);
                }

                $item['values'] = $data;
                array_push($choice_options, $item);
            }
        }

        if (!empty($request->choice_no)) {
            $product->attributes = json_encode($request->choice_no);
        }
        else {
            $product->attributes = json_encode(array());
        }

        $product->choice_options = json_encode($choice_options);
        $product->slug = createSlug($product->name);
        $product->save();

        //combinations start
        $options = array();
        if($request->has('colors_active') && $request->has('colors') && count($request->colors) > 0){
            $colors_active = 1;
            array_push($options, $request->colors);
        }

        if($request->has('choice_no')){
            foreach ($request->choice_no as $key => $no) {
                $name = 'choice_options_'.$no;
                $data = array();
                foreach (json_decode($request[$name][0]) as $key => $item) {
                    array_push($data, $item->value);
                }
                array_push($options, $data);
            }
        }

        //Generates the combinations of customer choice options
        $combinations = combinations($options);
        if(count($combinations[0]) > 0){
            $product->variant_product = 1;
            foreach ($combinations as $key => $combination){
                $str = '';
                foreach ($combination as $key => $item){
                    if($key > 0 ){
                        $str .= '-'.str_replace(' ', '', $item);
                    }
                    else{
                        if($request->has('colors_active') && $request->has('colors') && count($request->colors) > 0){
                            $color_name = \App\Color::where('code', $item)->first()->name;
                            $str .= $color_name;
                        }
                        else{
                            $str .= str_replace(' ', '', $item);
                        }
                    }
                }
                $product_stock = ProductSKU::where('product_id', $product->id)->where('variant', $str)->first();
                if($product_stock == null){
                    $product_stock = new ProductSKU;
                    $product_stock->product_id = $product->id;
                }

                $product_stock->variant = $str;
                $product_stock->price = $request['price_'.str_replace('.', '_', $str)];
                $product_stock->sku = $request['sku_'.str_replace('.', '_', $str)];
                $product_stock->qty = $request['qty_'.str_replace('.', '_', $str)];
                $product_stock->save();
            }
        }
        else{
            $product_stock = new ProductSKU;
            $product_stock->product_id = $product->id;
            $product_stock->price = $request->unit_price;
            $product_stock->qty = $request->current_stock;
            $product_stock->save();
        }
        //combinations end

        $product->save();

        flash(__('products.create-success'))->success();


        return redirect()->route('products.index');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::findOrFail($id);
        $product->name=$request->name;
        $product->category_id=$request->category_id;
        $product->supplier_id=$request->brand_id;
        $product->current_stock     = $request->current_stock;
        $images=[];
        if(!empty($request->photos)){
            $photos = explode(",",$request->photos);
            if(!empty($photos)&& count($photos)){
                foreach($photos as $value){
                    $url=api_asset($value);
                    if(!empty($url)){
                        $images[]=$url;
                    }
                }
            }
        };
        $product->images = implode(",",$images);
        $product->id_images= ltrim($request->photos,"0");
        $thumbnail_img = "";
        if(!empty($request->thumbnail_img)){
            $url=api_asset($request->thumbnail_img);
            if(!empty($url)){
                $thumbnail_img = $url;
            }
        };
        $product->thumbnail_img = $thumbnail_img;
        $product->thumbnail_img_id= $request->thumbnail_img;
        $product->description=$request->description;
        $product->video_provider = $request->video_provider;
        $product->video_link = $request->video_link;
        $product->unit_price = $request->unit_price;
        $product->purchase_price = $request->purchase_price;
        $product->meta_title = $request->meta_title;
        $product->meta_description = $request->meta_description;

        if($request->has('meta_img')){
            $product->meta_img = $request->meta_img;
        } else {
            $product->meta_img = $request->thumbnail_img;
        }
        if($product->meta_title == null) {
            $product->meta_title = $product->name;
        }
        if($product->meta_img == null) {
            $product->meta_img = $product->thumbnail_img_id;
        }

        if($product->meta_description == null) {
            $product->meta_description = strip_tags($product->description);
        }
        if($request->has('colors_active') && $request->has('colors') && count($request->colors) > 0){
            $product->colors = json_encode($request->colors);
        }
        else {
            $colors = array();
            $product->colors = json_encode($colors);
        }

        $choice_options = array();

        if($request->has('choice_no')){
            foreach ($request->choice_no as $key => $no) {
                $str = 'choice_options_'.$no;

                $item['attribute_id'] = $no;

                $data = array();
                foreach (json_decode($request[$str][0]) as $key => $eachValue) {
                    array_push($data, $eachValue->value);
                }

                $item['values'] = $data;
                array_push($choice_options, $item);
            }
        }

        if (!empty($request->choice_no)) {
            $product->attributes = json_encode($request->choice_no);
        }
        else {
            $product->attributes = json_encode(array());
        }

        $product->choice_options = json_encode($choice_options);
        $product->slug = createSlug($product->name);
        $product->save();

        //combinations start
        $options = array();
        if($request->has('colors_active') && $request->has('colors') && count($request->colors) > 0){
            $colors_active = 1;
            array_push($options, $request->colors);
        }

        if($request->has('choice_no')){
            foreach ($request->choice_no as $key => $no) {
                $name = 'choice_options_'.$no;
                $data = array();
                foreach (json_decode($request[$name][0]) as $key => $item) {
                    array_push($data, $item->value);
                }
                array_push($options, $data);
            }
        }

        //Generates the combinations of customer choice options
        $combinations = combinations($options);
        if(count($combinations[0]) > 0){
            $product->variant_product = 1;
            foreach ($combinations as $key => $combination){
                $str = '';
                foreach ($combination as $key => $item){
                    if($key > 0 ){
                        $str .= '-'.str_replace(' ', '', $item);
                    }
                    else{
                        if($request->has('colors_active') && $request->has('colors') && count($request->colors) > 0){
                            $color_name = \App\Color::where('code', $item)->first()->name;
                            $str .= $color_name;
                        }
                        else{
                            $str .= str_replace(' ', '', $item);
                        }
                    }
                }
                $product_stock = ProductSKU::where('product_id', $product->id)->where('variant', $str)->first();
                if($product_stock == null){
                    $product_stock = new ProductSKU;
                    $product_stock->product_id = $product->id;
                }

                $product_stock->variant = $str;
                $product_stock->price = $request['price_'.str_replace('.', '_', $str)];
                $product_stock->sku = $request['sku_'.str_replace('.', '_', $str)];
                $product_stock->qty = $request['qty_'.str_replace('.', '_', $str)];
                $product_stock->save();
            }
        }
        else{
            $product_stock = new ProductSKU;
            $product_stock->product_id = $product->id;
            $product_stock->price = $request->unit_price;
            $product_stock->qty = $request->current_stock;
            $product_stock->save();
        }
        //combinations end

        $product->save();

        flash(translate('Product has been updated successfully'))->success();
        return redirect()->route('products.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::findOrFail($id);

    }

    public function validatedProduct(ProductsRequest $request)
    {
        return response()->json();
    }

    public function sku_combination(Request $request)
    {
        $options = array();
        if($request->has('colors_active') && $request->has('colors') && count($request->colors) > 0){
            $colors_active = 1;
            array_push($options, $request->colors);
        }
        else {
            $colors_active = 0;
        }

        $unit_price = $request->unit_price;
        $product_name = $request->name;

        if($request->has('choice_no')){
            foreach ($request->choice_no as $key => $no) {
                $name = 'choice_options_'.$no;
                $data = array();
                if(!empty($request[$name][0])){
                    foreach (json_decode($request[$name][0]) as $key => $item) {
                        array_push($data, $item->value);
                    }
                    array_push($options, $data);
                }


            }
        }

        $combinations = combinations($options);
        return view('backend.products.products.sku_combinations', compact('combinations', 'unit_price', 'colors_active', 'product_name'));
    }

    public function sku_combination_edit(Request $request)
    {
        $product = Product::findOrFail($request->id);

        $options = array();
        if($request->has('colors_active') && $request->has('colors') && count($request->colors) > 0){
            $colors_active = 1;
            array_push($options, $request->colors);
        }
        else {
            $colors_active = 0;
        }

        $product_name = $request->name;
        $unit_price = $request->unit_price;

        if($request->has('choice_no')){
            foreach ($request->choice_no as $key => $no) {
                $name = 'choice_options_'.$no;
                $data = array();
                if(!empty($request[$name][0])){
                    foreach (json_decode($request[$name][0]) as $key => $item) {
                        array_push($data, $item->value);
                    }
                    array_push($options, $data);
                }

            }
        }

        $combinations = combinations($options);
        return view('backend.products.products.sku_combinations_edit', compact('combinations', 'unit_price', 'colors_active', 'product_name', 'product'));
    }

}
