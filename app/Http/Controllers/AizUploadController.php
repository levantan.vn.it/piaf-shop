<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\File;
use Response;
use Auth;
use Storage;
use App\Supplier;
use App\Banner;
use App\Product;
use App\Order;
use App\Collection;
use App\Notification;
use App\TeamMember;

class AizUploadController extends Controller
{


    public function index(Request $request){

        $all_uploads = File::query();
        $search = null;
        $sort_by = null;

        if ($request->search != null) {
            $search = $request->search;
            $all_uploads->where('file_original_name', 'like', '%'.$request->search.'%');
        }

        $sort_by = $request->sort;
        switch ($request->sort) {
            case 'newest':
                $all_uploads->orderBy('created_at', 'desc');
                break;
            case 'oldest':
                $all_uploads->orderBy('created_at', 'asc');
                break;
            case 'smallest':
                $all_uploads->orderBy('file_size', 'asc');
                break;
            case 'largest':
                $all_uploads->orderBy('file_size', 'desc');
                break;
            default:
                $all_uploads->orderBy('created_at', 'desc');
                break;
        }

        $all_uploads = $all_uploads->paginate(60)->appends(request()->query());

        return view('backend.uploaded_files.index', compact('all_uploads', 'search', 'sort_by') );
    }

    public function create(){
        return view('backend.uploaded_files.create');
    }


    public function show_uploader(Request $request){
        return view('uploader.aiz-uploader');
    }
    public function upload(Request $request){
        $type = array(
            "jpg"=>"image",
            "jpeg"=>"image",
            "png"=>"image",
            "svg"=>"image",
            "webp"=>"image",
            "gif"=>"image",
            "mp4"=>"video",
            "mpg"=>"video",
            "mpeg"=>"video",
            "webm"=>"video",
            "ogg"=>"video",
            "avi"=>"video",
            "mov"=>"video",
            "flv"=>"video",
            "swf"=>"video",
            "mkv"=>"video",
            "wmv"=>"video",
            "wma"=>"audio",
            "aac"=>"audio",
            "wav"=>"audio",
            "mp3"=>"audio",
            "zip"=>"archive",
            "rar"=>"archive",
            "7z"=>"archive",
            "doc"=>"document",
            "txt"=>"document",
            "docx"=>"document",
            "pdf"=>"document",
            "csv"=>"document",
            "xml"=>"document",
            "ods"=>"document",
            "xlr"=>"document",
            "xls"=>"document",
            "xlsx"=>"document"
        );

        if($request->hasFile('aiz_file')){
            $upload = new File;
            $upload->file_original_name = null;

            $arr = explode('.', $request->file('aiz_file')->getClientOriginalName());

            for($i=0; $i < count($arr)-1; $i++){
                if($i == 0){
                    $upload->file_original_name .= $arr[$i];
                }
                else{
                    $upload->file_original_name .= ".".$arr[$i];
                }
            }

            $upload->file_name = $request->file('aiz_file')->store('uploads/all');
            $upload->member_id = Auth::user()->id;
            $upload->extension = strtolower($request->file('aiz_file')->getClientOriginalExtension());
            if(isset($type[$upload->extension])){
                $upload->type = $type[$upload->extension];
            }
            else{
                $upload->type = "others";
            }
            $upload->file_size = $request->file('aiz_file')->getSize();
            $upload->save();

            return '{}';
        }
    }

    public function uploadSummernote(Request $request){
        $type = array(
            "jpg"=>"image",
            "jpeg"=>"image",
            "png"=>"image",
            "svg"=>"image",
            "webp"=>"image",
            "gif"=>"image",
            "mp4"=>"video",
            "mpg"=>"video",
            "mpeg"=>"video",
            "webm"=>"video",
            "ogg"=>"video",
            "avi"=>"video",
            "mov"=>"video",
            "flv"=>"video",
            "swf"=>"video",
            "mkv"=>"video",
            "wmv"=>"video",
            "wma"=>"audio",
            "aac"=>"audio",
            "wav"=>"audio",
            "mp3"=>"audio"
        );

        if($request->hasFile('aiz_file')){
            $upload = new File;
            $upload->file_original_name = null;

            $arr = explode('.', $request->file('aiz_file')->getClientOriginalName());

            for($i=0; $i < count($arr)-1; $i++){
                if($i == 0){
                    $upload->file_original_name .= $arr[$i];
                }
                else{
                    $upload->file_original_name .= ".".$arr[$i];
                }
            }

            $upload->file_name = $request->file('aiz_file')->store('uploads/all');
            $upload->member_id = Auth::user()->id;
            $upload->extension = strtolower($request->file('aiz_file')->getClientOriginalExtension());
            if(isset($type[$upload->extension])){
                $upload->type = $type[$upload->extension];
            }
            else{
                $upload->type = "others";
            }
            $upload->file_size = $request->file('aiz_file')->getSize();
            $upload->save();

            echo json_encode(['data'=>$upload]);exit();
        }
    }

    public function get_uploaded_files(Request $request)
    {
        $uploads = File::where('member_id', Auth::user()->id);
        if ($request->search != null) {
            $uploads->where('file_original_name', 'like', '%'.$request->search.'%');
        }
        if ($request->sort != null) {
            switch ($request->sort) {
                case 'newest':
                    $uploads->orderBy('created_at', 'desc');
                    break;
                case 'oldest':
                    $uploads->orderBy('created_at', 'asc');
                    break;
                case 'smallest':
                    $uploads->orderBy('file_size', 'asc');
                    break;
                case 'largest':
                    $uploads->orderBy('file_size', 'desc');
                    break;
                default:
                    $uploads->orderBy('created_at', 'desc');
                    break;
            }
        }
        return $uploads->paginate(60)->appends(request()->query());
    }

    public function destroy(Request $request,$id)
    {
        try{
            if(env('FILESYSTEM_DRIVER') == 's3'){
                Storage::disk('s3')->delete(File::where('id', $id)->first()->file_name);
            }
            else{
                $path = File::where('id', $id)->first()->file_name;
                if(\Storage::exists($path)){
                    unlink(public_path().'/'.$path);
                }
            }

            File::destroy($id);
            flash(__('upload_file.del_success'))->success();
        }
        catch(\Exception $e){
            //dd($e);
            flash(__('upload_file.opp_wrong'))->error();
        }
        return back();
    }

    public function get_preview_files(Request $request){
        $ids = explode(',', $request->ids);
        $files = File::whereIn('id', $ids)->get();
        return $files;
    }

    //Download project attachment
    public function attachment_download($id)
    {
        $project_attachment = File::find($id);
        try{
           $file_path = public_path($project_attachment->file_name);
            return Response::download($file_path);
        }catch(\Exception $e){
            flash(translate('File does not exist!'))->error();
            return back();
        }

    }
    //Download project attachment
    public function file_info(Request $request)
    {
        $file = File::findOrFail($request['id']);
        return view('backend.uploaded_files.info',compact('file'));
    }

    public function updateImage(Request $request){
        if(!empty($request->entity)){

            switch ($request->entity) {
                case 'supplier':
                    $data = Supplier::findOrFail($request->id);
                    $images = [];
                    if(!empty($request->id_files)){
                        $id_files = explode(",", $request->id_files);
                        if(!empty($id_files) && count($id_files)){
                            foreach ($id_files as $value) {
                                $url = api_asset($value);
                                if(!empty($url)){
                                    $images[] = $url;
                                }

                            }
                        }
                    }
                    $data->images = implode(",", $images);
                    $data->id_images = ltrim($request->id_files,"0,");

                    $images_main = [];
                    if(!empty($request->id_main_files)){
                        $id_main_files = explode(",", $request->id_main_files);
                        if(!empty($id_main_files) && count($id_main_files)){
                            foreach ($id_main_files as $value) {
                                $url = api_asset($value);
                                if(!empty($url)){
                                    $images_main[] = $url;
                                }

                            }
                        }
                    }

                    $data->main_image = implode(",", $images_main);
                    $data->main_image_id = ltrim($request->id_main_files,"0,");
                    $data->save();
                    flash(__('suppliers.noti_image'))->success();
                    break;
                case 'banner':
                    $data = Banner::findOrFail($request->id);
                    $images = NULL;
                    if(!empty($request->id_files)){
                        $url = api_asset($request->id_files);
                        if(!empty($url)){
                            $images = $url;
                        }
                    }
                    $data->images = $images;
                    $data->id_images = $request->id_files;
                    $data->save();
                    flash(__('banners.update-img'))->success();
                    break;
                case 'member':
                    $data = TeamMember::findOrFail($request->id);
                    $images = NULL;
                    if(!empty($request->id_files)){
                        $url = api_asset($request->id_files);
                        if(!empty($url)){
                            $images = $url;
                        }
                    }
                    $data->avatar = $images;
                    $data->id_images = $request->id_files;
                    $data->save();
                    flash(__('members.change_avatar'))->success();
                    break;
                case 'product':
                    $data = Product::findOrFail($request->id);
                    $images = [];
                    if(!empty($request->id_files)){
                        $id_files = explode(",", $request->id_files);
                        if(!empty($id_files) && count($id_files)){
                            foreach ($id_files as $value) {
                                $url = api_asset($value);
                                if(!empty($url)){
                                    $images[] = $url;
                                }

                            }
                        }
                    }
                    $data->images = implode(",", $images);
                    $data->id_images = ltrim($request->id_files,"0,");
                    $data->save();
                    flash('상품 이미지를 업데이트 했습니다')->success();
                    break;
                case 'order':
                    $data = Order::findOrFail($request->id);
                    $images = NULL;
                    if(!empty($request->id_files)){
                        $id_files = explode(",", $request->id_files);
                        if(!empty($id_files) && count($id_files)){
                            foreach ($id_files as $value) {
                                $url = api_asset($value);
                                if(!empty($url)){
                                    $images[] = $url;
                                }

                            }
                        }
                    }
                    $data->images = implode(",", $images);
                    $data->id_images = ltrim($request->id_files,"0,");
                    if ($data->status==3){
                        $data->status=4;
                    }
                    $data->save();
                    flash(__('orders.img-invoice-success'))->success();
                    break;
                case 'collection':
                    $data = Collection::findOrFail($request->id);
                    $images = [];
                    if(!empty($request->id_files)){
                        $id_files = explode(",", $request->id_files);
                        if(!empty($id_files) && count($id_files)){
                            foreach ($id_files as $value) {
                                $url = api_asset($value);
                                if(!empty($url)){
                                    $images[] = $url;
                                }

                            }
                        }
                    }
                    $data->images = implode(",", $images);
                    $data->id_images = ltrim($request->id_files,"0,");
                    $images_main = [];
                    if(!empty($request->id_main_files)){
                        $id_main_files = explode(",", $request->id_main_files);
                        if(!empty($id_main_files) && count($id_main_files)){
                            foreach ($id_main_files as $value) {
                                $url = api_asset($value);
                                if(!empty($url)){
                                    $images_main[] = $url;
                                }

                            }
                        }
                    }

                    $data->main_image = implode(",", $images_main);
                    $data->main_image_id = ltrim($request->id_main_files,"0,");
                    $data->save();
                    flash(__('collections.msg_image_success'))->success();
                    break;
                    case 'notification':
                        $data = Notification::findOrFail($request->id);
                        $images = [];
                        if(!empty($request->id_files)){
                            $id_files = explode(",", $request->id_files);
                            if(!empty($id_files) && count($id_files)){
                                foreach ($id_files as $value) {
                                    $url = api_asset($value);
                                    if(!empty($url)){
                                        $images[] = $url;
                                    }

                                }
                            }
                        }
                        $data->images = implode(",", $images);
                        $data->id_images = ltrim($request->id_files,"0,");
                        $data->save();
                        flash(__('notifications.update_image'))->success();
                        break;
                default:
                    return redirect()->route('admin.dashboard');
                    break;
            }
            return redirect()->route($request->route,$request->id);
        }
        return redirect()->route('admin.dashboard');

    }

}
