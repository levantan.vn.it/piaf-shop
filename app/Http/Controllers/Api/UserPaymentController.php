<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\PaymentMethodRequest;
use App\Http\Resources\UserPaymentResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Facades\JWTAuth;

class UserPaymentController extends Controller
{
    /**
     * Get Payment menthod
     * @param Request $request
     * @return JsonResource|JsonResponse
     */
    public function getPaymentMethod(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        if ((int) $request->type === 1) {
            return UserPaymentResource::make($user->paymentPrimary);
        }
        $payments = $user->payments()->orderBy('type')->latest()->get();
        return UserPaymentResource::collection($payments);
    }

    /**
     * Add Payment Method
     * @param PaymentMethodRequest $request
     * @return JsonResponse
     */
    public function addPaymentMethod(PaymentMethodRequest $request)
    {
        try {
            $paymentMethod =  DB::transaction(function ()  use ($request) {
                $user = JWTAuth::parseToken()->authenticate();
                $paymentMethod = $user->payments()->where('card_number', $request->card_number)->first();
                if ((int)$request->type === 1) {
                    $user->payments()->update(['type' => '0']);
                }
                if ($paymentMethod) {
                    $paymentMethod->update($request->validated());
                    return $paymentMethod;
                }
                return $user->payments()->create($request->validated());
            });
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'status' => 500,
                'message' => __('api_user_payment.add_failed'),
                'data' => $request->all()
            ], 500);
        }

        return UserPaymentResource::make($paymentMethod);
    }

    /**
     * Edit Payment Method
     * @param PaymentMethodRequest $request
     * @param int $id
     * @return JsonResponse
     */
    public function editPaymentMethod(PaymentMethodRequest $request, int $id)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $payment = $user->payments()->findOrFail($id);
        try {
            DB::transaction(function () use (&$payment, $request, $user) {
                if ((int) $request->type === 1) {
                    $user->payments()->update(['type' => '0']);
                }
                $payment->update($request->validated());
            });
        } catch (\Exception $e) {
            $request->merge(['id' => $id]);
            return response()->json([
                'success' => false,
                'status' => 500,
                'message' => __('api_user_payment.update_failed'),
                'data' => $request->all()
            ], 500);
        }

        return UserPaymentResource::make($payment);
    }

    /**
     * Delete Payment Method
     * @param int $id
     * @return JsonResponse
     */
    public function deletePaymentMethod(int $id)
    {
        $payment = JWTAuth::parseToken()->authenticate()->payments()->findOrFail($id);
        try {
            $payment->delete();
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'status' => 500,
                'message' => __('api_user_payment.delete_failed'),
                'data' => [
                    'id' => $id
                ],
            ], 500);
        }

        return response()->json([
            'success' => true,
            'status' => 200,
            'data' => null
        ]);
    }

    /**
     * Make primary Payment Method
     * @param int $id
     * @return JsonResponse
     */
    public function makePrimaryPaymentMethod(int $id)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $payment = $user->payments()->findOrFail($id);
        if ($payment->isNormal()) {
            try {
                DB::transaction(function () use (&$payment, $user) {
                    $user->payments()->update(['type' => '0']);
                    $payment->update(['type' => '1']);
                });
            } catch (\Exception $e) {
                return response()->json([
                    'success' => false,
                    'status' => 500,
                    'message' => __('api_user_payment.make_primary')
                ], 500);
            }
        }

        return UserPaymentResource::make($payment);
    }
}
