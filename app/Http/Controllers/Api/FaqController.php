<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\FaqCollection;
use Illuminate\Http\Request;
use App\Faq;

class FaqController extends Controller
{
    /**
     * Get FAQ
     * @return JsonResource
     */
    public function index()
    {
        return FaqCollection::collection(
            Faq::whereStatus(1)->get()
        );
    }
}
