<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\BannerCollection;
use Symfony\Component\HttpFoundation\Request;
use App\Banner;
class BannerController extends Controller
{

    public function index()
    {
        return new BannerCollection(json_decode(get_setting('home_banner1_images'), true));
    }

    public function banner(Request $request)
    {   
        $banner = new Banner;
        if (!empty($request->position)) {
            $banner = $banner->where('position',$request->position);
        }
        $banner = $banner->where([
            ['start_date','<=',date('Y-m-d')],
            ['end_date','>=',date('Y-m-d')],
            ['status','<>',3],
        ])->get();
        
        return new BannerCollection($banner);
    }
}
