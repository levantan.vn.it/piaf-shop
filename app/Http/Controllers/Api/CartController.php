<?php

namespace App\Http\Controllers\Api;

use App\Cart;
use App\Http\Requests\Api\AddCartRequest;
use App\Http\Resources\CartResource;
use App\ProductSKU;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\DeliveryManagement;
use App\Order;
use App\OrderProduct;
use App\Product;
use App\DeliveryCondition;
use App\User;
use App\Color;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;

class CartController extends Controller
{

    public function getCart(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $carts = $user->carts()->with('product', 'productSku')->get();
        return CartResource::collection($carts);
    }

    public function add(AddCartRequest $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $product = Product::find($request->product_id);

        $data = array();
        $data['id'] = $product->id;
        $str = '';

        //check the color enabled or disabled for the product
        if($request->has('color')){
            $data['color'] = $request['color'];
            $str = Color::where('code', $request['color'])->first()->name;
        }

        if (!empty($product->choice_options)) {
            //Gets all the choice values of customer choice option and generate a string like Black-S-Cotton
            foreach (json_decode($product->choice_options) as $key => $choice) {
                if($str != null){
                    $str .= '-'.str_replace(' ', '', $request['attribute_id_'.$choice->attribute_id]);
                }
                else{
                    $str .= str_replace(' ', '', $request['attribute_id_'.$choice->attribute_id]);
                }
            }
        }

        $data['variant'] = $str;

        if($str != null && $product->variant_product){
            $product_stock = $product->skus->where('variant', $str)->first();
            if (empty($product_stock)) {
                throw ValidationException::withMessages([
                    'product_id' => "product_stock not exist"
                ]);
            }
            $price = $product_stock->price;
            $quantity = $product_stock->qty;
            $max_qty = $product_stock->qty;
            if ($product_stock->price <= 0) {
                throw ValidationException::withMessages([
                    'product_id' => "product_stock not exist"
                ]);
            }

            if($quantity < $request['qty']){
                throw ValidationException::withMessages([
                    'qty' => __('api_cart.sold_out')
                ]);
            }
            $cart = $user->carts()->where([
                'product_id' => $request->product_id,
                'product_sku_id' => $product_stock->id
            ])->first();
            $qty_in_cart =  $cart->qty ?? 0;
        }else{
            $price = $product->unit_price;
            $cart = $user->carts()->where([
                'product_id' => $request->product_id
            ])->first();
            $qty_in_cart =  $cart->qty ?? 0;
            $max_qty = $product->qty;
        }

        $qty = $request->qty + ($qty_in_cart);

        if ($qty > $max_qty) {
            throw ValidationException::withMessages([
                'qty' => __('api_cart.not_qty_in_stock')
            ]);
        }

        $data['qty'] = $qty;
        $data['price'] = $price;

        if ($request['qty'] == null){
            $data['qty'] = 1;
        }

        if ($cart) {
            $cart->increment('qty', $request->qty);
        } else {
            $cart = $user->carts()->create($data);
        }
        return CartResource::make($cart);
    }

    public function update(Request $request, $id)
    {
        $data = $request->validate([
            'qty' => 'bail|required|integer|min:1|max:4294967295'
        ]);
        $user = JWTAuth::parseToken()->authenticate();
        $cart = $user->carts()->with('productSku')->findOrFail($id);
        $qty = optional($cart->productSku)->qty ?: 0;
        $qty_in_cart =  $user->carts()->where([
            'product_id' => $cart->product_id,
            'product_sku_id' => $cart->product_sku_id,
            ['id', '<>', $id]
        ])->sum('qty') ?: 0;
        $qty_in_cart += $request->qty;
        if ($qty < $qty_in_cart) {
            throw ValidationException::withMessages([
                'qty' => __('api_cart.not_qty_in_stock')
            ]);
        }
        $cart->update($data);
        return CartResource::make($cart);
    }

    public function delete($id)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $cart = $user->carts()->findOrFail($id);
        $cart->delete();
        return response()->json([
            'success' => true,
            'status' => 200,
            'data' => null
        ]);
    }
    public function deliveryType(Request $request)
    {
        $request->validate([
            'delivery_book_id' => 'bail|required|exists:delivery_books,id'
        ]);
        $user = JWTAuth::parseToken()->authenticate();
        $delivery_book = $user->deliveryBooks()->where('id', $request->delivery_book_id)->first();
        if (empty($delivery_book)) {
            return response()->json([
                'success' => false,
                'error' => 1,
                'message' => __('orders.add_delivery_book')
            ], 422);
        }
        $delivery_management_district = DeliveryManagement::join('delivery_management_city', 'delivery_management_city.delivery_id', '=', 'delivery_management.id')->where('district_id', $delivery_book->district_id)->orderBy('delivery_management_city.id', 'asc')->first();
        if (!empty($delivery_management_district)) {
            return response()->json([
                'data' => 1,
                'note'  =>  "distance"
            ], 200);
        }
        $delivery_management_car = DeliveryManagement::join('delivery_management_city', 'delivery_management_city.delivery_id', '=', 'delivery_management.id')->where('city_id', $delivery_book->city_id)->where('interprovincial_type', 1)->orderBy('delivery_management_city.id', 'asc')->first();
        $delivery_management_airplane = DeliveryManagement::join('delivery_management_city', 'delivery_management_city.delivery_id', '=', 'delivery_management.id')->where('city_id', $delivery_book->city_id)->where('interprovincial_type', 2)->orderBy('delivery_management_city.id', 'asc')->first();
        if (empty($delivery_management_car) && empty($delivery_management_airplane)) {
            return response()->json([
                'success' => false,
                'error' => 2,
                'message' => __('orders.address_not_support_shipping')
            ], 422);
        }
        if (!empty($delivery_management_car) && !empty($delivery_management_airplane)) {
            return response()->json([
                'data' => 4,
                'note' => 'car_or_airplane'
            ], 200);
        }
        if (!empty($delivery_management_car)) {
            return response()->json([
                'data' => 2,
                'note' => 'car'
            ], 200);
        }
        if (!empty($delivery_management_airplane)) {
            return response()->json([
                'data' => 3,
                'note' => 'airplane'
            ], 200);
        }
        return response()->json([
            'success' => false,
            'error' => 2,
            'message' => __('orders.address_not_support_shipping')
        ], 422);
    }

    public function deliveryFee(Request $request)
    {
        $request->validate([
            'delivery_book_id' => 'bail|required|exists:delivery_books,id',
            'interprovincial_type' =>  'bail|nullable|in:1,2'
        ]);
        $user = JWTAuth::parseToken()->authenticate();
        $delivery_book = $user->deliveryBooks()->where('id', $request->delivery_book_id)->first();
        if (empty($delivery_book)) {
            return response()->json([
                'success' => false,
                'error' => 1,
                'message' => __('orders.add_delivery_book')
            ], 422);
        }
        $carts = $user->carts()->count();
        if (empty($carts)) {
            return response()->json([
                'success' => false,
                'error' => 3,
                'message' => __('orders.cart_empty')
            ], 422);
        }
        $price_area = 0;
        // $delivery_management_district = DeliveryManagement::join('delivery_management_city', 'delivery_management_city.delivery_id', '=', 'delivery_management.id')->where('district_id', $delivery_book->district_id)->orderBy('delivery_management_city.id', 'asc')->first();
        // $price_area = 0;
        // if (empty($delivery_management_district)) {
        //     if (!empty($request->interprovincial_type)) {
        //         $delivery_management_city = DeliveryManagement::join('delivery_management_city', 'delivery_management_city.delivery_id', '=', 'delivery_management.id')->where('city_id', $delivery_book->city_id)->where('interprovincial_type', $request->interprovincial_type)->orderBy('delivery_management_city.id', 'asc')->first();
        //         $delivery_management_city = DeliveryManagement::join('delivery_management_city', 'delivery_management_city.delivery_id', '=', 'delivery_management.id')->where('city_id', $delivery_book->city_id)->orderBy('delivery_management_city.id', 'asc')->first();
        //         if (empty($delivery_management_city)) {
        //             return response()->json([
        //                 'success' => false,
        //                 'error' => 2,
        //                 'message' => __('orders.address_not_support_shipping')
        //             ], 422);
        //         } else {
        //             $price_area = $delivery_management_city->price;
        //         }
        //     } else {
        //         return response()->json([
        //             'success' => false,
        //             'error' => 2,
        //             'message' => __('orders.address_not_support_shipping')
        //         ], 422);
        //     }
        // } else {
        //     $price_area = $delivery_management_district->price;
        // }
        $price_weight = 0;
        $carts = $user->carts()->select(['product_sku.weight', 'carts.qty', 'product_sku.sku', 'carts.delivery_condition_id', 'carts.id'])->join('product_sku', 'product_sku.id', 'carts.product_sku_id')->get();
        $price_item_box = [];
        if (!empty($carts)) {
            foreach ($carts as $item) {
                $weight_box = $item->weight;
                $fee_delivery_condition = 0;
                if (!empty($item->delivery_condition_id)) {
                    $delivery_condition = DeliveryCondition::where('id', $item->delivery_condition_id)->first();
                    if (!empty($delivery_condition)) {
                        $fee_delivery_condition = $delivery_condition->price * $item->qty;
                    }
                }
                $price_item = get_setting('fixed_price');
                if (empty($price_item)) {
                    $price_item = 0;
                }
                // $delivery_management_weight = DeliveryManagement::where('type_delivery', 1)->where('delivery_from', '<=', $weight_box)->where('delivery_to', '>=', $weight_box)->first();
                // if (!empty($delivery_management_weight)) {
                //     $price_item = $delivery_management_weight->price;
                // } else {
                //     $delivery_management_weight = DeliveryManagement::where('type_delivery', 1)->where('delivery_to', '<=', $weight_box)->orderBy('delivery_to','DESC')->first();
                //     if (!empty($delivery_management_weight)) {
                //         $price_item = $delivery_management_weight->price;
                //     }
                // }
                $price_box = $price_item * $item->qty;
                $price_item_box[$item->id] = $price_box + $fee_delivery_condition;
                $price_weight += $price_box + $fee_delivery_condition;
            }
        }
        // $weights = $user->carts()->join('product_sku', 'product_sku.id', 'carts.product_sku_id')->sum(\DB::raw('product_sku.weight * carts.qty'));
        // $delivery_management_weight = DeliveryManagement::where('type_delivery', 1)->where('delivery_from', '<=', $weights)->where('delivery_to', '>=', $weights)->first();
        // if (!empty($delivery_management_weight)) {
        //     $price_weight = $delivery_management_weight->price;
        // } else {
        //     $delivery_management_weight = DeliveryManagement::where('type_delivery', 1)->where('delivery_to', '<=', $weights)->orderBy('delivery_to')->first();
        //     if (!empty($delivery_management_weight)) {
        //         $price_weight = $delivery_management_weight->price;
        //     }
        // }
        $priceShip = $price_area + $price_weight;
        return response()->json([
            'success' => true,
            'price' => $priceShip,
            'price_item_box'    =>  $price_item_box
        ], 200);
    }

    public function repurchase(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $request->validate([
            'order_id' => ['bail', 'required', Rule::exists('orders', 'id')->whereIn('status', [Order::COMPLETED_DELIVERY, Order::COMPLETED])->where('user_id', $user->id)]
        ]);

        try {
            OrderProduct::where('order_id', $request->order_id)->with('productSku')->each(function ($product) use ($user) {
                $cart = $user->carts()->with('productSku')
                    ->where('product_id', $product->product_id)
                    ->where('product_sku_id', $product->product_sku_id)
                    ->first();
                if ($cart) {
                    $cart->increment('qty', $product->qty);
                } else {
                    $user->carts()->create([
                        'product_id' => $product->product_id,
                        'product_sku_id' => $product->product_sku_id,
                        'delivery_condition_id' => $product->delivery_condition_id,
                        'qty' =>  $product->qty
                    ]);
                }
            });

            $carts = $user->carts()->with('product', 'productSku')->get();
            return CartResource::collection($carts);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'status' => 500,
                'message' => __('api_cart.product_unavailable')
            ], 500);
        }
    }

    public function getDeliveryCondition(Request $request)
    {
        $data = DeliveryCondition::select('id', 'title', 'price')->get();
        return response()->json([
            'success' => true,
            'data' => $data,
        ], 200);
    }
}
