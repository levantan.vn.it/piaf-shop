<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\ProductDetailResource;
use App\Http\Resources\ProductResource;
use App\LikeProduct;
use App\Order;
use App\OrderProduct;
use App\Product;
use App\ProductSKU;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Tymon\JWTAuth\Facades\JWTAuth;

class ProductController extends Controller
{
    /**
     * Get list product
     * @param Request $request
     * @return JsonResource
     */
    public function index(Request $request)
    {
        $page = !empty($request->page) ? $request->page : 1;
        $pageSize = !empty($request->pageSize) ? $request->pageSize : 1;

        $products = new Product();
        $products = $products->where('current_stock',">",0);
        if (!empty($request->category_id)) {
            $products = $products->where('category_id', $request->category_id);
        }
        if (!empty($request->name)) {
           $products = $products->where('name','like','%'.$request->name.'%');
        }
        if (!empty($request->brand)) {
            $products = $products->where('supplier_id',$request->brand);
        }
        if (!empty($request->from_price) || !empty($request->to_price)) {
            $products =  $products->whereBetween('purchase_price',array($request->from_price ,$request->to_price));
        }
        if (!empty($request->sort_method)) {
            switch ($request->sort_method) {
                case 1:
                    $products = $products->orderBy('id','desc');
                    break;
                case 2:
                    $products = $products->orderBy('total_sale','desc');
                    break;
                case 3:
                    $products = $products->orderBy('purchase_price','desc');
                    break;
                case 4:
                    $products = $products->orderBy('purchase_price','asc');
                    break;
            }
        }
        $products =  $products->paginate($pageSize, ['*'], 'page', $page);
        
        return ProductResource::collection($products);
    }

    /**
     * Get Product Detail
     * @param int|string $id
     * @return JsonResource
     */
    public function show($id)
    {
        $product = Product::with(['supplier', 'productSku', 'skus' => function ($query) {
            $query->where('product_sku.status', 1)->whereNotNull('variant');
        }])->where('products.status', 1)->withCount('authLike')->findOrFail($id);

        return ProductDetailResource::make($product);
    }

    /**
     * Like Product
     * @param int $id
     */
    public function likeProductAction(Request $request)
    {
        $checkLike = LikeProduct::where('product_id',$request->product_id)->where('user_no',$request->user_no)->first();  
        if (!empty($checkLike)) {
            $checkLike->delete();
            $data = [
                    'action' => 'unlike'
                ];
        }else{
            $like = LikeProduct::create(['product_id' => $request->product_id, 'user_no'  => $request->user_no]);
            $data = [
                'action' => 'like'
            ];
        }
         return response()->json([
            'success' => true,
            'status' => 200,
            'data' => $data
        ]);
    }

    /**
     * Get like of user
     */
    public function getLikeProduct(Request $request)
    {
        $user = $request->user_no;
        $likeProducts = LikeProduct::where('user_no',$user);
        if (!empty($request->product_id)) {
            $likeProducts = $likeProducts->where('product_id',$request->product_id);
        }
        $likeProducts = $likeProducts->with('products')->paginate($request->pageSize, ['*'], 'page', $request->page);
        $products = [];
        foreach ($likeProducts as $items) {
            if (!empty($items->products)) {
                $products[] = $items->products;
            }
          
        }
        return ProductResource::collection($products);
    }

    /**
     * Get Bought Product
     */
    public function getBoughtProduct()
    {
        $user = JWTAuth::parseToken()->authenticate();
        $products = Product::whereIn(
            'id',
            OrderProduct::select('product_id')
                ->join('orders', 'orders.id', 'order_product.order_id')
                ->where('orders.user_id', $user->id)->where('orders.status', Order::COMPLETED)
        )->with(['supplier', 'productSku', 'skus' => function ($query) {
            $query->activeInStock();
        }])->whereHas('skus', function ($query) {
            $query->select('product_id')->activeInStock();
        })->paginate(request('pageSize'));
        return ProductResource::collection($products);
    }

    /**
     * Amount of Orders are processing
     * @return JsonResponse
     */
    public function countOrdersProcessing()
    {
        $user = JWTAuth::parseToken()->authenticate();
        $count = Order::where('user_id', $user->id)->whereIn('status', Order::LIST_PROCESS)->count();
        return response()->json([
            'success' => true,
            'status' => 200,
            'data' => [
                'total' => $count
            ]
        ], 200);
    }

    public function getRangePrice()
    {
        return response()->json([
            'success' => true,
            'status' => 200,
            'data' => [
                'min' => ProductSKU::activeInStock()->where('status', 1)->min('price'),
                'max' => ProductSKU::activeInStock()->where('status', 1)->max('price')
            ]
        ]);
    }

    public function relateProducts(Request $request)
    {
        $limit = $request->limit ? $request->limit : 10;
        $product = Product::where('category_id',$request->category_id)->limit($limit)->get();
        return ProductResource::collection($product);
    }
    
    public function mostBougthProduct(Request $request)
    {
        $limit = $request->limit ? $request->limit : 10;
        $products = Product::orderBy('total_sale','desc')->limit($limit)->get();
        return ProductResource::collection($products);
    }
}
