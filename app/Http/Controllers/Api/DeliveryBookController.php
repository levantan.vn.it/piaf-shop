<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\DeliveryBookRequest;
use Illuminate\Http\Request;
use App\Http\Resources\DeliveryBookResource;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Facades\JWTAuth;

class DeliveryBookController extends Controller
{
    /**
     * Get primary location of user
     * @param Request $request
     * @return JsonResponse|JsonResource
     */
    public function getLocation(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        if (intval($request->type) === 1) {
            return DeliveryBookResource::make($user->deliveryBookPrimary);
        }

        $deliveryBooks = $user->deliveryBooks()->orderBy('type', 'desc')->latest()->get();

        return DeliveryBookResource::collection($deliveryBooks);
    }

    /**
     * Make Primary Delivery Book
     * @return JsonResponse
     */
    public function makePrimaryDeliveryBook(int $id)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $deliveryBook = $user->deliveryBooks()->findOrFail($id);

        if ($deliveryBook->isNormal()) {
            try {
                DB::transaction(function () use (&$deliveryBook, $user) {
                    $user->deliveryBooks()->update(['type' => '0']);
                    $deliveryBook->update(['type' => '1']);
                });
            } catch (\Exception $e) {
                return response()->json([
                    'success' => false,
                    'status' => 500,
                    'message' => __('api_delivery_book.make_primary'),
                    'data' => [
                        'id' => $id
                    ]
                ], 500);
            }
        }

        return DeliveryBookResource::make($deliveryBook);
    }

    /**
     * Add Delivery Book
     * @param DeliveryBookRequest $request
     * @return JsonResponse
     */
    public function addDeliveryBook(DeliveryBookRequest $request)
    {
        $user = JWTAuth::parseToken()->authenticate();

        try {
            $deliveryBook = DB::transaction(function () use (&$deliveryBook, $request, $user) {
                if ($request->type == 1) {
                    $user->deliveryBooks()->update(['type' => '0']);
                }
                return $user->deliveryBooks()->create($request->all());
            });
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'status' => 500,
                'message' => __('api_delivery_book.add_failed'),
                'data' => $e->getMessage()
            ], 500);
        }

        return DeliveryBookResource::make($deliveryBook);
    }

    /**
     * Edit Delivery Book
     * @param DeliveryBookRequest $request
     * @return JsonResponse
     */
    public function editDeliveryBook(DeliveryBookRequest $request, $id)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $deliveryBook = $user->deliveryBooks()->findOrFail($id);

        try {
            DB::transaction(function () use (&$deliveryBook, $request, $user) {
                if ($request->type == 1 && $deliveryBook->isNormal()) {
                    $user->deliveryBooks()->update(['type' => '0']);
                }
                $deliveryBook->update($request->validated());
            });
        } catch (\Exception $e) {
            $request->merge(['id' => $id]);
            return response()->json([
                'success' => false,
                'status' => 500,
                'message' => __('api_delivery_book.update_failed'),
                'data' => $request->all()
            ], 500);
        }

        return DeliveryBookResource::make($deliveryBook);
    }

    /**
     * Delete Delivery Book
     * @return JsonResponse
     */
    public function deleteDeliveryBook(Request $request, $id)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $deliveryBook = $user->deliveryBooks()->findOrFail($id);

        try {
            $deliveryBook->delete();
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'status' => 500,
                'message' => __('api_delivery_book.delete_failed'),
                'data' => $request->all()
            ], 500);
        }

        return response()->json([
            'success' => true,
            'status' => 200,
            'data' => null
        ]);
    }
}
