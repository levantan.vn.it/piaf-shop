<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ProductPopularResource;
use App\Http\Resources\SearchResource;
use App\Product;
use App\Search;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

class SearchController extends Controller
{

    /**
     * Get Recently Searched
     * @return JsonResource 
     */
    public function recent()
    {
        $user = JWTAuth::parseToken()->authenticate();
        $searches = $user->searches()->latest('pivot_last_search_at')->paginate(10, ['id', 'query']);
        return SearchResource::collection($searches);
    }

    /**
     * Get Popular Search
     * @return JsonResource 
     */
    public function popular()
    {
        $products = Product::activeInStock(true)->orderBy('search_total', 'desc')->latest()->paginate(10);
        return ProductPopularResource::collection($products);
    }

    /**
     * Get Popular Search
     * @return JsonResource 
     */
    public function addPopular($id)
    {
        $product = Product::findOrFail($id);
        $product->increment('search_total');

        return response()->json([
            'success' => true,
            'status' => 200,
            'data' => null
        ]);
    }

    /**
     * Add recently search
     * @return JsonResource 
     */
    public function addRecent(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $data = $request->validate([
            'query' => 'bail|required|string'
        ]);
        $search = Search::with('auth_search')->where('query', $request->get('query'))->first();
        try {
            if ($search && is_null($search->auth_search)) {
                $search->increment('count');
            }

            if (!$search) {
                $search = Search::create($data);
            }
            $user->searches()->syncWithoutDetaching([$search->id => ['last_search_at' => now()]]);
        } catch (\Exception $e) {
            return response()->json([
                'message' => 'error',
                'success' => false,
                'status' => 500,
                'data' => $request->all()
            ], 500);
        }
        return response()->json([
            'success' => true,
            'status' => 200,
            'data' => null
        ]);
    }

    /**
     * Get Suggestion Search
     * @param Request $request
     * @return JsonResource
     */
    public function suggest(Request $request)
    {
        $searches = Search::where('query', 'like', "%" . $request->get('query', '%') . '%')
            ->latest('count')
            ->paginate(10);


        return SearchResource::collection($searches);
    }

    /**
     * Delete recent search
     * @param Request $request
     */
    public function deleteRecent(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $search = $user->searches()->where('query', $request->get('query', ''))->firstOrFail();
        $user->searches()->detach($search->id);
        return response()->json([
            'success' => true,
            'status' => 200,
            'data' => null
        ]);
    }
}
