<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\OrderDetailResource;
use App\Http\Resources\OrderProductResource;
use App\Http\Resources\OrderResource;
use App\Order;
use App\OrderProduct;
use App\Product;
use App\ProductSKU;
use App\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Tymon\JWTAuth\Facades\JWTAuth;

class OrderController extends Controller
{
    /**
     * Add order
     */
    public function store(Request $request)
    {

        $request->validate([
            'merchant_uid' => 'bail|required|string',
            'imp_uid' => 'bail|required|string',
            'product' => 'bail|required|array',
            'product.*.product_id' => ['bail', 'required', Rule::exists('products', 'id')->whereNull('deleted_at')],
            'product.*.qty' => 'bail|required',
            'product.*.subtotal' => 'bail|required',
            'product.*.product_sku_id' => ['bail', 'required', Rule::exists('product_sku', 'id')->whereNull('deleted_at')],
            'subtotal'    => 'bail|required',
            'shippingFee'    => 'bail|required',
            'total'    => 'bail|required',
            'delivery_book_id' => 'bail|required|exists:delivery_books,id',
            'interprovincial_type'  =>  'nullable|in:1,2'
        ]);
        $user = JWTAuth::parseToken()->authenticate();
        $delivery_book = $user->deliveryBooks()->where('id', $request->delivery_book_id)->first();
        if (empty($delivery_book)) {
            return response()->json([
                'success' => false,
                'error' => 2,
                'message' => __('orders.add_delivery_book')
            ], 422);
        }

        // Acquire access token
        $curl = curl_init();
        $postData = [
            'imp_key'   => config('iamport.imp_apikey'),
            'imp_secret'    =>  config('iamport.imp_secret')
        ];
        $postData = json_encode($postData);
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.iamport.kr/users/getToken",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $postData,
            CURLOPT_HTTPHEADER => [
                'Content-Type: application/json'
            ]
        ));
        $getToken = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            return response()->json([
                'success' => false,
                'error' => 1,
                'message' => __('orders.failed_in_add_product')
            ], 500);
        }
        $getToken = json_decode($getToken, true);

        if (empty($getToken['response']['access_token'])) {
            return response()->json([
                'success' => false,
                'error' => 1,
                'message' => $getToken['message'] ?? __('orders.failed_in_add_product')
            ], 500);
        }
        if (!empty(config('iamport.imp_check'))) {
            $access_token = $getToken['response']['access_token'];
            // Query payment information from Iamport with imp_uid
            $curl = curl_init();
            $postData = [
                'imp_key'   => config('iamport.imp_apikey'),
                'imp_secret'    =>  config('iamport.imp_secret')
            ];
            $postData = json_encode($postData);
            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://api.iamport.kr/payments/" . $request->imp_uid,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "get",
                CURLOPT_HTTPHEADER => [
                    'Authorization: ' . $access_token
                ]
            ));
            $getPaymentData = curl_exec($curl);
            $getPaymentData = json_decode($getPaymentData, true);
            $err = curl_error($curl);
            curl_close($curl);
            if ($err) {
                return response()->json([
                    'success' => false,
                    'error' => 2,
                    'message' => $getPaymentData['message'] ?? null
                ], 500);
            }

            if (empty($getPaymentData['response'])) {
                return response()->json([
                    'success' => false,
                    'error' => 2,
                    'message' => $getPaymentData['message'] ?? __('orders.failed_in_add_product')
                ], 500);
            }
            if (empty($getPaymentData['response']['amount']) || $getPaymentData['response']['amount'] != $request->total) {
                return response()->json([
                    'success' => false,
                    'error' => 3,
                    'message' => __('orders.failed_in_add_product')
                ], 500);
            }
            if (empty($getPaymentData['response']['status']) || $getPaymentData['response']['status'] != 'paid') {
                return response()->json([
                    'success' => false,
                    'error' => 4,
                    'message' => __('orders.failed_in_add_product')
                ], 500);
            }
        }

        try {
            DB::beginTransaction();
            $order = Order::create([
                'displayname' => $user->displayname,
                'store_name' => $delivery_book->location_name,
                'phone' => $delivery_book->phone,
                'email' =>  $user->email,
                'delivery_address'  =>  $delivery_book->full_address,
                'total' => $request->total,
                'sub_total' => $request->subtotal,
                'delivery_fee' => $request->shippingFee,
                'user_id' => $user->id,
                'note'  =>  $request->note ?? null,
                'status'    =>  2,
                'interprovincial_type'  =>  $request->interprovincial_type ?? 0,
            ]);
            if (!empty($request->product)) {
                $product_sku_data = [];
                $product_sku_ids = [];
                foreach ($request->product as $product) {
                    $product_sku = ProductSKU::where('id', $product['product_sku_id'])->first();
                    if (!empty($product_sku)) {
                        $product_sku->updateQty($product['qty']);
                    }
                    // OrderProduct::create([
                    //     'order_id' => $order->id,
                    //     'product_id' => $product['product_id'],
                    //     'qty' =>  $product['qty'],
                    //     'product_sku_id' =>  $product_sku->id,
                    //     'subtotal' =>  $product['subtotal'],
                    // ]);

                    $product_sku_data[] = [
                        'product_id' => $product['product_id'],
                        'qty' =>  $product['qty'],
                        'product_sku_id' =>  $product['product_sku_id'],
                        'subtotal' =>  $product['subtotal'],
                        'variant' => $product_sku->variant ?? serialize(['title' => [], 'value' => []])
                    ];
                    $product_sku_ids[] = $product['product_sku_id'];
                }

                $order->OrderProduct()->createMany($product_sku_data);
                $user->carts()->whereIn('product_sku_id', $product_sku_ids)->delete();
            }
            $transaction = Transaction::create([
                'order_id' => $order->id,
                'user_id' => $user->id,
                'money' =>  $request->total,
                'method' =>  $getPaymentData['response']['pay_method'] ?? null,
                'transaction_id' =>  $request->imp_uid,
                'merchant_uid'  =>  $request->merchant_uid,
                'status'    => 1
            ]);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'success' => false,
                'status' => 500,
                'message' => $e->getMessage()
            ], 500);
        }
        return OrderDetailResource::make($order);
    }

    /**
     * History order
     * @param Request $request
     * @return JsonResource
     */
    public function historyOrder(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $orders = Order::with('OrderProduct', 'OrderProduct.product')
            ->where(function ($query) use ($request, $user) {
                $query->where('user_id', $user->id);
                if ($request->status && in_array($request->status, Order::LIST_STATUS)) {
                    $query->where('status', $request->status);
                } else {
                    $query->where('status', '<>', Order::DELETED);
                }
            })->latest()->paginate(request('pageSize'));

        return OrderResource::collection($orders);
    }

    /**
     * Get order detail
     */
    public function orderDetail($id)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $order = Order::where('user_id', $user->id)->withAllRelation()->findOrFail($id);

        return OrderDetailResource::make($order);
    }

    /**
     * Get product of order
     */
    public function orderProduct($id)
    {
        $user = JWTAuth::parseToken()->authenticate();
        Order::where('user_id', $user->id)->findOrFail($id);
        $products = OrderProduct::withAllRelation()->where('order_id', $id)->get();

        return OrderProductResource::collection($products);
    }

    public function createOrder(Request $request)
    {

        $request->validate([
            'product' => 'bail|required|array',
            'product.*.product_id' => ['bail', 'required', Rule::exists('products', 'id')->whereNull('deleted_at')],
            'product.*.qty' => 'bail|required',
            'product.*.subtotal' => 'bail|required',
            'product.*.product_sku_id' => ['bail', 'required', Rule::exists('product_sku', 'id')->whereNull('deleted_at')],
            'subtotal'    => 'bail|required',
            'shippingFee'    => 'bail|required',
            'total'    => 'bail|required',
            'delivery_book_id' => 'bail|required|exists:delivery_books,id',
            'interprovincial_type'  =>  'nullable|in:1,2',
            'product.*.delivery_condition_id' => 'bail|nullable',
        ]);
        $user = JWTAuth::parseToken()->authenticate();
        $delivery_book = $user->deliveryBooks()->where('id', $request->delivery_book_id)->first();
        if (empty($delivery_book)) {
            return response()->json([
                'success' => false,
                'error' => 2,
                'message' => __('orders.add_delivery_book')
            ], 422);
        }
        if (!empty($request->product)) {
            $product_sku_id_tmp = [];
            foreach ($request->product as $product) {
                if (!array_key_exists($product['product_sku_id'], $product_sku_id_tmp)) {
                    $product_sku_id_tmp[$product['product_sku_id']] = $product['qty'];
                }else{
                    $product_sku_id_tmp[$product['product_sku_id']] = $product_sku_id_tmp[$product['product_sku_id']] + $product['qty'];
                }
            }
            foreach ($product_sku_id_tmp as $key => $value) {
                $product_sku = ProductSKU::where('id', $key)->first();
                if (!empty($product_sku)) {
                    if($product_sku->qty < $value){
                        return response()->json([
                            'success' => false,
                            'data' => [
                                $key => $product_sku->qty
                            ]
                        ], 500);
                    }
                }else{
                    return response()->json([
                        'success' => false,
                        'data' => [
                            $key => 0
                        ]
                    ], 500);
                }
            }
        }

        try {
            DB::beginTransaction();
            $order = Order::create([
                'displayname' => $user->displayname,
                'store_name' => $delivery_book->location_name,
                'receiver' => $delivery_book->receiver,
                'phone' => $delivery_book->phone,
                'email' =>  $user->email,
                'delivery_address'  =>  $delivery_book->full_address,
                'total' => $request->total,
                'sub_total' => $request->subtotal,
                'delivery_fee' => $request->shippingFee,
                'user_id' => $user->id,
                'note'  =>  $request->note ?? null,
                'status'    =>  Order::UNPAID,
                'interprovincial_type'  =>  $request->interprovincial_type ?? 0,
                'delivery_method'   =>  get_setting('company_shipping_name'),
                'delivery_confirm_date' => now()
            ]);
            if (!empty($request->product)) {
                $product_sku_data = [];
                $product_sku_ids = [];
                foreach ($request->product as $product) {
                    $product_sku = ProductSKU::where('id', $product['product_sku_id'])->first();
                    if (!empty($product_sku)) {
                        $product_sku->updateQty($product['qty']);
                    }

                    $product_sku_data[] = [
                        'product_id' => $product['product_id'],
                        'qty' =>  $product['qty'],
                        'product_sku_id' =>  $product['product_sku_id'],
                        'delivery_condition_id' =>  $product['delivery_condition_id'],
                        'subtotal' =>  $product['subtotal'],
                        'variant' => $product_sku->variant ?? serialize(['title' => [], 'value' => []])
                    ];
                    $product_sku_ids[] = $product['product_sku_id'];
                }

                $order->OrderProduct()->createMany($product_sku_data);
                $user->carts()->whereIn('product_sku_id', $product_sku_ids)->delete();
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'success' => false,
                'status' => 500,
                'message' => $e->getMessage()
            ], 500);
        }
        return OrderDetailResource::make($order);
    }

    public function checkPayment(Request $request)
    {

        $request->validate([
            'merchant_uid' => 'bail|required|string',
            'imp_uid' => 'bail|required|string',
            'order_id' => 'bail|required|exists:orders,id'
        ]);
        $user = JWTAuth::parseToken()->authenticate();
        $order = Order::where('id', $request->order_id)->where('user_id', $user->id)->first();
        if (empty($order)) {
            return response()->json([
                'success' => false,
                'error' => 1,
                'message' => __('orders.not_found')
            ], 404);
        }
        // Acquire access token
        $curl = curl_init();
        $postData = [
            'imp_key'   => config('iamport.imp_apikey'),
            'imp_secret'    =>  config('iamport.imp_secret')
        ];
        $postData = json_encode($postData);
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.iamport.kr/users/getToken",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $postData,
            CURLOPT_HTTPHEADER => [
                'Content-Type: application/json'
            ]
        ));
        $getToken = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            return response()->json([
                'success' => false,
                'error' => 1,
                'message' => __('orders.failed_payment')
            ], 500);
        }
        $getToken = json_decode($getToken, true);

        if (empty($getToken['response']['access_token'])) {
            return response()->json([
                'success' => false,
                'error' => 1,
                'message' => $getToken['message'] ?? __('orders.failed_payment')
            ], 500);
        }
        if (!empty(config('iamport.imp_check'))) {
            $access_token = $getToken['response']['access_token'];
            // Query payment information from Iamport with imp_uid
            $curl = curl_init();
            $postData = [
                'imp_key'   => config('iamport.imp_apikey'),
                'imp_secret'    =>  config('iamport.imp_secret')
            ];
            $postData = json_encode($postData);
            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://api.iamport.kr/payments/" . $request->imp_uid,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "get",
                CURLOPT_HTTPHEADER => [
                    'Authorization: ' . $access_token
                ]
            ));
            $getPaymentData = curl_exec($curl);
            $getPaymentData = json_decode($getPaymentData, true);
            $err = curl_error($curl);
            curl_close($curl);
            if ($err) {
                return response()->json([
                    'success' => false,
                    'error' => 2,
                    'message' => $getPaymentData['message'] ?? null
                ], 500);
            }

            if (empty($getPaymentData['response'])) {
                return response()->json([
                    'success' => false,
                    'error' => 2,
                    'message' => $getPaymentData['message'] ?? __('orders.failed_payment')
                ], 500);
            }
            if (empty($getPaymentData['response']['amount']) || $getPaymentData['response']['amount'] != $order->total) {
                return response()->json([
                    'success' => false,
                    'error' => 3,
                    'message' => __('orders.failed_payment')
                ], 500);
            }
            if (empty($getPaymentData['response']['status']) || $getPaymentData['response']['status'] != 'paid') {
                return response()->json([
                    'success' => false,
                    'error' => 4,
                    'message' => __('orders.failed_payment')
                ], 500);
            }
        }

        try {
            DB::beginTransaction();
            $transaction = Transaction::create([
                'order_id' => $order->id,
                'user_id' => $user->id,
                'money' =>  $order->total,
                'method' =>  $getPaymentData['response']['pay_method'] ?? null,
                'transaction_id' =>  $request->imp_uid,
                'merchant_uid'  =>  $request->merchant_uid,
                'status'    => 1
            ]);
            $order->status = Order::PROCESSING;
            $order->save();
            DB::commit();
            return response()->json([
                'success' => true
            ], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'success' => false,
                'status' => 500,
                'message' => $e->getMessage()
            ], 500);
        }
    }

    public function deliveried(Request $request)
    {

        $request->validate([
            'order_id' => 'bail|required|exists:orders,id'
        ]);
        $user = JWTAuth::parseToken()->authenticate();
        $order = Order::where('id', $request->order_id)->where('user_id', $user->id)->first();
        if (empty($order)) {
            return response()->json([
                'success' => false,
                'error' => 1,
                'message' => __('orders.not_found')
            ], 404);
        }
        if(!$order->isCompletedDelivery()){

            return response()->json([
                'success' => false,
                'error' => 1,
                'message' => __('orders.not_deliveried')
            ], 404);
        }
        try {
            DB::beginTransaction();
            $order->status = Order::COMPLETED;
            $order->delivery_confirm_date = now();
            $order->save();
            DB::commit();
            return response()->json([
                'success' => true
            ], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'success' => false,
                'status' => 500,
                'message' => $e->getMessage()
            ], 500);
        }
    }

    public function checkStock(Request $request)
    {

        $request->validate([
            'product_sku_id' => 'bail|required|array',
            'product_sku_id.*' => ['bail', 'required'],
        ]);
        $user = JWTAuth::parseToken()->authenticate();
        try {
            if (!empty($request->product_sku_id)) {
                $product_sku_ids = [];
                foreach ($request->product_sku_id as $key) {
                    $product_sku = ProductSKU::where('id', $key)->first();
                    if (!empty($product_sku)) {
                        $product_sku_ids[$key] = $product_sku->qty;
                    }else{
                        $product_sku_ids[$key] = 0;
                    }
                }
            }
            return response()->json([
                'success' => true,
                'data' => $product_sku_ids
            ], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'success' => false,
                'status' => 500,
                'message' => $e->getMessage()
            ], 500);
        }
    }
}
