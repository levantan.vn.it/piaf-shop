<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\SettingsCollection;
use App\Models\Setting;

class SettingsController extends Controller
{
    public function index()
    {
        return new SettingsCollection(Setting::all());
    }
}
