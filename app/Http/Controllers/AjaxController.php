<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use File;

class AjaxController extends Controller
{
    public function getDistrict(Request $request)
    {
        $html = '';
        if ($request->ajax()) {
            if($request->filled('city_id')){
                $districts = \App\District::where('city_id', $request->city_id)->get();
                $html = '<option value="">'.__('settings.chooses_district').'</option>';
                foreach($districts as $item) {
                    $html.= '<option value="' . $item->id . '">' . $item->name . '</option>';
                }
            }else{
                $html = '<option value=""></option>';
            }
        }
        echo $html;
    }

    public function getWard(Request $request)
    {
        $html = '';
        if ($request->ajax()) {
            if($request->filled('district_id')){
                $wards = \App\Ward::where('district_id', $request->district_id)->get();
                $html = '<option value="">'.__('settings.chooses_ward').'</option>';
                foreach($wards as $item) {
                    $html.= '<option value="' . $item->id . '">' . $item->name . '</option>';
                }
            }else{
                $html = '<option value=""></option>';
            }
        }
        echo $html;
    }
}



