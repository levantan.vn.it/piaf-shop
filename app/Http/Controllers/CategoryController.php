<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CategoryRequest;
use App\Http\Requests\AddChildCategoryRequest;
use App\Http\Requests\EditCategoryRequest;
use App\Http\Requests\EditChildCategoryRequest;
use App\Http\Requests\AddCategoryRequest;
use App\Category;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use DB;
use App\Product;
class CategoryController extends Controller
{
    public $entity;
    public function __construct()
    {
        $this->entity = "Category";
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $sort_search =null;
        $categories = Category::where('type',1)->with('childrenCategories')->orderBy('id', 'desc');
        if ($request->has('search')){
            $sort_search = $request->search;
            $categories = $categories->where('title', 'like', '%'.$sort_search.'%');
        }
        $categories = $categories->paginate(15);
        return view('backend.categories.index', compact('categories', 'sort_search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::where('parent_id', 0)
            ->with('childrenCategories')
            ->get();

        return view('backend.categories.create', compact('categories'));
    }

    public function CreatePrimaryCategory(CategoryRequest $request){

        // return route('categories.index');
        // dd($request->all());
        $category =$request->validated();
        $category = new Category;
        $category->title = $request->title;
        $category->save();
        $images = NULL;
        if(!empty($request->id_files)){
            $url = api_asset($request->id_files);
            if(!empty($url)){
                $images = $url;
            }
        }
        $category->images = $images;
        $category->id_images = $request->id_files;
        $images_main = [];
        if(!empty($request->id_main_files)){
            $id_main_files = explode(",", $request->id_main_files);
            if(!empty($id_main_files) && count($id_main_files)){
                foreach ($id_main_files as $value) {
                    $url = api_asset($value);
                    if(!empty($url)){
                        $images_main[] = $url;
                    }

                }
            }
        }
        $category->main_image = implode(",", $images_main);
        $category->main_image_id = ltrim($request->id_main_files,"0,");
        flash(__('categories.noti_createpri'))->success();
        $category->save();
        return redirect()->route('categories.index');
    }

    public function CreateChildCategory(CategoryRequest $request){

        // return route('categories.index');
        // dd($request->all());
        $category =$request->validated();
        $category = new Category;

        $category->title = $request->title;
        $category->parent_id = $request->parent_id;
        $category->type =2;
        $images = NULL;
        if(!empty($request->id_files)){
            $url = api_asset($request->id_files);
            if(!empty($url)){
                $images = $url;
            }
        }
        $category->images = $images;
        $category->id_images = $request->id_files;
        $images_main = [];
        if(!empty($request->id_main_files)){
            $id_main_files = explode(",", $request->id_main_files);
            if(!empty($id_main_files) && count($id_main_files)){
                foreach ($id_main_files as $value) {
                    $url = api_asset($value);
                    if(!empty($url)){
                        $images_main[] = $url;
                    }

                }
            }
        }
        $category->main_image = implode(",", $images_main);
        $category->main_image_id = ltrim($request->id_main_files,"0,");
        flash(__('categories.noti_createchild'))->success();
        $category->save();
        return redirect()->route('categories.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $category = new Category;
            $category->title = $request->title;
            $category->type = $request->type;
            $category->parent_id = $request->parent_id;
            if($request->hasFile('image')){
                $category->image = $request->file('image')->store('uploads/category');
            }
            $category->save();
            flash(__('base.create_success',['entity'=>$this->entity]))->success();
        } catch (\Illuminate\Database\QueryException $e) {
            flash(__('base.error',['entity'=>$this->entity]))->error();
        }catch (\Exception $e) {
            flash(__('base.error',['entity'=>$this->entity]))->error();
        }

        return redirect()->route('categories.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $category = Category::findOrFail($id);
        $categories = Category::where('parent_id', 0)->with('childrenCategories')->where('id', '!=' , $category->id)->get();

        return view('backend.categories.edit', compact('category', 'categories'));
    }

    public function updatePrimary(CategoryRequest $request, $id)
    {
        // $category =$request->validated();
        $category = Category::findOrFail($id);
        $images_main = [];
        if(!empty($request->id_main_files)){
            $id_main_files = explode(",", $request->id_main_files);
            if(!empty($id_main_files) && count($id_main_files)){
                foreach ($id_main_files as $value) {
                    $url = api_asset($value);
                    if(!empty($url)){
                        $images_main[] = $url;
                    }
                }
            }
        }

        $category->main_image = implode(",", $images_main);
        $category->main_image_id = ltrim($request->id_main_files,"0,");

        $data['title'] = $request->title;
        $file = \App\File::find($request->id_file);
        if(!empty($file)){
            $data['images'] = $file->file_name;
            $data['id_images'] = $file->id;
        }
        $category->update($data);
        flash(__('categories.noti_updatepri'))->success();
        return redirect()->route('categories.index',$id);
    }

    public function updateChild(CategoryRequest $request, $id)
    {
        $category = Category::findOrFail($id);
        // $category =$request->validated();
        // $category =$request->only(['title','parent_id']);

        // $category['images'] = api_asset($request->id_files);
        // $category['id_images'] = $request->id_files;
        $images_main = [];
        if(!empty($request->id_main_files)){
            $id_main_files = explode(",", $request->id_main_files);
            if(!empty($id_main_files) && count($id_main_files)){
                foreach ($id_main_files as $value) {
                    $url = api_asset($value);
                    if(!empty($url)){
                        $images_main[] = $url;
                    }
                }
            }
        }

        $category->main_image = implode(",", $images_main);
        $category->main_image_id = ltrim($request->id_main_files,"0,");

        $images = [];
        if(!empty($request->id_files)){
            $id_files = explode(",", $request->id_files);
            if(!empty($id_files) && count($id_files)){
                foreach ($id_files as $value) {
                    $url = api_asset($value);
                    if(!empty($url)){
                        $images[] = $url;
                    }

                }
            }
        }
        $category->images = implode(",", $images);
        $category->id_images = ltrim($request->id_files,"0,");

        $data['title'] = $request->title;
        $data['parent_id'] = $request->parent_id;


        $file = \App\File::find($request->id_file);
        if(!empty($file)){
            $data['images'] = $file->file_name;
            $data['id_images'] = $file->id;
        }

        $file_main = \App\File::find($request->id_main_files);
        if(!empty($file_main)){
            $data['main_image'] = $file_main->file_name;
            $data['main_image_id'] = $file_main->id;
        }

        // $file_main = \App\File::find($request->id_main_files);
        // if(!empty($file_main)){
        //     $data['images_main'] = $file_main->file_name;
        //     $data['id_main_files'] = $file_main->id;
        // }
        $category->update($data);
        // Category::where('id', $id)->update($category);
        flash(__('categories.noti_updatechild'))->success();
        return redirect()->route('categories.index',$id);
    }

    public function view($id){
        $category = Category::with('parentCategory')->find($id);
        if(!$category){
            abort(404, 'Category not found!');
        }
        $categories = Category::where('type', 1)->get();
        $view = view('templates.popup.category-edit-child', compact('category', 'categories'))->render();
        return response()->json(['view' => $view, 'data' => $category], 200);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        try {
            $category = Category::findOrFail($id);
            $category->title = $request->title;
            $category->type = $request->type;
            $category->parent_id = $request->parent_id;
            if($request->hasFile('image')){
                if(!empty($category->image)){
                    Storage::delete($category->image);
                }
                $category->image = $request->file('image')->store('uploads/category');
            }
            $category->save();
            flash(__('base.update_sucess',['entity'=>$this->entity]))->success();
        } catch (\Illuminate\Database\QueryException $e) {
            dd($e);
            flash(__('base.error',['entity'=>$this->entity]))->error();
        }catch (\Exception $e) {
            dd($e);
            flash(__('base.error',['entity'=>$this->entity]))->error();
        }
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        try {
            $category = Category::findOrFail($id);
            if( count($category->products ?? []) ){
                flash(__('categories.noti_delete_pri_false',['entity'=>$this->entity]))->error();
        return redirect()->route('categories.index');
    }
            if(!empty($category->image)){
                Storage::delete($category->image);
            }
            $category->childrenCategories()->update(['parent_id'=>0,'type'=>1]);
            foreach (Product::where('category_id', $category->id)->get() as $product) {
                $product->category_id = null;
                $product->save();
            }
            $category->delete();
            flash(__('categories.noti_delete_pri',['entity'=>$this->entity]))->success();
        } catch (\Illuminate\Database\QueryException $e) {
            flash(__('base.error',['entity'=>$this->entity]))->error();
        }catch (\Exception $e) {
            flash(__('base.error',['entity'=>$this->entity]))->error();
        }
        return redirect()->route('categories.index');
    }

    public function deletechild($id){
        try {
            $category = Category::findOrFail($id);
            if(!empty($category->image)){
                Storage::delete($category->image);
            }
            $category->delete();
            flash(__('categories.noti_delete_child',['entity'=>$this->entity]))->success();
        } catch (\Illuminate\Database\QueryException $e) {
            flash(__('base.error',['entity'=>$this->entity]))->error();
        }catch (\Exception $e) {
            flash(__('base.error',['entity'=>$this->entity]))->error();
        }
        return redirect()->route('categories.index');
    }


    public function updateStatus($id)
    {
        $category = Category::findOrFail($id);
        $category->status = $category->isActive(2)  ? 2 : 1;

        if($category->save()){
            if($category->isActive()){
                $response = ['value'=>__('categories.lock'),'flash'=>__('categories.noti_lock'),'status'=>$category->status];
                // flash('Lock Category Success')->success();

            }else{
                // $response = __('categories.unlock');
                $response = ['value'=>__('categories.unlock'),'flash'=>__('categories.noti_unlock'),'status'=>$category->status];

                // flash('UNLock Category Success')->success();
            }
            return response()->json($response, 200);
        }
        $response = _('base.error',['entity'=>$this->entity]);
        return response()->json($response, 500);
    }

    public function getCategory($id){
        $category = Category::find($id);
        if(!$category){
            return response()->json('Not found', 404);
        }
        $view = view('templates.popup.edit-category-popup', compact('category'))->render();
        return response()->json($view, 200);
    }

    public function checkValidate(CategoryRequest $request){
        return response()->json(true, 200);
    }
}
