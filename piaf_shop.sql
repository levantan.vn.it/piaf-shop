-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Sep 13, 2021 at 05:07 AM
-- Server version: 8.0.19
-- PHP Version: 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `piaf_shop`
--

-- --------------------------------------------------------

--
-- Table structure for table `attributes`
--

CREATE TABLE `attributes` (
  `id` int NOT NULL,
  `name` varchar(255) CHARACTER SET utf32 COLLATE utf32_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_unicode_ci;

--
-- Dumping data for table `attributes`
--

INSERT INTO `attributes` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Size', '2020-02-24 05:55:07', '2020-02-24 05:55:07');

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `id` int NOT NULL,
  `position` int NOT NULL DEFAULT '1',
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `collection_id` bigint NOT NULL DEFAULT '0',
  `status` int NOT NULL DEFAULT '1',
  `images` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `id_images` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`id`, `position`, `start_date`, `end_date`, `title`, `collection_id`, `status`, `images`, `id_images`, `created_at`, `updated_at`) VALUES
(1, 1, '2021-03-09', '2021-03-30', 'Banner 1', 0, 2, 'uploads/all/mCOOvg0Xtehk6kufPLZrwRtOje2tb85TgXwLBx91.jpg', '16', '2021-03-09 01:59:09', '2021-06-07 00:43:35'),
(2, 2, '2021-03-09', '2021-03-30', 'Banner 2', 0, 2, NULL, NULL, '2021-03-09 01:59:09', '2021-03-09 01:59:09'),
(3, 2, '2021-03-09', '2021-03-30', 'Banner 3', 0, 2, NULL, NULL, '2021-03-09 01:59:09', '2021-03-09 01:59:09'),
(4, 1, '2021-03-09', '2021-03-30', 'Banner 4', 0, 2, NULL, NULL, '2021-03-09 01:59:09', '2021-03-09 01:59:09'),
(5, 2, '2021-03-09', '2021-03-30', 'Banner 5', 0, 2, NULL, NULL, '2021-03-09 01:59:09', '2021-03-09 01:59:09'),
(6, 2, '2021-03-09', '2021-03-30', 'Banner 6', 0, 2, NULL, NULL, '2021-03-09 01:59:09', '2021-03-09 01:59:09'),
(7, 3, '2021-03-09', '2021-03-30', 'Banner 7', 0, 2, NULL, NULL, '2021-03-09 01:59:09', '2021-03-09 01:59:09'),
(8, 1, '2021-03-09', '2021-03-30', 'Banner 8', 0, 2, NULL, NULL, '2021-03-09 01:59:09', '2021-03-09 01:59:09'),
(9, 3, '2021-03-09', '2021-03-30', 'Banner 9', 0, 2, NULL, NULL, '2021-03-09 01:59:09', '2021-03-09 01:59:09');

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

CREATE TABLE `carts` (
  `id` bigint UNSIGNED NOT NULL,
  `user_id` bigint UNSIGNED NOT NULL,
  `product_id` bigint NOT NULL,
  `qty` int UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `product_sku_id` bigint NOT NULL,
  `delivery_condition_id` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint NOT NULL,
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `type` enum('1','2') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `parent_id` int NOT NULL DEFAULT '0',
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `title`, `type`, `parent_id`, `image`, `status`, `updated_at`) VALUES
(1, 'Category 1', '1', 0, 'uploads/all/AJZpoShWAV3Ho2FWqIZxzuadxGNhWjxazmkEhL53.png', 1, '2021-03-09 01:20:13'),
(2, 'Category 2', '1', 0, 'uploads/all/oknrbyio2c520s8YsFfcA02THs5edNTyd7HuApaf.png', 1, '2021-03-09 01:20:13'),
(3, 'Category 3', '1', 0, 'uploads/all/L37mVULWRqsyaW0OruURxw4mmDV2sAokyFBJ4BoS.png', 1, '2021-03-09 01:20:13'),
(4, 'Category 4', '1', 0, 'uploads/all/7YuyL6Hv9ouIMcxL5jf0WPZC9ncd9c3IwKW8ikzI.png', 1, '2021-03-09 01:20:13'),
(5, 'Category 5', '2', 3, 'uploads/all/AJZpoShWAV3Ho2FWqIZxzuadxGNhWjxazmkEhL53.png', 1, '2021-03-09 01:20:13'),
(6, 'Category 6', '2', 1, 'uploads/all/1AltaWzbe42mqSIAtKPFNSJvNXpGXTv4p4TV9vl9.png', 1, '2021-03-09 01:20:13'),
(7, 'Category 7', '2', 3, 'uploads/all/4uD1E1B8D0WJrTSCny6ogAeJKjd8FY6cLsv39OpJ.png', 1, '2021-03-09 01:20:14'),
(8, 'Category 8', '2', 4, 'uploads/all/7YuyL6Hv9ouIMcxL5jf0WPZC9ncd9c3IwKW8ikzI.png', 1, '2021-03-09 01:20:14'),
(9, 'Category 9', '2', 1, 'uploads/all/HreldpMpglaz1pF2skU66aIRa0XOB3JfmIY90Hhf.png', 1, '2021-03-09 01:20:14');

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` int NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Gyeonggi', '2021-04-29 09:06:26', '2021-04-29 09:06:26'),
(2, 'Gyeongsang Bắc', '2021-04-29 09:06:26', '2021-04-29 09:06:26'),
(3, 'Seoul', '2021-04-29 09:06:26', '2021-04-29 09:06:26'),
(4, 'Busan', '2021-04-29 09:06:26', '2021-04-29 09:06:26'),
(5, 'Gangwon', '2021-04-29 09:06:26', '2021-04-29 09:06:26'),
(6, 'Jeju', '2021-04-29 09:06:26', '2021-04-29 09:06:26');

-- --------------------------------------------------------

--
-- Table structure for table `collections`
--

CREATE TABLE `collections` (
  `id` bigint NOT NULL,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `section` enum('1','2','3') CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '1',
  `images` text CHARACTER SET utf8 COLLATE utf8_bin,
  `id_images` text CHARACTER SET utf8 COLLATE utf8_bin,
  `main_image` text CHARACTER SET utf8 COLLATE utf8_bin,
  `main_image_id` text CHARACTER SET utf8 COLLATE utf8_bin,
  `short_description` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `brand_id` int NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `collections`
--

INSERT INTO `collections` (`id`, `name`, `section`, `images`, `id_images`, `main_image`, `main_image_id`, `short_description`, `start_date`, `end_date`, `brand_id`, `created_at`, `updated_at`) VALUES
(1, 'Winter Sale up to 50%', '1', 'uploads/all/1AltaWzbe42mqSIAtKPFNSJvNXpGXTv4p4TV9vl9.png,uploads/all/L37mVULWRqsyaW0OruURxw4mmDV2sAokyFBJ4BoS.png', '1,2', NULL, NULL, NULL, NULL, NULL, 0, '2021-03-23 11:51:07', '2021-03-23 08:47:14'),
(2, 'Winter Sale up to 50%', '2', 'uploads/all/HreldpMpglaz1pF2skU66aIRa0XOB3JfmIY90Hhf.png', '7', NULL, NULL, NULL, NULL, NULL, 0, '2021-03-23 11:51:07', '2021-03-23 11:51:07'),
(3, 'Winter Sale up to 50%', '3', 'uploads/all/HreldpMpglaz1pF2skU66aIRa0XOB3JfmIY90Hhf.png', '7', NULL, NULL, NULL, NULL, NULL, 0, '2021-03-23 11:53:06', '2021-03-23 11:53:06');

-- --------------------------------------------------------

--
-- Table structure for table `collection_products`
--

CREATE TABLE `collection_products` (
  `id` bigint NOT NULL,
  `product_id` bigint NOT NULL,
  `collection_id` bigint NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `colors`
--

CREATE TABLE `colors` (
  `id` int NOT NULL,
  `name` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `code` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `colors`
--

INSERT INTO `colors` (`id`, `name`, `code`, `created_at`, `updated_at`) VALUES
(1, 'IndianRed', '#CD5C5C', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(2, 'LightCoral', '#F08080', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(3, 'Salmon', '#FA8072', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(4, 'DarkSalmon', '#E9967A', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(5, 'LightSalmon', '#FFA07A', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(6, 'Crimson', '#DC143C', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(7, 'Red', '#FF0000', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(8, 'FireBrick', '#B22222', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(9, 'DarkRed', '#8B0000', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(10, 'Pink', '#FFC0CB', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(11, 'LightPink', '#FFB6C1', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(12, 'HotPink', '#FF69B4', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(13, 'DeepPink', '#FF1493', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(14, 'MediumVioletRed', '#C71585', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(15, 'PaleVioletRed', '#DB7093', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(16, 'LightSalmon', '#FFA07A', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(17, 'Coral', '#FF7F50', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(18, 'Tomato', '#FF6347', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(19, 'OrangeRed', '#FF4500', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(20, 'DarkOrange', '#FF8C00', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(21, 'Orange', '#FFA500', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(22, 'Gold', '#FFD700', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(23, 'Yellow', '#FFFF00', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(24, 'LightYellow', '#FFFFE0', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(25, 'LemonChiffon', '#FFFACD', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(26, 'LightGoldenrodYellow', '#FAFAD2', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(27, 'PapayaWhip', '#FFEFD5', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(28, 'Moccasin', '#FFE4B5', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(29, 'PeachPuff', '#FFDAB9', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(30, 'PaleGoldenrod', '#EEE8AA', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(31, 'Khaki', '#F0E68C', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(32, 'DarkKhaki', '#BDB76B', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(33, 'Lavender', '#E6E6FA', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(34, 'Thistle', '#D8BFD8', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(35, 'Plum', '#DDA0DD', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(36, 'Violet', '#EE82EE', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(37, 'Orchid', '#DA70D6', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(38, 'Fuchsia', '#FF00FF', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(39, 'Magenta', '#FF00FF', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(40, 'MediumOrchid', '#BA55D3', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(41, 'MediumPurple', '#9370DB', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(42, 'Amethyst', '#9966CC', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(43, 'BlueViolet', '#8A2BE2', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(44, 'DarkViolet', '#9400D3', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(45, 'DarkOrchid', '#9932CC', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(46, 'DarkMagenta', '#8B008B', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(47, 'Purple', '#800080', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(48, 'Indigo', '#4B0082', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(49, 'SlateBlue', '#6A5ACD', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(50, 'DarkSlateBlue', '#483D8B', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(51, 'MediumSlateBlue', '#7B68EE', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(52, 'GreenYellow', '#ADFF2F', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(53, 'Chartreuse', '#7FFF00', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(54, 'LawnGreen', '#7CFC00', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(55, 'Lime', '#00FF00', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(56, 'LimeGreen', '#32CD32', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(57, 'PaleGreen', '#98FB98', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(58, 'LightGreen', '#90EE90', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(59, 'MediumSpringGreen', '#00FA9A', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(60, 'SpringGreen', '#00FF7F', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(61, 'MediumSeaGreen', '#3CB371', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(62, 'SeaGreen', '#2E8B57', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(63, 'ForestGreen', '#228B22', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(64, 'Green', '#008000', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(65, 'DarkGreen', '#006400', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(66, 'YellowGreen', '#9ACD32', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(67, 'OliveDrab', '#6B8E23', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(68, 'Olive', '#808000', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(69, 'DarkOliveGreen', '#556B2F', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(70, 'MediumAquamarine', '#66CDAA', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(71, 'DarkSeaGreen', '#8FBC8F', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(72, 'LightSeaGreen', '#20B2AA', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(73, 'DarkCyan', '#008B8B', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(74, 'Teal', '#008080', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(75, 'Aqua', '#00FFFF', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(76, 'Cyan', '#00FFFF', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(77, 'LightCyan', '#E0FFFF', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(78, 'PaleTurquoise', '#AFEEEE', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(79, 'Aquamarine', '#7FFFD4', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(80, 'Turquoise', '#40E0D0', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(81, 'MediumTurquoise', '#48D1CC', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(82, 'DarkTurquoise', '#00CED1', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(83, 'CadetBlue', '#5F9EA0', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(84, 'SteelBlue', '#4682B4', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(85, 'LightSteelBlue', '#B0C4DE', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(86, 'PowderBlue', '#B0E0E6', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(87, 'LightBlue', '#ADD8E6', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(88, 'SkyBlue', '#87CEEB', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(89, 'LightSkyBlue', '#87CEFA', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(90, 'DeepSkyBlue', '#00BFFF', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(91, 'DodgerBlue', '#1E90FF', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(92, 'CornflowerBlue', '#6495ED', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(93, 'MediumSlateBlue', '#7B68EE', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(94, 'RoyalBlue', '#4169E1', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(95, 'Blue', '#0000FF', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(96, 'MediumBlue', '#0000CD', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(97, 'DarkBlue', '#00008B', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(98, 'Navy', '#000080', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(99, 'MidnightBlue', '#191970', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(100, 'Cornsilk', '#FFF8DC', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(101, 'BlanchedAlmond', '#FFEBCD', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(102, 'Bisque', '#FFE4C4', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(103, 'NavajoWhite', '#FFDEAD', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(104, 'Wheat', '#F5DEB3', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(105, 'BurlyWood', '#DEB887', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(106, 'Tan', '#D2B48C', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(107, 'RosyBrown', '#BC8F8F', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(108, 'SandyBrown', '#F4A460', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(109, 'Goldenrod', '#DAA520', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(110, 'DarkGoldenrod', '#B8860B', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(111, 'Peru', '#CD853F', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(112, 'Chocolate', '#D2691E', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(113, 'SaddleBrown', '#8B4513', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(114, 'Sienna', '#A0522D', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(115, 'Brown', '#A52A2A', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(116, 'Maroon', '#800000', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(117, 'White', '#FFFFFF', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(118, 'Snow', '#FFFAFA', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(119, 'Honeydew', '#F0FFF0', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(120, 'MintCream', '#F5FFFA', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(121, 'Azure', '#F0FFFF', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(122, 'AliceBlue', '#F0F8FF', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(123, 'GhostWhite', '#F8F8FF', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(124, 'WhiteSmoke', '#F5F5F5', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(125, 'Seashell', '#FFF5EE', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(126, 'Beige', '#F5F5DC', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(127, 'OldLace', '#FDF5E6', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(128, 'FloralWhite', '#FFFAF0', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(129, 'Ivory', '#FFFFF0', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(130, 'AntiqueWhite', '#FAEBD7', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(131, 'Linen', '#FAF0E6', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(132, 'LavenderBlush', '#FFF0F5', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(133, 'MistyRose', '#FFE4E1', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(134, 'Gainsboro', '#DCDCDC', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(135, 'LightGrey', '#D3D3D3', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(136, 'Silver', '#C0C0C0', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(137, 'DarkGray', '#A9A9A9', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(138, 'Gray', '#808080', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(139, 'DimGray', '#696969', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(140, 'LightSlateGray', '#778899', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(141, 'SlateGray', '#708090', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(142, 'DarkSlateGray', '#2F4F4F', '2021-08-25 07:57:50', '2021-08-25 07:57:50'),
(143, 'Black', '#000000', '2021-08-25 07:57:50', '2021-08-25 07:57:50');

-- --------------------------------------------------------

--
-- Table structure for table `consignment_products`
--

CREATE TABLE `consignment_products` (
  `id` bigint NOT NULL,
  `product_id` bigint NOT NULL,
  `sku` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `qty` int NOT NULL DEFAULT '0',
  `sales` int NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `id` int NOT NULL,
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `details` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `discount` double(20,2) NOT NULL,
  `discount_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `start_date` int NOT NULL,
  `end_date` int NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `coupon_usages`
--

CREATE TABLE `coupon_usages` (
  `id` int NOT NULL,
  `user_id` int NOT NULL,
  `coupon_id` int NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `delivery_books`
--

CREATE TABLE `delivery_books` (
  `id` int NOT NULL,
  `location_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `user_id` bigint UNSIGNED NOT NULL,
  `city_id` int NOT NULL DEFAULT '0',
  `district_id` int NOT NULL DEFAULT '0',
  `ward_id` int NOT NULL DEFAULT '0',
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `detailed_address` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `receiver` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` enum('1','0') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `post_code` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `delivery_books`
--

INSERT INTO `delivery_books` (`id`, `location_name`, `user_id`, `city_id`, `district_id`, `ward_id`, `address`, `detailed_address`, `receiver`, `phone`, `type`, `post_code`, `created_at`, `updated_at`) VALUES
(1, 'Abc', 20, 0, 0, 0, 'abc', NULL, 'abvc', '123123', '0', NULL, '2021-04-28 09:49:59', '2021-04-29 02:19:50'),
(2, 'Company', 20, 3, 1, 1, 'Hue', NULL, 'Tan Le', '0983886629', '0', '12345', '2021-04-28 19:42:11', '2021-04-29 02:19:50'),
(3, 'Company', 20, 3, 1, 1, 'Hue', NULL, 'Tan Le', '0983886629', '0', '12345', '2021-04-28 19:42:58', '2021-04-29 02:19:50'),
(4, 'Company 2', 20, 4, 4, 4, 'Hue', NULL, 'Tan Le', '0983886629', '1', '12345', '2021-04-29 02:19:50', '2021-04-29 02:19:50');

-- --------------------------------------------------------

--
-- Table structure for table `delivery_condition`
--

CREATE TABLE `delivery_condition` (
  `id` int NOT NULL,
  `title` varchar(191) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `price` int NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `delivery_condition`
--

INSERT INTO `delivery_condition` (`id`, `title`, `price`, `created_at`, `updated_at`) VALUES
(1, 'Normal Box', 0, '0000-00-00 00:00:00', '2021-05-31 04:53:46'),
(3, 'Special Box', 2000, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `delivery_management`
--

CREATE TABLE `delivery_management` (
  `id` bigint NOT NULL,
  `type_delivery` int NOT NULL DEFAULT '0' COMMENT '1=weight,2=distance,3=other',
  `distance_type` int NOT NULL DEFAULT '0' COMMENT '1=km,2=fix area',
  `delivery_from` int DEFAULT NULL,
  `delivery_to` int DEFAULT NULL,
  `price` int NOT NULL,
  `list_area` text CHARACTER SET utf8 COLLATE utf8_bin,
  `interprovincial_type` int NOT NULL DEFAULT '0' COMMENT '1=car,2=Airplane',
  `list_city` text CHARACTER SET utf8 COLLATE utf8_bin,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `delivery_management`
--

INSERT INTO `delivery_management` (`id`, `type_delivery`, `distance_type`, `delivery_from`, `delivery_to`, `price`, `list_area`, `interprovincial_type`, `list_city`, `created_at`, `updated_at`) VALUES
(4, 2, 2, NULL, NULL, 12000, NULL, 0, NULL, '2021-04-29 07:05:05', '2021-04-29 07:09:33'),
(5, 2, 2, NULL, NULL, 20000, NULL, 0, NULL, '2021-04-29 07:09:33', '2021-04-29 07:09:33'),
(6, 2, 1, 1, 10, 12000, NULL, 0, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 3, 0, NULL, NULL, 20000, NULL, 1, NULL, '2021-04-29 08:00:35', '2021-04-29 09:17:29'),
(12, 3, 0, NULL, NULL, 100000, NULL, 2, NULL, '2021-04-29 09:17:29', '2021-04-29 09:17:29'),
(13, 0, 0, 0, 5, 1000, NULL, 0, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 1, 0, 0, 5, 1000, NULL, 0, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `delivery_management_city`
--

CREATE TABLE `delivery_management_city` (
  `id` int NOT NULL,
  `city_id` int NOT NULL DEFAULT '0',
  `delivery_id` int NOT NULL DEFAULT '0',
  `district_id` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `delivery_management_city`
--

INSERT INTO `delivery_management_city` (`id`, `city_id`, `delivery_id`, `district_id`) VALUES
(2, 0, 4, 5),
(3, 0, 5, 4),
(4, 3, 8, 0),
(5, 3, 9, 0),
(6, 3, 10, 0),
(8, 3, 12, 0);

-- --------------------------------------------------------

--
-- Table structure for table `districts`
--

CREATE TABLE `districts` (
  `id` int NOT NULL,
  `city_id` int NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `districts`
--

INSERT INTO `districts` (`id`, `city_id`, `name`, `created_at`, `updated_at`) VALUES
(1, 3, 'Gangdong ', '2021-04-29 09:09:17', '2021-04-29 09:09:17'),
(2, 2, 'Songpa', '2021-04-29 09:09:17', '2021-04-29 09:09:17'),
(3, 1, 'Gangnam', '2021-04-29 09:09:17', '2021-04-29 09:09:17'),
(4, 4, 'Seocho', '2021-04-29 09:09:17', '2021-04-29 09:09:17'),
(5, 4, 'Gwanak', '2021-04-29 09:09:17', '2021-04-29 09:09:17'),
(6, 6, 'Dongjak', '2021-04-29 09:09:17', '2021-04-29 09:09:17');

-- --------------------------------------------------------

--
-- Table structure for table `entity_images`
--

CREATE TABLE `entity_images` (
  `id` bigint NOT NULL,
  `file_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `file_original_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `file_size` int NOT NULL,
  `extension` varchar(5) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `type` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `entity_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `entity_id` bigint NOT NULL,
  `zone` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `entity_images`
--

INSERT INTO `entity_images` (`id`, `file_name`, `file_original_name`, `file_size`, `extension`, `type`, `entity_type`, `entity_id`, `zone`, `created_at`, `updated_at`) VALUES
(1, 'uploads/all/7YuyL6Hv9ouIMcxL5jf0WPZC9ncd9c3IwKW8ikzI.png', 'img_grape', 19243, 'png', 'image', 'App\\Banner', 1, 'banner', '2021-03-09 08:59:09', '2021-03-15 02:24:16'),
(2, 'uploads/all/7YuyL6Hv9ouIMcxL5jf0WPZC9ncd9c3IwKW8ikzI.png', 'img_grape', 19243, 'png', 'image', 'App\\Banner', 2, 'banner', '2021-03-09 08:59:09', '2021-03-15 02:24:16'),
(3, 'uploads/all/L37mVULWRqsyaW0OruURxw4mmDV2sAokyFBJ4BoS.png', 'img_deal_mango', 24515, 'png', 'image', 'App/Banner', 3, 'banner', '2021-03-09 08:59:09', '2021-03-15 02:24:16'),
(4, 'uploads/all/U4ToWDgGaM4UDeBFBwoi6xzhoCN2ZZd1qeBa46qW.png', 'img_peach', 16684, 'png', 'image', 'App/Banner', 4, 'banner', '2021-03-09 08:59:09', '2021-03-15 02:24:16'),
(5, 'uploads/all/U4ToWDgGaM4UDeBFBwoi6xzhoCN2ZZd1qeBa46qW.png', 'img_peach', 16684, 'png', 'image', 'App/Banner', 5, 'banner', '2021-03-09 08:59:09', '2021-03-15 02:24:16'),
(6, 'uploads/all/7YuyL6Hv9ouIMcxL5jf0WPZC9ncd9c3IwKW8ikzI.png', 'img_grape', 19243, 'png', 'image', 'App/Banner', 6, 'banner', '2021-03-09 08:59:09', '2021-03-15 02:24:16'),
(7, 'uploads/all/oknrbyio2c520s8YsFfcA02THs5edNTyd7HuApaf.png', 'img_small_apple', 2699, 'png', 'image', 'App/Banner', 7, 'banner', '2021-03-09 08:59:09', '2021-03-15 02:24:16'),
(8, 'uploads/all/U4ToWDgGaM4UDeBFBwoi6xzhoCN2ZZd1qeBa46qW.png', 'img_peach', 16684, 'png', 'image', 'App/Banner', 8, 'banner', '2021-03-09 08:59:09', '2021-03-15 02:24:16'),
(9, 'uploads/all/U4ToWDgGaM4UDeBFBwoi6xzhoCN2ZZd1qeBa46qW.png', 'img_peach', 16684, 'png', 'image', 'App/Banner', 9, 'banner', '2021-03-09 08:59:10', '2021-03-15 02:24:16'),
(10, 'uploads/all/5CM9gVA8sFJVur80L41jYEz53QVEK9wRlCr10Rlv.png', 'img_mango', 19191, 'png', 'image', 'App/Product', 1, 'image', '2021-03-12 07:22:20', '2021-03-15 02:24:16'),
(11, 'uploads/all/HreldpMpglaz1pF2skU66aIRa0XOB3JfmIY90Hhf.png', 'img_deal_blueberry', 26935, 'png', 'image', 'App/Product', 1, 'image', '2021-03-12 07:22:20', '2021-03-15 02:24:16'),
(12, 'uploads/all/4uD1E1B8D0WJrTSCny6ogAeJKjd8FY6cLsv39OpJ.png', 'img_banana', 9667, 'png', 'image', 'App/Product', 1, 'image', '2021-03-12 07:22:20', '2021-03-15 02:24:16'),
(13, 'uploads/all/AJZpoShWAV3Ho2FWqIZxzuadxGNhWjxazmkEhL53.png', 'img_logo', 12234, 'png', 'image', 'App/Product', 2, 'image', '2021-03-12 07:22:20', '2021-03-15 02:24:16'),
(14, 'uploads/all/5CM9gVA8sFJVur80L41jYEz53QVEK9wRlCr10Rlv.png', 'img_mango', 19191, 'png', 'image', 'App/Product', 2, 'image', '2021-03-12 07:22:20', '2021-03-15 02:24:16'),
(15, 'uploads/all/oknrbyio2c520s8YsFfcA02THs5edNTyd7HuApaf.png', 'img_small_apple', 2699, 'png', 'image', 'App/Product', 2, 'image', '2021-03-12 07:22:20', '2021-03-15 02:24:16'),
(16, 'uploads/all/oknrbyio2c520s8YsFfcA02THs5edNTyd7HuApaf.png', 'img_small_apple', 2699, 'png', 'image', 'App/Product', 3, 'image', '2021-03-12 07:22:20', '2021-03-15 02:24:16'),
(17, 'uploads/all/wCeMnIMQRXBpUr47KpQPDfXss6KiO9IEZxi0qsvE.png', 'img_orange', 14040, 'png', 'image', 'App/Product', 3, 'image', '2021-03-12 07:22:20', '2021-03-15 02:24:16'),
(18, 'uploads/all/7YuyL6Hv9ouIMcxL5jf0WPZC9ncd9c3IwKW8ikzI.png', 'img_grape', 19243, 'png', 'image', 'App/Product', 3, 'image', '2021-03-12 07:22:20', '2021-03-15 02:24:16'),
(19, 'uploads/all/5CM9gVA8sFJVur80L41jYEz53QVEK9wRlCr10Rlv.png', 'img_mango', 19191, 'png', 'image', 'App/Product', 4, 'image', '2021-03-12 07:22:20', '2021-03-15 02:24:16'),
(20, 'uploads/all/L37mVULWRqsyaW0OruURxw4mmDV2sAokyFBJ4BoS.png', 'img_deal_mango', 24515, 'png', 'image', 'App/Product', 4, 'image', '2021-03-12 07:22:20', '2021-03-15 02:24:16'),
(21, 'uploads/all/oknrbyio2c520s8YsFfcA02THs5edNTyd7HuApaf.png', 'img_small_apple', 2699, 'png', 'image', 'App/Product', 4, 'image', '2021-03-12 07:22:20', '2021-03-15 02:24:16'),
(22, 'uploads/all/4uD1E1B8D0WJrTSCny6ogAeJKjd8FY6cLsv39OpJ.png', 'img_banana', 9667, 'png', 'image', 'App/Product', 5, 'image', '2021-03-12 07:22:20', '2021-03-15 02:24:16'),
(23, 'uploads/business_document/KKoJjzkUj50qyX9evpYxwUTU94qpXqY5VIgtPT71.docx', 'Le_Van_Tan', 42422, 'docx', 'document', 'App/Product', 5, 'image', '2021-03-12 07:22:20', '2021-03-15 02:24:16'),
(24, 'uploads/all/L37mVULWRqsyaW0OruURxw4mmDV2sAokyFBJ4BoS.png', 'img_deal_mango', 24515, 'png', 'image', 'App/Product', 5, 'image', '2021-03-12 07:22:20', '2021-03-15 02:24:16'),
(25, 'uploads/all/AJZpoShWAV3Ho2FWqIZxzuadxGNhWjxazmkEhL53.png', 'img_logo', 12234, 'png', 'image', 'App/Product', 6, 'image', '2021-03-12 07:22:20', '2021-03-15 02:24:16'),
(26, 'uploads/all/5CM9gVA8sFJVur80L41jYEz53QVEK9wRlCr10Rlv.png', 'img_mango', 19191, 'png', 'image', 'App/Product', 6, 'image', '2021-03-12 07:22:20', '2021-03-15 02:24:16'),
(27, 'uploads/all/U4ToWDgGaM4UDeBFBwoi6xzhoCN2ZZd1qeBa46qW.png', 'img_peach', 16684, 'png', 'image', 'App/Product', 6, 'image', '2021-03-12 07:22:20', '2021-03-15 02:24:16'),
(28, 'uploads/all/AJZpoShWAV3Ho2FWqIZxzuadxGNhWjxazmkEhL53.png', 'img_logo', 12234, 'png', 'image', 'App/Product', 7, 'image', '2021-03-12 07:22:21', '2021-03-15 02:24:17'),
(29, 'uploads/all/5CM9gVA8sFJVur80L41jYEz53QVEK9wRlCr10Rlv.png', 'img_mango', 19191, 'png', 'image', 'App/Product', 7, 'image', '2021-03-12 07:22:21', '2021-03-15 02:24:17'),
(30, 'uploads/all/HreldpMpglaz1pF2skU66aIRa0XOB3JfmIY90Hhf.png', 'img_deal_blueberry', 26935, 'png', 'image', 'App/Product', 7, 'image', '2021-03-12 07:22:21', '2021-03-15 02:24:17'),
(31, 'uploads/all/L37mVULWRqsyaW0OruURxw4mmDV2sAokyFBJ4BoS.png', 'img_deal_mango', 24515, 'png', 'image', 'App/Product', 8, 'image', '2021-03-12 07:22:21', '2021-03-15 02:24:17'),
(32, 'uploads/all/AJZpoShWAV3Ho2FWqIZxzuadxGNhWjxazmkEhL53.png', 'img_logo', 12234, 'png', 'image', 'App/Product', 8, 'image', '2021-03-12 07:22:21', '2021-03-15 02:24:17'),
(33, 'uploads/all/U4ToWDgGaM4UDeBFBwoi6xzhoCN2ZZd1qeBa46qW.png', 'img_peach', 16684, 'png', 'image', 'App/Product', 8, 'image', '2021-03-12 07:22:21', '2021-03-15 02:24:17'),
(34, 'uploads/all/5CM9gVA8sFJVur80L41jYEz53QVEK9wRlCr10Rlv.png', 'img_mango', 19191, 'png', 'image', 'App/Product', 9, 'image', '2021-03-12 07:22:21', '2021-03-15 02:24:17'),
(35, 'uploads/all/oknrbyio2c520s8YsFfcA02THs5edNTyd7HuApaf.png', 'img_small_apple', 2699, 'png', 'image', 'App/Product', 9, 'image', '2021-03-12 07:22:21', '2021-03-15 02:24:17'),
(36, 'uploads/all/HreldpMpglaz1pF2skU66aIRa0XOB3JfmIY90Hhf.png', 'img_deal_blueberry', 26935, 'png', 'image', 'App/Product', 9, 'image', '2021-03-12 07:22:21', '2021-03-15 02:24:17'),
(37, 'uploads/business_document/aMnWaPm7Tht1Aae7dYaj0WeHQuVZgxExlcMC2dfR.docx', 'Le_Van_Tan', 42422, 'docx', 'document', 'App/Product', 1, 'image', '2021-03-12 08:25:32', '2021-03-15 02:24:17'),
(38, 'uploads/all/7YuyL6Hv9ouIMcxL5jf0WPZC9ncd9c3IwKW8ikzI.png', 'img_grape', 19243, 'png', 'image', 'App/Product', 1, 'image', '2021-03-12 08:25:32', '2021-03-15 02:24:17'),
(39, 'uploads/business_document/KKoJjzkUj50qyX9evpYxwUTU94qpXqY5VIgtPT71.docx', 'Le_Van_Tan', 42422, 'docx', 'document', 'App/Product', 1, 'image', '2021-03-12 08:25:32', '2021-03-15 02:24:17'),
(40, 'uploads/all/7YuyL6Hv9ouIMcxL5jf0WPZC9ncd9c3IwKW8ikzI.png', 'img_grape', 19243, 'png', 'image', 'App/Product', 2, 'image', '2021-03-12 08:25:32', '2021-03-15 02:24:17'),
(41, 'uploads/business_document/aMnWaPm7Tht1Aae7dYaj0WeHQuVZgxExlcMC2dfR.docx', 'Le_Van_Tan', 42422, 'docx', 'document', 'App/Product', 2, 'image', '2021-03-12 08:25:32', '2021-03-15 02:24:17'),
(42, 'uploads/all/wCeMnIMQRXBpUr47KpQPDfXss6KiO9IEZxi0qsvE.png', 'img_orange', 14040, 'png', 'image', 'App/Product', 2, 'image', '2021-03-12 08:25:32', '2021-03-15 02:24:17'),
(43, 'uploads/business_document/KKoJjzkUj50qyX9evpYxwUTU94qpXqY5VIgtPT71.docx', 'Le_Van_Tan', 42422, 'docx', 'document', 'App/Product', 3, 'image', '2021-03-12 08:25:32', '2021-03-15 02:24:17'),
(44, 'uploads/all/oknrbyio2c520s8YsFfcA02THs5edNTyd7HuApaf.png', 'img_small_apple', 2699, 'png', 'image', 'App/Product', 3, 'image', '2021-03-12 08:25:32', '2021-03-15 02:24:17'),
(45, 'uploads/all/4uD1E1B8D0WJrTSCny6ogAeJKjd8FY6cLsv39OpJ.png', 'img_banana', 9667, 'png', 'image', 'App/Product', 3, 'image', '2021-03-12 08:25:32', '2021-03-15 02:24:17'),
(46, 'uploads/all/5CM9gVA8sFJVur80L41jYEz53QVEK9wRlCr10Rlv.png', 'img_mango', 19191, 'png', 'image', 'App/Product', 4, 'image', '2021-03-12 08:25:32', '2021-03-15 02:24:17'),
(47, 'uploads/all/U4ToWDgGaM4UDeBFBwoi6xzhoCN2ZZd1qeBa46qW.png', 'img_peach', 16684, 'png', 'image', 'App/Product', 4, 'image', '2021-03-12 08:25:32', '2021-03-15 02:24:17'),
(48, 'uploads/business_document/aMnWaPm7Tht1Aae7dYaj0WeHQuVZgxExlcMC2dfR.docx', 'Le_Van_Tan', 42422, 'docx', 'document', 'App/Product', 4, 'image', '2021-03-12 08:25:32', '2021-03-15 02:24:17'),
(49, 'uploads/all/7YuyL6Hv9ouIMcxL5jf0WPZC9ncd9c3IwKW8ikzI.png', 'img_grape', 19243, 'png', 'image', 'App/Product', 5, 'image', '2021-03-12 08:25:33', '2021-03-15 02:24:17'),
(50, 'uploads/all/4uD1E1B8D0WJrTSCny6ogAeJKjd8FY6cLsv39OpJ.png', 'img_banana', 9667, 'png', 'image', 'App/Product', 5, 'image', '2021-03-12 08:25:33', '2021-03-15 02:24:17'),
(51, 'uploads/all/wCeMnIMQRXBpUr47KpQPDfXss6KiO9IEZxi0qsvE.png', 'img_orange', 14040, 'png', 'image', 'App/Product', 5, 'image', '2021-03-12 08:25:33', '2021-03-15 02:24:17'),
(52, 'uploads/all/1AltaWzbe42mqSIAtKPFNSJvNXpGXTv4p4TV9vl9.png', 'img_avocado_home', 8118, 'png', 'image', 'App/Product', 6, 'image', '2021-03-12 08:25:33', '2021-03-15 02:24:17'),
(53, 'uploads/business_document/aMnWaPm7Tht1Aae7dYaj0WeHQuVZgxExlcMC2dfR.docx', 'Le_Van_Tan', 42422, 'docx', 'document', 'App/Product', 6, 'image', '2021-03-12 08:25:33', '2021-03-15 02:24:17'),
(54, 'uploads/all/L37mVULWRqsyaW0OruURxw4mmDV2sAokyFBJ4BoS.png', 'img_deal_mango', 24515, 'png', 'image', 'App/Product', 6, 'image', '2021-03-12 08:25:33', '2021-03-15 02:24:17'),
(55, 'uploads/all/wCeMnIMQRXBpUr47KpQPDfXss6KiO9IEZxi0qsvE.png', 'img_orange', 14040, 'png', 'image', 'App/Product', 7, 'image', '2021-03-12 08:25:33', '2021-03-15 02:24:17'),
(56, 'uploads/all/5CM9gVA8sFJVur80L41jYEz53QVEK9wRlCr10Rlv.png', 'img_mango', 19191, 'png', 'image', 'App/Product', 7, 'image', '2021-03-12 08:25:33', '2021-03-15 02:24:17'),
(57, 'uploads/all/1AltaWzbe42mqSIAtKPFNSJvNXpGXTv4p4TV9vl9.png', 'img_avocado_home', 8118, 'png', 'image', 'App/Product', 7, 'image', '2021-03-12 08:25:33', '2021-03-15 02:24:17'),
(58, 'uploads/all/AJZpoShWAV3Ho2FWqIZxzuadxGNhWjxazmkEhL53.png', 'img_logo', 12234, 'png', 'image', 'App/Product', 8, 'image', '2021-03-12 08:25:33', '2021-03-15 02:24:17'),
(59, 'uploads/business_document/KKoJjzkUj50qyX9evpYxwUTU94qpXqY5VIgtPT71.docx', 'Le_Van_Tan', 42422, 'docx', 'document', 'App/Product', 8, 'image', '2021-03-12 08:25:33', '2021-03-15 02:24:17'),
(60, 'uploads/all/4uD1E1B8D0WJrTSCny6ogAeJKjd8FY6cLsv39OpJ.png', 'img_banana', 9667, 'png', 'image', 'App/Product', 8, 'image', '2021-03-12 08:25:33', '2021-03-15 02:24:17'),
(61, 'uploads/all/5CM9gVA8sFJVur80L41jYEz53QVEK9wRlCr10Rlv.png', 'img_mango', 19191, 'png', 'image', 'App/Product', 9, 'image', '2021-03-12 08:25:33', '2021-03-15 02:24:17'),
(62, 'uploads/all/1AltaWzbe42mqSIAtKPFNSJvNXpGXTv4p4TV9vl9.png', 'img_avocado_home', 8118, 'png', 'image', 'App/Product', 9, 'image', '2021-03-12 08:25:33', '2021-03-15 02:24:17'),
(63, 'uploads/all/wCeMnIMQRXBpUr47KpQPDfXss6KiO9IEZxi0qsvE.png', 'img_orange', 14040, 'png', 'image', 'App/Product', 9, 'image', '2021-03-12 08:25:33', '2021-03-15 02:24:17'),
(64, 'uploads/business_document/KKoJjzkUj50qyX9evpYxwUTU94qpXqY5VIgtPT71.docx', 'Le_Van_Tan', 42422, 'docx', 'document', 'App/Product', 1, 'image', '2021-03-12 08:26:29', '2021-03-15 02:24:17'),
(65, 'uploads/all/wCeMnIMQRXBpUr47KpQPDfXss6KiO9IEZxi0qsvE.png', 'img_orange', 14040, 'png', 'image', 'App/Product', 1, 'image', '2021-03-12 08:26:29', '2021-03-15 02:24:17'),
(66, 'uploads/all/4uD1E1B8D0WJrTSCny6ogAeJKjd8FY6cLsv39OpJ.png', 'img_banana', 9667, 'png', 'image', 'App/Product', 1, 'image', '2021-03-12 08:26:29', '2021-03-15 02:24:17'),
(67, 'uploads/all/L37mVULWRqsyaW0OruURxw4mmDV2sAokyFBJ4BoS.png', 'img_deal_mango', 24515, 'png', 'image', 'App/Product', 2, 'image', '2021-03-12 08:26:29', '2021-03-15 02:24:17'),
(68, 'uploads/business_document/aMnWaPm7Tht1Aae7dYaj0WeHQuVZgxExlcMC2dfR.docx', 'Le_Van_Tan', 42422, 'docx', 'document', 'App/Product', 2, 'image', '2021-03-12 08:26:29', '2021-03-15 02:24:18'),
(69, 'uploads/all/oknrbyio2c520s8YsFfcA02THs5edNTyd7HuApaf.png', 'img_small_apple', 2699, 'png', 'image', 'App/Product', 2, 'image', '2021-03-12 08:26:29', '2021-03-15 02:24:18'),
(70, 'uploads/all/oknrbyio2c520s8YsFfcA02THs5edNTyd7HuApaf.png', 'img_small_apple', 2699, 'png', 'image', 'App/Product', 3, 'image', '2021-03-12 08:26:29', '2021-03-15 02:24:18'),
(71, 'uploads/all/AJZpoShWAV3Ho2FWqIZxzuadxGNhWjxazmkEhL53.png', 'img_logo', 12234, 'png', 'image', 'App/Product', 3, 'image', '2021-03-12 08:26:29', '2021-03-15 02:24:18'),
(72, 'uploads/all/U4ToWDgGaM4UDeBFBwoi6xzhoCN2ZZd1qeBa46qW.png', 'img_peach', 16684, 'png', 'image', 'App/Product', 3, 'image', '2021-03-12 08:26:29', '2021-03-15 02:24:18'),
(73, 'uploads/all/U4ToWDgGaM4UDeBFBwoi6xzhoCN2ZZd1qeBa46qW.png', 'img_peach', 16684, 'png', 'image', 'App/Product', 4, 'image', '2021-03-12 08:26:29', '2021-03-15 02:24:18'),
(74, 'uploads/all/7YuyL6Hv9ouIMcxL5jf0WPZC9ncd9c3IwKW8ikzI.png', 'img_grape', 19243, 'png', 'image', 'App/Product', 4, 'image', '2021-03-12 08:26:29', '2021-03-15 02:24:18'),
(75, 'uploads/all/HreldpMpglaz1pF2skU66aIRa0XOB3JfmIY90Hhf.png', 'img_deal_blueberry', 26935, 'png', 'image', 'App/Product', 4, 'image', '2021-03-12 08:26:29', '2021-03-15 02:24:18'),
(76, 'uploads/all/wCeMnIMQRXBpUr47KpQPDfXss6KiO9IEZxi0qsvE.png', 'img_orange', 14040, 'png', 'image', 'App/Product', 5, 'image', '2021-03-12 08:26:30', '2021-03-15 02:24:18'),
(77, 'uploads/all/1AltaWzbe42mqSIAtKPFNSJvNXpGXTv4p4TV9vl9.png', 'img_avocado_home', 8118, 'png', 'image', 'App/Product', 5, 'image', '2021-03-12 08:26:30', '2021-03-15 02:24:18'),
(78, 'uploads/all/HreldpMpglaz1pF2skU66aIRa0XOB3JfmIY90Hhf.png', 'img_deal_blueberry', 26935, 'png', 'image', 'App/Product', 5, 'image', '2021-03-12 08:26:30', '2021-03-15 02:24:18'),
(79, 'uploads/all/oknrbyio2c520s8YsFfcA02THs5edNTyd7HuApaf.png', 'img_small_apple', 2699, 'png', 'image', 'App/Product', 6, 'image', '2021-03-12 08:26:30', '2021-03-15 02:24:18'),
(80, 'uploads/all/AJZpoShWAV3Ho2FWqIZxzuadxGNhWjxazmkEhL53.png', 'img_logo', 12234, 'png', 'image', 'App/Product', 6, 'image', '2021-03-12 08:26:30', '2021-03-15 02:24:18'),
(81, 'uploads/all/1AltaWzbe42mqSIAtKPFNSJvNXpGXTv4p4TV9vl9.png', 'img_avocado_home', 8118, 'png', 'image', 'App/Product', 6, 'image', '2021-03-12 08:26:30', '2021-03-15 02:24:18'),
(82, 'uploads/all/AJZpoShWAV3Ho2FWqIZxzuadxGNhWjxazmkEhL53.png', 'img_logo', 12234, 'png', 'image', 'App/Product', 7, 'image', '2021-03-12 08:26:30', '2021-03-15 02:24:18'),
(83, 'uploads/business_document/aMnWaPm7Tht1Aae7dYaj0WeHQuVZgxExlcMC2dfR.docx', 'Le_Van_Tan', 42422, 'docx', 'document', 'App/Product', 7, 'image', '2021-03-12 08:26:30', '2021-03-15 02:24:18'),
(84, 'uploads/all/wCeMnIMQRXBpUr47KpQPDfXss6KiO9IEZxi0qsvE.png', 'img_orange', 14040, 'png', 'image', 'App/Product', 7, 'image', '2021-03-12 08:26:30', '2021-03-15 02:24:18'),
(85, 'uploads/all/7YuyL6Hv9ouIMcxL5jf0WPZC9ncd9c3IwKW8ikzI.png', 'img_grape', 19243, 'png', 'image', 'App/Product', 8, 'image', '2021-03-12 08:26:30', '2021-03-15 02:24:18'),
(86, 'uploads/business_document/KKoJjzkUj50qyX9evpYxwUTU94qpXqY5VIgtPT71.docx', 'Le_Van_Tan', 42422, 'docx', 'document', 'App/Product', 8, 'image', '2021-03-12 08:26:30', '2021-03-15 02:24:18'),
(87, 'uploads/all/1AltaWzbe42mqSIAtKPFNSJvNXpGXTv4p4TV9vl9.png', 'img_avocado_home', 8118, 'png', 'image', 'App/Product', 8, 'image', '2021-03-12 08:26:30', '2021-03-15 02:24:18'),
(88, 'uploads/all/1AltaWzbe42mqSIAtKPFNSJvNXpGXTv4p4TV9vl9.png', 'img_avocado_home', 8118, 'png', 'image', 'App/Product', 9, 'image', '2021-03-12 08:26:30', '2021-03-15 02:24:18'),
(89, 'uploads/all/7YuyL6Hv9ouIMcxL5jf0WPZC9ncd9c3IwKW8ikzI.png', 'img_grape', 19243, 'png', 'image', 'App/Product', 9, 'image', '2021-03-12 08:26:31', '2021-03-15 02:24:18'),
(90, 'uploads/all/oknrbyio2c520s8YsFfcA02THs5edNTyd7HuApaf.png', 'img_small_apple', 2699, 'png', 'image', 'App/Product', 9, 'image', '2021-03-12 08:26:31', '2021-03-15 02:24:18'),
(91, 'uploads/all/1AltaWzbe42mqSIAtKPFNSJvNXpGXTv4p4TV9vl9.png', 'img_avocado_home', 8118, 'png', 'image', 'App\\User', 10, 'document', '2021-03-22 10:17:17', '2021-03-22 10:17:17'),
(92, 'uploads/all/L37mVULWRqsyaW0OruURxw4mmDV2sAokyFBJ4BoS.png', 'img_deal_mango', 24515, 'png', 'image', 'App\\User', 10, 'document', '2021-03-22 10:17:17', '2021-03-22 10:17:17');

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

CREATE TABLE `faqs` (
  `id` bigint NOT NULL,
  `title` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `status` enum('1','2') CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `faqs`
--

INSERT INTO `faqs` (`id`, `title`, `content`, `status`, `created_at`, `updated_at`) VALUES
(1, 'What is Lorem Ipsum 1', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '1', '2021-03-09 08:59:24', '2021-03-09 08:59:24'),
(2, 'What is Lorem Ipsum 2', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '1', '2021-03-09 08:59:24', '2021-03-09 08:59:24'),
(3, 'What is Lorem Ipsum 3', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '1', '2021-03-09 08:59:24', '2021-03-09 08:59:24'),
(4, 'What is Lorem Ipsum 4', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '1', '2021-03-09 08:59:24', '2021-03-09 08:59:24'),
(5, 'What is Lorem Ipsum 5', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '1', '2021-03-09 08:59:24', '2021-03-09 08:59:24'),
(6, 'What is Lorem Ipsum 6', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '1', '2021-03-09 08:59:24', '2021-03-09 08:59:24'),
(7, 'What is Lorem Ipsum 7', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '1', '2021-03-09 08:59:24', '2021-03-09 08:59:24'),
(8, 'What is Lorem Ipsum 8', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '1', '2021-03-09 08:59:24', '2021-03-09 08:59:24'),
(9, 'What is Lorem Ipsum 9', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '1', '2021-03-09 08:59:24', '2021-03-09 08:59:24');

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE `files` (
  `id` bigint NOT NULL,
  `file_original_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `file_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `member_id` bigint NOT NULL,
  `user_id` bigint DEFAULT '0',
  `file_size` int NOT NULL,
  `extension` varchar(5) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `type` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `files`
--

INSERT INTO `files` (`id`, `file_original_name`, `file_name`, `member_id`, `user_id`, `file_size`, `extension`, `type`, `created_at`, `updated_at`) VALUES
(1, 'img_avocado_home', 'uploads/all/1AltaWzbe42mqSIAtKPFNSJvNXpGXTv4p4TV9vl9.png', 1, 10, 8118, 'png', 'image', '2021-03-09 08:17:20', '2021-03-09 08:17:20'),
(2, 'img_deal_mango', 'uploads/all/L37mVULWRqsyaW0OruURxw4mmDV2sAokyFBJ4BoS.png', 1, 10, 24515, 'png', 'image', '2021-03-09 08:17:20', '2021-03-09 08:17:20'),
(3, 'img_grape', 'uploads/all/7YuyL6Hv9ouIMcxL5jf0WPZC9ncd9c3IwKW8ikzI.png', 1, 0, 19243, 'png', 'image', '2021-03-09 08:17:20', '2021-03-09 08:17:20'),
(4, 'img_banana', 'uploads/all/4uD1E1B8D0WJrTSCny6ogAeJKjd8FY6cLsv39OpJ.png', 1, 0, 9667, 'png', 'image', '2021-03-09 08:17:20', '2021-03-09 08:17:20'),
(5, 'img_logo', 'uploads/all/AJZpoShWAV3Ho2FWqIZxzuadxGNhWjxazmkEhL53.png', 1, 0, 12234, 'png', 'image', '2021-03-09 08:17:20', '2021-03-09 08:17:20'),
(6, 'img_deal_blueberry', 'uploads/all/HreldpMpglaz1pF2skU66aIRa0XOB3JfmIY90Hhf.png', 1, 0, 26935, 'png', 'image', '2021-03-09 08:17:21', '2021-03-09 08:17:21'),
(7, 'img_mango', 'uploads/all/5CM9gVA8sFJVur80L41jYEz53QVEK9wRlCr10Rlv.png', 1, 0, 19191, 'png', 'image', '2021-03-09 08:17:21', '2021-03-09 08:17:21'),
(8, 'img_orange', 'uploads/all/wCeMnIMQRXBpUr47KpQPDfXss6KiO9IEZxi0qsvE.png', 1, 0, 14040, 'png', 'image', '2021-03-09 08:17:21', '2021-03-09 08:17:21'),
(9, 'img_small_apple', 'uploads/all/oknrbyio2c520s8YsFfcA02THs5edNTyd7HuApaf.png', 1, 0, 2699, 'png', 'image', '2021-03-09 08:17:21', '2021-03-09 08:17:21'),
(10, 'img_peach', 'uploads/all/U4ToWDgGaM4UDeBFBwoi6xzhoCN2ZZd1qeBa46qW.png', 1, 0, 16684, 'png', 'image', '2021-03-09 08:17:21', '2021-03-09 08:17:21'),
(12, 'Le_Van_Tan', 'uploads/business_document/KKoJjzkUj50qyX9evpYxwUTU94qpXqY5VIgtPT71.docx', 19, 19, 42422, 'docx', 'document', '2021-03-10 10:03:14', '2021-03-10 10:03:14'),
(13, 'Le_Van_Tan', 'uploads/business_document/aMnWaPm7Tht1Aae7dYaj0WeHQuVZgxExlcMC2dfR.docx', 19, 19, 42422, 'docx', 'document', '2021-03-10 10:06:24', '2021-03-10 10:06:24'),
(14, '1Jx6uXfAEDapp', 'uploads/all/RUSBPRyLoAQP2ncvzJVAFfVnyyu5cCPywH5akYiV.png', 1, 0, 696973, 'jpg', 'image', '2021-03-22 08:36:14', '2021-03-22 08:36:14'),
(15, 'nrJSCGbfcP', 'uploads/all/TTu4hdUNvqqwrsOlizHlsSpy8pMNgrkp50T2Qhgo.jpg', 1, 0, 1572082, 'jpeg', 'image', '2021-03-22 08:36:46', '2021-03-22 08:36:46'),
(16, '1-Album-cưới', 'uploads/all/mCOOvg0Xtehk6kufPLZrwRtOje2tb85TgXwLBx91.jpg', 1, 0, 398720, 'jpg', 'image', '2021-03-22 09:18:54', '2021-03-22 09:18:54'),
(17, 'bn', 'uploads/business_document/cZhsadNHmXZfFHjHgqKxOKySEEnHvtyuXwHPgoMG.jpg', 18, 18, 167771, 'jpg', 'image', '2021-03-23 02:02:17', '2021-03-23 02:02:17'),
(18, 'ohwao-1', 'uploads/business_document/JjRjozJClXS22LgLVhx2BGShxFsw4nE9vhrImt8q.png', 18, 18, 30978, 'png', 'image', '2021-03-30 04:24:08', '2021-03-30 04:24:08'),
(19, 'ohwao-1', 'uploads/business_document/70rhOpUZXUqPjzSTrjaH5fekKEEsXZhD6JzmlmbU.png', 18, 18, 30978, 'png', 'image', '2021-03-30 04:24:17', '2021-03-30 04:24:17'),
(20, 'reviews', 'uploads/all/HvG8AWhNxHCWZtL6qFXl3UnPIn1VAI9B7QjtGxte', 1, 0, 2298368, 'xlsx', 'document', '2021-05-21 06:53:47', '2021-05-21 06:53:47'),
(21, 'logo-nails', 'uploads/all/p8fqv7lCHJQJTO5TsaKUElXhUtkFIvuFg1hl00H5.png', 1, 0, 9923, 'png', 'image', '2021-05-24 02:31:12', '2021-05-24 02:31:12'),
(22, 'logo-nails', 'uploads/all/TNZFqYeqBWdDSuFOzwMkS7GOn5Vs8BMQGgQG3fSx.png', 1, 0, 9923, 'png', 'image', '2021-05-24 02:32:06', '2021-05-24 02:32:06'),
(23, 'logo-nails', 'uploads/all/bZv3LRxc9lhPeUvDRYel8hlW1Jpmbo2JVB3GGcWL.png', 1, 0, 9923, 'png', 'image', '2021-05-24 02:33:19', '2021-05-24 02:33:19'),
(24, 'info', 'uploads/all/8x6yqYTlkPeWSDgajHXrv7hhhMknj7E1uopQPB7t.png', 1, 0, 1986, 'png', 'image', '2021-09-08 03:12:11', '2021-09-08 03:12:11');

-- --------------------------------------------------------

--
-- Table structure for table `like_products`
--

CREATE TABLE `like_products` (
  `id` bigint UNSIGNED NOT NULL,
  `user_id` bigint UNSIGNED NOT NULL,
  `product_id` bigint NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `log_activities`
--

CREATE TABLE `log_activities` (
  `id` bigint NOT NULL,
  `user_id` bigint UNSIGNED NOT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `entity_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `entity_id` bigint NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int UNSIGNED NOT NULL,
  `migration` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `batch` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_id` bigint UNSIGNED NOT NULL,
  `data` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `notification_admin`
--

CREATE TABLE `notification_admin` (
  `id` bigint NOT NULL,
  `title` varchar(191) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `type` int NOT NULL COMMENT '1. Announcement\r\n2. Promotion\r\n3. System',
  `content` longtext CHARACTER SET utf8 COLLATE utf8_bin,
  `status` int NOT NULL DEFAULT '1' COMMENT '1=active,2=deactive',
  `images` text CHARACTER SET utf8 COLLATE utf8_bin,
  `id_images` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int DEFAULT NULL,
  `client_id` int UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int NOT NULL,
  `client_id` int UNSIGNED NOT NULL,
  `scopes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int UNSIGNED NOT NULL,
  `user_id` int DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'eR2y7WUuem28ugHKppFpmss7jPyOHZsMkQwBo1Jj', 'http://localhost', 1, 0, 0, '2019-07-13 06:17:34', '2019-07-13 06:17:34'),
(2, NULL, 'Laravel Password Grant Client', 'WLW2Ol0GozbaXEnx1NtXoweYPuKEbjWdviaUgw77', 'http://localhost', 0, 1, 0, '2019-07-13 06:17:34', '2019-07-13 06:17:34');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int UNSIGNED NOT NULL,
  `client_id` int UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2019-07-13 06:17:34', '2019-07-13 06:17:34');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint NOT NULL,
  `displayname` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `store_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `delivery_address` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `sub_total` double NOT NULL DEFAULT '0',
  `delivery_fee` double NOT NULL DEFAULT '0',
  `interprovincial_type` int NOT NULL DEFAULT '0',
  `total` double NOT NULL DEFAULT '0',
  `delivery_method` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `delivery_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipper_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipper_phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `delivery_date` date DEFAULT NULL,
  `status` enum('1','2','3','4','5','6','7','8') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `images` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `id_images` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `invoice_images` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `invoice_id_images` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `upload_file_pdf` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `id_upload_file_pdf` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `user_id` bigint UNSIGNED DEFAULT NULL,
  `note` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `delivery_confirm_date` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `displayname`, `phone`, `email`, `store_name`, `delivery_address`, `sub_total`, `delivery_fee`, `interprovincial_type`, `total`, `delivery_method`, `delivery_id`, `shipper_name`, `shipper_phone`, `delivery_date`, `status`, `images`, `id_images`, `invoice_images`, `invoice_id_images`, `upload_file_pdf`, `id_upload_file_pdf`, `user_id`, `note`, `delivery_confirm_date`, `created_at`, `updated_at`) VALUES
(3, 'abvc', '123123', 'email9@g', NULL, 'abc', 100000, 20000, 0, 120000, NULL, NULL, NULL, NULL, NULL, '5', NULL, NULL, NULL, NULL, NULL, NULL, 20, NULL, NULL, '2021-06-09 20:33:34', '2021-06-09 20:33:34'),
(4, 'abvc', '123123', 'email9@g', NULL, 'abc', 100000, 20000, 0, 120000, NULL, NULL, NULL, NULL, NULL, '5', NULL, NULL, NULL, NULL, NULL, NULL, 20, NULL, NULL, '2021-06-09 20:33:34', '2021-06-09 20:33:34'),
(5, 'abvc', '123123', 'email9@g', NULL, 'abc', 100000, 20000, 0, 120000, NULL, NULL, NULL, NULL, NULL, '5', NULL, NULL, NULL, NULL, NULL, NULL, 20, NULL, NULL, '2021-06-09 20:33:34', '2021-06-09 20:33:34'),
(6, 'abvc', '123123', 'email9@g', NULL, 'abc', 100000, 20000, 0, 120000, NULL, NULL, NULL, NULL, NULL, '5', NULL, NULL, NULL, NULL, NULL, NULL, 20, NULL, NULL, '2021-06-09 20:33:34', '2021-06-09 20:33:34');

-- --------------------------------------------------------

--
-- Table structure for table `order_product`
--

CREATE TABLE `order_product` (
  `id` bigint NOT NULL,
  `order_id` bigint NOT NULL,
  `product_id` bigint NOT NULL,
  `variant` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `qty` int NOT NULL DEFAULT '0',
  `subtotal` double NOT NULL,
  `product_sku_id` bigint NOT NULL DEFAULT '0',
  `delivery_condition_id` int NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `status` enum('1','2') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `category_id` bigint NOT NULL DEFAULT '0',
  `supplier_id` bigint DEFAULT '0',
  `variant` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `sku` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `images` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `id_images` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `description` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `status` enum('1','2','3','4') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL,
  `total_sale` int NOT NULL DEFAULT '0',
  `thumbnail_img` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `thumbnail_img_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `video_provider` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `video_link` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `unit_price` double(20,2) NOT NULL,
  `purchase_price` double(20,2) DEFAULT NULL,
  `variant_product` int DEFAULT '0',
  `attributes` text COLLATE utf8_unicode_ci,
  `choice_options` mediumtext COLLATE utf8_unicode_ci,
  `colors` mediumtext COLLATE utf8_unicode_ci,
  `meta_title` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `meta_description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `meta_img` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `current_stock` int DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `category_id`, `supplier_id`, `variant`, `sku`, `images`, `id_images`, `description`, `status`, `created_at`, `updated_at`, `deleted_at`, `total_sale`, `thumbnail_img`, `thumbnail_img_id`, `video_provider`, `video_link`, `unit_price`, `purchase_price`, `variant_product`, `attributes`, `choice_options`, `colors`, `meta_title`, `meta_description`, `meta_img`, `slug`, `current_stock`) VALUES
(6, 'Kem dưỡng trắng nâng tone CHICA Y CHICO Nude Fantasy Whitening Cream', 1, 1, NULL, NULL, '', NULL, '<p>11111</p>', '1', '2021-09-09 19:29:51', '2021-09-09 19:29:51', NULL, 0, '', '', 'youtube', NULL, 10.00, 10.00, 0, '[]', '[]', '[]', 'Kem dưỡng trắng nâng tone CHICA Y CHICO Nude Fantasy Whitening Cream', '<p>11111</p>', NULL, 'kem-duong-trang-nang-tone-chica-y-chico-nude-fantasy-whitening-cream', 10),
(7, 'Kem dưỡng trắng nâng tone CHICA Y CHICO Nude Fantasy Whitening Cream', 1, 1, NULL, NULL, 'uploads/all/mCOOvg0Xtehk6kufPLZrwRtOje2tb85TgXwLBx91.jpg,uploads/all/TTu4hdUNvqqwrsOlizHlsSpy8pMNgrkp50T2Qhgo.jpg', '16,15', '<p>11111111111111111111</p>', '1', '2021-09-09 19:43:46', '2021-09-09 19:43:46', NULL, 0, 'uploads/all/RUSBPRyLoAQP2ncvzJVAFfVnyyu5cCPywH5akYiV.png', '14', 'youtube', NULL, 10.00, 10.00, 0, '[]', '[]', '[]', 'Kem dưỡng trắng nâng tone CHICA Y CHICO Nude Fantasy Whitening Cream', '<p>11111111111111111111</p>', '14', 'kem-duong-trang-nang-tone-chica-y-chico-nude-fantasy-whitening-cream-2', 10),
(8, 'Kem dưỡng trắng nâng tone CHICA Y CHICO Nude Fantasy Whitening Cream', 1, 1, NULL, NULL, 'uploads/all/HreldpMpglaz1pF2skU66aIRa0XOB3JfmIY90Hhf.png,uploads/all/5CM9gVA8sFJVur80L41jYEz53QVEK9wRlCr10Rlv.png', '6,7', '<p>1111111111111111111</p>', '1', '2021-09-09 19:47:21', '2021-09-09 20:17:11', NULL, 0, 'uploads/all/L37mVULWRqsyaW0OruURxw4mmDV2sAokyFBJ4BoS.png', '2', 'youtube', NULL, 10.00, 10.00, 1, '[\"1\"]', '[{\"attribute_id\":\"1\",\"values\":[\"M\",\"L\"]}]', '[]', 'Kem dưỡng trắng nâng tone CHICA Y CHICO Nude Fantasy Whitening Cream', '1111111111111111111', '2', 'kem-duong-trang-nang-tone-chica-y-chico-nude-fantasy-whitening-cream-4', 10);

-- --------------------------------------------------------

--
-- Table structure for table `product_sku`
--

CREATE TABLE `product_sku` (
  `id` bigint NOT NULL,
  `product_id` bigint NOT NULL,
  `sku` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `weight` int NOT NULL DEFAULT '0',
  `price` double NOT NULL,
  `qty` int NOT NULL,
  `status` enum('1','2') CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '1',
  `sale` int NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `variant` text CHARACTER SET utf8 COLLATE utf8_bin,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `product_sku`
--

INSERT INTO `product_sku` (`id`, `product_id`, `sku`, `title`, `weight`, `price`, `qty`, `status`, `sale`, `created_at`, `updated_at`, `variant`, `deleted_at`) VALUES
(1, 2, 'qaz', NULL, 0, 10000, 10, '1', 0, '2021-09-09 05:02:19', '2021-09-09 05:02:19', 'Amethyst-M', NULL),
(2, 2, 'er', NULL, 0, 10000, 10, '1', 0, '2021-09-09 05:02:19', '2021-09-09 05:02:19', 'Amethyst-L', NULL),
(3, 2, 'rt', NULL, 0, 10000, 10, '1', 0, '2021-09-09 05:02:19', '2021-09-09 05:02:19', 'AntiqueWhite-M', NULL),
(4, 2, 'ty', NULL, 0, 10000, 10, '1', 0, '2021-09-09 05:02:19', '2021-09-09 05:02:19', 'AntiqueWhite-L', NULL),
(5, 3, 'qw', NULL, 0, 20000, 10, '1', 0, '2021-09-10 01:24:07', '2021-09-10 01:24:07', 'M', NULL),
(6, 3, 're', NULL, 0, 20000, 10, '1', 0, '2021-09-10 01:24:07', '2021-09-10 01:24:07', 'L', NULL),
(7, 4, '', NULL, 0, 100000, 20, '1', 0, '2021-09-10 02:23:39', '2021-09-10 02:23:39', NULL, NULL),
(8, 5, '', NULL, 0, 10, 10, '1', 0, '2021-09-10 02:25:18', '2021-09-10 02:25:18', NULL, NULL),
(9, 6, '', NULL, 0, 10, 10, '1', 0, '2021-09-10 02:29:51', '2021-09-10 02:29:51', NULL, NULL),
(10, 7, '', NULL, 0, 10, 10, '1', 0, '2021-09-10 02:43:46', '2021-09-10 02:43:46', NULL, NULL),
(11, 8, '', NULL, 0, 10, 10, '1', 0, '2021-09-10 02:47:21', '2021-09-10 02:47:21', NULL, NULL),
(12, 8, 'M', NULL, 0, 10, 10, '1', 0, '2021-09-10 03:11:42', '2021-09-10 03:11:42', 'M', NULL),
(13, 8, 'L', NULL, 0, 10, 10, '1', 0, '2021-09-10 03:11:42', '2021-09-10 03:11:42', 'L', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint NOT NULL,
  `name` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `permissions` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `permissions`, `created_at`, `updated_at`) VALUES
(1, 'Admin', '[\"Dashboard\",\"Users\",\"Orders\",\"Suppliers\",\"Products\",\"Categories\",\"Collections\",\"Banners\",\"Notifications\",\"Messages\",\"Members\",\"Settings\"]', '2018-10-10 04:39:47', '2018-10-10 04:51:37'),
(2, 'Accountant', '[\"Dashboard\",\"Users\",\"Collections\",\"Banners\"]', '2018-10-10 04:52:09', '2021-03-24 01:28:38');

-- --------------------------------------------------------

--
-- Table structure for table `searches`
--

CREATE TABLE `searches` (
  `id` int NOT NULL,
  `query` varchar(1000) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `count` int NOT NULL DEFAULT '1',
  `user_id` bigint UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int NOT NULL,
  `type` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `value` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `type`, `value`, `created_at`, `updated_at`) VALUES
(37, 'email_verification', '0', '2019-04-30 07:30:07', '2019-04-30 07:30:07'),
(40, 'current_version', '3.5', '2019-06-11 09:46:18', '2019-06-11 09:46:18'),
(64, 'term_service', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '2021-03-09 01:59:18', '2021-03-09 01:59:18'),
(65, 'policy_service', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '2021-03-09 01:59:18', '2021-03-09 01:59:18'),
(66, 'thank_you_order', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '2021-03-09 01:59:18', '2021-03-09 01:59:18'),
(67, 'company_infor', '{\"address\":\"123\",\"business_number\":\"123\",\"email\":\"business@gmail.com\",\"hotline\":\"123\",\"description\":\"123\"}', '2021-04-28 20:49:19', '2021-05-18 03:47:49'),
(68, 'company_name', 'VITPR', '2021-05-17 19:04:08', '2021-05-17 19:04:08'),
(69, 'company_logo', '14', '2021-05-17 19:04:08', '2021-05-17 19:04:08'),
(70, 'company_delivery_time', '1-3 days', '2021-05-17 19:04:08', '2021-05-17 19:04:08'),
(71, 'fixed_price', '1000', '2021-06-09 21:59:09', '2021-06-09 21:59:09');

-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

CREATE TABLE `suppliers` (
  `id` bigint NOT NULL,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `location` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `representative` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `status` enum('1','2') CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '1',
  `images` text CHARACTER SET utf8 COLLATE utf8_bin,
  `id_images` text CHARACTER SET utf8 COLLATE utf8_bin,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `suppliers`
--

INSERT INTO `suppliers` (`id`, `name`, `address`, `location`, `email`, `phone`, `representative`, `status`, `images`, `id_images`, `created_at`, `updated_at`) VALUES
(1, 'Suppliers 1', 'Hue', 'Hue', 'suppliers1@gmail.com', '123123', 'suppliers', '1', 'uploads/all/TTu4hdUNvqqwrsOlizHlsSpy8pMNgrkp50T2Qhgo.jpg,uploads/all/RUSBPRyLoAQP2ncvzJVAFfVnyyu5cCPywH5akYiV.png,uploads/all/HreldpMpglaz1pF2skU66aIRa0XOB3JfmIY90Hhf.png', '15,14,6', '2021-03-12 10:00:16', '2021-03-23 02:28:06');

-- --------------------------------------------------------

--
-- Table structure for table `team_members`
--

CREATE TABLE `team_members` (
  `id` bigint NOT NULL,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `birthday` date DEFAULT NULL,
  `phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `id_number` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `status` enum('1','2','3') CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '1',
  `date_of_issue` date NOT NULL,
  `role_id` bigint DEFAULT '0',
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `team_members`
--

INSERT INTO `team_members` (`id`, `name`, `birthday`, `phone`, `password`, `email`, `address`, `id_number`, `status`, `date_of_issue`, `role_id`, `avatar`, `created_at`, `updated_at`) VALUES
(1, 'Admin', NULL, '123123', '$2y$10$McNZLabHHVBYNY1pF1JS5enDAMUF3sC0yEl52RF8cfZ0OLxe3Tfx.', 'admin@gmail.com', 'Hue', '123123', '1', '2020-02-20', 1, NULL, '2021-03-05 14:25:56', '2021-03-05 14:25:59');

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` bigint NOT NULL,
  `user_id` bigint UNSIGNED NOT NULL,
  `order_id` bigint NOT NULL,
  `money` double NOT NULL,
  `method` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `status` enum('1','2') CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '1',
  `transaction_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `merchant_uid` text CHARACTER SET utf8 COLLATE utf8_bin,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`id`, `user_id`, `order_id`, `money`, `method`, `status`, `transaction_id`, `merchant_uid`, `created_at`, `updated_at`) VALUES
(2, 20, 3, 120000, NULL, '1', NULL, NULL, '2021-04-28 10:15:28', '2021-04-28 10:15:28'),
(3, 20, 4, 120000, NULL, '1', NULL, NULL, '2021-04-28 10:18:29', '2021-04-28 10:18:29'),
(4, 20, 5, 120000, NULL, '1', '123', NULL, '2021-04-29 10:53:39', '2021-04-29 10:53:39'),
(5, 20, 6, 120000, NULL, '1', '123', NULL, '2021-05-04 01:05:08', '2021-05-04 01:05:08');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint UNSIGNED NOT NULL,
  `displayname` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `fullname` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `birthday` date DEFAULT NULL,
  `gender` enum('1','2') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `verify_phone` enum('0','1') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `code_phone` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` enum('1','2','3') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '2',
  `business_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `tax_invoice` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `representative` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `store_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `store_address` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `store_detailed_address` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `post_code` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `city_id` int NOT NULL DEFAULT '0',
  `district_id` int NOT NULL DEFAULT '0',
  `ward_id` int NOT NULL DEFAULT '0',
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `document` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `document_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `kakao_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `naver_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `displayname`, `email`, `password`, `remember_token`, `fullname`, `phone`, `birthday`, `gender`, `verify_phone`, `code_phone`, `status`, `business_name`, `tax_invoice`, `representative`, `store_name`, `store_address`, `store_detailed_address`, `post_code`, `city_id`, `district_id`, `ward_id`, `avatar`, `document`, `document_id`, `kakao_id`, `naver_id`, `created_at`, `updated_at`) VALUES
(10, 'Ji Hyun', 'email1@gmail.com', '$2y$10$/TNXE6.soT03Wv/nfrpk7utRFjIR9XsE03fLA0XeYhO7Ez8GCPCWa', NULL, 'Kim Ji Hyun', 'gfdhhj', '1994-01-12', '2', '1', NULL, '1', 'Blueberry', '123123', '123123', 'Blueberry Store', '18 Le Loi st', NULL, NULL, 0, 0, 0, 'uploads/avatar/oJmdnqsT1Zcb0UhKbJI1m4nxQ1t1IpUN7hyFXMdd.jpg', NULL, NULL, NULL, NULL, '2021-03-09 01:20:07', '2021-03-14 21:53:55'),
(11, 'user2', 'email2@gmail.com', '$2y$10$je2eW18kTd7vrUXvkvQ/Ae3dL2SWQBusHoM38It6.oKkzxNWrY632', NULL, 'User Test 2', '090929292', '1994-08-02', '2', '1', NULL, '1', 'VITPR', '123123', '123123', 'VITPR', 'Hue', NULL, NULL, 0, 0, 0, 'uploads/avatar/L4spzzWaY88zcCpcL9vZzyqxptKFyGNz7628W5TR.jpg', 'uploads/business_document/Z6gmctkaXleF1bi2jrEEtz4DR8a1tHl7Tx2fcKoO.jpg,uploads/business_document/WMfv9W1saCeqKcLa8dWa0EOX7MlOjiMUztW5x0ef.jpg', '34,35', NULL, NULL, '2021-03-09 01:20:07', '2021-04-29 01:30:25'),
(12, 'user3', 'email3@gmail.com', '$2y$10$BGYJTrJ829A/AnkCbIHgk.INhGLUlTKJIkCRLblQ.raf9MSui.HLW', NULL, 'User Test 3', '090929293', '1994-08-03', '2', '1', NULL, '1', 'Company 3', '090929293', 'representative', 'Store name3', 'Address 3', NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, '2021-03-09 01:20:07', '2021-03-09 01:20:07'),
(13, 'user4', 'email4@gmail.com', '$2y$10$CDSAI95TFHLkRhKSFJk1ceQUhvoYuvfM/gfN1XFx1wyQD1lYzyxku', NULL, 'User Test 4', '090929294', '1994-08-04', '2', '1', NULL, '1', 'Company 4', '090929294', 'representative', 'Store name4', 'Address 4', NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, '2021-03-09 01:20:07', '2021-03-09 01:20:07'),
(14, 'user5', 'email5@gmail.com', '$2y$10$cV.tPE.ihUZvUIrctB9wlunwMe1kAzhZdHL82s2IIkwofQrehIBBW', NULL, 'User Test 5', '090929295', '1994-08-05', '2', '1', NULL, '1', 'Company 5', '090929295', 'representative', 'Store name5', 'Address 5', NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, '2021-03-09 01:20:07', '2021-03-09 01:20:07'),
(15, 'user6', 'email6@gmail.com', '$2y$10$mk6vX4FkP7R9XgMKs74Q..aPiaNfytAE72Ed/H9R.oB8A5sBYXgMu', NULL, 'User Test 6', '090929296', '1994-08-06', '1', '1', NULL, '1', 'Company 6', '090929296', 'representative', 'Store name6', 'Address 6', NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, '2021-03-09 01:20:07', '2021-03-09 01:20:07'),
(16, 'user7', 'email7@gmail.com', '$2y$10$qSvlDBd8WroULqv7HfZ7hOC8SsTHNf9a751Og8hDMP6IBV5KhFEzG', NULL, 'User Test 7', '090929297', '1994-08-07', '1', '1', NULL, '1', 'Company 7', '090929297', 'representative', 'Store name7', 'Address 7', NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, '2021-03-09 01:20:07', '2021-03-09 01:20:07'),
(17, 'user8', 'email8@gmail.com', '$2y$10$4Lep22RdZhQmkKNB488w4O2HoktjcriRSb4i.u7f2wtiE3aq2sv22', NULL, 'User Test 8', '090929298', '1994-08-08', '1', '1', NULL, '1', 'Company 8', '090929298', 'representative', 'Store name8', 'Address 8', NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, '2021-03-09 01:20:07', '2021-03-09 01:20:07'),
(18, 'user9', 'email9@gmail.com', '$2y$10$LPAxOpuK5F0zwOOoWcijWO3CwGdIz/Fnnh17iPJ05mec8EhvqD.b2', NULL, 'User Test 9', '090929299', '1994-08-09', '2', '1', NULL, '1', 'Company 9', '090929299', 'representative', 'Store name9', 'Address 9', NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, '2021-03-09 01:20:07', '2021-03-09 01:20:07'),
(19, 'Test', 'dev1test2019@gmail.com', NULL, NULL, 'Le Van Test', '123123', '1994-01-12', '1', '0', NULL, '1', 'VITPR', '123123', '123123', 'VITPR', 'Huesss', NULL, NULL, 0, 0, 0, 'uploads/avatar/YTmGqXWQNMcusjHDvRHY3lc2qLQWCvP8Bc5ecpYm.png', NULL, NULL, '1111', NULL, '2021-03-10 02:12:03', '2021-03-10 20:12:28'),
(20, 'Quyết Thắng', 'thang13513110061@gmail.com', '$2y$10$Avg3IwMOUS/S5NzphMGsGOF1/wytOvzmbcdAz99e0WrhkxyAWUrIS', NULL, 'ndnđndj', '03938395043247665645', '2017-04-01', '2', '1', NULL, '1', 'Xoai', '22', 'dia', 'onetwo', '23 Lê lợi', NULL, NULL, 0, 0, 0, 'uploads/avatar/x7A5uNKkdXcVzoUytVF4pt912uFR0VUozPxlCVx9.jpg', ',uploads/business_document/OxUVgqShT6G04uHLcGolBWRIx7riGahZWJrfe6cu.jpg', ',95', NULL, NULL, '2021-03-14 20:11:41', '2021-04-29 03:08:15'),
(21, 'Test', 'tanle1223@gmail.com', '$2y$10$Yrrb8cgWOcFmCNAaKxGJSOYfXD4oQabomDvJCTsZg417blxgsHSba', NULL, NULL, '090867882', NULL, '1', '1', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, '2021-03-14 20:48:45', '2021-03-14 20:48:45'),
(22, 'Test', 'test1app2020@gmail.com', '$2y$10$h8LNp9y2ZhDSS17pxZyGSupmGcJNgfwKIt54exocDUhLaDTQ36acm', NULL, NULL, '03442317777', NULL, '1', '1', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, '2021-03-14 20:51:54', '2021-03-14 20:51:54'),
(23, 'Chen Chen', 'test2app2020@gmail.com', '$2y$10$4a6efNypRpBzkZzISYvvWOkxJmtxT4y1MhZdpnbaaM6qYxRD/oHAG', NULL, NULL, '0344231750', NULL, '1', '1', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, '2021-03-14 21:04:48', '2021-03-14 21:04:48'),
(24, 'Dev 2 Test 2019', 'dev2test2019@gmail.com', '$2y$10$Pq8.Ss/Iz6GYOaqnfSvwyOxz0Ju6nR7q4broj8xBLzldueuXlbI3q', NULL, NULL, '0919185542', NULL, '1', '1', NULL, '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, '2021-03-14 21:31:44', '2021-03-25 02:48:30'),
(25, 'Test', 'tanle122773@gmail.com', '$2y$10$AgxeskgsO3jsk9vQin0xJenpqzI33uTM8eR3/WcgEEpTXCxqlpjVC', NULL, NULL, 'kgghh', NULL, '1', '1', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, '2021-03-14 21:36:25', '2021-03-14 21:36:25'),
(26, 'tetting@gmail.com', 'tetting123@gmail.com', '$2y$10$Z6Sk8NyPn3DA3DqloGVYwevTUm2FsYAdUtodRNkIpd8xrbRbo4vgG', NULL, NULL, '0852741963', NULL, '1', '1', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, '2021-03-16 20:01:30', '2021-03-16 20:01:30'),
(27, 'Rosie', 'dicbicr7@gmail.com', '$2y$10$ay2yeCDtwoEp1l5hX3SMZ./KJfxoDPychMZpo0LmnSOyES8S3RjPu', NULL, NULL, '0967830396', NULL, '1', '1', NULL, '1', 'Rosie Fruit', '345667', '0967830396', 'Rosie Fruit store', '18 Le loi st', NULL, NULL, 0, 0, 0, 'uploads/avatar/zludm4JES7CU42apsMyBRz7Icbh6DxoskzubGvtx.jpg', NULL, NULL, NULL, NULL, '2021-03-16 20:21:26', '2021-03-16 20:57:55'),
(31, NULL, 'babe@gmail.com', '$2y$10$AnvIR0jcliC/2VD14jTFw.XBWCti2Zjy5oyKL2rVspiHpRhDHfXPy', NULL, NULL, '', NULL, '1', '1', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, '2021-03-17 03:09:03', '2021-03-20 05:30:52'),
(32, 'hnnnnn', 'snsnsns@gmail.com', '$2y$10$1iVYfy0rGfl9S.hZp5U0m.HN6wFGVl8g9DbeHE7lGPYORaojNIU9K', NULL, NULL, '0741852963', NULL, '1', '1', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, '2021-03-17 19:58:46', '2021-03-21 23:55:19'),
(33, 'banana', 'banana@gmail.com', '$2y$10$I4jmSwyTR3wyGj8OjFVwm.a5MzR457UkPLSjUrfoSYI1DMp/HbG6a', NULL, NULL, '0741236589', NULL, '1', '1', NULL, '2', 'mr lee min ho', 'mr lee min ho', 'cch12345', 'very much', '123 namsang seoul', NULL, NULL, 0, 0, 0, 'uploads/avatar/jcLe6VNqQ6lCvWIjx7GT3UelBwG2Ib6wTYJiR4Cg.jpg', NULL, NULL, NULL, NULL, '2021-03-17 20:08:35', '2021-03-20 05:27:28'),
(34, 'ffff', 'fff@gmail.com', '$2y$10$UsFzM2LKh9ot0nqIUosFy.k7eHckhDkpeQdsKhBil1PH1ubCKb6h.', NULL, NULL, '0963541782', NULL, '1', '1', NULL, '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, '2021-03-21 18:40:14', '2021-03-22 21:46:37'),
(35, 'minhle', 'minhlebbq@gmail.com', '$2y$10$PgWFXY9tl0hvh9/BMd5.5u0j.CzzbFicr2O1ecD8S/Nv3n9WrIL7y', NULL, 'Nguyen Thi Minh Le', '0934983990', '1997-10-24', '2', '1', NULL, '1', 'coa cai nay khong ta', '1234567890', 'Xuong Ca Store', 'Xuong Ca', '18 LeLoi', NULL, NULL, 0, 0, 0, 'uploads/avatar/3asRSpFv9nFCGnFf1SKBXhXctqiskZQR8KEGjxQs.jpg', 'uploads/business_document/hOPuH5i3fm9yXJjsWU19rsku2gBloxhDUhZcPMCU.jpg,uploads/business_document/ZBKc2CKeNqZVEvFjWroyAGb6lG5bT5iKb2UAyn4U.jpg,uploads/business_document/xiPQaz0rJ5jfOP9wSj3bazdUQVSX2YgmKCIpqyqv.jpg,uploads/business_document/4TiFgXmNLiy3XFy0vGlYwWNPDbzkw1DTRRPIqufX.jpg', '89,97,175,297', NULL, NULL, '2021-03-22 00:39:52', '2021-04-28 00:36:51'),
(36, 'abcdef123', NULL, '$2y$10$Vff50DuEYJHZbU4Unw4PuuRhy4wRS7r7yNIRwYFCJM5r.w6qYKc5G', NULL, '213123', '0835269741', NULL, '1', '1', NULL, '2', 'fqas', 'fgghhh', 'gghbb123', NULL, NULL, NULL, NULL, 0, 0, 0, 'uploads/avatar/dXjhBNCOyhUjkb6oPUicuPY7YXU1JBVgvhltFOo4.jpg', 'uploads/all/4uD1E1B8D0WJrTSCny6ogAeJKjd8FY6cLsv39OpJ.png', '4', NULL, NULL, '2021-03-22 00:46:11', '2021-03-23 19:02:32'),
(37, '멍청 Noo', 'noo@gmail.com', '$2y$10$6vjCpF3mDcMFt8r2hlr5cO00AjJ3KAmU8j38Q1Ge.HWHQJszCuDAe', NULL, NULL, '090867312', NULL, '1', '1', NULL, '1', '하하', '1234567888', 'Free', '하하', '회', NULL, NULL, 0, 0, 0, 'uploads/avatar/vZ6Pou2yXkb8HbC4pUbWj1wbU9ciD8bJl97503Ae.jpg', 'uploads/business_document/ACRxExgS3LjSlpR92XJkUMwTRSqpfYNVAeKvAcZj.jpg,uploads/business_document/HqF8oAqwDtmBc5FbSjqvKCRY675TMDqTM5z6vXLK.jpg,uploads/business_document/aIFKlJxtv4A5rbx9tPiJX7DCYjiUBt91EUb9uHzD.jpg', '71,72,90', NULL, NULL, '2021-03-23 19:17:12', '2021-04-20 03:25:11'),
(38, 'Ghanaian', 'test1@gmail.com', '$2y$10$CBKs8VuH30Gyxt1Uw9NxbuL8T2aSh2.ZU8CxnH3B28QgEuxbZUN72', NULL, 'test 2021 04 08', '03896817400', '1980-02-08', '2', '1', NULL, '1', 'DisFruit1223', '852963', 'disname 124', 'DisF123', '123 at home', NULL, NULL, 0, 0, 0, 'uploads/avatar/SeBd4KrQcANkfGACfSNVxz7YmWZE8EppVa9PQb7m.jpg', 'uploads/all/HreldpMpglaz1pF2skU66aIRa0XOB3JfmIY90Hhf.png,uploads/all/L37mVULWRqsyaW0OruURxw4mmDV2sAokyFBJ4BoS.png,uploads/all/4uD1E1B8D0WJrTSCny6ogAeJKjd8FY6cLsv39OpJ.png', '6,2,4', NULL, NULL, '2021-03-23 23:59:58', '2021-04-08 21:24:38'),
(40, 'no.123', 'no1@gmail.com', '$2y$10$yrCyInHDqxjQURrFpHvkTuGDaIF6Ki1IEDXpnFft6xWDSGaQMCqAa', NULL, 'nobody', '09638541278', NULL, '1', '1', NULL, '1', 'NoF', 'oneNoFruit', 'one', 'NoFruit', '23 trung nu vuong', NULL, NULL, 0, 0, 0, NULL, 'uploads/all/L37mVULWRqsyaW0OruURxw4mmDV2sAokyFBJ4BoS.png,uploads/all/HreldpMpglaz1pF2skU66aIRa0XOB3JfmIY90Hhf.png,uploads/all/7YuyL6Hv9ouIMcxL5jf0WPZC9ncd9c3IwKW8ikzI.png,uploads/all/5CM9gVA8sFJVur80L41jYEz53QVEK9wRlCr10Rlv.png,uploads/all/wCeMnIMQRXBpUr47KpQPDfXss6KiO9IEZxi0qsvE.png', '2,6,3,7,8', NULL, NULL, '2021-03-24 03:43:35', '2021-03-24 21:37:14'),
(41, 'name', 'name@gmail.com', '$2y$10$YE6tcPoYv//Ymr/rJ51jhen7BU2Bdl/KFK1FqSkBdIlIm2WpVUo1q', NULL, NULL, '0985632741', NULL, '1', '1', NULL, '1', 'vodat', 'vodat', '111111', '23333', 'Hue', NULL, NULL, 0, 0, 0, NULL, 'uploads/all/5CM9gVA8sFJVur80L41jYEz53QVEK9wRlCr10Rlv.png,uploads/all/wCeMnIMQRXBpUr47KpQPDfXss6KiO9IEZxi0qsvE.png,uploads/all/oknrbyio2c520s8YsFfcA02THs5edNTyd7HuApaf.png', '7,8,9', NULL, NULL, '2021-03-25 19:36:20', '2021-03-29 23:57:09'),
(42, 'road', 'road@gmail.com', '$2y$10$om/VA3C1b2NbvjB3HQIpOueFy/H0aBSIyKbx5CqJV4V3wJrQGd3dq', NULL, NULL, '0989741253', NULL, '1', '1', NULL, '3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, '2021-03-25 19:39:34', '2021-03-25 20:09:46'),
(43, 'car', 'car@gmail.com', '$2y$10$Wo6ToAkILQFf07cAUh2NguCWwaSMcmmMSOhpg2vWgqLJxBOd0.Qce', NULL, 'moto car', '0258963741', NULL, '1', '1', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, '2021-03-25 20:27:49', '2021-03-25 21:31:07'),
(44, 'one', 'one@gmail.com', '$2y$10$bor3zQs6TpIqb3qKTn7uDeZDuiI7qI64TJ7qcUijw8QZZh377ruAG', NULL, NULL, '0852963742', NULL, '1', '1', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, '2021-03-25 21:34:29', '2021-03-25 21:34:29'),
(45, 'two', 'two@gmail.com', '$2y$10$0hU/a5yqlc/PldWsV0NDR.MItOmzw1Go6tcRFAAdLFSUvFWwrmixq', NULL, 'two hi', '0852741234', NULL, '1', '1', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, '2021-03-25 21:35:03', '2021-03-28 19:48:12'),
(46, 'ba', 'ba@gmail.com', '$2y$10$yJXDKAJfUXpwXjrhoopCTOFWP0rphJ4PqTbbvA5XRpw/P5kSbMYD.', NULL, NULL, '0852963963', NULL, '1', '1', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, '2021-03-25 21:36:27', '2021-03-25 21:36:27'),
(47, 'bon', 'bon@gmail.com', '$2y$10$x1T9C7dYOP9w47ec9f99luuUA8B4uxdj39Dmezoi2Nsk8kCGrVA5O', NULL, NULL, '0852852852', NULL, '1', '1', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, '2021-03-25 21:37:13', '2021-03-25 21:37:13'),
(48, 'Saint', 'test10app2020@gmail.com', '$2y$10$hIVhSSELeI.fPmTS3MV5ju/R2lQqozacWHQ5oG4QCMINkMCZ.ZmWS', NULL, 'Saint Laurent', '09678303096', '2021-03-31', '2', '1', NULL, '1', 'Celine', '57676977', 'CELINE', 'bxnxnnxnx', '18 Le Loi', NULL, NULL, 0, 0, 0, 'uploads/avatar/wS7AdyLcr4eXatExfJ6t5K5xSkkeQcOLzFxm8rmR.jpg', ',uploads/business_document/3ZUdo122vgBzdBoPBlpGFRgXCtsxP6xmn04WDeGp.jpg', ',91', NULL, NULL, '2021-03-29 01:37:14', '2021-04-01 02:43:46'),
(49, 'Chaeyoung', 'tanle122344@gmail.com', '$2y$10$HuWrIDHHCSF.myORvYvZIelBK.CGwY1arEAtTpMOdCBcH6U50wSFa', NULL, 'Chaeyoung Par', '65677756', NULL, '1', '1', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, '2021-03-29 07:34:41', '2021-04-06 01:49:19'),
(50, 'red', 'red@gmail.com', '$2y$10$4jwol2OX7zZyk0caAs4YEOwbEbF2quTqO5lF9M4gY1yQgnFGeSNCm', NULL, 'red loichod', '0852147369', NULL, '1', '1', NULL, '3', '234', '234234234235235', '111111', '2ds', 'sdggdfg', NULL, NULL, 0, 0, 0, NULL, '', '', NULL, NULL, '2021-03-30 23:38:20', '2021-04-19 23:50:35'),
(51, 'By Riu', 'lndieu2508@gmail.com', '$2y$10$vZdlVQgmU8B/.7RYsEgLVONgBeenASHfmEFr4h7iOSlLhvkkkM81e', NULL, 'Riu Riu', '0868247215', '2021-04-29', '1', '1', NULL, '1', 'minhle', '12345678888', 'Free', 'MinhLe', 'Hue', NULL, NULL, 0, 0, 0, 'uploads/avatar/1cNNsTlKuNJwp3rLQEV3touFGlimLez1qXiugNCL.jpg', ',uploads/business_document/c4lxrDKZCSj9EiKC3M1Kah7xsvF91lIjDOwo7yGn.jpg', ',115', NULL, NULL, '2021-03-31 02:35:41', '2021-04-29 03:22:51'),
(53, 'Kim Kim', 'test4app2020@gmail.com', '$2y$10$Mtrxj6bWBzfx5JUIYI8jvucemE4iThkhFuv11TriWHqc3yFC9ZOIS', NULL, NULL, '03124879089', NULL, '1', '1', NULL, '1', 'business name', '18789kk', 'vkkkv', 'representuce', '18 le loi', NULL, NULL, 0, 0, 0, 'uploads/avatar/yFRka7z3Co5NPkSbsuPRrLusADoK5QDHqrqqqRtl.jpg', NULL, NULL, NULL, NULL, '2021-03-31 02:49:22', '2021-03-31 03:20:38'),
(55, 'test 1st', 'test1o1@gmail.com', '$2y$10$DR6fP5Op8t8Ffa9nZxJWoub51tGup.xMG86DBEbwP2SaY74z2/SmK', NULL, 'full name', '0399027861699', '2021-03-31', '2', '1', NULL, '2', 'test 1o1', '737494o', '6383o3o', 'test tesst', '18 le loi street', NULL, NULL, 0, 0, 0, 'uploads/avatar/8CPbEUm4eyaOmAACVd67VQPsQQ99vITQFXYzzvtY.jpg', NULL, NULL, NULL, NULL, '2021-03-31 02:56:19', '2021-04-06 20:58:14'),
(56, 'test1712', 'test1712@gmail.com', '$2y$10$ly68WQLgxnWbWt5q3wqnr.vL6mzfENdxJzUqVnrs3..cdrjfFJCZa', NULL, NULL, '01666818149', NULL, '1', '1', NULL, '1', 'Test1712', '123123', '123123', 'VITPR', 'Hue', NULL, NULL, 0, 0, 0, 'uploads/avatar/sVGnKETYEHp51HtnqNyhfqhhvXKwQTVYGwEc9G4F.png', NULL, NULL, NULL, NULL, '2021-03-31 18:18:04', '2021-03-31 19:09:11'),
(57, 'test2410', 'test2410@gmail.com', '$2y$10$RZ91TEFbMe9.wI366uSBWe/rkDubFf.9.RapqSrh9DstWAeaV6GDG', NULL, NULL, '0366818149', NULL, '1', '1', NULL, '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, '2021-03-31 18:37:50', '2021-03-31 21:40:48'),
(58, 'Test', 'tanle122333@gmail.com', '$2y$10$1fy9U1Gz4ZcHCgHF/iYwX.S9t2hSIkpSzxtPMABLVudjfABQwtSLG', NULL, 'beola baba', '0908678824324', NULL, '1', '1', NULL, '1', 'NoF12333', 'RoF123', 'two', 'RoF123', '45 NGO GIA TU', NULL, NULL, 0, 0, 0, NULL, 'uploads/all/HreldpMpglaz1pF2skU66aIRa0XOB3JfmIY90Hhf.png,uploads/all/5CM9gVA8sFJVur80L41jYEz53QVEK9wRlCr10Rlv.png,uploads/all/wCeMnIMQRXBpUr47KpQPDfXss6KiO9IEZxi0qsvE.png,uploads/all/oknrbyio2c520s8YsFfcA02THs5edNTyd7HuApaf.png', '6,7,8,9', NULL, NULL, '2021-04-01 01:29:51', '2021-04-01 01:34:57'),
(59, 'test1712', 'test241007@gmail.com', '$2y$10$P.C06CyyUIj4k8GjmgNZUuAWN0G8/zCsI4DdhKSng.3qxhxUnApW2', NULL, 'beola baba', '0781234567', NULL, '1', '1', NULL, '1', 'minhle', 'minhle', 'minhle', 'minhle', 'minhle', NULL, NULL, 0, 0, 0, 'uploads/avatar/cmJ2yje2mNpfVZMD1fBE3tLeC8uEmeUd53bQYA42.jpg', 'uploads/all/AftL2frFjVRWaFOpzamyjtKZc8L9hgkne5jkYWCK.jpg', '99', NULL, NULL, '2021-04-01 01:43:07', '2021-04-06 19:20:32'),
(60, 'test17121', 'test2410071@gmail.com', '$2y$10$lMQcGCzbn8y8MLuOqUxv/ejs75n.ZCeghApmCWp8nf30It2P.P.iy', NULL, NULL, '07812345671', NULL, '1', '1', NULL, '1', 'ád', 'đá', 'ads', 'ád', 'ád', NULL, NULL, 0, 0, 0, NULL, 'uploads/all/5SjUtCX5FInVcXeQqGXIv68J1rygq6nFEf3bttBr.jpg', '100', NULL, NULL, '2021-04-01 01:46:34', '2021-04-15 03:02:53'),
(61, 'Quyet Thang', 'thang1351311006112@gmail.com', '$2y$10$91JSD9WGNhrjjKM92sYMW.f5WmQF061vDKuDBCSUhPKWZSrezt33m', NULL, 'Quyet Thang Hoang 1', '0393839501', NULL, '1', '1', NULL, '1', 'Quyet Thang', '5545657676', 'QTH 1', 'QT store', '18 Kang Nam', NULL, NULL, 0, 0, 0, NULL, 'uploads/all/5SjUtCX5FInVcXeQqGXIv68J1rygq6nFEf3bttBr.jpg,uploads/all/AftL2frFjVRWaFOpzamyjtKZc8L9hgkne5jkYWCK.jpg', '100,99', NULL, NULL, '2021-04-01 02:58:31', '2021-04-08 04:16:47'),
(62, 'Chaeyoung', 'rosie@gmail.com', NULL, NULL, 'Park Chae Young', '0355682657', NULL, '1', '0', NULL, '3', 'Rose Rosie', '1231234123', 'Rosie Park', '23435556', '18 Kang Nam, Seoul', NULL, NULL, 0, 0, 0, NULL, '', '', '1690528980', NULL, '2021-04-08 04:22:38', '2021-04-20 08:39:57'),
(63, 'beri', 'nnkchau2021@gmail.com', '$2y$10$uwshqSZXOA7GUdF5cVv.POczggL/sLMsrjnT0c0u.jV6NZ4fPAQY.', NULL, 'nnkchau', '0935072115', NULL, '1', '1', NULL, '3', 'lambam', '123ert', '132564789', 'LaBa', '18 Lê Lợi', NULL, NULL, 0, 0, 0, 'uploads/avatar/wcpCAt0DT8MD9BKq1awHcpOMoQh8Org8CTbQSrzP.jpg', '', '', NULL, NULL, '2021-04-16 01:16:55', '2021-05-03 20:03:53'),
(64, 'alada', 'alada@gmail.com', '$2y$10$30sVkCtFoFdg79WT3wzTkOAydPXZTQjsbKUyaIgeErlhhGP262x0K', NULL, 'sfgg', '0389681740', NULL, '2', '1', NULL, '1', 'ALADA', 'alada', '1234508677', 'alada1234', '123 alada hada bama', NULL, NULL, 0, 0, 0, 'uploads/avatar/aivGHMyhI3ofaXu4xVnEQtyNhpcTDnKQCoUzSBdc.jpg', 'uploads/all/5SjUtCX5FInVcXeQqGXIv68J1rygq6nFEf3bttBr.jpg', '100', NULL, NULL, '2021-04-19 18:25:07', '2021-04-22 20:38:50'),
(65, 'Hehe', 'test100app2020@gmail.com', '$2y$10$aDtV9VDu3fbVb7pnRu7UU.tGFezElv5ARNJkafpIKGd/2uj6kSdNy', NULL, 'aa', '0794549323', NULL, '1', '1', NULL, '3', 'sá', 'a', 'âsa', 'sâsấ', 'sa', NULL, NULL, 0, 0, 0, NULL, '', '', NULL, NULL, '2021-04-22 21:06:05', '2021-05-03 20:38:17');

-- --------------------------------------------------------

--
-- Table structure for table `user_device_fcms`
--

CREATE TABLE `user_device_fcms` (
  `id` bigint UNSIGNED NOT NULL,
  `user_id` bigint UNSIGNED NOT NULL,
  `token` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `device` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'ios/android/website',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_payments`
--

CREATE TABLE `user_payments` (
  `id` bigint NOT NULL,
  `user_id` bigint UNSIGNED NOT NULL,
  `card_number` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `card_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `expiry_date` varchar(6) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `ccv` varchar(6) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `type` enum('1','0') CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `wards`
--

CREATE TABLE `wards` (
  `id` int NOT NULL,
  `district_id` int NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `wards`
--

INSERT INTO `wards` (`id`, `district_id`, `name`, `created_at`, `updated_at`) VALUES
(1, 1, 'Yongsan-gu 1', '2021-04-29 09:12:27', '2021-04-29 09:12:27'),
(2, 2, 'Yongsan-gu 2', '2021-04-29 09:12:27', '2021-04-29 09:12:27'),
(3, 3, 'Yongsan-gu 3', '2021-04-29 09:12:27', '2021-04-29 09:12:27'),
(4, 4, 'Yongsan-gu 4', '2021-04-29 09:12:27', '2021-04-29 09:12:27'),
(5, 5, 'Yongsan-gu 5', '2021-04-29 09:12:27', '2021-04-29 09:12:27'),
(6, 6, 'Yongsan-gu 6', '2021-04-29 09:12:27', '2021-04-29 09:12:27');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attributes`
--
ALTER TABLE `attributes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `carts_product_id_foreign` (`product_id`),
  ADD KEY `carts_user_id_foreign` (`user_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `collections`
--
ALTER TABLE `collections`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `collection_products`
--
ALTER TABLE `collection_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `collection_id` (`collection_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `colors`
--
ALTER TABLE `colors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `consignment_products`
--
ALTER TABLE `consignment_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupon_usages`
--
ALTER TABLE `coupon_usages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `delivery_books`
--
ALTER TABLE `delivery_books`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `delivery_condition`
--
ALTER TABLE `delivery_condition`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `delivery_management`
--
ALTER TABLE `delivery_management`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `delivery_management_city`
--
ALTER TABLE `delivery_management_city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `districts`
--
ALTER TABLE `districts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `entity_images`
--
ALTER TABLE `entity_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faqs`
--
ALTER TABLE `faqs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `like_products`
--
ALTER TABLE `like_products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id_product_id` (`user_id`,`product_id`),
  ADD KEY `like_products_ibfk_2` (`product_id`);

--
-- Indexes for table `log_activities`
--
ALTER TABLE `log_activities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notifications_notifiable_type_notifiable_id_index` (`notifiable_type`,`notifiable_id`);

--
-- Indexes for table `notification_admin`
--
ALTER TABLE `notification_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `order_product`
--
ALTER TABLE `order_product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_id` (`order_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `product_sku`
--
ALTER TABLE `product_sku`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `searches`
--
ALTER TABLE `searches`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suppliers`
--
ALTER TABLE `suppliers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `team_members`
--
ALTER TABLE `team_members`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_id` (`role_id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_id` (`order_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_device_fcms`
--
ALTER TABLE `user_device_fcms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_payments`
--
ALTER TABLE `user_payments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `wards`
--
ALTER TABLE `wards`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `attributes`
--
ALTER TABLE `attributes`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `carts`
--
ALTER TABLE `carts`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `collections`
--
ALTER TABLE `collections`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `collection_products`
--
ALTER TABLE `collection_products`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `colors`
--
ALTER TABLE `colors`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=144;

--
-- AUTO_INCREMENT for table `consignment_products`
--
ALTER TABLE `consignment_products`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `coupon_usages`
--
ALTER TABLE `coupon_usages`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `delivery_books`
--
ALTER TABLE `delivery_books`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `delivery_condition`
--
ALTER TABLE `delivery_condition`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `delivery_management`
--
ALTER TABLE `delivery_management`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `delivery_management_city`
--
ALTER TABLE `delivery_management_city`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `districts`
--
ALTER TABLE `districts`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `entity_images`
--
ALTER TABLE `entity_images`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=93;

--
-- AUTO_INCREMENT for table `faqs`
--
ALTER TABLE `faqs`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `files`
--
ALTER TABLE `files`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `like_products`
--
ALTER TABLE `like_products`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `log_activities`
--
ALTER TABLE `log_activities`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `notification_admin`
--
ALTER TABLE `notification_admin`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `order_product`
--
ALTER TABLE `order_product`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `product_sku`
--
ALTER TABLE `product_sku`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `searches`
--
ALTER TABLE `searches`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;

--
-- AUTO_INCREMENT for table `suppliers`
--
ALTER TABLE `suppliers`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `team_members`
--
ALTER TABLE `team_members`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT for table `user_device_fcms`
--
ALTER TABLE `user_device_fcms`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_payments`
--
ALTER TABLE `user_payments`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wards`
--
ALTER TABLE `wards`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `carts`
--
ALTER TABLE `carts`
  ADD CONSTRAINT `carts_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `carts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `collection_products`
--
ALTER TABLE `collection_products`
  ADD CONSTRAINT `collection_products_ibfk_1` FOREIGN KEY (`collection_id`) REFERENCES `collections` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `collection_products_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `consignment_products`
--
ALTER TABLE `consignment_products`
  ADD CONSTRAINT `consignment_products_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `delivery_books`
--
ALTER TABLE `delivery_books`
  ADD CONSTRAINT `delivery_books_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `like_products`
--
ALTER TABLE `like_products`
  ADD CONSTRAINT `like_products_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `like_products_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `log_activities`
--
ALTER TABLE `log_activities`
  ADD CONSTRAINT `log_activities_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `order_product`
--
ALTER TABLE `order_product`
  ADD CONSTRAINT `order_product_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `order_product_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `searches`
--
ALTER TABLE `searches`
  ADD CONSTRAINT `searches_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `team_members`
--
ALTER TABLE `team_members`
  ADD CONSTRAINT `team_members_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE SET NULL ON UPDATE SET NULL;

--
-- Constraints for table `transactions`
--
ALTER TABLE `transactions`
  ADD CONSTRAINT `transactions_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `transactions_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_payments`
--
ALTER TABLE `user_payments`
  ADD CONSTRAINT `user_payments_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
