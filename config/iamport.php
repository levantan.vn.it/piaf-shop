<?php

return [
    'imp_check'    => env('imp_check', false),
    'imp_apikey'    => env('imp_apikey', ''),
    'imp_secret'    => env('imp_secret', '')
];
 ?>
