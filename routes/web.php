<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\TermsController;
use Illuminate\Support\Facades\Route;


Route::namespace('\App\Http\Controllers\Frontend')->group(function (){
    Route::get('/', 'HomeController@index')->name('homepage');

    // my page 
    Route::prefix('my-page')->group(function () {
        Route::get('/', 'MyPageController@index')->name('history_order');
        Route::get('/edit-profile', 'MyPageController@editProfile')->name('my_editprofile');
        Route::put('/edit-profile/{id}','MyPageController@updateProfile')->name('updateProfile');
        // get total orders and point of user
        Route::get('point-orders','MyPageController@countOrderAndPoint');
    });

    Route::prefix('category')->group(function () {
        Route::get('/{id_category}', 'CategoryController@index')->name('get_category');
    });
});
// Route::get('', function () {
//     return redirect()->route('admin.dashboard');
// });

Route::get('webview/address', function () {
    return view('webview.address');
});
Route::get('/policy','PageController@policy')->name('policy.index');
Route::get('/terms','PageController@term')->name('terms.index');


Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

Route::get('/cart', function () {
    // Route::get('/', 'CartController@index')->name('cart_index');
    return view('frontend.carts.index');
});

// my order page

Route::get('/product/{product}', function ($product) {
    return view('frontend.products.show');
});

