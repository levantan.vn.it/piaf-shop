<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'register_success'  =>  'Registration Successful. Please log in to your account.',
    'token_invalid' =>  "Token invalid",
    'token_expired' =>  "Token expired",
    'token_not_found'   =>  "Token not found",
    'unauthorized'  =>  "Email or password not correct",
    'change_password_success'   =>  "Change password success",
    'verify_account'   =>  "Please verify your account",
    'success_logout'    =>  "Successfully logged out",
    'not_register'  =>  "Not register",
    'current_password_not_correct'  =>  "Current password not correct",
    'phone_not_register'    =>  "Phone not yet register",
    'phone_register'    =>  "Phone have been register",
    'user_deactive' =>  "User have been deactive",
    'email_not_register'    =>  "Email not yet register",
    'email_register'    =>  "Email have been register",
    'sns_not_register'    =>  "SNS not yet register",
    'sns_register'    =>  "SNS have been register",
    'update_success'    =>  "Update Successfully",
    'invalid_email_password' =>  "Invalid email or password"
];
