<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'products' => 'PRODUCTS',
    'add' => 'ADD NEW PRODUCT',
    'add-date' => 'Added Date',
    'id' => 'Product ID',
    'title' => 'Product Title',
    'skus' => 'SKUs',
    'var' => 'Variant',
    'vars' => 'Variants',
    'price' => 'Price',
    'lasted' => 'Lasted update',
    'quantity' => 'Quantity',
    'status' => 'Status',
    'in' => 'In-Stock',
    'out' => 'Out-Stock',
    'created' => 'Created Date',
    'pr-img' => 'Product Images',
    'pr-des' => 'Product Description',
    'pr-var' => 'Variant Informations',
    'pr-skus' => 'SKUs Information',
    'sku' => 'SKU',
    'pr-consign' => 'Consignment ID',
    'cate' => 'Category',
    'sup' => 'Supplier',
    'ori' => 'Origin',
    'size' => 'Size',
    "description" => "Description",
    'please_add_product_before_sku' =>  "Please save product before add SKU",
    'update-info-success'=>'Updated product information.',
    'update-sku-success'=>'Updated the product SKU.',
    'delete-success'=>'The product has been removed.',
    'change-status-success'=>'The status of the product has changed.',
    'create-success'=>'Create product success.',
    'description-success'=>'Updated product description',
];
