<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
// supplier-index
    'supplier' => 'SUPPLIERS',
    'supplier_detail' => 'SUPPLIER DETAIL',
    'search-sup' => 'Search Supplier ID / Supplier Name',
    'add'  =>  "ADD NEW SUPPLIERS",
    'create'=> "CREATE SUPPLIER",
    'search'  =>  "SEARCH",
    'reset'  =>  "RESET",
    'excel'  =>  "EXCEL",
    'add-date'  =>  "Added Date",
    'status'  =>  "Status",
    'active'  =>  "Active",
    'inactive'  =>  "InActive",
    'lock'  =>  "Lock",
    'id'  =>  "Supplier ID",
    'name'  =>  "Name",
    'email'  =>  "Email",
    'location'  =>  "Location",
    'products'  =>  "Products",
    'j-date'  =>  "Joined date",
    'view'  =>  "View",
    'del'  =>  "Delete",

     // supplier create

    'product'=>"Products",
    'add-sup-info'=>"Add Supplier Information",
    'add-sup-img'=>"Add Supplier Images",

    "add-sup-id"=>"Enter Supplier ID",
    "add-sup-name"=>"Enter Supplier Name",
    "add-sup-address"=>"Enter Supplier Address",
    "add-sup-loc"=>"Enter Supplier Location",
    "add-sup-email"=>"Enter Supplier Email",
    "add-sup-phone"=>"Enter Supplier Phone Number",
    "add-sup-rep"=>"Enter Supplier Representative",
    "add-sup-date"=>"Choose Supplier Joined date",

    // supplier detail
    'img-sup'  =>  "Supplier Images",
    'sup-name'  =>  "Supplier Name",
    'sup-address'  =>  "Address",
    'sup-phone'  =>  "Phone Number",
    'sup-rep'  =>  "Representative",
    'save' => 'Save',
    'cancel' => 'Cancel',

    //supplier notifications

    'noti_updated' => 'Supplier information has been updated',
    'noti_deleted' => 'Supplier has been deleted',
    'noti_status' => 'Supplier Status has been changed',
    'noti_image' => 'Supplier Images has been updated',

    'noti_name' => 'The name field is required.',
    'noti_address ' => 'The address field is required.',
    'noti_location ' => 'The location field is required.',
    'noti_email1 ' => 'The email field is required.',
    'noti_email2 ' => 'The email must be a valid email address',
    'noti_phone ' => 'The phone field is required.',
    'noti_representative ' => 'The representative field is required.',
    'required-image'=>'The image field is required.',


    'delete'=> 'Do you want to delete this supplier?',
    'ques_inactive'=>'Inactive Supplier?',
    'ques_active'=>'Active Supplier?',
    'continue'=> 'Do you want to continue?',
    'yes'=>'Yes',
    'no'=>'No',

    'deleted'=>'Deleted',
    'delete_false'=>'Delete failed!!'













];
