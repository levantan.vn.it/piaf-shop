<?php

return [
  'make_primary' => 'Failed in Make Primary Payment Method.',
  'add_failed' => 'Failed in Add Payment Method.',
  'update_failed' => 'Failed in update Payment Method.',
  'delete_failed' => 'Failed in Delete Payment Method.'
];
