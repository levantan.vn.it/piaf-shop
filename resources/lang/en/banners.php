<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'banners' => 'BANNERS',
    'search_id' => 'Search Banner ID',
    'add'  =>  "ADD NEW BANNER",
    'search'  =>  "SEARCH",
    'reset'  =>  "RESET",
    'excel'  =>  "EXCEL",
    'state'  =>  "Status",
    'id'  =>  "Banner ID",
    'img'  =>  "Images",
    'pos'  =>  "Position",
    'link'  =>  "Link to",
    'start'  =>  "Start Date",
    'end'  =>  "End Date",
    'status'  =>  "Status",
    'bn_id'  =>  "#BN1234",
    'bn_pos'  =>  "Home",
    'bn_cate' => "Category",
    'bn_link'  =>  "#CLL1234",
    'bn_start'  =>  "2021/02/12",
    'bn_end'  =>  "2021/02/12",
    'bn_status'  =>  "Active",
    'bn_inac'  =>  "In-Active",
    'bn_lock'  =>  "Lock",
    'date'  =>  "Date",
    'detail'  =>  "Detail",
    'del'  =>  "Delete",
    'link2'  =>  "Link to Collection",
    'bn_img'  =>  "Banner Images",
    'winter'  =>  "Winter Sale up to 50%",
    'change-status' => "Change state",
    'do-you-want'   => "Do you want to continue?",
    'yes' => "Yes",
    'no'    => "No",
    'saved' =>"Saved",
    'not-saved'=> "Changes not saved",
    'save'=> "Save",
    'cancel'=>"Cancel",
    'create-banner'=>"Add banner",
    'add-info'=>"Add banner information",
    "add-img"=>"Add banner images",
    'add-position'=>'Add banner position',
    'change-statuss'=>'Do you want to change the status of the banner?',
    'change-status-success'=>'The banner status has changed.',
    'delete-success'=>'The banner has been removed.',
    'collection-success'=>'The colection information has been updated.',
    'delete-banner'=>'Delete a banner?',
    'nothing'=>'Nothing selected',
    'complete'=>'Complete',
    'stand_by'=>'Stand by',
    'on_air'=>'On air',
    'create-success'=>'Create banner success.',
    'update-info'=>'Updated banner information.',
    'update-img'=>'Updated banner image.',

];