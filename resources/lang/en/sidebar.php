
<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'Dashboard' => 'Dashboard',
    'Users' => 'Users',
    'Orders' => 'Orders',
    'Suppliers' => 'Suppliers',
    'Products' => 'Products',
    'Categories' => 'Categories',
    'Collections' => 'Collections',
    'Banners' => 'Banners',
    'Notifications' => 'Notifications',
    'Messages' => 'Messages',
    'Team-Members' => 'Team-Members',
    'Settings' => 'Settings',

    'search-menu'=>'Search menu',
    'email' => 'Email',
    'pass' => 'Password',
    'sign-up' => 'Sign up',
    'forgotPassword' => 'Forgot password?',
    'profile'=>'Profile',
    'logout'=>'Logout',
    'welcome-to'=>'Welcome to',
    'login'=>"Login",
    'uploaded-files'    =>  "Upload file management",
    'Coupon'=>"Coupon",






];

