
<?php

return [

    'select_file' => '파일 선택',
    'add'  =>  "파일 추가",
    'upload_new'  =>  "새 파일 업로드",
    'sort_newest'  =>  " 최신순으로 정렬",
    'sort_oldest'  =>  "오래된 순으로 정렬",
    'sort_smallest'  =>  "작은 순서로 정렬",
    'sort_largest'  =>  "큰 순서로 정렬",
    'selected_only'  =>  "선택된 것만 정렬",
    'search_file'  =>  "파일 검색",
    'files_found'  =>  "파일이 없습니다",
    'file_selected'  =>  "개의 파일 선택됨 ",
    'clear'  =>  "지우기 ",
    'prev'  =>  "이전",
    'next'  =>  "다음",
    'image' => '이미지',

];
