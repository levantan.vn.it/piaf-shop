<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'dashboard' => '대시보드',
    'day' => "일",
    'week' => "주",
    'month' => "달",
    'db_revenue' => "총 수익",
    'db_order' => "총 주문",
    'total'  =>  "합계",
    'price' => '원',
    'success' => '결제성공',
    'sold' => '판매',
    't_product'  =>  "Top 상품",
    't_order'  =>  "Top 주문",
    'latest'  =>  "최근 결제 내역",
    'view'  =>  "모두 보기",
    'active'  =>  "결제성공",
    'del'  =>  "삭제",
    'nodata_trans' => '최근 결제 내역이 없습니다.',
    'nodata_order' => '최근 top 주문 내역이 없습니다.',
    'nodata_product' => '최근 top 상품 내역이 없습니다.',
    'number_revenue' => '수익',
    'number_order' => '주문 수',


];
