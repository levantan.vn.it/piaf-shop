<?php

return [

    // index
    'title' => '업로드 파일 관리',
    'title_create' => '새 파일 업로드',
    'all_upload' => '업로드된 모든 파일',
    'all_file'  =>  "모든 파일 ",
    'upload_new'  =>  "새 파일 업로드",
    'sort_newest'  =>  " 최신순으로 정렬",
    'sort_oldest'  =>  "오래된 순으로 정렬",
    'sort_smallest'  =>  "작은 순서로 정렬",
    'sort_largest'  =>  "큰 순서로 정렬",
    'search_file'  =>  "파일 검색",
    'search'  =>  "검색",

    'detail_info'  =>  "상세 정보 ",
    'down'  =>  "다운로드",
    'copy_link'  =>  "링크 복사",
    'delete'  =>  "삭제",
    'confirm_delete'  =>  "삭제 확인",
    'question_delete'  =>  "이 파일을 삭제하시겠습니까?",
    'cancel'  =>  "취소",
    'file_info'  =>  "파일 정보 ",
    'link_copy'  =>  "클립보드에 복사된 링크.",
    'oops'  =>  "복사할 수 없습니다.",

    // create
    'back'  =>  "업로드된 파일로 돌아가기",
    'drag_file'  =>  "내 컴퓨터 파일을 끌어다 놓기",

    // info
    'file_name'  =>  "파일명",
    'file_type'  =>  "파일 종류",
    'file_size'  =>  "파일 크기",
    'upload_at'  =>  "업로드 일자",

    //notification
    'del_success'  =>  "파일이 삭제되었습니다.",
    'opp_wrong'  =>  "시스템 장애가 발생했습니다!",

    'nothing_found' => '검색된 결과가 없습니다.'

];
