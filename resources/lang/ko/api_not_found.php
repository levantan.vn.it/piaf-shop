<?php

return [
  'Category' => '카테고리를 찾을 수 없습니다.',
  'Collection' => '기획전을 찾을 수 없습니다.',
  'Notification' => '알림을 찾을 수 없습니다.',
  'Order' => '주문 내역을 찾을 수 없습니다.',
  'Product' => '상품을 찾을 수 없습니다.',
  'ProductSKU' => '상품의 SKU를 찾을 수 없습니다.',
  'Supplier' => '브랜드 공급자를 찾을 수 없습니다.',
  'User' => '회원을 찾을 수 없습니다.',
  'UserPayment' => '선택한 결제수단을 찾을 수 없습니다.',
  // Not use
  'Banner' => '배너를 찾을 수 없습니다.', // TODO
  'Cart' => '장바구니가 없습니다.', // TODO
  'CollectionProduct' => '컬렉션 제품을 찾을 수 없습니다.', // TODO
  'ConsignmentProduct' => '위탁 제품을 찾을 수 없습니다.', // TODO
  'DeliveryManagement' => '배달 관리를 찾을 수 없습니다.', // TODO
  'EntityImage' => '엔티티 이미지를 찾을 수 없습니다.', // TODO
  'Faq' => 'FAQ를 찾을 수 없습니다.', // TODO
  'File' => '파일을 찾을 수 없습니다.', // TODO
  'LikeProduct' => '제품을 찾을 수없는 것과 같습니다.', // TODO
  'LogActivity' => '로그 활동을 찾을 수 없습니다.', // TODO
  'Message' => '메시지를 찾을 수 없습니다.', // TODO
  'OrderProduct' => '주문 제품을 찾을 수 없습니다.', // TODO
  'Page' => '페이지를 찾을 수 없습니다.', // TODO
  'Role' => '역할을 찾을 수 없습니다.', // TODO
  'Search' => '검색 할 수 없습니다.', // TODO
  'Setting' => '설정을 찾을 수 없습니다.', // TODO
  'TeamMember' => '팀 구성원을 찾을 수 없습니다.', // TODO
  'Transaction' => '거래를 찾을 수 없습니다.', // TODO
  'UserDeviceFcms' => '사용자 장치 FCM을 찾을 수 없습니다.', // TODO
  'UserSearch' => '사용자 검색을 찾을 수 없습니다.', // TODO
  'DeliveryBook' => '배달 책 찾을 수 없습니다.', // TODO
];
