
<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'banners' => '배너관리',
    'search_id' => '배너 ID',
    'add'  =>  "배너 추가",
    'search'  =>  "검색",
    'reset'  =>  "초기화",
    'excel'  =>  "Excel",
    'state'  =>  "상태",
    'id'  =>  "배너 ID",
    'img'  =>  "이미지 ",
    'pos'  =>  "위치",
    'link'  =>  "연결상품 또는 컬렉션",
    'start'  =>  "시작일",
    'end'  =>  "종료일",
    'status'  =>  "상태",
    'bn_id'  =>  "#BN1234",
    'bn_pos'  =>  "집",
    'bn_cate' => "범주",
    'bn_link'  =>  "#CLL1234",
    'bn_start'  =>  "2021/02/12",
    'bn_end'  =>  "2021/02/12",
    'bn_status'  =>  "액티브",
    'bn_inac'  =>  "비활성",
    'bn_lock'  =>  "자물쇠",
    'date'  =>  "날짜",
    'detail'  =>  "전망",
    'del'  =>  "삭제",
    'link2'  =>  "컬렉션링크",
    'bn_img'  =>  "배너 이미지",
    'winter'  =>  "겨울 세일 최대 50%",
    'change-status' => "상태 변경",
    'do-you-want'   => "계속 진행하시겠습니까?",
    'yes' => "예",
    'no'    => "아니오",
    'saved' =>"저장",
    'not-saved'=> "변경 내용이 저장되지 않음",
    'save'=> "저장",
    'cancel'=>"취소",
    'create-banner'=>"배너 추가",
    'add-info'=>"배너 정보 추가",
    "add-img"=>"배너 이미지 추가",
    'add-position'=>'배너 위치 추가',
    'change-statuss'=>'배너의 상태를 변경하시겠습니까?',
    'change-status-success'=>'배너 상태가 변경되었습니다.',
    'delete-success'=>'배너가 삭제되었습니다.',
    'collection-success'=>'기획전 정보가 업데이트 되었습니다.',
    'delete-banner'=>'배너 삭제?',
    'nothing'=>'선택 안됨',
    'complete'=>'완료',
    'stand_by'=>'스탠바이',
    'on_air'=>'게시중',
    'create-success'=>'배너를 생성하였습니다.',
    'update-info'=>'배너 정보를 업데이트 하였습니다.',
    'update-img'=>'배너 이미지를 업데이트 하였습니다.',
    'banner-detail'=>'배너 관리 상세',
    'requỉred-image'=>'배너 이미지 항목은 필수입력 입니다.',



];
