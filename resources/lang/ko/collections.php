<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
// collection-index
    'collection' => '컬렉션 관리',
    'search-collection' => ' 컬렉션 ID / 컬렉션 타이틀',
    'add'  =>  "컬렉션 추가 ",
    'search'  =>  "검색",
    'reset'  =>  "초기화",
    'excel'  =>  "EXCEL",
    'created-date'  =>  "생성일",
    'status'  =>  "상태",
    'active'  =>  "액티브",
    'inactive'  =>  "비활성",
    'lock'  =>  "자물쇠",
    'id'  =>  "컬렉션 ID",
    'img'  =>  "이미지",
    'title'  =>  "컬렉션 타이틀",
    'section'  =>  "섹션",
    'products'  =>  "상품",
    'view'  =>  "조회",
    'del'  =>  "삭제",
    'sort_descript' => '짧은 설명',
    'start_date' => '시작일',
    'end_date' => '종료일',
    'brand_id' => '관련 브랜드',
    'product_management' => '상품관리',
    'status_change' => '수집 상태 변경?',
    'delete_coll' => '기획전 삭제',
    'main-img'=>'기획전 메인 이미지',


// collection-create
    'create'  =>  "컬렉션 만들기",
    'add-cl-info'  =>  "컬렉션 정보 추가",
    'add-cl-img'  =>  "컬렉션 이미지 추가",
    'add-product'  =>  "제품 추가",
    'select_section'=> "섹션 선택",
    'select_brand' => "브랜드 링크 선택",

// collection information
    'collection_detail' => '수집 정보',
    'img-cl'  =>  "컬렉션 이미지 ",
    'save' => '저장',
    'cancel' => '취소',
    'search-pro' => '검색 상품관리',
    'hot_deal'  =>  '핫딜',
    'special_events'    =>  "스페셜 이벤트",
    'end_season_sale'    =>  "계절 세일",
    'no_pro'=> '제품 없음',
    'no_image' =>'컬렉션 이미지 없음',

//collection notifications

    'noti_create'=>'컬렉션이 생성되었습니다.',
    'msg_infor_success' => "기획전 정보가 업데이트 되었습니다.",
    'msg_add_product' => "상품이 추가되었습니다.",
    'msg_delete_product' => "상품이 삭제되었습니다.",
    'msg_delete_collection' => "기획전이 삭제되었습니다.",
    'msg_status_success' => "기획전 상태가 변경되었습니다.",
    'msg_image_success' => "컬렉션 이미지가 업데이트 되었습니다.",
    'popup_status' => '기획전 상태 변경',
    'confirm_popup' => '계속 진행하시겠습니까?',
    'yes' => '예',
    'no' => '아니오',
    "no_data_product" => "검색된 결과가 없습니다."
];
