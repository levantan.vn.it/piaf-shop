<?php

return [
  'not_qty_in_stock' => '재고가있는 제품이 충분하지 않습니다.',
  'product_unavailable' => '상품을 구매할 수 없습니다.',
  'sold_out' => '이 상품은 품절입니다.'
];
