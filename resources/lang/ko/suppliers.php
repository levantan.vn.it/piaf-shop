<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
// supplier-index
    'supplier' => '브랜드관리',
    'supplier_detail' => '브랜드 상세',
    'search-sup' => ' 브랜드 ID / 브랜드명',
    'add'  =>  "브랜드 추가",
    'create'=> "브랜드 추가",
    'search'  =>  "검색",
    'reset'  =>  "초기화",
    'excel'  =>  "EXCEL",
    'add-date'  =>  "가입일",
    'status'  =>  "상태",
    'active'  =>  "액티브",
    'inactive'  =>  "비활성",
    'lock'  =>  "자물쇠",
    'id'  =>  "브랜드 ID",
    'name'  =>  "브랜드명",
    'email'  =>  "Email",
    'location'  =>  "지역",
    'products'  =>  "상품",
    'j-date'  =>  "가입일",
    'view'  =>  "조회",
    'del'  =>  "삭제",
    'main-img'=>'브랜드 메인 이미지.',


     // supplier create

    'product'=>"상품",
    'add-sup-info'=>"공급 업체 정보 추가",
    'add-sup-img'=>"공급 업체 이미지 추가",


    "add-sup-name"=>"브랜드명을 입력해 주세요.",
    "add-sup-address"=>"주소를 입력해 주세요.",
    "add-sup-loc"=>"지역을 입력해 주세요.",
    "add-sup-email"=>"Email을 입력해 주세요.",
    "add-sup-phone"=>"폰번호를 입력해 주세요.",
    "add-sup-rep"=>"대표명을 입력해 주세요.",


    // supplier detail
    'img-sup'  =>  "브랜드 이미지",
    'sup-name'  =>  "브랜드명",
    'sup-address'  =>  "주소",
    'sup-phone'  =>  "폰번호",
    'sup-rep'  =>  "대표명",
    'save' => '저장',
    'cancel' => '취소',

    //supplier notifications

    'noti_updated' => '공급자의 정보가 변경되었습니다.',
    'noti_deleted' => '공급자가 삭제되었습니다.',
    'noti_status' => '공급자의 상태가 변경되었습니다.',
    'noti_image' => '공급자의 이미지가 변경되었습니다.',

    'noti_name' => '이름 필드는 필수입니다.',
    'noti_address ' => '주소 필드는 필수입니다.',
    'noti_location ' => '위치 필드는 필수입니다.',
    'noti_email1 ' => '이메일 필드는 필수입니다.',
    'noti_email2 ' => '이메일은 유효한 이메일 주소 여야합니다.',
    'noti_phone ' => '전화 필드는 필수입니다.',
    'noti_representative ' => '대표 필드는 필수입니다.',
    'required-image'=>'브랜드 이미지 필드는 필수입니다.',


    'delete'=> '공급 업체를 삭제 하시겠습니까?',
    'ques_inactive'=>'비활성 공급 업체?',
    'ques_active'=>'활성 공급 업체?',
    'continue'=> '계속 하시겠습니까?',
    'yes'=>'예',
    'no'=>'아니오',

    'deleted'=>'삭제',
    'delete_false'=>'삭제 실패!'

];
