<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    // 404
    "page_not_found"=> "페이지를 찾을 수 없습니다!",
    "page_not_found_server"=> "찾으시는 페이지는 저희 서버에 존재하지 않습니다.",

    //419
    "page_expired"=>"페이지의 세션이 만료 되었습니다!",
    "page_reload"=>"페이지를 새로고침 후 다시 시도해 주세요.",
    //500
    "something_wrong"=> "시스템 장애가 발생했습니다! 관리자에게 문의해 주세요.",
    "sorry"=>"불편을 드려 죄송합니다. 현재 서버 작업 중입니다.",

    "error_code"=>"에러 코드",
    //503

    "we_are"=>"지금은 서버 작업 중입니다!",
    "we_will"=>"서버 작업이 완료되는 대로 정상화 될 예정입니다.",

];
