@extends('backend.layouts.app')
@section('title')
@lang('dashboards.dashboard')
@endsection
@section('content')
<div class="aiz-titlebar text-left mt-2 mb-3">
	<div class="align-items-center">
		<h1 class="h3"><strong>@lang('dashboards.dashboard')</strong></h1>
	</div>
</div>
<div class="row gutters-10 chart">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header ">
                <h6 class="mb-0 fs-14">@lang('dashboards.db_revenue')</h6>
                <div class="total">
                    <ul class="nav nav-pills mb-0  fs-14" id="pills-tab" role="tablist">
                        <li class="nav-item">
                        <a class="nav-link active btn-ab" id="pills-day-tab" data-toggle="pill" href="#pills-day" role="tab" aria-controls="pills-day" aria-selected="true">@lang('dashboards.day')</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link btn-ab" id="pills-week-tab" data-toggle="pill" href="#pills-week" role="tab" aria-controls="pills-week" aria-selected="false">@lang('dashboards.week')</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link btn-ab" id="pills-jun-tab" data-toggle="pill" href="#pills-jun" role="tab" aria-controls="pills-jun" aria-selected="false">@lang('dashboards.month')</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="tab-content" id="pills-tabContent">
                <div class="tab-pane fade show active" id="pills-day" role="tabpanel" aria-labelledby="pills-day-tab">
                    <div class="card-body-title">
                        <h6 class="card-title label">{{__('dashboards.total')}}: {{format_price($revenueHourTotal)}} <span>{{__('dashboards.price')}}</span></h6>
                    </div>
                    <div class="card-body">
                        <div class="chart">
                            <canvas id="hour1" class="w-100" height="500"></canvas>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="pills-week" role="tabpanel" aria-labelledby="pills-week-tab">
                    <div class="card-body-title">
                        <h6 class="card-title label">{{__('dashboards.total')}}: {{format_price($revenueWeekTotal)}}<span>{{__('dashboards.price')}}</span></h6>
                    </div>
                    <div class="card-body">
                        <div class="chart">
                            <canvas id="day1" class="w-100" style="height: 5000px"></canvas>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="pills-jun" role="tabpanel" aria-labelledby="pills-jun-tab">
                    <div class="card-body-title">
                        <h6 class="card-title label">{{__('dashboards.total')}}: {{format_price($revenueMonthTotal)}}<span>{{__('dashboards.price')}}</span></h6>
                    </div>
                    <div class="card-body">
                        <div class="chart">
                            <canvas id="month1" class="w-100" height="500"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">
                <h6 class="mb-0 fs-14">@lang('dashboards.db_order')</h6>
                <div class="total">
                    <ul class="nav nav-pills mb-0  fs-14" id="pills-tab2" role="tablist">
                        <li class="nav-item">
                        <a class="nav-link active btn-ab" id="pills-day2-tab" data-toggle="pill" href="#pills-day2" role="tab" aria-controls="pills-day" aria-selected="true">@lang('dashboards.day')</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link btn-ab" id="pills-week2-tab" data-toggle="pill" href="#pills-week2" role="tab" aria-controls="pills-week" aria-selected="false">@lang('dashboards.week')</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link btn-ab" id="pills-jun2-tab" data-toggle="pill" href="#pills-jun2" role="tab" aria-controls="pills-jun" aria-selected="false">@lang('dashboards.month')</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="tab-content" id="pills-tabContent2">
                <div class="tab-pane fade show active" id="pills-day2" role="tabpanel" aria-labelledby="pills-day2-tab">
                    <div class="card-body-title">
                        <h6 class="card-title label">{{__('dashboards.total')}}: {{$orderHourTotal}} </h6>
                    </div>
                    <div class="card-body">
                        <div class="chart">
                            <canvas id="hour2" class="w-100" height="500"></canvas>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="pills-week2" role="tabpanel" aria-labelledby="pills-week2-tab">
                    <div class="card-body-title">
                        <h6 class="card-title label">{{__('dashboards.total')}}: {{$orderWeekToTal}} </h6>
                    </div>
                    <div class="card-body">
                        <div class="chart">
                            <div class="chart">
                                <canvas id="day2" class="w-100" height="500"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="pills-jun2" role="tabpanel" aria-labelledby="pills-jun2-tab">
                    <div class="card-body-title">
                        <h6 class="card-title label">{{__('dashboards.total')}}: {{$orderMonthTotal}} </h6>
                    </div>
                    <div class="card-body">
                        <div class="chart">
                            <canvas id="month2" class="w-100" height="500"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row gutters-10 dashboard">
    <div class="col-md-4">
        <div class="card">
            <div class="card-header">
                <h6 class="mb-0 fs-14"><strong>@lang('dashboards.t_product')</strong></h6>
            </div>
            <div class="card-body table-responsive">
                 <div class="">
                     @if(count($top_products) && !empty($top_products))
                        @foreach($top_products as $top)
                            <div class="d-flex bd-highlight h-70px mb-2">
                                <div class="bd-highlight">
                                    {{-- <img class="img-table mr-2" src="/public/assets/img/verified.png" alt=""> --}}
                                    @if($top->images)
                                    @php
                                        $avatar = explode(',',$top->images);
                                    @endphp
                                    <img src="{{ static_asset($avatar[0]) }}" class="img-table mr-2" alt="Search">
                                    @else
                                    <img src="{{ static_asset('assets/img/placeholder.jpg') }}" class="img-table mr-2" alt="Search">
                                    @endif
                                </div>
                                <div class="bd-highlight dash-flex">
                                    @php
                                        $min_price = (collect($top->skus)->min('price'));
                                    @endphp
                                    <p class="mb-1 text-dark"> <b>#PRO{{optional($top)->id}}</b></p>
                                    <label class="mb-1"> {{optional($top)->name}} </label>
                                    <label class="mb-1"> {{format_price($min_price)}} {{__('dashboards.price')}} </label>
                                </div>
                                <div class="ml-auto pt-2 bd-highlight">
                                    <label class="pt-2 mb-0">{{__('dashboards.sold')}}</label> <br>
                                    <label ><strong>{{optional($top)->total_sale}} </strong></label>
                                </div>
                            </div>
                        @endforeach
                        @else 
                        <div class="nodata">{{__('dashboards.nodata_product')}}</div>
                        @endif
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card">
            <div class="card-header">
                <h6 class="mb-0 fs-14"><strong>@lang('dashboards.t_order')</strong></h6>
            </div>
            <div class="card-body">
                <div class="tab-order">
                    @if(count($top_orders) && !empty($top_orders))
                    @foreach($top_orders as $order)
                        <div class="d-flex bd-highlight h-70px mb-2">
                            <div class="bd-highlight">
                                @if($order->images)
                                @php
                                    $avatar = explode(',',$order->images);
                                @endphp
                                <img src="{{ static_asset($avatar[0]) }}" class="img-ava4 mr-2" alt="Search">
                                @else
                                <img src="{{ static_asset('assets/img/placeholder.jpg') }}" class="img-ava4 mr-2" alt="Search">
                                @endif
                            </div>
                            <div class="bd-highlight dash-flex">
                                <p class="mb-1 text-dark"> <b>#ORD{{optional($order)->id}}</b></p>
                                <label class="mb-1"> {{format_price(optional($order)->total)}}{{__('dashboards.price')}} </label>
                                <label class="mb-1">#USERID{{optional($order)->user_id}} </label>
                            </div>
                        </div>
                    @endforeach 
                    @else 
                    <div class="nodata">{{__('dashboards.nodata_order')}}</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card">
            <div class="card-header">
                <h6 class="mb-0 fs-14"><strong>@lang('dashboards.latest')</strong></h6>
            </div>
            @if(count($top_trans) && !empty($top_trans))
            @foreach($top_trans as $trans)
            <div class="card-body table-responsive d-flex bd-highlight">
                <div class="mr-auto px-2 bd-highlight">
                    {{-- <table>
                        <tr>
                            <th><p class="text-dark mb-1"> #TRANSACTIOND{{optional($trans)->id}} </p> </th>
                        </tr>
                        <tr>
                            <td >{{format_price(optional($trans)->money)}} {{__('dashboards.price')}}</td>
                        </tr>
                    </table> --}}
                    <div class="bd-highlight dash-flex">
                        <p class="mb-1 text-dark"> <b>#TRANSACTIOND{{optional($trans)->id}}</b></p>
                        <label class="mt-1"> {{format_price(optional($trans)->money)}} {{__('dashboards.price')}} </label>
                    </div>
                    
                </div>
                <div class="px-2 bd-highlight">
                    <div class="ml-auto bd-highlight">
                        <label class="pt-0 mb-1">{{date('Y-m-d',strtotime(optional($trans)->created_at))}}</label> <br>
                        <label class="mt-1 mb-0" >{{__('dashboards.success')}}</label>
                    </div>
                    {{-- <table>
                        <tr>
                            <td>{{date('Y-m-d',strtotime(optional($trans)->created_at))}}</td>
                        </tr>
                        <tr>
                            <td class="text-end"><p class="pt-1">{{__('dashboards.success')}}</p></td>
                        </tr>
                    </table> --}}
                </div>
            </div>
            <hr>
            @endforeach
            @if(count($top_trans) >= 5)
            <div class="card-footer text-ft ">
            <a href="{{route('users.transactions')}}">@lang('dashboards.view')</a>
            </div>
            @endif
            @else 
            <div class="nodata">{{__('dashboards.nodata_trans')}}</div>
            @endif
        </div>
    </div>
</div>

@endsection
@section('script')
<script type="text/javascript">
    AIZ.plugins.chart('#hour1',{
        type: 'bar',
        data: {
            labels: {!! $labelsHour !!},
            datasets: [{
                label: "{{__('dashboards.number_revenue')}}",
                data: {!! $dataRevenueHour !!},
                backgroundColor: ['rgba(55, 125, 255, 0.4)','rgba(55, 125, 255, 0.4)','rgba(55, 125, 255, 0.4)','rgba(55, 125, 255, 0.4)',
                'rgba(55, 125, 255, 0.4)','rgba(55, 125, 255, 0.4)','rgba(55, 125, 255, 0.4)','rgba(55, 125, 255, 0.4)',
                'rgba(55, 125, 255, 0.4)','rgba(55, 125, 255, 0.4)','rgba(55, 125, 255, 0.4)','rgba(55, 125, 255, 0.4)'],
                borderColor: ['rgba(55, 125, 255, 1)','rgba(55, 125, 255, 1)','rgba(55, 125, 255, 1)',
                'rgba(55, 125, 255, 1)','rgba(55, 125, 255, 1)','rgba(55, 125, 255, 1)','rgba(55, 125, 255, 1)','rgba(55, 125, 255, 1)',
                'rgba(55, 125, 255, 1)','rgba(55, 125, 255, 1)','rgba(55, 125, 255, 1)','rgba(55, 125, 255, 1)','rgba(55, 125, 255, 1)'],
                borderWidth: 1
            }]
        },
        options: {
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    gridLines: {
                        color: '#f2f3f8',
                        zeroLineColor: '#f2f3f8'
                    },
                    ticks: {
                        fontColor: "#8b8b8b",
                        fontFamily: 'Poppins',
                        fontSize: 10,
                        beginAtZero: true
                    }
                }],
                xAxes: [{
                    gridLines: {
                        color: '#f2f3f8'
                    },
                    ticks: {
                        fontColor: "#8b8b8b",
                        fontFamily: 'Poppins',
                        fontSize: 10
                    }
                }]
            },
            legend:{
                labels: {
                    fontFamily: 'Poppins',
                    boxWidth: 10,
                    usePointStyle: true
                },
                onClick: function () {
                    return '';
                },
            }
        }
    });
    AIZ.plugins.chart('#day1',{
        type: 'bar',
        data: {
            labels: {!! $labelsWeek !!},
            datasets: [{
                label: "{{__('dashboards.number_revenue')}}",
                data: {!! $dataRevenueWeek !!},
                backgroundColor: ['rgba(55, 125, 255, 0.4)','rgba(55, 125, 255, 0.4)','rgba(55, 125, 255, 0.4)','rgba(55, 125, 255, 0.4)',
                'rgba(55, 125, 255, 0.4)','rgba(55, 125, 255, 0.4)','rgba(55, 125, 255, 0.4)','rgba(55, 125, 255, 0.4)',
                'rgba(55, 125, 255, 0.4)','rgba(55, 125, 255, 0.4)','rgba(55, 125, 255, 0.4)','rgba(55, 125, 255, 0.4)'],
                borderColor: ['rgba(55, 125, 255, 1)','rgba(55, 125, 255, 1)','rgba(55, 125, 255, 1)',
                'rgba(55, 125, 255, 1)','rgba(55, 125, 255, 1)','rgba(55, 125, 255, 1)','rgba(55, 125, 255, 1)','rgba(55, 125, 255, 1)',
                'rgba(55, 125, 255, 1)','rgba(55, 125, 255, 1)','rgba(55, 125, 255, 1)','rgba(55, 125, 255, 1)','rgba(55, 125, 255, 1)'],
                borderWidth: 1
            }]
        },
        options: {
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    gridLines: {
                        color: '#f2f3f8',
                        zeroLineColor: '#f2f3f8'
                    },
                    ticks: {
                        fontColor: "#8b8b8b",
                        fontFamily: 'Poppins',
                        fontSize: 10,
                        beginAtZero: true
                    }
                }],
                xAxes: [{
                    gridLines: {
                        color: '#f2f3f8'
                    },
                    ticks: {
                        fontColor: "#8b8b8b",
                        fontFamily: 'Poppins',
                        fontSize: 10
                    }
                }]
            },
            legend:{
                labels: {
                    fontFamily: 'Poppins',
                    boxWidth: 10,
                    usePointStyle: true
                },
                onClick: function () {
                    return '';
                },
            }
        }
    });
    AIZ.plugins.chart('#month1',{
        type: 'bar',
        data: {
            labels: {!! $labelsMonth !!},
            datasets: [{
                label: "{{__('dashboards.number_revenue')}}",
                data: {!! $dataRevenueMonth !!},
                backgroundColor: ['rgba(55, 125, 255, 0.4)','rgba(55, 125, 255, 0.4)','rgba(55, 125, 255, 0.4)','rgba(55, 125, 255, 0.4)',
                'rgba(55, 125, 255, 0.4)','rgba(55, 125, 255, 0.4)','rgba(55, 125, 255, 0.4)','rgba(55, 125, 255, 0.4)',
                'rgba(55, 125, 255, 0.4)','rgba(55, 125, 255, 0.4)','rgba(55, 125, 255, 0.4)','rgba(55, 125, 255, 0.4)'],
                borderColor: ['rgba(55, 125, 255, 1)','rgba(55, 125, 255, 1)','rgba(55, 125, 255, 1)',
                'rgba(55, 125, 255, 1)','rgba(55, 125, 255, 1)','rgba(55, 125, 255, 1)','rgba(55, 125, 255, 1)','rgba(55, 125, 255, 1)',
                'rgba(55, 125, 255, 1)','rgba(55, 125, 255, 1)','rgba(55, 125, 255, 1)','rgba(55, 125, 255, 1)','rgba(55, 125, 255, 1)'],
                borderWidth: 1
            }]
        },
        options: {
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    gridLines: {
                        color: '#f2f3f8',
                        zeroLineColor: '#f2f3f8'
                    },
                    ticks: {
                        fontColor: "#8b8b8b",
                        fontFamily: 'Poppins',
                        fontSize: 10,
                        beginAtZero: true
                    }
                }],
                xAxes: [{
                    gridLines: {
                        color: '#f2f3f8'
                    },
                    ticks: {
                        fontColor: "#8b8b8b",
                        fontFamily: 'Poppins',
                        fontSize: 10
                    }
                }]
            },
            legend:{
                labels: {
                    fontFamily: 'Poppins',
                    boxWidth: 10,
                    usePointStyle: true
                },
                onClick: function () {
                    return '';
                },
            }
        }
    });
    AIZ.plugins.chart('#hour2',{
        type: 'bar',
        data: {
            labels: {!! $labelsHour !!},
            datasets: [{
                label: "{{__('dashboards.number_order')}}",
                data: {!! $dataOrderHour !!},
                backgroundColor: ['rgba(253, 57, 149, 0.4)',
                'rgba(253, 57, 149, 0.4)','rgba(253, 57, 149, 0.4)',
                'rgba(253, 57, 149, 0.4)','rgba(253, 57, 149, 0.4)',
                'rgba(253, 57, 149, 0.4)','rgba(253, 57, 149, 0.4)',
                'rgba(253, 57, 149, 0.4)','rgba(253, 57, 149, 0.4)','rgba(253, 57, 149, 0.4)','rgba(253, 57, 149, 0.4)','rgba(253, 57, 149, 0.4)'],
                borderColor: ['rgba(253, 57, 149, 1)','rgba(253, 57, 149, 1)','rgba(253, 57, 149, 1)'
                ,'rgba(253, 57, 149, 1)','rgba(253, 57, 149, 1)',
                'rgba(253, 57, 149, 1)','rgba(253, 57, 149, 1)',
                'rgba(253, 57, 149, 1)','rgba(253, 57, 149, 1)','rgba(253, 57, 149, 1)','rgba(253, 57, 149, 1)','rgba(253, 57, 149, 1)'],
                borderWidth: 1
            }]
        },
        options: {
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    gridLines: {
                        color: '#f2f3f8',
                        zeroLineColor: '#f2f3f8'
                    },
                    ticks: {
                        fontColor: "#8b8b8b",
                        fontFamily: 'Poppins',
                        fontSize: 10,
                        beginAtZero: true
                    }
                }],
                xAxes: [{
                    gridLines: {
                        color: '#f2f3f8'
                    },
                    ticks: {
                        fontColor: "#8b8b8b",
                        fontFamily: 'Poppins',
                        fontSize: 10
                    }
                }]
            },
            legend:{
                labels: {
                    fontFamily: 'Poppins',
                    boxWidth: 10,
                    usePointStyle: true
                },
                onClick: function () {
                    return '';
                },
            }
        }
    });
    AIZ.plugins.chart('#day2',{
        type: 'bar',
        data: {
            labels: {!! $labelsWeek !!},
            datasets: [{
                label: "{{__('dashboards.number_order')}}",
                data: {!! $dataOrderWeek !!},
                backgroundColor: ['rgba(253, 57, 149, 0.4)',
                'rgba(253, 57, 149, 0.4)','rgba(253, 57, 149, 0.4)',
                'rgba(253, 57, 149, 0.4)','rgba(253, 57, 149, 0.4)',
                'rgba(253, 57, 149, 0.4)','rgba(253, 57, 149, 0.4)',
                'rgba(253, 57, 149, 0.4)','rgba(253, 57, 149, 0.4)','rgba(253, 57, 149, 0.4)','rgba(253, 57, 149, 0.4)','rgba(253, 57, 149, 0.4)'],
                borderColor: ['rgba(253, 57, 149, 1)','rgba(253, 57, 149, 1)','rgba(253, 57, 149, 1)'
                ,'rgba(253, 57, 149, 1)','rgba(253, 57, 149, 1)',
                'rgba(253, 57, 149, 1)','rgba(253, 57, 149, 1)',
                'rgba(253, 57, 149, 1)','rgba(253, 57, 149, 1)','rgba(253, 57, 149, 1)','rgba(253, 57, 149, 1)','rgba(253, 57, 149, 1)'],
                borderWidth: 1
            }]
        },
        options: {
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    gridLines: {
                        color: '#f2f3f8',
                        zeroLineColor: '#f2f3f8'
                    },
                    ticks: {
                        fontColor: "#8b8b8b",
                        fontFamily: 'Poppins',
                        fontSize: 10,
                        beginAtZero: true
                    }
                }],
                xAxes: [{
                    gridLines: {
                        color: '#f2f3f8'
                    },
                    ticks: {
                        fontColor: "#8b8b8b",
                        fontFamily: 'Poppins',
                        fontSize: 10
                    }
                }]
            },
            legend:{
                labels: {
                    fontFamily: 'Poppins',
                    boxWidth: 10,
                    usePointStyle: true
                },
                onClick: function () {
                    return '';
                },
            }
        }
    });
    AIZ.plugins.chart('#month2',{
        type: 'bar',
        data: {
            labels: {!! $labelsMonth!!},
            datasets: [{
                label: "{{__('dashboards.number_order')}}",
                data: {!! $dataOrderMonth !!},
                backgroundColor: ['rgba(253, 57, 149, 0.4)',
                'rgba(253, 57, 149, 0.4)','rgba(253, 57, 149, 0.4)',
                'rgba(253, 57, 149, 0.4)','rgba(253, 57, 149, 0.4)',
                'rgba(253, 57, 149, 0.4)','rgba(253, 57, 149, 0.4)',
                'rgba(253, 57, 149, 0.4)','rgba(253, 57, 149, 0.4)','rgba(253, 57, 149, 0.4)','rgba(253, 57, 149, 0.4)','rgba(253, 57, 149, 0.4)'],
                borderColor: ['rgba(253, 57, 149, 1)','rgba(253, 57, 149, 1)','rgba(253, 57, 149, 1)'
                ,'rgba(253, 57, 149, 1)','rgba(253, 57, 149, 1)',
                'rgba(253, 57, 149, 1)','rgba(253, 57, 149, 1)',
                'rgba(253, 57, 149, 1)','rgba(253, 57, 149, 1)','rgba(253, 57, 149, 1)','rgba(253, 57, 149, 1)','rgba(253, 57, 149, 1)'],
                borderWidth: 1
            }]
        },
        options: {
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    gridLines: {
                        color: '#f2f3f8',
                        zeroLineColor: '#f2f3f8'
                    },
                    ticks: {
                        fontColor: "#8b8b8b",
                        fontFamily: 'Poppins',
                        fontSize: 10,
                        beginAtZero: true
                    }
                }],
                xAxes: [{
                    gridLines: {
                        color: '#f2f3f8'
                    },
                    ticks: {
                        fontColor: "#8b8b8b",
                        fontFamily: 'Poppins',
                        fontSize: 10
                    }
                }]
            },
            legend:{
                labels: {
                    fontFamily: 'Poppins',
                    boxWidth: 10,
                    usePointStyle: true
                },
                onClick: function () {
                    return '';
                },
            }
        }
    });

  
</script>
@endsection
