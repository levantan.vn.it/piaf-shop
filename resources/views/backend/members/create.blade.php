@extends('backend.layouts.app')
@section('title')
@lang('members.noti-invite')
@endsection
@section('content')
<div class="backpage mb-5">
    <a href="{{route('members.index')}}" class="back btn"><i class="fas fa-chevron-left"></i></a>
</div>
@php
    $trans = ["Dashboard" => __('members.dashboard'),
    "Users" => __('members.users'),
    "Orders" => __('members.orders'),
    "Suppliers" => __('members.suppliers'),
    "Products" => __('members.products'),
    "Categories" => __('members.categories'),
    "Collections" => __('members.collections'),
    "Banners" => __('members.banners'),
    "Notifications" => __('members.notifications'),
    "Messages" => __('members.messages'),
    "Members" => __('members.members'),
    "Settings" => __('members.settings')
    ]
@endphp
{{-- @dump($errors) --}}
<div class="row  m-0 justify-content-space-between">
    <div class="row m-0">
        <div class="image-avt">
            <input type="file" name="avatar" id="avatar" class="d-none" accept="image/*">
            <img src='{{static_asset("/assets/img/avatar-place.png")}}' id="img_avt" class="img-ava2 mr-2" alt="Avatar">
            <div class="overlay-avt"><span>{{__('members.upload_image')}}</span></div>
        </div>
        {{-- <div>
            <p class="mb-0 font-weight-800 font-size-18"><strong>#{{Auth::user()->id}}</strong> </p>
            <p class="mb-1 font-weight-800 font-size-18"><strong>{{Auth::user()->name}}</strong></p>
            <label class="label-status {{Auth::user()->status == 1 ? 'active' : ''}}">{{Auth::user()->status == 1 ? 'Active' : 'In Active'}}</label>
        </div> --}}
    </div>
    <div class="all-BtnReset">
        {{-- <button type="submit"  class="btn btn-info btn-invite ">@lang('members.noti-invite')</button> --}}
        <a href="javascript:void(0)" data-action="{{route('members.validated')}}"  class="btn btn-info btn-invite ">@lang('members.noti-invite')</a>
    </div>
</div>
<div class="row mt-3 ml-0 mr-0">
    <div class="col-md-5 p-0 detail-content4">
        <button class="btn-create-members members-information p-0 data-info">
            <i class="fas fas-create-collection fa-plus-circle img-redo mr-2 check"></i>
            <i class="fas fa-check fas-create-collection checked text-success"></i>
            <p class="mt-3 font-weight-500">@lang('members.noti-addInfo')</p>
        </button>
    </div>
    <div class="col-md-5 p-0 detail-content4">
        <button class="btn-create-members members-role p-0 data-role">
            <i class="fas fas-create-collection fa-plus-circle img-redo mr-2 check"></i>
            <i class="fas fa-check fas-create-collection checked text-success"></i>
            <p class="mt-3 font-weight-500">@lang('members.noti-addRole')</p>
        </button>
    </div>
</div>
@endsection

@section('modal')
    <form action="{{route('members.store')}}" method="POST" class="form_global">
        @csrf
        @include('templates.popup.members-role',['edit' => false])
        @include('templates.popup.members-information', ['edit'=> false])
        @include('templates.popup.upload-img-member',['edit'=> false])
    </form>
@endsection

@section('script')
<script src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script type="text/javascript">

        $('.members-information').click(function() {
            let url = $(this).attr('href');
            $('.members-informations').addClass('show');
        });
        $('.members-role').click(function() {
            let url = $(this).attr('href');
            $('.members-roles').addClass('show');
        });
        //
        function myFunction() {
            document.getElementById("status6");
        }
        function myFunction() {
            document.getElementById("status7");
        }
        $(document).ready(function(){
            $('.btn-cancel-infor').click(function(){
                $('.members-informations').removeClass('show');
                $('.form-infor')[0].reset();
            })
            $('.btn-cancel-role').click(function(){
                $('.members-roles').removeClass('show');
                $('.form-role')[0].reset();
            })
            $('.btn-save1').click(function(){
                $('.members-informations').removeClass('show');
            })
            $('.btn-save2').click(function(){
                $('.members-roles').removeClass('show');
            })

            $('.select_role').on('change',function(e){
                var role_id  = e.target.value ;
                console.log(role_id);
                $('.access').removeClass('active');
                $('#'+ role_id).addClass('active');
            })
        })
        function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function(e) {
            $('#img_avt').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]); // convert to base64 string
        }
        }

        $("#avatar").change(function() {
            readURL(this);
        });
        $('.overlay-avt').click(function(){
            $('.mark-member-img').addClass('show');
        })
            $('#created_at').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                drops:'up',
                locale: {
                    format: 'YYYY/MM/DD',
                    applyLabel: "대다",
                    cancelLabel: "취소",
                        "monthNames":[
                            "일월" , // Tháng 1
                            "이월" , // Tháng 2
                            "삼월" , // Tháng 3
                            "사월" , // Tháng 4
                            "오월" , // Tháng 5
                            "유월" , // Tháng 6
                            "칠월" , // Tháng 7
                            "팔월" , // Tháng 8
                            "구월" , // Tháng 9
                            "시월" , // Tháng 10
                            "십일월" , // Tháng 11
                            "십이월" , // Tháng 12
                        ],
                        daysOfWeek:[
                            "월요일" ,// Thứ 2
                            "화요일" ,// Thứ 3
                            "수요일" ,// Thứ 4
                            "목요일" ,// Thứ 5
                            "금요일" ,// Thứ 6
                            "토요일" ,// Thứ 7
                            "일요일" ,//  Chủ nhật.
                        ]
                }
            });

            $('#member-issue').daterangepicker({
                    singleDatePicker: true,
                    autoUpdateInput: false,
                    minDate: '1921/01/01',
                    showDropdowns: true,
                    drops:'up',
                    locale: {
                        format: 'YYYY/MM/DD',
                        applyLabel: "대다",
                        cancelLabel: "취소",
                            "monthNames":[
                                "일월" , // Tháng 1
                                "이월" , // Tháng 2
                                "삼월" , // Tháng 3
                                "사월" , // Tháng 4
                                "오월" , // Tháng 5
                                "유월" , // Tháng 6
                                "칠월" , // Tháng 7
                                "팔월" , // Tháng 8
                                "구월" , // Tháng 9
                                "시월" , // Tháng 10
                                "십일월" , // Tháng 11
                                "십이월" , // Tháng 12
                            ],
                            daysOfWeek:[
                                "월요일" ,// Thứ 2
                                "화요일" ,// Thứ 3
                                "수요일" ,// Thứ 4
                                "목요일" ,// Thứ 5
                                "금요일" ,// Thứ 6
                                "토요일" ,// Thứ 7
                                "일요일" ,//  Chủ nhật.
                            ]
                    }
            });
            $('#member-issue').on('apply.daterangepicker', function (ev, picker) {
                $(this).val(picker.startDate.format('YYYY/MM/DD'));
            });

            $('#member-issue').on('cancel.daterangepicker', function (ev, picker) {
                $(this).val('');
            });
            // $('#member-issue').val('{{ request()->get('date_of_issue') }}');
            $('#member-issue').val('{{request()->get('date_of_issue')}}');
            $('#birthday').daterangepicker({
                    singleDatePicker: true,
                    autoUpdateInput: false,
                    drops:'up',
                    minDate: '1921/01/01',
                    showDropdowns: true,
                    locale: {
                        format: 'YYYY/MM/DD',
                        applyLabel: "대다",
                        cancelLabel: "취소",
                            "monthNames":[
                                "일월" , // Tháng 1
                                "이월" , // Tháng 2
                                "삼월" , // Tháng 3
                                "사월" , // Tháng 4
                                "오월" , // Tháng 5
                                "유월" , // Tháng 6
                                "칠월" , // Tháng 7
                                "팔월" , // Tháng 8
                                "구월" , // Tháng 9
                                "시월" , // Tháng 10
                                "십일월" , // Tháng 11
                                "십이월" , // Tháng 12
                            ],
                            daysOfWeek:[
                                "월요일" ,// Thứ 2
                                "화요일" ,// Thứ 3
                                "수요일" ,// Thứ 4
                                "목요일" ,// Thứ 5
                                "금요일" ,// Thứ 6
                                "토요일" ,// Thứ 7
                                "일요일" ,//  Chủ nhật.
                            ]
                    }
            });
            $('#birthday').on('apply.daterangepicker', function (ev, picker) {
                $(this).val(picker.startDate.format('YYYY/MM/DD'));
            });

            $('#birthday').on('cancel.daterangepicker', function (ev, picker) {
                $(this).val('');
            });
            // $('#birthday').val('{{ request()->get('birthday') }}');
            $('#birthday').val('{{request()->get('birthday')}}');
            $(document).on('click','.btn_cancel',function(){
                $('.file-preview-item').remove();
                $('.selected-files').val('');
            })
            $(document).on('click','.btn-create-infor',function(){
                var url = $(this).data('action');
                const parent = $(this).parents('form');
                $.ajax({
                    url : url,
                    data: parent.serialize(),
                    type : "POST",
                    statusCode : {
                        200 : () => {
                            $('.'+$(this).data('text')).addClass('done');
                            parent.find('.members-informations .error').remove();
                        },
                        422 : ({responseJSON}) => {
                            parent.find('.members-informations .error').remove();
                            const e = Object.entries(responseJSON.errors);
                            console.log(e);
                            for (const [key,val] of e) {
                                parent.find(`[name="${key}"]`).after(`
                                    <div class="error">${val}</div>
                                `)
                            }
                        }
                    },
                    success : function(res) {
                        $('.members-informations').removeClass('show');
                    },
                    error : function(err) {
                        console.log(err)
                    }
                })
            })

            $(document).on('click','.btn-create-role',function(){
                var url = $(this).data('action');
                const parent = $(this).parents('form');
                $.ajax({
                    url : url,
                    data: parent.serialize(),
                    type : "POST",
                    statusCode : {
                        200 : () => {
                            $('.'+$(this).data('text')).addClass('done');
                            parent.find('.members-roles .error').remove();
                        },
                        422 : ({responseJSON}) => {
                            parent.find('.members-roles .error').remove();
                            const e = Object.entries(responseJSON.errors);
                            for (const [key,val] of e) {
                                parent.find(`[name="${key}"]`).parents('.wrapper').after(`
                                    <div class="error">${val}</div>
                                `)
                            }
                        }
                    },
                    success : function(res) {
                        $('.members-roles').removeClass('show');
                    },
                    error : function(err) {
                        console.log(err)
                    }
                })
            })

            $(document).on('click','.btn-saveimg',function(){
                var url = $(this).data('action');
                const parent = $(this).parents('form');
                $.ajax({
                    url : url,
                    data: parent.serialize(),
                    type : "POST",
                    statusCode : {
                        200 : () => {
                            parent.find('.mark-member-img .error').remove();
                        },
                        422 : ({responseJSON}) => {
                            parent.find('.mark-member-img .error').remove();
                            const e = Object.entries(responseJSON.errors);
                            // console.log(e);
                            for (const [key,val] of e) {
                                parent.find(`[name="${key}"]`).parents('.row').after(`
                                    <div class="error">${val}</div>
                                `)
                            }
                        }
                    },
                    success : function(res) {
                        var img = $('.img-fit').attr('src');
                        $('#img_avt').attr('src', img);
                        $('.mark-member-img').removeClass('show');
                        
                    },
                    error : function(err) {
                        console.log(err)
                    }
                })
            })

            $(document).on('click','.btn-invite',function(){
            var url = $(this).data('action');
            const parent = $(this).parents('body').find('.form_global');
            $.ajax({
                url : url,
                data: parent.serialize(),
                type : "POST",
                statusCode : {
                    200 : () => {
                        $('.'+$(this).data('text')).addClass('done');
                        parent.find('.error').remove();
                    },
                    422 : ({responseJSON}) => {
                        parent.find('.error').remove();
                        const e = Object.entries(responseJSON.errors);
                        console.log(e);
                        const arr_infor = ['name','email','phone','birthday','address','id_number','date_of_issue']
                        const arr_img = ['id_files'];
                        const arr_role = ['role_id'];
                        for (const [key,val] of e) {
                            if(arr_infor.includes(key)) {
                                $('.members-informations').addClass('show');
                            }
                            if(arr_img.includes(key)) {
                                $('.mark-member-img').addClass('show');
                            }
                            if(arr_role.includes(key)) {
                                $('.members-roles').addClass('show');
                            }
                            if(key == 'role_id') {
                            parent.find(`[name="${key}"]`).parents('.dropdown').after(`
                            <div class="error">${val}</div>
                            `)
                            }else if(key == 'id_files') {
                                parent.find(`[name="${key}"]`).parents('.row').after(`
                                <div class="error">${val}</div>
                            `)
                            }else {
                                parent.find(`[name="${key}"]`).after(`
                                <div class="error">${val}</div>
                            `)
                            }
                            
                            
                        }
                    }
                },
                success : function(res) {
                    $('.form_global').submit();
                },
                error : function(err) {
                    console.log(err)
                }
            })
            })
</script>

@endsection