@extends('backend.layouts.app')
@section('title')
@lang('products.products')
@endsection
@section('content')
<div class="aiz-titlebar text-left mt-2 mb-3">
    <div class="align-items-center">
        <h1 class="h3"><strong>@lang('products.products')</strong> </h1>
    </div>
</div>
<form class="" id="sort_customers" action="" method="GET">
    <div class="card">
    <div class="card-header d-flex">
        <div>
            <h6 class="mb-0">Danh sách sản phẩm</h6>
        </div>
        <div>
            <h6 class="lits-add"><label>Trang chủ </label> <label> > Sản Phẩm > </label> <u class="text-dark">Danh sách sản phẩm</u></h6>
        </div>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-12">
                <div class="d-flex  border-top border-right border-left">
                    <div class="p-2 border-right name-content">
                        <p class="mb-0 text-content w-title">Hạng mục tìm kiếm</p>
                    </div>
                    <div class="p-2 d-flex flex-column ">
                        <?php
                            $Ranksearch = request('rank_search') ?? [];
                            $search = request('search') ?? [];
                        ?>
                        <div class="add-select-product">
                            @if(count($Ranksearch ?? []))
                                @foreach ($Ranksearch as $index=>$item)
                                    <div class="add_delete">
                                        <select class="select-product "  name="rank_search[{{$index }}]">
                                            <option @if($item == 'name') selected @endif value="name">Tên sản phẩm</option>
                                            <option @if($item == 'id') selected @endif value="id">STT sản phẩm</option>
                                            <option @if($item == 'sku') selected @endif value="sku">Mã sản phẩm</option>
                                        </select>
                                        <input class="input-qty mb-1" autocomplete="off" id="search" name="search[]" type="text" value="{{$search[$index]?? ""}}" >
                                        <input class="is-form delete_add" type="button" value="-">
                                        @if(count($Ranksearch) == $index+1)
                                            <input class="plus is-form add_field " type="button" value="+">
                                        @endif
                                    </div>
                                @endforeach
                                    
                            @else
                                <div class="add_delete ">
                                    <select class="select-product" name="rank_search[]">
                                        <option value="name">Tên sản phẩm</option>
                                        <option value="id">STT sản phẩm</option>
                                        <option value="sku">Mã sản phẩm</option>
                                    </select>
                                    <input class="input-qty" autocomplete="off" name="search[]" type="text" value="{{request('rank_search')}}">
                                    <input class="is-form delete_add" type="button" value="-">
                                    <input class="plus is-form add_field" type="button" value="+">
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="d-flex border-top border-right border-left">
                    <div class="p-2 border-right name-content">
                        <p class="mb-0 text-content w-title">Danh mục sản phẩm</p>
                    </div>
                    {{-- {{@dump($item->id)}} --}}
                    <div class="p-2">
                        <select class="select-product " id="" name="category_1">
                            <option value="">- Danh mục lớn -</option>
                            @foreach ($categories as $item)
                                <option @if(request('category_1') == $item->id) selected @endif value="{{ $item->id}}"> {{ $item->title}} </option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="d-flex  border-top border-right border-left">
                    <div class="p-2 border-right name-content">
                        <p class="mb-0 text-content w-title">Ngày đăng sản phẩm</p>
                    </div>
                    <div class="p-2 d-flex">
                        <div class="">
                            <select class="select-product res-post" name="date_post">
                                <option @if(request('date_post') == 'created_at') selected @endif value="created_at" >Ngày đăng sản phẩm</option>
                                <option @if(request('date_post') == 'updated_at') selected @endif value="updated_at" >Ngày sửa gần nhất</option>
                            </select>
                            <button class="@if( request('start_date')==Date('Y-m-d') && request('end_date') == Date('Y-m-d') )  minus @endif timeline is-form2" type="button" value="{{ Date('Y-m-d ')}}">Hôm nay </button>
                            <button class="@if( (Date('Y-m-d', strtotime("-3 days"))) == (request('start_date')) )  minus @endif timeline is-form2" type="button" value="{{ Date('Y-m-d ', strtotime("-3 days"))}}">3 ngày </button>
                            <button class="@if( (Date('Y-m-d', strtotime("-7 days"))) == (request('start_date')) )  minus @endif timeline is-form2" type="button" value="{{ Date('Y-m-d ', strtotime("-7 days"))}}">7 ngày </button>
                            <button class="@if( (Date('Y-m-d', strtotime("-30 days"))) == (request('start_date')) )  minus @endif timeline is-form2" type="button" value="{{ Date('Y-m-d ', strtotime("-30 days"))}}">1 tháng </button>
                            <button class="@if( (Date('Y-m-d', strtotime("-90 days"))) == (request('start_date')) )  minus @endif timeline is-form2" type="button" value="{{ Date('Y-m-d ', strtotime("-90 days"))}}">3 tháng </button>
                            <button class="@if( (Date('Y-m-d', strtotime("-365 days"))) == (request('start_date')) )  minus @endif timeline is-form2" type="button" value="{{ Date('Y-m-d ', strtotime("-365 days"))}}">1 năm </button>
                            <button class="@if(!request('start_date') && !request('end_date')) minus @endif timeline is-form2 " type="button" value="" id="all-button">Tất cả </button>  
                            <input type="hidden" class="hiddenVal" name="type_date" value=" {{request('type_date', '')}} ">
                        </div>
                        <div class="daterpickder">
                            <label for="start_date">
                                <input class="datepicker date1" name="start_date" onkeypress='return event.charCode >= 48 && event.charCode <= 57' 
                                    id="start_date" autocomplete="off" placeholder="{{Date('Y-m-d ', strtotime("-365 days"))}}" value="{{ request('start_date') }}">
                                <span><i class="far fa-calendar-alt fa-lg"></i> - </span>
                            </label>
                            <label for="end_date">
                                <input class="datepicker date2" name="end_date" onkeypress='return event.charCode >= 48 && event.charCode <= 57' 
                                    id="end_date" autocomplete="off" placeholder="{{ date('Y-m-d ')}}" value="{{ request('end_date')}}">
                                <span class="icon-datepicker"><i class="far fa-calendar-alt fa-lg"></i></span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 d-flex">
                <div class="col-md-6 p-0 border d-flex">
                    <div class="p-2 border-right name-content">
                        <p class="mb-0 text-content w-title">Trạng thái trưng bày</p>
                    </div>
                    <div class="p-2">
                        <div class="form-check-inline">
                            <label class="form-check-label" for="radio1">
                                <input type="radio" @if(empty(request('optradio'))) checked @endif class="form-check-input mr-1" id="radio1" name="optradio" value="" > Tất cả
                            </label>
                        </div>
                        <div class="form-check-inline">
                            <label class="form-check-label" for="radio2">
                                <input type="radio" @if(request('optradio') == 1) checked @endif class="form-check-input mr-1" id="radio2" name="optradio" value="1"> Trưng bày
                            </label>
                        </div>
                        <div class="form-check-inline">
                            <label class="form-check-label" for="radio3">
                                <input type="radio" @if(request('optradio') == 2) checked @endif class="form-check-input mr-1" id="radio3" name="optradio" value="2"> Không trưng bày
                            </label>
                        </div>
                    </div>
                </div>
                <div class=" col-md-6 p-0 border-right border-top border-bottom d-flex">
                    <div class="p-2  name-content">
                        <p class="mb-0 text-content">Trạng thái bán hàng</p>
                    </div>
                    <div class="p-2">
                        <div class="form-check-inline">
                            <label class="form-check-label" for="radio4">
                                <input type="radio" @if(empty(request('optradio2'))) checked @endif class="form-check-input mr-1" id="radio4" name="optradio2" value=""> Tất cả
                            </label>
                        </div>
                        <div class="form-check-inline">
                            <label class="form-check-label" for="radio5">
                                <input type="radio" @if(request('optradio2') == 1) checked @endif class="form-check-input mr-1" id="radio5" name="optradio2" value="1"> Bán
                            </label>
                        </div>
                        <div class="form-check-inline">
                            <label class="form-check-label" for="radio6">
                                <input type="radio" @if(request('optradio2') == 2) checked @endif class="form-check-input mr-1" id="radio6" name="optradio2" value="2"> Không bán
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-4 mb-1">
            <div class="text-center col-md-12">
                <button type="submit" class="button button-tim">Tìm kiếm</button>
                <a id="reset" class="button button-reset" href="{{url()->current()}}">Reset</a>
            </div>
        </div>
    </div>
</div>
</form>

<div class="card">
    <div class="custom-overflow">
        <table class="table aiz-table mb-0">
            <thead>
                <tr>
                    <th class="w-10">@lang('products.id')</th>
                    <th class="w-30">@lang('products.title')</th>
                    <th class="w-12">@lang('products.sku')</th>
                    <th class="w-12">@lang('products.price')</th>
                    <th class="w-12">@lang('orders.quantity')</th>
                    <th class="w-12">@lang('products.status')</th>
                    <th class="w-12">@lang('products.lasted')</th>
                    <th class="text-right">{{translate('')}}</th>
                   
                </tr>
            </thead>
            <tbody>
                  
                @if(count($products ?? []))
                @foreach($products as $item)

                <tr>
                    <th class="font-weight-800">#{{$item->id}}</th>
                    @if(!empty($item->thumbnail_img))
                        <th class="d-flex font-weight-500">
                            <img src="{{static_asset($item->thumbnail_img) }}" class="img-ava mr-2" alt="image">
                            {{$item->name}}
                        </th>
                    @else
                    <th class="d-flex font-weight-500">
                        <img class="img-ava mr-2" src="/public/assets/img/icons/no-img.png" alt="">{{ $item->name }}

                    @endif

                    <th class="font-weight-500">{{optional($item->skus)->count('sku')}} @lang('products.skus')</th>
                    <th class="font-weight-500">
                        @if($item->skus->min('price'))
                        {{format_price(optional($item)->skus->min('price'))}}
                        @else
                        @endif

                    </th>
                    <th class="font-weight-500">{{optional($item->skus)->sum('qty')}}</th>
                    <th class="font-weight-500">{{optional($item)->checkStatus() }}</th>
                    <th class="font-weight-500">{{optional($item)->updated_at->format('Y/m/d')}}</th>
                    <th class="text-right p-0">
                        <div class="dropdown">
                            <button class="btn btn-drop mt-1" type="button"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-ellipsis-h"></i>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="{{ route('products.detail', ['id'=>$item->id]) }}">@lang('orders.view')</a>
                                <a class="dropdown-item click-productChange" status_id="{{$item->id}}" href="#">{{$item->changeStatus()}}</a>
                                <a class="dropdown-item btn-delete" href="javascript:void(0)" data-id="{{$item->id}}">@lang('banners.del')</a>
                            </div>
                        </div>
                    </th>
                </tr>
                @endforeach
                @endif

            </tbody>
        </table>
    </div>

</div>
<div class="aiz-pagination paginationOrder">
    {{ $products->appends(request()->input())->links()}}
</div>

@endsection

@section('modal')
@include('modals.delete_modal')
@endsection

@section('script')
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script type="text/javascript" >
    //search
    $('.timeline').click(function(){
        $('.timeline').removeClass('minus')
        $(this).toggleClass('minus')
    });
    //click input del active buton
    $('.datepicker').click(function(){
        $('.timeline').removeClass('minus')
    });
 
    //add date on input
    var Datenow = new Date().toISOString().slice(0, 10);
    $('.timeline').click(function(){
        if($(this).attr('value')){
            $('.date1').val($(this).attr('value'));
            $(".date2").val(Datenow);
        }else{
            $('.date1').val('');
            $(".date2").val('');
        }
    });

    //add input search
    $(document).ready(function() {
        var max_fields = 10;
        var wrapper = $(".add-select-product");
        var x = 1;

        $(document).on('click', '.add_field', function(e) {
            e.preventDefault();
            if (x < max_fields) {
                x++;
                $(wrapper).append(`<div class="mt-1 add_delete ${$('.add_delete').length} "> <select class="select-product quantity" name="rank_search[]"> <option  value="name">Tên sản phẩm</option> <option value="id">STT sản phẩm</option> <option value="sku">Mã sản phẩm</option> </select> <input class="input-qty" autocomplete="off" name="search[]" type="text" value="" > <input class="is-form delete_add" type="button" value="-"> </div>`);
            } else {
                alert('You Reached the limits')
            }
            $(this).remove();
            $('.add_delete').eq($('.add_delete').length - 1).append(`<input class="plus is-form add_field" type="button" value="+">`);
        });

        $(document).on("click", ".delete_add", function(e) {
            e.preventDefault();
            if($('.add_delete').length > 1){
                $(this).parents('.add_delete').remove();
                // $('.add_delete').eq(2).remove();
                x--;
            }
            const endElm = $('.add_delete').eq($('.add_delete').length - 1);
            if(!endElm.find('.add_field').length){
                endElm.append(`<input class="plus is-form add_field" type="button" value="+">`);
            }
        });
    });
  
    $('#start_date').daterangepicker({
        autoUpdateInput: false,
        minDate: '1921/01/01',
        singleDatePicker: true,
        showDropdowns: true,
        locale: {
            format: 'YYYY/MM/DD',
            applyLabel: "Ok",
            cancelLabel: "Cancel",
                "monthNames":[
                    "Tháng 1" , // Tháng 1
                    "Tháng 2" , // Tháng 2
                    "Tháng 3" , // Tháng 3
                    "Tháng 4" , // Tháng 4
                    "Tháng 5" , // Tháng 5
                    "Tháng 6" , // Tháng 6
                    "Tháng 7" , // Tháng 7
                    "Tháng 8" , // Tháng 8
                    "Tháng 9" , // Tháng 9
                    "Tháng 10" , // Tháng 10
                    "Tháng 11" , // Tháng 11
                    "Tháng 12" , // Tháng 12
                ],
                daysOfWeek:[
                    "Thứ 2 " ,// Thứ 2
                    "Thứ 3 " ,// Thứ 3
                    "Thứ 4 " ,// Thứ 4
                    "Thứ 5 " ,// Thứ 5
                    "Thứ 6 " ,// Thứ 6
                    "Thứ 7 " ,// Thứ 7
                    "Chủ nhật",//  Chủ nhật.
                ]
        }
    });

    $('#start_date').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('YYYY-MM-DD'));
    });

    $('#start_date').on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('');
    });
    // $('#start_date').addClass()
    
    // $('#start_date').on('show.daterangepicker', function(ev, picker) {
    //     if($("button#all-button").hasClass("minus")){
    //     console.log(123);
    //     $('#start_date').daterangepicker( "option", "disabled", true );
    //     }
    // });
    
    //date 2
    $('#end_date').daterangepicker({
        autoUpdateInput: false,
        minDate: '1921/01/01',
        singleDatePicker: true,
        showDropdowns: true,
        locale: {
            format: 'YYYY/MM/DD',
            applyLabel: "Ok",
            cancelLabel: "Cancel",
                "monthNames":[
                    "Tháng 1" , // Tháng 1
                    "Tháng 2" , // Tháng 2
                    "Tháng 3" , // Tháng 3
                    "Tháng 4" , // Tháng 4
                    "Tháng 5" , // Tháng 5
                    "Tháng 6" , // Tháng 6
                    "Tháng 7" , // Tháng 7
                    "Tháng 8" , // Tháng 8
                    "Tháng 9" , // Tháng 9
                    "Tháng 10" , // Tháng 10
                    "Tháng 11" , // Tháng 11
                    "Tháng 12" , // Tháng 12
                ],
                daysOfWeek:[
                    "Thứ 2 " ,// Thứ 2
                    "Thứ 3 " ,// Thứ 3
                    "Thứ 4 " ,// Thứ 4
                    "Thứ 5 " ,// Thứ 5
                    "Thứ 6 " ,// Thứ 6
                    "Thứ 7 " ,// Thứ 7
                    "Chủ nhật",//  Chủ nhật.
                ]
        }
    });
    $('#end_date').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.endDate.format('YYYY-MM-DD'));
    });

    $('#end_date').on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('');
    });

    // @if(request('end_date'))
    //     $('#end_date').value("{{request('end_date')}}");
    // @endif
    //     $('input[name="end_date"]').val('');
    //end search

    $('.custom-drop').click(function() {
        // alert();
        $(this).find('.custom-ul').toggle('d-none');
    })
    $(document).ready(function() {
        //$('#container').removeClass('mainnav-lg').addClass('mainnav-sm');
    });

    function sort_products(el) {
        $('#sort_products').submit();
    }


    // popup change status
    $(document).on('click', '.click-productChange', function() {
        var status_id = $(this).attr('status_id');
        // alert(test);
        Swal.fire({
            title: '@lang('banners.change-status')',
            text: "{{__('notifications.confirm_popup')}}",
            // icon: 'error',
            confirmButtonText: "{{__('notifications.yes')}}",
            cancelButtonText: "{{__('notifications.no')}}",
            showCancelButton: true,
            showCloseButton: true,

        }).then((result) => {
            if (result.isConfirmed) {
                var data = {
                    "_token": "{{ csrf_token() }}",
                    "id": status_id,
                };
                $.ajax({
                    method: "POST",
                    url: '{{route ("products.toggle-status",0)}}' + status_id,
                    data: data,
                    success: function(response) {
                        /* Read more about isConfirmed, isDenied below */
                        location.reload()
                        // Swal.fire('@lang('banners.saved')', '', 'success').then(() => {
                        // });
                    },
                    error: function(err) {
                        // Swal.fire('@lang('banners.not-saved')', '', 'info');
                    }
                });
            }
        });
    });

    $(document).on('click', '.btn-delete', function() {
        var delete_id = $(this).attr('data-id');
        // alert(test);
        Swal.fire({
            title: '@lang('products.delete-pro')',
            text: "{{__('notifications.confirm_popup')}}",
            // icon: 'error',
            confirmButtonText: "{{__('notifications.yes')}}",
            cancelButtonText: "{{__('notifications.no')}}",
            showCancelButton: true,
            showCloseButton: true,

        }).then((result) => {
            if (result.isConfirmed) {
                var data = {
                    "_token": "{{ csrf_token() }}",
                    "id": delete_id,
                };
                $.ajax({
                    method: "DELETE",
                    url: '{{route ("products.delete",0)}}' + delete_id,
                    data: data,
                    success: function(response) {
                        /* Read more about isConfirmed, isDenied below */

                        location.reload()
                        // Swal.fire('@lang('banners.saved')', '', 'success').then(() => {
                        // });
                    },
                    error: function(err) {
                        // Swal.fire('@lang('banners.not-saved')', '', 'info');
                    }
                });
            }
        });
    });

    function confirm_ban(url) {
        $('#confirm-ban').modal('show', {
            backdrop: 'static'
        });
        document.getElementById('confirmation').setAttribute('href', url);
    }

    function confirm_unban(url) {
        $('#confirm-unban').modal('show', {
            backdrop: 'static'
        });
        document.getElementById('confirmationunban').setAttribute('href', url);
    }
    $(document).on('click','.click-addAttr',function(){
        $(this).attr("selected");
    })

    //date picker products
    $('#joined_date38').daterangepicker({
        autoUpdateInput: false,
        singleDatePicker: true,
        showDropdowns: true,
        minDate: '1921/01/01',
        locale: {
            format: 'YYYY/MM/DD',
        }
    });
    $('#joined_date38').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('YYYY/MM/DD'));
    });

    $('#joined_date38').on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('');
    });
    $('input[name="date"]').val('{{ request()->get('date') }}');
</script>
@endsection
