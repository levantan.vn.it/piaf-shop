@extends('backend.layouts.app')
@section('title')
Update product
@endsection
@section('content')
<div class="aiz-titlebar text-left mt-2 mb-3">
    <h1 class="mb-0 h6">{{ translate('Edit Product') }}</h5>
</div>
<div class="col-lg-8 mx-auto">
	<form class="form form-horizontal mar-top" action="{{route('products.update', $product->id)}}" method="POST" enctype="multipart/form-data" id="choice_form">
		<input name="_method" type="hidden" value="POST">
		<input type="hidden" name="id" value="{{ $product->id }}">
		@csrf
		<div class="card">
			<div class="card-body">
				<div class="form-group row">
                    <label class="col-lg-3 col-from-label">{{translate('Product Name')}} <span class="text-danger">*</span></label>
                    <div class="col-lg-8">
                        <input type="text" class="form-control" name="name" placeholder="{{translate('Product Name')}}" value="{{ $product->name }}" required>
                    </div>
                </div>
                <div class="form-group row" id="category">
                    <label class="col-lg-3 col-from-label">{{translate('Category')}} <span class="text-danger">*</span></label>
                    <div class="col-lg-8">
                        <select class="form-control aiz-selectpicker" name="category_id" id="category_id" data-selected="{{ $product->category_id }}" data-live-search="true" required>
                            @foreach ($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->title }}</option>
                                @foreach ($category->childrenCategories as $childCategory)
                                    @include('categories.child_category', ['child_category' => $childCategory])
                                @endforeach
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row" id="brand">
                    <label class="col-lg-3 col-from-label">{{translate('Brand')}} <span class="text-danger">*</span></label>
                    <div class="col-lg-8">
                        <select class="form-control aiz-selectpicker" name="brand_id" id="brand_id" data-live-search="true" required>
							<option value="">{{ ('Select Brand') }}</option>
							@foreach ($supplier as $brand)
								<option value="{{ $brand->id }}" @if($product->supplier_id == $brand->id) selected @endif>{{ $brand->name }}</option>
							@endforeach
                        </select>
                    </div>
                </div>
			</div>
		</div>
		<div class="card">
			<div class="card-header">
				<h5 class="mb-0 h6">{{translate('Product Images')}}</h5>
			</div>
			<div class="card-body">

                <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="signinSrEmail">{{translate('Gallery Images')}}</label>
                    <div class="col-md-8">
                        <div class="input-group" data-toggle="aizuploader" data-type="image" data-multiple="true">
                            <div class="input-group-prepend">
                                <div class="input-group-text bg-soft-secondary font-weight-medium">{{ translate('Browse')}}</div>
                            </div>
                            <div class="form-control file-amount">{{ translate('Choose File') }}</div>
                            <input type="hidden" name="photos" value="{{ $product->id_images }}" class="selected-files">
                        </div>
                        <div class="file-preview box sm">
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="signinSrEmail">{{translate('Thumbnail Image')}} <small>(290x300)</small></label>
                    <div class="col-md-8">
                        <div class="input-group" data-toggle="aizuploader" data-type="image">
                            <div class="input-group-prepend">
                                <div class="input-group-text bg-soft-secondary font-weight-medium">{{ translate('Browse')}}</div>
                            </div>
                            <div class="form-control file-amount">{{ translate('Choose File') }}</div>
                            <input type="hidden" name="thumbnail_img" value="{{ $product->thumbnail_img_id }}" class="selected-files">
                        </div>
                        <div class="file-preview box sm">
                        </div>
                    </div>
                </div>

			</div>
		</div>
		<div class="card">
			<div class="card-header">
				<h5 class="mb-0 h6">{{translate('Product Videos')}}</h5>
			</div>
			<div class="card-body">
				<div class="form-group row">
					<label class="col-lg-3 col-from-label">{{translate('Video Provider')}}</label>
					<div class="col-lg-8">
						<select class="form-control aiz-selectpicker" name="video_provider" id="video_provider">
							<option value="youtube" <?php if($product->video_provider == 'youtube') echo "selected";?> >{{translate('Youtube')}}</option>
							<option value="dailymotion" <?php if($product->video_provider == 'dailymotion') echo "selected";?> >{{translate('Dailymotion')}}</option>
							<option value="vimeo" <?php if($product->video_provider == 'vimeo') echo "selected";?> >{{translate('Vimeo')}}</option>
						</select>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-lg-3 col-from-label">{{translate('Video Link')}}</label>
					<div class="col-lg-8">
						<input type="text" class="form-control" name="video_link" value="{{ $product->video_link }}" placeholder="{{ translate('Video Link') }}">
					</div>
				</div>
			</div>
		</div>
		<div class="card">
			<div class="card-header">
				<h5 class="mb-0 h6">{{translate('Product Variation')}}</h5>
			</div>
			<div class="card-body">
				<div class="form-group row">
					<div class="col-lg-3">
						<input type="text" class="form-control" value="{{translate('Colors')}}" disabled>
					</div>
					<div class="col-lg-8">
						<select class="form-control aiz-selectpicker" data-live-search="true" data-selected-text-format="count" name="colors[]" id="colors" multiple>
							@foreach (\App\Color::orderBy('name', 'asc')->get() as $key => $color)
								<option
									value="{{ $color->code }}"
									data-content="<span><span class='size-15px d-inline-block mr-2 rounded border' style='background:{{ $color->code }}'></span><span>{{ $color->name }}</span></span>"
									<?php if(in_array($color->code, json_decode($product->colors))) echo 'selected'?>
								></option>
							@endforeach
						</select>
					</div>
					<div class="col-lg-1">
                        <label class="aiz-switch aiz-switch-success mb-0">
                            <input value="1" type="checkbox" name="colors_active" <?php if(count(json_decode($product->colors)) > 0) echo "checked";?> >
                            <span></span>
                        </label>
					</div>
				</div>

				<div class="form-group row">
					<div class="col-lg-3">
						<input type="text" class="form-control" value="{{translate('Attributes')}}" disabled>
					</div>
                    <div class="col-lg-8">
                        <select name="choice_attributes[]" id="choice_attributes" data-selected-text-format="count" data-live-search="true" class="form-control aiz-selectpicker" multiple data-placeholder="{{ translate('Choose Attributes') }}">
							@foreach (\App\Attribute::all() as $key => $attribute)
								<option value="{{ $attribute->id }}" @if($product->attributes != null && in_array($attribute->id, json_decode($product->attributes, true))) selected @endif>{{ $attribute->name }}</option>
							@endforeach
                        </select>
                    </div>
                </div>

				<div class="">
					<p>{{ translate('Choose the attributes of this product and then input values of each attribute') }}</p>
					<br>
				</div>

				<div class="customer_choice_options" id="customer_choice_options">
					@foreach (json_decode($product->choice_options) as $key => $choice_option)
						<div class="form-group row">
							<div class="col-lg-3">
								<input type="hidden" name="choice_no[]" value="{{ $choice_option->attribute_id }}">
								<input type="text" class="form-control" name="choice[]" value="{{ \App\Attribute::find($choice_option->attribute_id)->getTranslation('name') }}" placeholder="{{ translate('Choice Title') }}" disabled>
							</div>
							<div class="col-lg-8">
								<input type="text" class="form-control aiz-tag-input" name="choice_options_{{ $choice_option->attribute_id }}[]" placeholder="{{ translate('Enter choice values') }}" value="{{ implode(',', $choice_option->values) }}" data-on-change="update_sku">
							</div>
						</div>
					@endforeach
				</div>
			</div>
		</div>
		<div class="card">
			<div class="card-header">
				<h5 class="mb-0 h6">{{translate('Product price + stock')}}</h5>
			</div>
			<div class="card-body">
				<div class="form-group row">
                    <label class="col-lg-3 col-from-label">{{translate('Unit price')}} <span class="text-danger">*</span></label>
                    <div class="col-lg-6">
                        <input type="text" placeholder="{{translate('Unit price')}}" name="unit_price" class="form-control" value="{{$product->unit_price}}" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-from-label">{{translate('Purchase price')}} <span class="text-danger">*</span></label>
                    <div class="col-lg-6">
                        <input type="number" min="0" step="0.01" placeholder="{{translate('Purchase price')}}" name="purchase_price" class="form-control" value="{{$product->purchase_price}}" required>
                    </div>
                </div>
				<div class="form-group row" id="quantity">
					<label class="col-lg-3 col-from-label">{{translate('Quantity')}} <span class="text-danger">*</span></label>
					<div class="col-lg-6">
						<input type="number" value="{{ $product->current_stock }}" step="1" placeholder="{{translate('Quantity')}}" name="current_stock" class="form-control" required>
					</div>
				</div>
				<br>
				<div class="sku_combination" id="sku_combination">

				</div>
			</div>
		</div>
		<div class="card">
			<div class="card-header">
				<h5 class="mb-0 h6">{{translate('Product Description')}}</h5>
			</div>
			<div class="card-body">
				<div class="form-group row">
                    <label class="col-lg-3 col-from-label">{{translate('Description')}} <span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <textarea class="aiz-text-editor" required name="description">{{ $product->description }}</textarea>
                    </div>
                </div>
			</div>
		</div>
		<div class="card">
			<div class="card-header">
				<h5 class="mb-0 h6">{{translate('SEO Meta Tags')}}</h5>
			</div>
			<div class="card-body">
				<div class="form-group row">
					<label class="col-lg-3 col-from-label">{{translate('Meta Title')}}</label>
					<div class="col-lg-8">
						<input type="text" class="form-control" name="meta_title" value="{{ $product->meta_title }}" placeholder="{{translate('Meta Title')}}">
					</div>
				</div>
				<div class="form-group row">
					<label class="col-lg-3 col-from-label">{{translate('Description')}}</label>
					<div class="col-lg-8">
						<textarea name="meta_description" rows="8" class="form-control">{{ strip_tags($product->meta_description) }}</textarea>
					</div>
				</div>
                <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="signinSrEmail">{{translate('Meta Images')}}</label>
                    <div class="col-md-8">
                        <div class="input-group" data-toggle="aizuploader" data-type="image" data-multiple="true">
                            <div class="input-group-prepend">
                                <div class="input-group-text bg-soft-secondary font-weight-medium">{{ translate('Browse')}}</div>
                            </div>
                            <div class="form-control file-amount">{{ translate('Choose File') }}</div>
                            <input type="hidden" name="meta_img" value="{{ $product->meta_img }}" class="selected-files">
                        </div>
                        <div class="file-preview box sm">
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-3 col-form-label">{{translate('Slug')}}</label>
                    <div class="col-md-8">
                        <input type="text" readonly placeholder="{{translate('Slug')}}" id="slug" name="slug" value="{{ $product->slug }}" class="form-control">
                    </div>
                </div>
			</div>
		</div>
		<div class="mb-3 text-right">
			<button type="submit" name="button" class="btn btn-info">{{ translate('Update Product') }}</button>
		</div>
	</form>
</div>

@endsection

@section('script')

<script type="text/javascript">

	function add_more_customer_choice_option(i, name){
        $('#customer_choice_options').append('<div class="form-group row"><div class="col-md-3"><input type="hidden" name="choice_no[]" value="'+i+'"><input type="text" class="form-control" name="choice[]" value="'+name+'" placeholder="{{ translate('Choice Title') }}" readonly></div><div class="col-md-8"><input type="text" class="form-control aiz-tag-input" name="choice_options_'+i+'[]" placeholder="{{ translate('Enter choice values') }}" data-on-change="update_sku"></div></div>');

    	AIZ.plugins.tagify();
	}

	$('input[name="colors_active"]').on('change', function() {
        var elm = $(this).parents('.form-group').find('.dropdown-toggle');
	    if(!$('input[name="colors_active"]').is(':checked')){
			$('#colors').prop('disabled', true);
            elm.addClass('disabled');
		}
		else{
			$('#colors').prop('disabled', false);
            elm.removeClass('disabled');
		}
		update_sku();
	});

	$('#colors').on('change', function() {
	    update_sku();
	});

	function delete_row(em){
		$(em).closest('.form-group').remove();
		update_sku();
	}

    function delete_variant(em){
		$(em).closest('.variant').remove();
	}

	function update_sku(){
		$.ajax({
		   type:"POST",
		   url:'{{ route('products.sku_combination_edit') }}',
		   data:$('#choice_form').serialize(),
		   success: function(data){
			   $('#sku_combination').html(data);
			   if (data.length > 1) {
				   $('#quantity').hide();
			   }
			   else {
					$('#quantity').show();
			   }
		   }
	   });
	}

    AIZ.plugins.tagify();

	$(document).ready(function(){
		update_sku();

		$('.remove-files').on('click', function(){
            $(this).parents(".col-md-4").remove();
        });
	});

	$('#choice_attributes').on('change', function() {
		$.each($("#choice_attributes option:selected"), function(j, attribute){
			flag = false;
			$('input[name="choice_no[]"]').each(function(i, choice_no) {
				if($(attribute).val() == $(choice_no).val()){
					flag = true;
				}
			});
            if(!flag){
				add_more_customer_choice_option($(attribute).val(), $(attribute).text());
			}
        });

		var str = @php echo $product->attributes @endphp;

		$.each(str, function(index, value){
			flag = false;
			$.each($("#choice_attributes option:selected"), function(j, attribute){
				if(value == $(attribute).val()){
					flag = true;
				}
			});
            if(!flag){
				$('input[name="choice_no[]"][value="'+value+'"]').parent().parent().remove();
			}
		});

		update_sku();
	});

</script>

@endsection
