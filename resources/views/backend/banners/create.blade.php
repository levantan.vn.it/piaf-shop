@extends('backend.layouts.app')
@section('title')
@lang('banners.add')
@endsection
@section('content')
<div class="mb-4">
    {{--@dump($errors)--}}
    <a href="{{route('banners.index')}}">
        <button type="submit" class="btn-back">
            <img src="{{ static_asset('assets/img/icons/back-admin.svg') }}" class="img-search mr-2" alt="Search">
        </button>
    </a>
</div>
<div class="row m-0 ">
   <div class="icon-lock product-iconCre col-md">
    {{--<button type="submit" form="create" class="btn-info btn clickBannerCreate ">@lang('banners.create-banner')</button>--}}
    <a href="javascript:void(0)" data-action="{{route('banners.validated')}}" class="btn-info btn btn-create-banner-now">@lang('banners.create-banner')</a>
   </div>
</div>
<div class="create-collection row mt-3">
    <div class="col-md-3 mb-3 clickAddVaidildate">
        <button class="btn-create-collection bannerAddInfo">
            <i class="fas fas-create-collection fa-plus-circle img-redo mr-2"></i>
            <p class="mt-3">@lang('banners.add-info')</p>
        </button>
    </div>
    <div class="col-md-3 mb-3 d-none clickDoneVaidildate">
        <a href="javascript:void(0)" class="content1-img clickEditBanner">
            <img src="{{ static_asset('assets/img/icons/edit.png') }}" class="img-EditDetail2 alt="Search">
        </a>
        <button class="btn-create-collection ">
            <i class="text-success fas fas-create-collection fa-check img-redo mr-2"></i>
            <p class="mt-3">@lang('banners.add-info')</p>
        </button>
    </div>
    <div class="col-md-3 mb-3 clickAddImg">
        <button class="btn-create-collection bannerAddImg">
            <i class="fas fas-create-collection fa-plus-circle img-redo mr-2"></i>
            <p class="mt-3">@lang('banners.add-img')</p>
		</button>
    </div>

    <div class="col-md-3 mb-3 d-none clickDoneImg">
        <a href="javascript:void(0)" class="content1-img clickEditImgBanner">
            <img src="{{ static_asset('assets/img/icons/edit.png') }}" class="img-EditDetail2 alt="Search">
        </a>
        <button class="btn-create-collection ">
            <i class="text-success fas fas-create-collection fa-check img-redo mr-2"></i>
            <p class="mt-3">@lang('banners.add-info')</p>
        </button>
    </div>
    <div class="col-md mb-3 clickAddCollection">
        <button class="btn-create-collection bannerAddCollection" >
            <i class="fas fas-create-collection fa-plus-circle img-redo mr-2"></i>
            <p class="mt-3">@lang('banners.add-position')</p>
        </button>
    </div>
    <div class="col-md mb-3 d-none clickDoneCollection">
        <a href="javascript:void(0)" class="content1-img clickEditCollectionBanner">
            <img src="{{ static_asset('assets/img/icons/edit.png') }}" class="img-EditDetail2 alt="Search">
        </a>
        <button class="btn-create-collection ">
            <i class="text-success fas fas-create-collection fa-check img-redo mr-2"></i>
            <p class="mt-3">@lang('banners.add-info')</p>
        </button>
    </div>
</div>

@endsection
@section('modal')
<form action="{{route('banners.store')}}" method="POST" name="create" class="form-banner" id="form-banner">
@csrf
    @include('templates.popup.banners-edit',['edit'=>false])
    @include('templates.popup.banner-collection-create')
    @include('templates.popup.banner-img-create')
</form>
@endsection
@section('script')
<script src="{{ static_asset('plugins/select2/js/select2.min.js') }}" ></script>
<script src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script type="text/javascript">
//click submit

                $(document).on('click','.btn-create-banner-now',function(){
                    var url = $(this).data('action');
                    const parent = $(this).parents('body').find('.form-banner');
                    $.ajax({
                        url : url,
                        data: parent.serialize(),
                        type : "POST",
                        statusCode : {
                            200 : () => {
                                $('.'+$(this).data('text')).addClass('done');
                                parent.find('.error').remove();
                            },
                            422 : ({responseJSON}) => {
                                parent.find('.error').remove();
                                const e = Object.entries(responseJSON.errors);
                                console.log(e);
                                const arr_infor = ['position','start_date','end_date'];
                                const arr_img = ['id_files'];
                                const arr_title = ['collection_id','title'];

                                // const arr_product = ['product'];
                                for (const [key,val] of e) {
                                    if(arr_infor.includes(key)) {
                                        $('.banner-infos').addClass('show');
                                    }
                                    if(arr_img.includes(key)) {
                                        $('.mark-banners-img').addClass('show');
                                    }
                                    if(arr_title.includes(key)) {
                                        $('.collectionsEdit2').addClass('show');
                                    }
                                    // if(arr_product.includes(key)) {
                                    //     $('.edit-product-collection').addClass('show');
                                    // }
                                    if(key == 'position') {
                                    parent.find(`[name="${key}"]`).parents('.dropdown').after(`
                                    <div class="error">${val}</div>
                                    `)
                                    }else if(key == 'id_files') {
                                        parent.find(`[name="${key}"]`).parents('.row').after(`
                                        <div class="error">${val}</div>
                                    `)
                                    }else if(key == 'collection_id') {
                                            parent.find(`[name="${key}"]`).parents('.dropdown').after(`
                                            <div class="error">${val}</div>
                                    `)
                                    }else {
                                        parent.find(`[name="${key}"]`).after(`
                                        <div class="error">${val}</div>
                                    `)
                                    }
                                    
                                }
                            }
                        },
                        success : function(res) {
                            $('.form-banner').submit();
                        },
                        error : function(err) {
                            console.log(err)
                        }
                    })
                })

        $(document).on('click','.clickBannerCreate',function(){
            $('.form-banner').submit();
        });

        $('#status6').daterangepicker({
            drops: 'up',
            autoUpdateInput: false,
            singleDatePicker: true,
            minDate: '1921/01/01',
            showDropdowns: true,
            locale: {
                format: 'YYYY/MM/DD',
                applyLabel: "대다",
                cancelLabel: "취소",
                "monthNames":[
                    "일월" , // Tháng 1
                    "이월" , // Tháng 2
                    "삼월" , // Tháng 3
                    "사월" , // Tháng 4
                    "오월" , // Tháng 5
                    "유월" , // Tháng 6
                    "칠월" , // Tháng 7
                    "팔월" , // Tháng 8
                    "구월" , // Tháng 9
                    "시월" , // Tháng 10
                    "십일월" , // Tháng 11
                    "십이월" , // Tháng 12
                ],
                daysOfWeek:[
                    "월요일" ,// Thứ 2
                    "화요일" ,// Thứ 3
                    "수요일" ,// Thứ 4
                    "목요일" ,// Thứ 5
                    "금요일" ,// Thứ 6
                    "토요일" ,// Thứ 7
                    "일요일" ,//  Chủ nhật.
                ]
            }
        });

        $('#status6').on('apply.daterangepicker', function (ev, picker) {
            $(this).val(picker.startDate.format('YYYY/MM/DD'));
        });

        $('#status6').on('cancel.daterangepicker', function (ev, picker) {
            $(this).val('');
        });

        $('#status7').daterangepicker({
            singleDatePicker: true,
            autoUpdateInput: false,
            drops:'up',
            minDate: '1921/01/01',
            showDropdowns: true,
            locale: {
                format: 'YYYY/MM/DD',
                applyLabel: "대다",
                cancelLabel: "취소",
                "monthNames":[
                    "일월" , // Tháng 1
                    "이월" , // Tháng 2
                    "삼월" , // Tháng 3
                    "사월" , // Tháng 4
                    "오월" , // Tháng 5
                    "유월" , // Tháng 6
                    "칠월" , // Tháng 7
                    "팔월" , // Tháng 8
                    "구월" , // Tháng 9
                    "시월" , // Tháng 10
                    "십일월" , // Tháng 11
                    "십이월" , // Tháng 12
                ],
                daysOfWeek:[
                    "월요일" ,// Thứ 2
                    "화요일" ,// Thứ 3
                    "수요일" ,// Thứ 4
                    "목요일" ,// Thứ 5
                    "금요일" ,// Thứ 6
                    "토요일" ,// Thứ 7
                    "일요일" ,//  Chủ nhật.
                ]
            }
        });
        $('#status7').on('apply.daterangepicker', function (ev, picker) {
            $(this).val(picker.startDate.format('YYYY/MM/DD'));
        });

        $('#status7').on('cancel.daterangepicker', function (ev, picker) {
            $(this).val('');
        });

        //active popup
        $('.bannerAddInfo').click(function() {
            $('.banner-infos').addClass('show');
        });

        $('.clickEditBanner').click(function(){
            $('.banner-infos').addClass('show');
        });

        $('.form-kus').click(function() {
            let url = $(this).attr('href');
            $('.form-skus').addClass('show');
        });

        $('.bannerAddImg').click(function() {
            $('.mark-banners-img').addClass('show');
        });

        $('.bannerAddCollection').click(function() {
            $('.collectionsEdit2').addClass('show');
        });

        $(document).on('click','.clickImgAddBanner',function(){
            $('.mark-banners-img .error').remove();
            $('.text-danger-image').remove();
            let countImage= $(this).parent().parent('.popup-products-edit').children('.banner-img-title').find('.d-flex.justify-content-between.align-items-center.file-preview-item').length;
            console.log(countImage);
            if(countImage<1){
                $('.banner-img-title').append(`<div class='error mt-3 text-danger-image'>@lang('banners.requỉred-image')</div>`);
            }
            if(countImage>=1){
                $('.mark-banners-img').removeClass('show');
                $('.clickAddImg').addClass('d-none');
                $('.clickDoneImg').removeClass('d-none');
            }
        });

        $(document).on('click','.clickEditImgBanner',function(){
            $('.mark-banners-img p.error.mt-1').remove();
            $('.mark-banners-img').addClass('show');
        })
        
        //ajax validate
        $(document).on('click','.clickvalidateBannerInfor',function(){
            
            var url = $(this).data('action');
            console.log(url);
            var position = $('#status5').val();
            var start_date = $('#status6').val();
            var end_date = $('#status7').val();
            data = {
                "token" : "{{csrf_token()}}",
                "position": position,
                "start_date": start_date,
                "end_date" : end_date,
            };
            $.ajax({
                url: url,
                data: data,
                type: "POST",
                beforeSend: function(){
                    $('.banner-infos .error').remove();
                },
                success: function(res){
                    $('.banner-infos').removeClass('show');
                    $('.banner-infos .error').hide();
                    document.querySelector('.banner-infos').classList.remove('show');
                    $('.clickAddVaidildate').addClass('d-none');
                    $('.clickDoneVaidildate').removeClass('d-none')
                },
                error : function(err) {
                    // var response= JSON.parse(err.responseText);
                    // showErrorMsg(response.errors);
                    const {errors} = err.responseJSON;
                    console.log(err);
                    Object.keys(errors).forEach(item=>{
                        let errorElement = $(` <p class="error mt-1"> ${errors[item]}</p>`);
                        $(`[name="${item}"]`).parent().append(errorElement);
                    });
                }
            })
        });

        $(document).on('click','.clickAddCollection',function(){
            var url = $(this).data('action');
            var title = $('#joined_date411').val();
            var collection_id = $('#status514').val();

            data = {
                "token" : "{{csrf_token()}}",
                "title": title,
                "collection_id": collection_id,
            };
            
            $.ajax({
                url: url,
                data: data,
                type: "POST",
                beforeSend: function(){
                    $('.clickAddCollection .error').remove();
                },
                success: function(res){
                    $('.collectionsEdit2').removeClass('show');
                    $('.error').hide();
                    document.querySelector('.clickAddCollection').classList.remove('show');
                    $('.clickAddCollection').addClass('d-none');
                    $('.clickDoneCollection').removeClass('d-none')
                },
                error : function(err) {
                    $('.collectionsEdit2 div.error').remove();
                    $('p.error.mt-1').remove();
                    // var response= JSON.parse(err.responseText);
                    // showErrorMsg(response.errors);
                    const {errors} = err.responseJSON;
                    console.log(err);
                    Object.keys(errors).forEach(item=>{
                        let errorElement = $(` <p class="error mt-1"> ${errors[item]}</p>`);
                        $(`[name="${item}"]`).parent().append(errorElement);
                    });
                }
            })

        });

        $(document).on('click','.clickEditCollectionBanner',function(){
            $('.collectionsEdit2').addClass('show');
        });


        // $(document).on('click','.checkInfoBanner',function(){
        //     const parent=$(this).parents('form');
        //     $('.error-popup').remove();
        //     $.ajax({
        //         url:"{{route('banners.checkStore')}}",
        //         type:'post',
        //         data: parent.serialize(),
        //         statusCode:{
        //             422: ({responseJSON})=>{
        //                 parent.find('.is-invalid').removeClass('.is-invalid');
        //                 parent.find('.invalid-feedback').remove();
        //                 const e = Object.entries(responseJSON.errors);
        //                 for(const [key,val] of e){
        //                     parent.find(`[name="${key}"]`).addClass('is-invalid').after(`
        //                         <div class="invalid-feedback">${val}</div>
        //                     `)
        //                 }
        //             }
        //         },
        //         success: function(res){
        //             $('.clickAddCollection').removeClass('show');
        //             parent.submit();
        //         },
        //         error:function(err){
        //             console.error(err);
        //         }

        //     })
        // });



        //Show image
        $(".imgAdd").click(function(){
            $(this).closest(".row").find('.imgAdd').before('<div class="col-sm-2 imgUp"><div class="imagePreviews"></div><label class="btn btn-add-image-supplier">Upload<input type="file" class="uploadFile img" value="Upload Photo" style="width:0px;height:0px;overflow:hidden;"></label><i class="fa fa-times del"></i></div>');
            });
            $(document).on("click", "i.del" , function() {
                $(this).parent().remove();
        });

        $(function() {
            $(document).on("change",".uploadFile", function()
            {
                var uploadFile = $(this);
                var files = !!this.files ? this.files : [];
                if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support

                if (/^image/.test( files[0].type)){ // only image file
                    var reader = new FileReader(); // instance of the FileReader
                        reader.readAsDataURL(files[0]); // read the local file

                        reader.onloadend = function(){ // set image data as background of div
                            //alert(uploadFile.closest(".upimage").find('.imagePreview').length);
                uploadFile.closest(".imgUp").find('.imagePreviews').css("background-image", "url("+this.result+")");
                }
            }
            });
        });     

        
        @if($errors->has('id_files'))
            $('button.btn-create-collection.bannerAddImg').click();
        @endif
        @if($errors->has('end_date')||$errors->has('start_date'))
            $('.bannerAddInfo').click();
        @endif
        @if($errors->has('title')||$errors->has('collection_id'))
          $('.bannerAddCollection').click();
        @endif
</script>
@endsection


