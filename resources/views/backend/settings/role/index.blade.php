@extends('backend.layouts.app')
@section('title')
@lang('settings.role_title')
@endsection
@php
    $icons = [
            [
                'class' => 'las la-home aiz-side-nav-icon',
                'name' =>  __('sidebar.Dashboard') ,
                'value' => 'Dashboard'
            ],
            [
                'class' => 'las la-user-friends aiz-side-nav-icon',
                'name' => __('sidebar.Users'),
                'value' => 'Users'
            ],
            [
                'class' => 'las la-money-bill aiz-side-nav-icon',
                'name' =>  __('sidebar.Orders') ,
                'value' => 'Orders'
            ],
            [
                'class' => 'las la-campground aiz-side-nav-icon',
                'name' =>  __('sidebar.Suppliers') ,
                'value' => 'Suppliers'
            ],
            [
                'class' => 'las la-apple-alt aiz-side-nav-icon',
                'name' =>  __('sidebar.Products') ,
                'value' => 'Products'
            ],
            [
                'class' => 'las la-radiation-alt aiz-side-nav-icon',
                'name' =>  __('sidebar.Categories') ,
                'value' => 'Categories'
            ],
            [
                'class' => 'las la-chart-area aiz-side-nav-icon',
                'name' =>  __('sidebar.Collections') ,
                'value' => 'Collections'
            ],
            [
                'class' => 'las la-columns aiz-side-nav-icon',
                'name' =>  __('sidebar.Banners') ,
                'value' => 'Banners'
            ],
            [
                'class' => 'las la-bell aiz-side-nav-icon',
                'name' =>  __('sidebar.Notifications') ,
                'value' => 'Notifications'
            ],
            [
                'class' => 'las la-comment-alt aiz-side-nav-icon',
                'name' =>  __('sidebar.Messages') ,
                'value' => 'Messages'
            ],
            [
                'class' => 'las la-user-friends aiz-side-nav-icon',
                'name' =>  __('sidebar.Team-Members') ,
                'value' => 'Members'
            ],
            [
                'class' => 'las la-dharmachakra aiz-side-nav-icon',
                'name' =>  __('sidebar.Settings') ,
                'value' => 'Settings'
            ]
        ]
@endphp
@section('content')
<div class="backpage mb-5">
    <a href="{{route('settings.index')}}" class="back btn"><i class="fas fa-chevron-left"></i></a>
</div>
@if (session('success'))
        <div class="alert alert-success noti" role="alert">
            {{ session('success') }}
        </div>
@endif
<div class="aiz-titlebar text-left mt-2 mb-3">
	<div class="align-items-center">
		<h1 class="h3 setting-title"><strong>@lang('settings.role_title')</strong></h1>
	</div>
</div>
<div class="company-information faq setting-page role-page">
    <div class="setting-list row">
        <div class="col-md-3 left">
            <div class="card p-3">
                <div class="title-field">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        @if(count($roles)?? [])
                        @foreach($roles as $role)
                        <li class="nav-item mb-1">
                                <a class="nav-link edit {{ $loop->first ?  'active' : '' }}" id="role{{$role->id}}-tab" data-toggle="tab" href="#role{{$role->id}}"
                                 role="tab" aria-controls="role{{$role->id}}" aria-selected="true">
                                 <span class="text">{{$role->name}}</span><button data-id="{{$role->id}}" class="btn_edit btn_option"><i class="far fa-edit"></i></button></a>
                                 <a class="nav-link save" id="role{{$role->id}}-tab" data-toggle="tab" href="#role{{$role->id}}"
                                    role="tab" aria-controls="role{{$role->id}}" aria-selected="true">
                                    <form method="post">
                                        @csrf
                                        <input type="text" name="name" class="role_name" value="{{$role->name}}" id="">
                                    </form>
                                    <button data-id="{{$role->id}}" data-action="{{route('settings.role.updateTitle',$role->id)}}" class="btn_save btn_option"><i class="far fa-edit"></i></button>
                                </a>
                                    <p class="error mt-1"></p>
                        </li>
                        
                        @endforeach
                        @endif
                      </ul>
                      <div class="create-title mt-2">
                          <form action="{{route('settings.role.store')}}" method="post">
                              @csrf
                              <input type="text" name="name" class="form-control mb-1"  placeholder="@lang('settings.placeholder_roletitle')">
                              @if($errors->has('name'))
                              <p class="error mt-1">{{$errors->first('name')}}</p>
                              @endif
                              <button type="submit"><i class="fas fa-plus-circle"></i>@lang('settings.btn_add_role')</button>
                          </form>
                      </div>
                      
                </div>
            </div>
        </div>
        <div class="col-md-9 right">
            <div class="card">
                <div class="tab-content" id="myTabContent">
                    <p class="right-title"><strong>@lang('settings.module_listing')</strong></p>
                    @if(count($roles) && !empty($roles))
                    @foreach($roles as $index => $role)
                    {{-- @dump($role->permissions) --}}
                    @php
                        $permis = json_decode($role->permissions,true) ?? [];
                    @endphp
                    <div class="tab-pane fade show {{ $loop->first ?  'active' : ''}}" id="role{{$role->id}}" role="tabpanel" aria-labelledby="role{{$role->id}}-tab">
                        <form id="form-update-module">
                            @csrf
                            <div class="list-item row">
                                @foreach($icons as $index => $icon)
                                <div class="item col-md-2 mb-3">
                                    <input type="checkbox" name="item[]" class="choose" value="{{$icon['value']}}" {{ in_array($icon['value'],$permis) ? 'checked' : '' }}  id="check{{$index}}">
                                    <div class="wrapper">
                                        <i class="{{$icon['class']}}"></i>
                                        <p class="name">{{$icon['name']}}</p>
                                    </div>
                                </div>
                                @endforeach
                                
                            </div>
                            <div class="btn-option-faq">
                                <a href="javascript:void(0)" data-id="{{optional($role)->id}}" class="btn-destroy btn" >{{__('settings.btn_delete')}}</a>
                                <a href="javascript:void(0)" class="btn-role btn-info" id="btn-update"  data-action="{{route('settings.role.update',$role->id)}}">@lang('settings.btn_save')</a>
                            </div>
                        </form>
                    </div>
                    @endforeach
                    @endif
                  </div>
                
            </div>
        </div>
    </div>
</div>
@endsection


@section('script')
    <script type="text/javascript">
        $(document).ready(function(){
            $(document).on('click','.btn_option',function(){
                $(this).parents('.nav-item').toggleClass('show');
            })
            $('.nav-item .error').hide();
            $(document).on('click','.btn_save',function(){
                var id = $(this).data('id');
                url = $(this).data('action');
                var name = $(this).parents('.save').find('.role_name').val();
                var err_msg = $(this).parents('.nav-item').find('.error');
                $.ajax({
                    url: url,
                    type: "POST",
                    data : {
                        "_token" : "{{csrf_token()}}",
                        "name" : name
                    },
                    statusCode : {
                        200 : (res) => {
                            $(this).parents('.nav-item').find('.nav-link .text').html(name);
                            AIZ.plugins.notify('success', "{{__('settings.msg_title_role')}}");
                            err_msg.hide();
                            $(this).parents('.nav-item').find('.edit').click();
                        },
                        422 : (err) => {
                            err_msg.show()
                            var errx = JSON.parse(err.responseText);
                            var errrs = errx.errors.name[0];
                            err_msg.html(errrs);
                        }
                    },
                    success : function(res) {
                        console.log(res);
                    },
                    error : function(err) {
                    }
                }) 
            })


            $(document).on('click','#btn-update',function(){
                var  checks= $(this).parents('#form-update-module').find('.choose');
                var arr = [];
                checks.each(function() {
                    if($(this).prop('checked') == true)
                    {
                        arr.push($(this).val());
                    }
                })
                console.log(arr);
                var url = $(this).data('action');
                $.ajax({
                    url: url,
                    type : "POST",
                    data : {
                        "_token" : "{{csrf_token()}}",
                        "item" : arr
                    },
                    success : function(res) {
                            AIZ.plugins.notify('success', "{{__('settings.msg_module_success')}}");
                    }
                })
            })
            $(document).on('click', '.btn-destroy', function() {
                let delete_id= $(this).attr('data-id');
                Swal.fire({
                    title: "{{__('settings.popup_delete')}}",
                    text: "{{__('settings.confirm_popup')}}",
                    // icon: 'error',
                    confirmButtonText: "{{__('settings.yes')}}",
                    cancelButtonText: "{{__('settings.no')}}",
                    showCancelButton: true,
                    showCloseButton: true,

                }).then((result) => {
                    if (result.isConfirmed) {
                        var data = {
                            "_token": "{{ csrf_token() }}",
                            "id": delete_id,
                        };
                        $.ajax({
                            type: "DELETE",
                            url: '/admin/settings/role/destroy/'+delete_id,
                            data: data,
                            success: function (response){
                                /* Read more about isConfirmed, isDenied below */
                                location.reload();
                                
                            },
                            error : function(err) {
                                Swal.fire('Changes are not saved', '', 'info')
                            }
                        });
                    }
                });
            });
        })
    </script>
@endsection
