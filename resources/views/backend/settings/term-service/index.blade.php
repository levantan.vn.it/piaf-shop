@extends('backend.layouts.app')
@section('title')
@lang("settings.terms_title")
@endsection
@section('content')
<div class="backpage mb-5">
    <a href="{{route('settings.index')}}" class="back btn"><i class="fas fa-chevron-left"></i></a>
</div>
@if (session('success'))
        <div class="alert alert-success noti" role="alert">
            {{ session('success') }}
        </div>
@endif
<div class="aiz-titlebar text-left mt-2 mb-3">
	<div class="align-items-center">
		<h1 class="h3 setting-title"><strong>@lang("settings.terms_title")</strong></h1>
	</div>
</div>
<div class="company-information setting-page">
    <div class="setting-list row">
        <div class="col-md-4 left">
            <div class="card p-3">
                <p class="title mb-0 p-2"><strong>@lang("settings.terms_title")</strong></p>
            </div>
        </div>
        <div class="col-md-8 right">
            <div class="card">
                <form action="{{route('settings.service.update')}}" method="POST">
                    @csrf
                    <div class="item mb-3">
                        <label for="">@lang("settings.description") <span class="required-icon"> *</span></label>

                        {{-- <input type="text" name="description"  value="{{$setting->description?? null}}" placeholder="Description" class="form-control"> --}}
                        <textarea name="description"  rows="6" placeholder="@lang("settings.description")" class="form-control aiz-text-editor">{{optional($service)->value?? null}}</textarea>
                        @if($errors->has('description'))
                            <p class="error mt-1">{{$errors->first('description')}}</p>
                        @endif
                    </div>
                    <button type="submit" class="btn-info">{{__('settings.btn_save')}}</button>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection


@section('script')
<script type="text/javascript">
    $(document).ready(function(){
        setTimeout(function(){
            $('.noti').fadeOut(1000);
        },2000)
    })
</script>
@endsection
