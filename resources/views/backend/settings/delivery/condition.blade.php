@extends('backend.layouts.app')
@section('title')
@lang('settings.special_delivery')
@endsection
@section('content')
<div class="backpage mb-5">
    <a href="{{route('settings.index')}}" class="back btn"><i class="fas fa-chevron-left"></i></a>
</div>
<div class="aiz-titlebar text-left mt-2 mb-3">
	<div class="align-items-center">
		<h1 class="h3 setting-title"><strong>@lang('settings.special_delivery')</strong></h1>
	</div>
</div>
<div class="company-information setting-page delivery-page">
    <div class="setting-list row">
        <div class="col-md-3 left">
            <div class="card p-3">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link {{Request::is('admin/settings/weight-formula') ? 'active' : ''}}" id="weight-tab" href="{{route('settings.weight')}}" >@lang('settings.weight_formu')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{Request::is('admin/settings/delivery-condition') ? 'active' : ''}}" id="weight-tab" href="{{route('settings.delivery-condition')}}" >@lang('settings.special_delivery')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{Request::is('admin/settings/delivery-company') ? 'active' : ''}}" id="weight-tab" href="{{route('settings.delivery-company')}}" >@lang('settings.delivery_company')</a>
                    </li>
                      {{-- <li class="nav-item">
                        <a class="nav-link {{Request::is('admin/settings/distance-formula') ? 'active' : ''}}" id="distance-tab" href="{{route('settings.distance')}}">@lang('settings.distance_formu')</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link {{Request::is('admin/settings/interprovincial-delivery') ? 'active' : ''}}" id="interprovincial-tab" href="{{route('settings.interprovincial')}}">@lang('settings.inter_deli')</a>
                      </li> --}}
                  </ul>
            </div>
        </div>
        <div class="col-md-9 right">
            <div class="card" id="delivery">
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active weight_tabs" id="weight" role="tabpanel" aria-labelledby="weight-tab">
                        <p class="deli_title">@lang('settings.delivery_condition')</p>
                        <form class="form" action="{{route('settings.delivery-condition.post')}}" method="post" id="add">
                            @csrf
                            <div class="list-weight-range">
                                @if(count($deliveries) && !empty($deliveries))
                                @foreach($deliveries as $index => $delivery)
                                <div class="weight-range update">
                                    <input type="hidden" name="delivery[{{$index}}][id]" value="{{$delivery->id}}">
                                    <div class="row weight_row">
                                        <div class="col-md-6 field">
                                            <input type="text" name="delivery[{{$index}}][title]" value="{{optional($delivery)->title}}" id="">

                                        </div>
                                        <div class="col-md-6 field">
                                            <input type="number" name="delivery[{{$index}}][price]" id="" value="{{optional($delivery)->price}}">
                                            <span class="unit">@lang('settings.price')</span>
                                        </div>
                                        <a href="javascript:void(0)" data-id="{{optional($delivery)->id}}" class="btn-del btn-del-now"><i class="fas fa-trash"></i></a>
                                    </div>
                                </div>
                                @endforeach
                                @endif
                            </div>
                            <div class="addnew-range">
                                <a class="btn-add-range" href="javascript:void(0)"><i class="fas fa-plus-circle"></i> @lang('settings.add_delivery_condition')</a>
                            </div>
                            <button type="submit" class="btn-info"> @lang('settings.btn_save')</button>
                        </form>
                    </div>
                  </div>
            </div>
        </div>
    </div>
</div>

@endsection


@section('script')
<script type="text/javascript">
    let i = $('.update').length;
    $(document).on('click','.weight_tabs .btn-add-range',function(){
        $('.weight_tabs .list-weight-range ').append(`
        <div class="weight-range update">
                                    <input type="hidden" name="delivery[${i}][id]" value="">
                                    <div class="row weight_row">
                                        <div class="col-md-6 field">
                                            <input type="text" name="delivery[${i}][title]" value="">

                                        </div>
                                        <div class="col-md-6 field">
                                            <input type="number" name="delivery[${i}][price]" value="">
                                            <span class="unit">@lang('settings.price')</span>
                                        </div>
                                        <a href="javascript:void(0)" class="btn-del btn-remove"><i class="fas fa-trash"></i></a>
                                    </div>
                                </div>`)

        i++;
    })
    $(document).on('click','.weight_tabs .btn-remove',function(){
        $(this).parents('.weight_tabs .weight-range').remove();
    })
    $(document).on('click', '.btn-del-now', function() {
        let delete_id= $(this).attr('data-id');
        Swal.fire({
            title: "{{__('settings.popup_delete')}}",
            text: "{{__('settings.confirm_popup')}}",
            // icon: 'error',
            confirmButtonText: "{{__('settings.yes')}}",
            cancelButtonText: "{{__('settings.no')}}",
            showCancelButton: true,
            showCloseButton: true,

        }).then((result) => {
            if (result.isConfirmed) {
                var data = {
                    "_token": "{{ csrf_token() }}",
                    "id": delete_id,
                };
                $.ajax({
                    type: "DELETE",
                    url: '/admin/settings/delivery-condition/destroy/'+delete_id,
                    data: data,
                    success: function (response){
                        /* Read more about isConfirmed, isDenied below */
                        location.reload();

                    },
                    error : function(err) {
                        Swal.fire('Changes are not saved', '', 'info');
                    }
                });
            }
        });
    });

    $(document).on('submit', '#add', function(e){
        e.preventDefault();
        const form = e.target;
        $.ajax({
            url: form.action,
            method: form.method,
            data: new FormData(form),
            contentType: false,
            processData: false,
            statusCode: {
                422: (err)=>{
                    console.log(err);
                    const errors = Object.entries(err.responseJSON.errors);
                    console.log(errors);
                    for (let [name, message] of errors) {
                        name = name.split('.');
                        name = name[0] + '[' + name[1] + ']['+name[2]+']';
                        $(`input[name="${name}"]`).addClass('is-invalid').after(`
                            <p class="error">${message}</p>
                        `);
                    }

                }
            },
            success:(data)=>{
                AIZ.plugins.notify('success', "{{__('settings.msg_weight_sucess')}}");
                setTimeout(
                function() {
                    window.location.reload(true);
                }, 2000);
                $('.error').hide();
            }
        })
    });
</script>
@endsection
