@extends('backend.layouts.app')
@section('title')
@lang('settings.delivery_title')
@endsection
@section('content')
<style type="text/css">
    .company-information .right .card button div{
        color: #000;
        text-transform: initial;
    }
</style>
<div class="backpage mb-5">
    <a href="{{route('settings.index')}}" class="back btn"><i class="fas fa-chevron-left"></i></a>
</div>
<div class="aiz-titlebar text-left mt-2 mb-3">
	<div class="align-items-center">
		<h1 class="h3 setting-title"><strong>@lang('settings.delivery_title')</strong></h1>
	</div>
</div>
<div class="company-information setting-page delivery-page">
    <div class="deli-formula">
        <div class="wrapper">
            <label class="box">@lang('settings.deli_fee')</label>
            <span>=</span>
            <label class="box">@lang('settings.weight')</label>
            <span>+</span>
            <label class="box">@lang('settings.distance')</label>
            <span>|</span>
            <label class="box">@lang('settings.interpro')</label>

        </div>
    </div>
    <div class="setting-list row">
        <div class="col-md-3 left">
            <div class="card p-3">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link {{Request::is('admin/settings/weight-formula') ? 'active' : ''}}" id="weight-tab" href="{{route('settings.weight')}}" >@lang('settings.weight_formu')</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link {{Request::is('admin/settings/distance-formula') ? 'active' : ''}}" id="distance-tab" href="{{route('settings.distance')}}">@lang('settings.distance_formu')</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link {{Request::is('admin/settings/interprovincial-delivery') ? 'active' : ''}}" id="interprovincial-tab" href="{{route('settings.interprovincial')}}">@lang('settings.inter_deli')</a>
                      </li>
                  </ul>
            </div>
        </div>
        <div class="col-md-9 right">
            <div class="card" id="delivery">
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade interpro_tabs show active" id="interprovincial" role="tabpanel" aria-labelledby="interprovincial-tab">
                        <p class="deli_title">@lang('settings.inter_deli')</p>
                        <form action="{{route('settings.updateOrInsertInter')}}" method="post" id="add_inter">
                            @csrf
                            <div class="bycar">
                                <p class="subtitle"><i class="far fa-check-square mr-2"></i>@lang('settings.by_car')</p>

                                <div class="list-weight-range car_list">
                                    @if(count($cars) && !empty($cars) )
                                        @foreach($cars as $index => $car)
                                            @php
                                                $deliveryCity = $car->deliveryCity()->pluck('city_id')->toArray();
                                            @endphp
                                            <div class="weight-range car">
                                                <label class="font-600">@lang('settings.city')</label>
                                                <input type="hidden" name="inter[{{$index}}][id]" value="{{optional($car)->id}}">
                                                <div class="row weight_row">
                                                    <div class="col-md-6 field speacial_valid">
                                                        <select name="inter[{{$index}}][city_id][]" class="aiz-selectpicker form-control" multiple>
                                                            <option value="">@lang('settings.chooses_city')</option>
                                                            @if(!empty($cities) && count($cities))
                                                                @foreach($cities as $city)
                                                                    <option value="{{ $city->id }}"{!! !empty($deliveryCity) && in_array($city->id,$deliveryCity)? ' selected' : null !!}>{{ $city->name }}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                    <div class="col-md-6 field">
                                                        <input type="number" name="inter[{{$index}}][price]" value="{{optional($car)->price}}" id="">
                                                        <span class="unit">@lang('settings.price')</span>
                                                    </div>
                                                    <a href="javascript:void(0)" data-id="{{optional($car)->id}}" class="btn-del btn-del-now"><i class="fas fa-trash"></i></a>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                                <div class="addnew-range">
                                    <a class="btn-add-range" href="javascript:void(0)"><i class="fas fa-plus-circle"></i> @lang('settings.add_city')</a>
                                </div>
                            </div>
                            <div class="byairplane">
                                <p class="subtitle"><i class="far fa-check-square mr-2"></i>@lang('settings.by_airplane')</p>
                                    <div class="list-weight-range airplane_list">
                                        @if(count($airplanes) && !empty($airplanes) )
                                            @foreach($airplanes as $index => $airplane)
                                                @php
                                                    $deliveryCity = $airplane->deliveryCity()->pluck('city_id')->toArray();
                                                @endphp
                                                <div class="weight-range airplane">
                                                    <label class="font-600">@lang('settings.city')</label>
                                                    <input type="hidden" name="airplane[{{$index}}][id]" value="{{optional($airplane)->id}}">
                                                    <div class="row weight_row">
                                                        <div class="col-md-6 field speacial_valid">
                                                            <select name="airplane[{{$index}}][city_id][]" class="aiz-selectpicker form-control" multiple>
                                                                <option value="">@lang('settings.chooses_city')</option>
                                                                @if(!empty($cities) && count($cities))
                                                                    @foreach($cities as $city)
                                                                        <option value="{{ $city->id }}"{!! !empty($deliveryCity) && in_array($city->id,$deliveryCity)? ' selected' : null !!}>{{ $city->name }}</option>
                                                                    @endforeach
                                                                @endif
                                                            </select>
                                                        </div>
                                                        <div class="col-md-6 field">
                                                            <input type="number" name="airplane[{{$index}}][price]" value="{{optional($airplane)->price}}" id="">
                                                            <span class="unit">@lang('settings.price')</span>
                                                        </div>
                                                        <a href="javascript:void(0)" data-id="{{optional($airplane)->id}}" class="btn-del btn-del-now"><i class="fas fa-trash"></i></a>
                                                    </div>
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                    <div class="addnew-range">
                                        <a class="btn-add-range" href="javascript:void(0)"><i class="fas fa-plus-circle"></i> @lang('settings.add_city')</a>
                                    </div>
                                    <button type="submit" class="btn-info"> @lang('settings.btn_save')</button>
                                </form>
                            </div>

                    </div>
                  </div>
            </div>
        </div>
    </div>
</div>

@endsection


@section('script')
<script src="{{static_asset('plugins/bootstrap-tagsinput/bootstrap-tagsinput.js')}}"></script>
<script type="text/javascript">
    let j = $('.interpro_tabs .bycar .update').length;
    $(document).on('click','.interpro_tabs .bycar .btn-add-range',function(){
        $('.interpro_tabs .bycar .car_list').append(`
        <div class="weight-range"><label class="font-600">{{__('settings.city')}}</label>
            <input type="hidden" name="inter[${j}][type_delivery]" value="3" id="">
            <input type="hidden" name="inter[${j}][interprovincial_type]" value="1" id="">
            <div class="row weight_row">
                <div class="col-md-6 field speacial_valid">
                <select name="inter[${j}][city_id][]" class="aiz-selectpicker form-control" multiple>
                    <option value="">{{__('settings.chooses_city')}}</option>
                    @if(!empty($cities) && count($cities))
                        @foreach($cities as $city)
                            <option value="{{ $city->id }}">{{ $city->name }}</option>
                        @endforeach
                    @endif
                </select>
            </div>
            <div class="col-md-6 field">
                <input type="number" name="inter[${j}][price]" value="" id="">
                <span class="unit">{{__('settings.price')}}</span>
            </div><a href="javascript:void(0)" class="btn-del btn-remove">
            <i class="fas fa-trash"></i></a> </div> </div>`)

        j++;
        $(function () {
            $('.aiz-selectpicker').selectpicker();
        });
    })
    $(document).on('click','.interpro_tabs .bycar .btn-remove',function(){
        $(this).parents('.interpro_tabs .bycar .weight-range').remove();
    })
    let i = $('.interpro_tabs .byairplane .update').length;;
    $(document).on('click','.interpro_tabs .byairplane .btn-add-range',function(){
        $('.interpro_tabs .byairplane .airplane_list').append(`
        <div class="weight-range"><label class="font-600">{{__('settings.city')}}<span class="required-icon"> *</span></label>
            <input type="hidden" name="airplane[${i}][type_delivery]" value="3" id="">
            <input type="hidden" name="airplane[${i}][interprovincial_type]" value="2" id="">
            <div class="row weight_row">
                <div class="col-md-6 field speacial_valid">
                <select name="airplane[${i}][city_id][]" class="aiz-selectpicker form-control" multiple>
                    <option value="">{{__('settings.chooses_city')}}</option>
                    @if(!empty($cities) && count($cities))
                        @foreach($cities as $city)
                            <option value="{{ $city->id }}">{{ $city->name }}</option>
                        @endforeach
                    @endif
                </select>
            </div>
            <div class="col-md-6 field">
                <input type="number" name="airplane[${i}][price]" class="form-control" value="" id="">
                <span class="unit">{{__('settings.price')}}</span>
            </div><a href="javascript:void(0)" class="btn-del btn-remove">
            <i class="fas fa-trash"></i></a> </div> </div>`)

        i++;
        $(function () {
            $('.aiz-selectpicker').selectpicker();
        });
    })
    $(document).on('click','.interpro_tabs .byairplane .btn-remove',function(){
        $(this).parents('.interpro_tabs .byairplane .weight-range').remove();
    })
    $(document).on('click', '.interpro_tabs .btn-del-now', function() {
        let delete_id= $(this).attr('data-id');
        Swal.fire({
            title: "{{__('settings.popup_delete')}}",
            text: "{{__('settings.confirm_popup')}}",
            // icon: 'error',
            confirmButtonText: "{{__('settings.yes')}}",
            cancelButtonText: "{{__('settings.no')}}",
            showCancelButton: true,
            showCloseButton: true,

        }).then((result) => {
            if (result.isConfirmed) {
                var data = {
                    "_token": "{{ csrf_token() }}",
                    "id": delete_id,
                };
                $.ajax({
                    type: "DELETE",
                    url: '/admin/settings/delivery-management/destroy/'+delete_id,
                    data: data,
                    success: function (response){
                        /* Read more about isConfirmed, isDenied below */
                        location.reload();
                    },
                    error : function(err) {
                        Swal.fire('Changes are not saved', '', 'info');
                    }
                });
            }
        });
    });
    $(document).on('submit', '#add_inter', function(e){
        e.preventDefault();
        const form = e.target;
        console.log('o day');
        $.ajax({
            url: form.action,
            method: form.method,
            data: new FormData(form),
            contentType: false,
            processData: false,
            statusCode: {
                422: (err)=>{
                    console.log(err);
                    const errors = Object.entries(err.responseJSON.errors);
                    console.log(errors);
                    for (let [name, message] of errors) {
                        name = name.split('.');
                        name = name[0] + '[' + name[1] + ']['+name[2]+']';
                        $(`input[name="${name}"]`).addClass('is-invalid').after(`
                            <p class="error">${message}</p>
                        `);
                    }

                }
            },
            success:(data)=>{
                console.log(data);
                setTimeout(function() {
                    window.location.reload(true);
                }, 2000);
                AIZ.plugins.notify('success', "{{__('settings.msg_inter_success')}}");
                $('.error').hide();
            }
        })
    })
</script>
@endsection
