@extends('backend.layouts.app')
@section('title')
@lang('settings.delivery_title')
@endsection
@section('content')
<style type="text/css">
    .company-information .right .card button{
        min-width: auto;
    }
    .delivery-page p.error{
        position: relative;
        bottom: 0;
        left: 0;
        font-size: 12px;
    }
</style>
<div class="backpage mb-5">
    <a href="{{route('settings.index')}}" class="back btn"><i class="fas fa-chevron-left"></i></a>
</div>
<div class="aiz-titlebar text-left mt-2 mb-3">
    <div class="align-items-center">
        <h1 class="h3 setting-title"><strong>@lang('settings.delivery_title')</strong></h1>
    </div>
</div>
<div class="company-information setting-page delivery-page">
    <div class="setting-list row">
        <div class="col-md-3 left">
            <div class="card p-3">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link {{Request::is('admin/settings/weight-formula') ? 'active' : ''}}" id="weight-tab" href="{{route('settings.weight')}}" >@lang('settings.weight_formu')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{Request::is('admin/settings/delivery-condition') ? 'active' : ''}}" id="weight-tab" href="{{route('settings.delivery-condition')}}" >@lang('settings.special_delivery')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{Request::is('admin/settings/delivery-company') ? 'active' : ''}}" id="weight-tab" href="{{route('settings.delivery-company')}}" >@lang('settings.delivery_company')</a>
                    </li>
                  </ul>
            </div>
        </div>
        <div class="col-md-9 right">
            <div class="card" id="delivery">
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active weight_tabs" id="weight" role="tabpanel" aria-labelledby="weight-tab">
                        <p class="deli_title">@lang('settings.delivery_company')</p>
                        <form class="form" action="" method="post" id="add">
                            @csrf
                            <div class="item mb-3">
                                <label for="">@lang('settings.company_shipping_name') <span class="text-danger">*</span></label>
                                <input type="text" name="company_shipping_name" value="{{get_setting('company_shipping_name')?? old('company_shipping_name')}}" id="" placeholder="@lang('settings.company_shipping_name')" class="form-control">
                                @if($errors->has('company_shipping_name'))
                                    <p class="error mt-1">{{$errors->first('company_shipping_name')}}</p>
                                @endif
                            </div>
                            <div class="item mb-3">
                                <label for="">@lang('settings.company_logo') <span class="text-danger">*</span></label>
                                <div class="kt-img row">
                                    @include('backend.inc.upload_file',['values_file'=>get_setting('company_logo'),'type'=>'single'])
                                </div>
                                @if($errors->has('company_logo'))
                                    <p class="error mt-1">{{$errors->first('company_logo')}}</p>
                                @endif
                            </div>
                            <div class="item mb-3">
                                <label for="">@lang('settings.company_delivery_time') <span class="text-danger">*</span></label>
                                <input type="text" name="company_delivery_time" value="{{get_setting('company_delivery_time')?? old('company_delivery_time')}}" id="" placeholder="@lang('settings.company_delivery_time')" class="form-control">
                                @if($errors->has('company_delivery_time'))
                                    <p class="error mt-1">{{$errors->first('company_delivery_time')}}</p>
                                @endif
                            </div>
                            <button type="submit" class="btn-info"> @lang('settings.btn_save')</button>
                            {{-- <button class="btn-submit" data-action="{{route('settings.updateOrInsert')}}">SAVE</button> --}}
                        </form>
                    </div>
                  </div>
            </div>
        </div>
    </div>
</div>

@endsection


@section('script')
<script src="{{static_asset('plugins/bootstrap-tagsinput/bootstrap-tagsinput.js')}}"></script>
<script type="text/javascript">

</script>
@endsection
