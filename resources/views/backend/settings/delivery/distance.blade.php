@extends('backend.layouts.app')
@section('title')
@lang('settings.delivery_title')
@endsection
@section('content')
<style type="text/css">
    .company-information .right .card button div{
        color: #000;
        text-transform: initial;
    }
</style>
<div class="backpage mb-5">
    <a href="{{route('settings.index')}}" class="back btn"><i class="fas fa-chevron-left"></i></a>
</div>
<div class="aiz-titlebar text-left mt-2 mb-3">
	<div class="align-items-center">
		<h1 class="h3 setting-title"><strong>@lang('settings.delivery_title')</strong></h1>
	</div>
</div>
<div class="company-information setting-page delivery-page">
    <div class="deli-formula">
        <div class="wrapper">
            <label class="box">@lang('settings.deli_fee')</label>
            <span>=</span>
            <label class="box">@lang('settings.weight')</label>
            <span>+</span>
            <label class="box">@lang('settings.distance')</label>
            <span>|</span>
            <label class="box">@lang('settings.interpro')</label>

        </div>
    </div>
    <div class="setting-list row">
        <div class="col-md-3 left">
            <div class="card p-3">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                      <a class="nav-link {{Request::is('admin/settings/weight-formula') ? 'active' : ''}}" id="weight-tab" href="{{route('settings.weight')}}" >@lang('settings.weight_formu')</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link {{Request::is('admin/settings/distance-formula') ? 'active' : ''}}" id="distance-tab" href="{{route('settings.distance')}}">@lang('settings.distance_formu')</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link {{Request::is('admin/settings/interprovincial-delivery') ? 'active' : ''}}" id="interprovincial-tab" href="{{route('settings.interprovincial')}}">@lang('settings.inter_deli')</a>
                    </li>
                  </ul>
            </div>
        </div>
        <div class="col-md-9 right">
            <div class="card" id="delivery">
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade distance_tabs show active" id="distance" role="tabpanel" aria-labelledby="distance-tab">
                        <p class="deli_title">@lang('settings.distance_title')</p>
                          <ul class="nav nav-tabs" id="tabChild" role="tablist">
                            <li class="nav-item">
                              <a class="nav-link active" id="kilo-tab" data-toggle="tab" href="#kilo" role="tab" aria-controls="kilo" aria-selected="true"><i class="far fa-dot-circle checked"></i><i class="far fa-circle none"></i>@lang('settings.kilo')</a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link" id="fixed-tab" data-toggle="tab" href="#fixed" role="tab" aria-controls="fixed" aria-selected="false"><i class="far fa-dot-circle checked"></i><i class="far fa-circle none"></i>@lang('settings.fixed_area')</a>
                            </li>
                          </ul>
                          <div class="tab-content" id="myTabContent">
                            <div class="tab-pane tabs_km fade show active" id="kilo" role="tabpanel" aria-labelledby="kilo-tab">
                                <form action="{{route('settings.updateOrInsertDistance')}}" method="post" id="add_distance">
                                    @csrf
                                    <div class="list-weight-range">
                                        @if(count($distances) && !empty($distances))
                                            @foreach($distances as $index => $distance)
                                                <div class="weight-range update">
                                                    <input type="hidden" name="distance[{{$index}}][id]" value="{{$distance->id}}">
                                                    <label class="font-600">{{$index == 0 ? __('settings.first') : __('settings.next')}} </label>
                                                    <div class="row weight_row">
                                                        <div class="col-md-6 field">
                                                            @if(optional($distance)->delivery_from == 0)
                                                            <input type="hidden" name="distance[{{$index}}][delivery_from]" value="{{optional($distance)->delivery_from}}" id="">
                                                            <input type="number" name="distance[{{$index}}][delivery_to]" value="{{optional($distance)->delivery_to}}" id="">
                                                            <span class="unit">@lang('settings.km')</span>
                                                            @else
                                                            <div class="to-from row">
                                                                <div class="col-md-6 field">
                                                                    <input type="number"  name="distance[{{$index}}][delivery_from]" value="{{optional($distance)->delivery_from}}" id="">
                                                                    <span class="unit">{{__('settings.km')}}</span>
                                                                </div>
                                                                {{-- <span>-</span> --}}
                                                                <div class="col-md-6 field">
                                                                    <input type="number"  name="distance[{{$index}}][delivery_to]" value="{{optional($distance)->delivery_to}}" id="">
                                                                    <span class="unit">{{__('settings.km')}}</span>
                                                                </div>
                                                            </div>
                                                            @endif
                                                        </div>
                                                        <div class="col-md-6 field">
                                                            <input type="number" name="distance[{{$index}}][price]" value="{{optional($distance)->price}}" id="">
                                                            <span class="unit">@lang('settings.price')</span>
                                                        </div>
                                                        <a href="javascript:void(0)" class="btn-del btn-del-now" data-id="{{optional($distance)->id}}"><i class="fas fa-trash"></i></a>
                                                    </div>
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                    <div class="addnew-range">
                                        <a class="btn-add-range" href="javascript:void(0)"><i class="fas fa-plus-circle"></i> @lang('settings.add_distance')</a>
                                    </div>
                                    <button type="submit" class="btn-info"> @lang('settings.btn_save')</button>
                                </form>
                            </div>
                            <div class="tab-pane tabs_area fade" id="fixed" role="tabpanel" aria-labelledby="fixed-tab">
                                <form action="{{route('settings.updateOrInsertArea')}}" method="post" id="add_fixed">
                                    @csrf
                                    <div class="list-weight-range">
                                        @if(!empty($distancefixeds) && count($distancefixeds))
                                            @foreach($distancefixeds as $index => $item)
                                                @php
                                                    $deliveryDistrict = $item->deliveryDistrict()->pluck('district_id')->toArray();
                                                @endphp
                                                <div class="weight-range update">
                                                    <label class="font-600">@lang('settings.area') </label>
                                                    <input type="hidden" name="area[{{$index}}][id]" value="{{optional($item)->id}}">
                                                    <div class="row weight_row">
                                                        <div class="col-md-6 field speacial_valid">
                                                            <select name="area[{{$index}}][district_id][]" class="aiz-selectpicker form-control" multiple>
                                                                <option value="">@lang('settings.chooses_district')</option>
                                                                @if(!empty($company_info_value['city_id']))

                                                                @foreach(get_districts_by_city($company_info_value['city_id']) as $district)
                                                                    <option value="{{ $district->id }}"{!! !empty($deliveryDistrict) && in_array($district->id,$deliveryDistrict)? ' selected' : null !!}>{{ $district->name }}</option>
                                                                @endforeach
                                                                @endif
                                                            </select>
                                                        </div>
                                                        <div class="col-md-6 field">
                                                            <input type="number" name="area[{{$index}}][price]" value="{{optional($item)->price}}" id="">
                                                            <span class="unit">@lang('settings.price')</span>
                                                        </div>
                                                        <a href="javascript:void(0)" data-id="{{optional($item)->id}}" class="btn-del btn-del-now"><i class="fas fa-trash"></i></a>
                                                    </div>
                                                </div>
                                            @endforeach
                                        @endif
                                        {{-- @dump(request('area.3.price')) --}}
                                    </div>
                                    <div class="addnew-range">
                                        <a class="btn-add-range" href="javascript:void(0)"><i class="fas fa-plus-circle"></i> @lang('settings.add_area')</a>
                                    </div>
                                    <button type="submit" class="btn-info"> @lang('settings.btn_save')</button>
                                </form>
                            </div>
                          </div>
                    </div>
                  </div>
            </div>
        </div>
    </div>
</div>

@endsection


@section('script')
<script src="{{static_asset('plugins/bootstrap-tagsinput/bootstrap-tagsinput.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function(){
    $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
                localStorage.setItem('activeTab', $(e.target).attr('href'));
            });
            var activeTab = localStorage.getItem('activeTab');
            if(activeTab){
                $('#tabChild a[href="' + activeTab + '"]').tab('show');
            }
            if(!location.href.includes('distance-formula')) {
                localStorage.removeItem('activeTab')
            }
    })
    // Base on Distance
    let i = $('.distance_tabs .tabs_km .update').length;

    $(document).on('click','.distance_tabs .tabs_km .btn-add-range',function(){
        $('.distance_tabs .tabs_km .list-weight-range').append(`
        <div class="weight-range"><label class="font-600">{{__('settings.next')}}</label>
            <input type="hidden" name="distance[${i}][type_delivery]" value="2" id="">
            <input type="hidden" name="distance[${i}][distance_type]" value="1" id="">
            <div class="row weight_row">
                <div class="col-md-6 field">
                    <div class="to-from row">
                        <div class="col-md-6 field">
                            <input type="number"  name="distance[${i}][delivery_from]" value="" id="">
                             <span class="unit">{{__('settings.km')}}</span>
                             @error('delivery_from')
                             <p class="error mt-1">{{$message}}</p>
                             @enderror
                             </div> <div class="field col-md-6">
                                <input type="number"  name="distance[${i}][delivery_to]" value="" id="">
                                 <span class="unit">{{__('settings.km')}}</span></div> </div> </div> <div class="col-md-6 field">
                                    <input type="number" name="distance[${i}][price]" id="" value="">
                                     <span class="unit">{{__('settings.price')}}</span>
                                     </div> <a href="javascript:void(0)" class="btn-del btn-remove">
                                        <i class="fas fa-trash"></i></a> </div> </div>`)

        i++;
    })
    $(document).on('click','.distance_tabs .tabs_km .btn-remove',function(){
        $(this).parents('.distance_tabs .tabs_km .weight-range').remove();
    })
    $(document).on('click', '.distance_tabs  .btn-del-now', function() {
        let delete_id= $(this).attr('data-id');
        Swal.fire({
            title: "{{__('settings.popup_delete')}}",
            text: "{{__('settings.confirm_popup')}}",
            // icon: 'error',
            confirmButtonText: "{{__('settings.yes')}}",
            cancelButtonText: "{{__('settings.no')}}",
            showCancelButton: true,
            showCloseButton: true,

        }).then((result) => {
            if (result.isConfirmed) {
                var data = {
                    "_token": "{{ csrf_token() }}",
                    "id": delete_id,
                };
                $.ajax({
                    type: "DELETE",
                    url: '/admin/settings/delivery-management/destroy/'+delete_id,
                    data: data,
                    success: function (response){
                        /* Read more about isConfirmed, isDenied below */
                        location.reload();

                    },
                    error : function(err) {
                        Swal.fire('Changes are not saved', '', 'info')
                    }
                });
            }
        });
    });
    $(document).on('submit', '#add_distance', function(e){
        e.preventDefault();
        const form = e.target;
        console.log(form.action);
        $.ajax({
            url: form.action,
            method: form.method,
            data: new FormData(form),
            contentType: false,
            processData: false,
            statusCode: {
                422: (err)=>{
                    console.log(err);
                    const errors = Object.entries(err.responseJSON.errors);
                    console.log(errors);
                    for (let [name, message] of errors) {
                        name = name.split('.');
                        name = name[0] + '[' + name[1] + ']['+name[2]+']';
                        $(`input[name="${name}"]`).addClass('is-invalid').after(`
                            <p class="error">${message}</p>
                        `);
                    }

                }
            },
            success:(data)=>{
                 setTimeout(
                function() {
                    window.location.reload(true);
                }, 2000);
                $('.error').hide();
                AIZ.plugins.notify('success', "{{__('settings.msg_distance_sucess')}}")
            }
        })
    })

    // Base on Fixed Area
    let j = $('.distance_tabs .tabs_area .update').length;
    $(document).on('click','.distance_tabs .tabs_area .btn-add-range',function(){
        $('.distance_tabs .tabs_area .list-weight-range').append(`
        <div class="weight-range"><label class="font-600">{{__('settings.area')}}</label>
            <input type="hidden" name="area[${j}][type_delivery]" value="2" id="">
            <input type="hidden" name="area[${j}][distance_type]" value="2" id="">
            <div class="row weight_row ">
                <div class="col-md-6 field speacial_valid">
                <select name="area[${j}][district_id][]" class="aiz-selectpicker form-control" multiple>
                    <option value="">{{__('settings.chooses_district')}}</option>
                    @if(!empty($company_info_value['city_id']))
                    @foreach(get_districts_by_city($company_info_value['city_id']) as $district)
                        <option value="{{ $district->id }}">{{ $district->name }}</option>
                    @endforeach
                    @endif
                </select></div>
                <div class="col-md-6 field">
                <input type="number" name="area[${j}][price]" value="" id="">
                <span class="unit">{{__('settings.price')}}</span>
                </div><a href="javascript:void(0)" class="btn-del btn-remove">
                <i class="fas fa-trash"></i></a>
                </div>
            </div>`)

        j++;
        $('.aiz-selectpicker').selectpicker();
    });
    $(document).on('click','.distance_tabs .tabs_area .btn-remove',function(){
        $(this).parents('.distance_tabs .tabs_area .weight-range').remove();
    })
    $(document).on('submit', '#add_fixed', function(e){
        e.preventDefault();
        const form = e.target;
        $.ajax({
            url: form.action,
            method: form.method,
            data: new FormData(form),
            contentType: false,
            processData: false,
            statusCode: {
                422: (err)=>{
                    console.log(err);
                    const errors = Object.entries(err.responseJSON.errors);
                    console.log(errors);
                    for (let [name, message] of errors) {
                        name = name.split('.');
                        name = name[0] + '[' + name[1] + ']['+name[2]+']';
                        $(`input[name="${name}"]`).addClass('is-invalid').after(`
                            <p class="error">${message}</p>
                        `);
                    }

                }
            },
            success:(data)=>{
                setTimeout(function() {
                    window.location.reload(true);
                }, 2000);
                AIZ.plugins.notify('success', "{{__('settings.msg_distance_sucess')}}")
                $('.error').hide();
            }
        })
    })
</script>
@endsection
