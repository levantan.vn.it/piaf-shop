@extends('backend.layouts.app')
@section('title')
@lang('orders.order')
@endsection
@section('css')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
@endsection

@section('content')

<div class="aiz-titlebar text-left mt-2 mb-3">
	<div class="align-items-center">
        <h1 class="h3"><strong>@lang('orders.order')</strong></h1>
	</div>
</div>
<form class=""  action="" method="GET">
    <div class="card">
        <div class="card-header d-flex">
            <div>
                <h6 class="mb-0">Danh sách sản phẩm</h6>
            </div>
            <div>
                <h6 class="lits-add"><label>Trang chủ </label> <label> > Đơn hàng > </label> <u class="text-dark">Danh sách tất cả đơn hàng</u></h6>
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="d-flex  border-top border-right border-left">
                        <div class="p-2 border-right name-content">
                            <p class="mb-0 text-content w-title">Từ khóa</p>
                        </div>
                        <div class="p-2 d-flex flex-column ">
                        <?php
                            $Ranksearch = request('rank_search') ?? [];
                            $search = request('search') ?? [];
                        ?>
                        <div class="add-select-product">
                            @if(count($Ranksearch ?? []))
                                @foreach ($Ranksearch as $index=>$item)
                                    <div class="add_delete">
                                        <select class="select-product "  name="rank_search[{{$index }}]">
                                            <option @if($item == 'orders.order_code') selected @endif value="orders.order_code">Mã đơn hàng</option>
                                            <option @if($item == 'orders.shipping_code') selected @endif value="orders.shipping_code">Mã vận đơn</option>
                                            <option @if($item == 'tdm_user.nick_name') selected @endif value="tdm_user.nick_name">Tên người đặt hàng</option>
                                            <option @if($item == 'tdm_user.id') selected @endif value="tdm_user.id">ID người đặt hàng</option>
                                            <option @if($item == 'tdm_user.email') selected @endif value="tdm_user.email">Email đặt hàng</option>
                                            <option @if($item == 'tdm_user.cellphone') selected @endif value="tdm_user.cellphone">Số ĐTDĐ người đặt hàng</option>
                                            <option @if($item == 'orders.store_name') selected @endif value="orders.store_name">Tên người nhận</option>
                                            <option @if($item == 'orders.phone') selected @endif value="orders.phone">Số ĐTDĐ người nhận</option>
                                            <option @if($item == 'orders.delivery_address') selected @endif value="orders.delivery_address">Địa chỉ giao hàng</option>
                                        </select>
                                        <input class="input-qty mb-1" autocomplete="off" id="search" name="search[]" type="text" value="{{$search[$index]?? ""}}" >
                                        <input class="is-form delete_add" type="button" value="-">
                                        @if(count($Ranksearch) == $index+1)
                                            <input class="plus is-form add_field " type="button" value="+">
                                        @endif
                                    </div>
                                @endforeach
                                    
                            @else
                                <div class="add_delete ">
                                    <select class="select-product" name="rank_search[]">
                                        <option value="orders.order_code">Mã đơn hàng</option>
                                        <option value="orders.shipping_code">Mã vận đơn</option>
                                        <option value="tdm_user.real_name">Tên người đặt hàng</option>
                                        <option value="tdm_user.id">ID người đặt hàng</option>
                                        <option value="tdm_user.email">Email đặt hàng</option>
                                        <option value="tdm_user.cellphone">Số ĐTDĐ người đặt hàng</option>
                                        <option value="orders.store_name">Tên người nhận</option>
                                        <option value="orders.phone">Số ĐTDĐ người nhận</option>
                                        <option value="orders.delivery_address">Địa chỉ giao hàng</option>
                                    </select>
                                    <input class="input-qty" autocomplete="off" name="search[]" type="text">
                                    <input class="is-form delete_add" type="button" value="-">
                                    <input class="plus is-form add_field" type="button" value="+">
                                </div>
                            @endif
                        </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="d-flex  border-top border-right border-left">
                        <div class="p-2 border-right name-content">
                            <p class="mb-0 text-content w-title">Thời gian</p>
                        </div>
                        <div class="p-2 d-flex">
                            <div class="">
                                <select class="select-product res-post" name="date_post" > 
                                    <option @if(request('date_post') == 'orders.created_at') selected @endif value="orders.created_at" >Ngày đặt hàng</option>
                                    <option @if(request('date_post') == 'orders.updated_at') selected @endif value="orders.updated_at" >Ngày nhập mã vận đơn</option>
                                    <option @if(request('date_post') == 'orders.delivery_confirm_date') selected @endif value="orders.delivery_confirm_date" >Ngày hoàn tất vận chuyển</option>
                                </select>
                                <button class="@if( request('start_date')==Date('Y-m-d') && request('end_date') == Date('Y-m-d') )  minus @endif timeline is-form2" type="button" value="{{ Date('Y-m-d ')}}">Hôm nay </button>
                                <button class="@if( (Date('Y-m-d', strtotime("-1 days"))) == (request('start_date')) )  minus @endif timeline is-form2" type="button" value="{{ Date('Y-m-d ', strtotime("-1 days"))}}">Hôm qua </button>
                                <button class="@if( (Date('Y-m-d', strtotime("-3 days"))) == (request('start_date')) )  minus @endif timeline is-form2" type="button" value="{{ Date('Y-m-d ', strtotime("-3 days"))}}">3 ngày </button>
                                <button class="@if( (Date('Y-m-d', strtotime("-7 days"))) == (request('start_date')) )  minus @endif timeline is-form2" type="button" value="{{ Date('Y-m-d ', strtotime("-7 days"))}}">7 ngày </button>
                                <button class="@if( (Date('Y-m-d', strtotime("-15 days"))) == (request('start_date')) )  minus @endif timeline is-form2" type="button" value="{{ Date('Y-m-d ', strtotime("-15 days"))}}">15 ngày </button>
                                <button class="@if(!request('start_date') && !request('end_date') || (Date('Y-m-d', strtotime("-30 days"))) == (request('start_date')) )  minus @endif timeline is-form2" type="button" value="{{ Date('Y-m-d ', strtotime("-30 days"))}}">1 tháng </button>
                                <button class="@if( (Date('Y-m-d', strtotime("-90 days"))) == (request('start_date')) )  minus @endif timeline is-form2" type="button" value="{{ Date('Y-m-d ', strtotime("-90 days"))}}">3 tháng </button>
                                <button class="@if( (Date('Y-m-d', strtotime("-180 days"))) == (request('start_date')) )  minus @endif timeline is-form2 " type="button" value="{{ Date('Y-m-d ', strtotime("-180 days"))}}">6 tháng </button>  
                                <input type="hidden" class="hiddenVal" name="type_date" value=" {{request('type_date', '')}} ">
                            </div>
                            <div class="daterpickder">
                                <label for="start_date">
                                    <input class="datepicker date1" name="start_date" onkeypress='return event.charCode >= 48 && event.charCode <= 57' 
                                        id="start_date" autocomplete="off" placeholder="{{Date('Y-m-d ', strtotime("-30 days"))}}" value="{{ request('start_date') }}">
                                    <span id="icon_date"><i class="far fa-calendar-alt fa-lg" type="button"></i> ~ </span>
                                </label>
                                <label for="end_date">
                                    <input class="datepicker date2" name="end_date" onkeypress='return event.charCode >= 48 && event.charCode <= 57' 
                                        id="end_date" autocomplete="off" placeholder="{{ date('Y-m-d ')}}" value="{{ request('end_date') }}">
                                    <span class="icon-datepicker"><i class="far fa-calendar-alt fa-lg" type="button"></i></span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="d-flex border-top border-right border-left">
                        <div class="p-2 border-right name-content">
                            <p class="mb-0 text-content w-title">Sản phẩm</p>
                        </div>
                        <div class="p-2">
                            <select class="select-product " name="products_1">
                                <option  @if(request('products_1') == 'products.name') selected @endif value="products.name">Tên sản phẩm</option>
                                <option  @if(request('products_1') == 'products.sku') selected @endif value="products.sku">Mã sản phẩm</option>
                            </select>
                            <input class="input-qty" autocomplete="off" name="search_pro" type="text" value="{{request('search_pro')}}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="d-flex border-top border-right border-left">
                        <div class="p-2 border-right name-content">
                            <p class="mb-0 text-content w-title">Trạng thái đơn hàng</p>
                        </div>
                        <div class="p-2">
                            <?php
                                $RequestStatus = request('orderStatus') ?? [];
                            ?>
                                <label class="gLabel">
                                    <input type="checkbox" @if( count($RequestStatus) == "5") checked @endif name="all" class="fCheck" id="checkall" value="" > Tất cả
                                </label>
                                <label class="gLabel">
                                    <input type="checkbox" @if( in_array("1",$RequestStatus)) checked @endif name="orderStatus[]" class="fCheck element" value="1"> Chuẩn bị vận chuyển
                                </label>
                                <label class="gLabel">
                                    <input type="checkbox" @if( in_array("2",$RequestStatus)) checked @endif name="orderStatus[]" class="fCheck element" value="2"> Bảo lưu vận chuyển
                                </label>
                                <label class="gLabel">
                                    <input type="checkbox" @if( in_array("3",$RequestStatus)) checked @endif name="orderStatus[]" class="fCheck element" value="3"> Chờ vận chuyển
                                </label>
                                <label class="gLabel">
                                    <input type="checkbox" @if( in_array("4",$RequestStatus)) checked @endif name="orderStatus[]" class="fCheck element" value="4"> Đang vận chuyển
                                </label>
                                <label class="gLabel">
                                    <input type="checkbox" @if( in_array("5",$RequestStatus)) checked @endif name="orderStatus[]" class="fCheck element" value="5"> Hoàn tất vận chuyển
                                </label>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-12 d-flex">
                    <div class="col-md-6 p-0 border d-flex">
                        <div class="p-2 border-right name-content">
                            <p class="mb-0 text-content w-title">Trạng thái chuyển tiền/thanh toán</p>
                        </div>
                        <div class="p-2">
                            <div class="form-check-inline mr-0">
                                <label class="gLabel">
                                    <input type="radio"  class="fChk mb-1 mr-1" name="optradio" checked value="" > Tất cả
                                </label>
                            </div>
                            <div class="form-check-inline mr-0">
                                <label class="gLabel">
                                    <input type="radio" @if(request('optradio') == 1) checked @endif class="fChk mb-1 mr-1" name="optradio" value="1"> Trước thanh toán
                            </div>
                            <div class="form-check-inline mr-0">
                                <label class="gLabel">
                                    <input type="radio" @if(request('optradio') == 2) checked @endif class="fChk mb-1 mr-1" name="optradio" value="2">Chờ thanh toán bổ sung
                                </label>
                            </div>
                            <div class="form-check-inline mr-0">
                                <label class="gLabel">
                                    <input type="radio" @if(request('optradio') == 3) checked @endif class="fChk mb-1 mr-1" name="optradio" value="3">Hoàn tất chuyển tiền
                                </label>
                            </div>
                            <div class="form-check-inline mr-0">
                                <label class="gLabel">
                                    <input type="radio" @if(request('optradio') == 4) checked @endif class="fChk mb-1 mr-1" name="optradio" value="4">Hoàn tất thanh toán
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class=" col-md-6 p-0 border-right border-top border-bottom d-flex">
                        <div class="p-2  name-content">
                            <p class="mb-0 text-content">Phân loại thành viên</p>
                        </div>
                        <div class="p-2 mb-auto mt-auto">
                            <div class="form-check-inline mr-0">
                                <label class="gLabel">
                                    <input type="radio" class="fChk mb-1 mr-1" name="optradio2" checked value=""> Tất cả
                                </label>
                            </div>
                            <div class="form-check-inline mr-0">
                                <label class="gLabel">
                                    <input type="radio" @if(request('optradio2') == "user_no1") checked @endif class="fChk mb-1 mr-1" name="optradio2" value="user_no1"> Thành viên
                                </label>
                            </div>
                            <div class="form-check-inline mr-0">
                                <label class="gLabel">
                                    <input type="radio" @if(request('optradio2') == "user_no") checked @endif class="fChk mb-1 mr-1" name="optradio2" value="user_no"> Khách
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-4 mb-1">
                <div class="text-center col-md-12">
                    <button type="submit" class="button button-tim">Tìm kiếm</button>
                    <a id="reset" class="button button-reset" href="{{url()->current()}}">Reset</a>
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-body-table ">
            <table class="table aiz-table mb-0 table-hover">
                <thead>
                    <tr >
                        <th>@lang('orders.od_id')</th>
                        <th>@lang('orders.od_date')</th>
                        <th>@lang('orders.us_id')</th>
                        <th>@lang('orders.email')</th>
                        <th>@lang('orders.product')</th>
                        <th>@lang('orders.price')</th>
                        <th>@lang('orders.od_state')</th>
                        <th class="text-right">@lang('')</th>
                    </tr>
                </thead>
                <tbody> 
                @if(count($orders ?? []))
                @foreach($orders as $item)
                    <tr >
                        <th>#{{$item->id}}</th>
                        <td class="align-middle">{{$item->created_at->format('Y/m/d')}}</td>
                        <td class="align-middle">#{{$item->user_id}}</td>
                        <td class="align-middle">@if(!empty($item->email)) {{$item->email}} @else{{optional($item->user)->email}}@endif</td>
                        <td class="align-middle">{{optional($item->products)->count('id')}} @lang('orders.product')</td>
                        <td class="align-middle">{{format_price(optional($item->OrderProduct)->sum('subtotal'))}} @lang('orders.pri')</td>
                        <td class="align-middle">{{$item->checkStatus()}}</td>
                        <td>
                            <div class="dropdown">
                                <button class="btn btn-drop" type="button"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-ellipsis-h"></i>
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="{{route('orders.detail', ['id'=>$item->id])}}">@lang('orders.view')</a>
                                <!-- <a class="dropdown-item" href="#">@lang('orders.active')</a> -->
                                <a class="dropdown-item click-delete" data-id="{{$item->id}}" href="#">@lang('orders.del')</a>
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>
    <div class="aiz-pagination paginationOrder">
        {{ $orders->appends(request()->input())->links() }}
    </div> 
</form>


<div class="modal fade" id="confirm-ban">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title h6">{{translate('Confirmation')}}</h5>
                <button type="button" class="close" data-dismiss="modal"></button>
            </div>
            <div class="modal-body">
                <p>{{translate('Do you really want to ban this Customer?')}}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal">{{translate('Cancel')}}</button>
                <a type="button" id="confirmation" class="btn btn-primary">{{translate('Proceed!')}}</a>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="confirm-unban">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title h6">{{translate('Confirmation')}}</h5>
                <button type="button" class="close" data-dismiss="modal"></button>
            </div>
            <div class="modal-body">
                <p>{{translate('Do you really want to unban this Customer?')}}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal">{{translate('Cancel')}}</button>
                <a type="button" id="confirmationunban" class="btn btn-primary">{{translate('Proceed!')}}</a>
            </div>
        </div>
    </div>
</div>
@endsection

@section('modal')
    @include('modals.delete_modal')
@endsection

@section('script')
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

<script type="text/javascript">

    //search
     $('.timeline').click(function(){
        $('.timeline').removeClass('minus')
        $(this).toggleClass('minus')
    });

    //click input del active buton
    $('.datepicker').click(function(){
        $('.timeline').removeClass('minus')
    });

    //add date on input
    var Datenow = new Date().toISOString().slice(0, 10);
    $('.timeline').click(function(){
        if($(this).attr('value')){
            $('.date1').val($(this).attr('value'));
            $(".date2").val(Datenow);
        }else{
            $('.date1').val('');
            $(".date2").val('');
        }
    });
   
    //add input search
    $(document).ready(function() {
        var max_fields = 10;
        var wrapper = $(".add-select-product");
        var x = 1;

        $(document).on('click', '.add_field', function(e) {
            e.preventDefault();
            if (x < max_fields) {
                x++;
                $(wrapper).append(`<div class="mt-1 add_delete ${$('.add_delete').length} "> <select class="select-product quantity" name="rank_search[]"> <option value="order_code">Mã đơn hàng</option> <option value="shipping_code">Mã vận đơn</option> <option value="real_name">Tên người đặt hàng</option> <option value="id">ID người đặt hàng</option> <option value="email">Email đặt hàng</option> <option value="cellphone">Số ĐTDĐ người đặt hàng</option> <option value="receiver">Tên người nhận</option> <option value="phone">Số ĐTDĐ người nhận</option> <option value="delivery_address">Địa chỉ giao hàng</option> </select> <input class="input-qty" autocomplete="off" name="search[]" type="text" value="" > <input class="is-form delete_add" type="button" value="-"> </div>`);
            } else {
                alert('You Reached the limits')
            }
            $(this).remove();
            $('.add_delete').eq($('.add_delete').length - 1).append(`<input class="plus is-form add_field" type="button" value="+">`);
        });

        $(document).on("click", ".delete_add", function(e) {
            e.preventDefault();
            if($('.add_delete').length > 1){
                $(this).parents('.add_delete').remove();
                // $('.add_delete').eq(2).remove();
                x--;
            }
            const endElm = $('.add_delete').eq($('.add_delete').length - 1);
            if(!endElm.find('.add_field').length){
                endElm.append(`<input class="plus is-form add_field" type="button" value="+">`);
            }
        });
    });

    $('#start_date').daterangepicker({
        autoUpdateInput: false,
        minDate: '1921/01/01',
        singleDatePicker: true,
        showDropdowns: true,
        // showOn: 'button',
        locale: {
            format: 'YYYY/MM/DD',
            applyLabel: "Ok",
            cancelLabel: "Cancel",
                "monthNames":[
                    "Tháng 1" , // Tháng 1
                    "Tháng 2" , // Tháng 2
                    "Tháng 3" , // Tháng 3
                    "Tháng 4" , // Tháng 4
                    "Tháng 5" , // Tháng 5
                    "Tháng 6" , // Tháng 6
                    "Tháng 7" , // Tháng 7
                    "Tháng 8" , // Tháng 8
                    "Tháng 9" , // Tháng 9
                    "Tháng 10" , // Tháng 10
                    "Tháng 11" , // Tháng 11
                    "Tháng 12" , // Tháng 12
                ],
                daysOfWeek:[
                    "Thứ 2 " ,// Thứ 2
                    "Thứ 3 " ,// Thứ 3
                    "Thứ 4 " ,// Thứ 4
                    "Thứ 5 " ,// Thứ 5
                    "Thứ 6 " ,// Thứ 6
                    "Thứ 7 " ,// Thứ 7
                    "Chủ nhật",//  Chủ nhật.
                ]
        }
    });

    $('#start_date').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('YYYY-MM-DD'));
    });

    $('#start_date').on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('');
    });
    
    //date 2
    $('#end_date').daterangepicker({
        autoUpdateInput: false,
        minDate: '1921/01/01',
        singleDatePicker: true,
        showDropdowns: true,
        locale: {
            format: 'YYYY/MM/DD',
            applyLabel: "Ok",
            cancelLabel: "Cancel",
                "monthNames":[
                    "Tháng 1" , // Tháng 1
                    "Tháng 2" , // Tháng 2
                    "Tháng 3" , // Tháng 3
                    "Tháng 4" , // Tháng 4
                    "Tháng 5" , // Tháng 5
                    "Tháng 6" , // Tháng 6
                    "Tháng 7" , // Tháng 7
                    "Tháng 8" , // Tháng 8
                    "Tháng 9" , // Tháng 9
                    "Tháng 10" , // Tháng 10
                    "Tháng 11" , // Tháng 11
                    "Tháng 12" , // Tháng 12
                ],
                daysOfWeek:[
                    "Thứ 2 " ,// Thứ 2
                    "Thứ 3 " ,// Thứ 3
                    "Thứ 4 " ,// Thứ 4
                    "Thứ 5 " ,// Thứ 5
                    "Thứ 6 " ,// Thứ 6
                    "Thứ 7 " ,// Thứ 7
                    "Chủ nhật",//  Chủ nhật.
                ]
        }
    });
    $('#end_date').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.endDate.format('YYYY-MM-DD'));
    });

    $('#end_date').on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('');
    });

    //all checkbox 
    $('#checkall').change(function () {
        $('.element').prop('checked',this.checked);
    });

    $('.element').change(function () {
        if ($('.element:checked').length == $('.element').length){
            $('#checkall').prop('checked',true);
        }
        else {
            $('#checkall').prop('checked',false);
        }
    });

        // console.log(fCheck:checked);
    // function checkedAll(allCheckbox){
    // var allCheckboxes = document.getElementsByClassName('fCheck');
    // for (var i = 0; i < allCheckboxes.length; i++){
    //     var curCheckbox = allCheckboxes[i];
    //     if (curCheckbox.id != 'all'){
    //     curCheckbox.checked = allCheckbox.checked;
    //     }
    //     }
    // }
    // function checkedAll2(allCheckbox){
    // var allCheckboxes = document.getElementsByClassName('fCheck2');
    // for (var i = 0; i < allCheckboxes.length; i++){
    //     var curCheckbox = allCheckboxes[i];
    //     if (curCheckbox.id != 'all'){
    //     curCheckbox.checked = allCheckbox.checked;
    //     }
    //     }
    // }
    //end search


    // doubole click
    // $('[data-redirect]').on('dblclick', function() {
    //     let url = $(this).attr('data-redirect');
    //     if (url && url.length) {
    //         location.href = url;
    //     }
    // });
    $(document).ready(function() {
    //$('#container').removeClass('mainnav-lg').addClass('mainnav-sm');
    });

    function sort_customers(el){
        $('#sort_customers').submit();
    }
    function confirm_ban(url)
    {
        $('#confirm-ban').modal('show', {backdrop: 'static'});
        document.getElementById('confirmation').setAttribute('href' , url);
    }

    function confirm_unban(url)
    {
        $('#confirm-unban').modal('show', {backdrop: 'static'});
        document.getElementById('confirmationunban').setAttribute('href' , url);
    }
    // delete order
    $(document).on('click', '.click-delete', function() {
        var delete_id = $(this).attr('data-id');
        // alert(delete_id);
        Swal.fire({
            title: "{{__('orders.delete-order')}}",
            text: "{{__('notifications.confirm_popup')}}",
            // icon: 'error',
            confirmButtonText: "{{__('notifications.yes')}}",
            cancelButtonText: "{{__('notifications.no')}}",
            showCancelButton: true,
            showCloseButton: true,

        }).then((result) => {
            if (result.isConfirmed) {
                var data = {
                    "_token": "{{ csrf_token() }}",
                    "id": delete_id,
                };
                $.ajax({
                    method: "DELETE",
                    url: '{{route ("orders.deleteOrder",0)}}' + delete_id,
                    data: data,
                    success: function(response) {
                        /* Read more about isConfirmed, isDenied below */
                        // Swal.fire('저장!', '', 'success').then(() => {
                            location.reload()
                        // });
                    },
                    error: function(err) {
                        // Swal.fire('변경 내용이 저장되지 않음', '', 'info');
                    }
                });
            }
        });
    });

    $('input[name="created_at"]').val('{{ request()->get('created_at') }}');


    </script>
@endsection
