@extends('backend.layouts.app')
@section('title')
@lang('notifications.noti-push')
@endsection
@section('content')
<div class="mb-4">
    {{-- @dump($errors) --}}
        <div class="backpage mb-5">
            <a href="{{route('notifications.index')}}" class="back btn"><i class="fas fa-chevron-left"></i></a>
        </div>
</div>
<div class="row  mb-0 d-flex justify-content-end">
    {{-- <div class="row m-0">
        <div>
            <img src="{{ static_asset('assets/img/icons/ava.png') }}" class="img-ava2 mr-2" alt="Search">
        </div>
        <div>
            <p class="mb-0 font-weight-800 font-size-18"><strong>#NOTI123</strong> </p>
            <p class="mb-1 font-weight-800 font-size-18"><strong>Hurry Up ! Winter sale are coming to town</strong></p>
            <button type="submit" class="btn btn-info ">@lang('notifications.active')</button>
        </div>
    </div> --}}
    <div class="all-BtnReset mb-3 ">
        {{-- <button type="submit" class="btn btn-info btn-push-noti">@lang('notifications.noti-push')</button> --}}
        <a href="javascript:void(0)" data-action="{{route('notifications.validated')}}" class="btn btn-info btn-push-noti">@lang('notifications.noti-push')</a>
    </div>
</div>
<div class="row mt-3 ml-0 mr-0">
    <div class="col-md-3 p-0 detail-content4">
        <button class="btn-create-members noti-info data-info p-0">
            <i class="fas fas-create-collection fa-plus-circle img-redo mr-2 check"></i>
            <i class="fas fa-check fas-create-collection checked text-success"></i>
            <p class="mt-3 font-weight-500">@lang('notifications.noti-information')</p>
        </button>
    </div>
    <div class="col-md-3 p-0 detail-content4">
        <button class="btn-create-members mark-notification-img p-0 data-image">
            <i class="fas fas-create-collection fa-plus-circle img-redo mr-2 check"></i>
            <i class="fas fa-check fas-create-collection checked text-success"></i>
            <p class="mt-3 font-weight-500">@lang('notifications.noti-images')</p>
        </button>
    </div>
    <div class="col-md p-0 detail-content4">
        <button class="btn-create-members noti-Description p-0 data-role">
            <i class="fas fas-create-collection fa-plus-circle img-redo mr-2 check"></i>
            <i class="fas fa-check fas-create-collection checked text-success "></i>
            <p class="mt-3 font-weight-500">@lang('notifications.noti-content')</p>
        </button>
    </div>
</div>

@endsection
@section('modal')
    <form action="{{route('notifications.store')}}" method="post" class="form-noti">
        @csrf
        @include('templates.popup.notification-img-create')
        @include('templates.popup.notifications-info',['edit'=>false])
        @include('templates.popup.notifications-description',['edit'=>false])
    </form>
@endsection

@section('script')
<script src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script type="text/javascript">

        $('#created_at').daterangepicker({
                singleDatePicker: true,
                drops:'up',
                showDropdowns: true,
                locale: {
                    format: 'YYYY/MM/DD',
                    applyLabel: "대다",
                    cancelLabel: "취소",
                    "monthNames":[
                        "일월" , // Tháng 1
                        "이월" , // Tháng 2
                        "삼월" , // Tháng 3
                        "사월" , // Tháng 4
                        "오월" , // Tháng 5
                        "유월" , // Tháng 6
                        "칠월" , // Tháng 7
                        "팔월" , // Tháng 8
                        "구월" , // Tháng 9
                        "시월" , // Tháng 10
                        "십일월" , // Tháng 11
                        "십이월" , // Tháng 12
                    ],
                    daysOfWeek:[
                        "월요일" ,// Thứ 2
                        "화요일" ,// Thứ 3
                        "수요일" ,// Thứ 4
                        "목요일" ,// Thứ 5
                        "금요일" ,// Thứ 6
                        "토요일" ,// Thứ 7
                        "일요일" ,//  Chủ nhật.
                    ]
                }
        });

        $('.noti-info').click(function() {
            let url = $(this).attr('href');
            $('.noti-infos').addClass('show');
        });
        $('.noti-Description').click(function() {
            let url = $(this).attr('href');
            $('.noti-Descriptions').addClass('show');
        });
        $('.mark-notification-img').click(function() {
            let url = $(this).attr('href');
            $('.mark-notifications-img').addClass('show');
        });
        //
        function myFunction() {
            document.getElementById("status6");
        }
        function myFunction() {
            document.getElementById("status7");
        } 

        $(document).on('click','.btn_addimg',function(){
            var url = $(this).data('action');
            const parent = $(this).parents('form');
            $.ajax({
                url : url,
                data: parent.serialize(),
                type : "POST",
                statusCode : {
                    200 : () => {
                        $('.'+$(this).data('text')).addClass('done');
                        parent.find('.mark-notifications-img .error').remove();
                    },
                    422 : ({responseJSON}) => {
                        parent.find('.mark-notifications-img .error').remove();
                        const e = Object.entries(responseJSON.errors);
                        // console.log(e);
                        for (const [key,val] of e) {
                            parent.find(`[name="${key}"]`).parents('.row').after(`
                                <div class="error">${val}</div>
                            `)
                        }
                    }
                },
                success : function(res) {
                    $('.mark-notifications-img').removeClass('show');
                },
                error : function(err) {
                    console.log(err)
                }
            })
        })

        $(document).on('click','.btn-create-info',function(){
                const parent = $(this).parents('form');
                $.ajax({
                    url : "{{ route('notifications.validateinfor') }}",
                    data: parent.serialize(),
                    type : "POST",
                    statusCode : {
                        200 : () => {
                            $('.'+$(this).data('text')).addClass('done');
                            parent.find('.noti-infos .error').remove();
                        },
                        422 : ({responseJSON}) => {
                            parent.find('.noti-infos .error').remove();
                            const e = Object.entries(responseJSON.errors);
                            for (const [key,val] of e) {
                                parent.find(`[name="${key}"]`).parents('.wrapper').after(`
                                    <div class="error">${val}</div>
                                `)
                            }
                        }
                    },
                    success : function(res) {
                        $('.noti-infos').removeClass('show');
                    },
                    error : function(err) {
                        console.log(err);
                    }
                })
                
            })

            $(document).on('click','.btn_adddescript',function(){
                const parent = $(this).parents('form');
                $.ajax({
                    url : "{{ route('notifications.validatedescript') }}",
                    data: parent.serialize(),
                    type : "POST",
                    statusCode : {
                        200 : () => {
                            $('.'+$(this).data('text')).addClass('done');
                            parent.find('.noti-Descriptions .error').remove();
                        },
                        422 : ({responseJSON}) => {
                            parent.find('.noti-Descriptions .error').remove();
                            const e = Object.entries(responseJSON.errors);
                            for (const [key,val] of e) {
                                parent.find(`[name="${key}"]`).after(`
                                    <div class="error">${val}</div>
                                `)
                            }
                        }
                    },
                    success : function(res) {;
                        console.log(res);
                        $('.noti-Descriptions').removeClass('show');
                    },
                    error : function(err) {
                        
                        console.log(err);
                    }
                })
            })
            $(document).on('click','.btn_cancel',function(){
                $('.file-preview-item').remove();
                $('.selected-files').val('');
            })
            $(document).on('click','.btn-push-noti',function(){
            var url = $(this).data('action');
            const parent = $(this).parents('body').find('.form-noti');
            $.ajax({
                url : url,
                data: parent.serialize(),
                type : "POST",
                statusCode : {
                    200 : () => {
                        $('.'+$(this).data('text')).addClass('done');
                        parent.find('.error').remove();
                    },
                    422 : ({responseJSON}) => {
                        parent.find('.error').remove();
                        const e = Object.entries(responseJSON.errors);
                        const arr_infor = ['title','type']
                        const arr_img = ['id_files'];
                        const arr_content = ['content'];
                        for (const [key,val] of e) {
                            if(arr_infor.includes(key)) {
                                $('.noti-infos').addClass('show');
                            }
                            if(arr_img.includes(key)) {
                                $('.mark-notifications-img').addClass('show');
                            }
                            if(arr_content.includes(key)) {
                                $('.noti-Descriptions').addClass('show');
                            }
                            if(key == 'type') {
                            parent.find(`[name="${key}"]`).parents('.dropdown').after(`
                            <div class="error">${val}</div>
                            `)
                            }else if(key == 'id_files') {
                                parent.find(`[name="${key}"]`).parents('.row').after(`
                                <div class="error">${val}</div>
                            `)
                            }else {
                                parent.find(`[name="${key}"]`).after(`
                                <div class="error">${val}</div>
                            `)
                            }
                            
                            
                        }
                    }
                },
                success : function(res) {
                    $('.form-noti').submit();
                },
                error : function(err) {
                    console.log(err)
                }
            })
            })
       
</script>

@endsection