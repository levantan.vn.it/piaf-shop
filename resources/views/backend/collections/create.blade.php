@extends('backend.layouts.app')
@section('title')
    @lang('collections.create')
@endsection
@section('content')
{{-- @dump($errors) --}}
<div class="mb-4">
    {{-- @dump($allproduct) --}}
        <div class="backpage mb-5">
            <a href="{{route('collections.index')}}" class="back btn"><i class="fas fa-chevron-left"></i></a>
        </div>
</div>
<div class="row m-0 ">
   <div class="icon-lock col-md">
    {{-- <button type="submit" class="btn-info btn btn-create-now">@lang('collections.create')</button> --}}
    <a href="javascript:void(0)" data-action="{{route('collections.validated')}}" class="btn-info btn btn-create-now">@lang('collections.create')</a>
   </div>
</div>

<div class="create-collection row mt-3">
    <div class="col-md-3 box_collect">
        <a href="javascript:void(0)" class="btn-create-collection edit-info-collect data-info">
            <i class="fas fas-create-collection fa-plus-circle img-redo mr-2 check"></i>
            <i class="fas fa-check fas-create-collection checked text-success"></i>
            <p class="mt-3">@lang('collections.add-cl-info')</p>
        </a>
    </div>
    <div class="col-md-3 box_collect">
        <a href="javascript:void(0)" class="btn-create-collection edit-images-collect data-image">
            <i class="fas fas-create-collection fa-plus-circle img-redo mr-2 check"></i>
            <i class="fas fa-check fas-create-collection checked text-success"></i>
            <p class="mt-3">@lang('collections.add-cl-img')</p>
        </a>
    </div>
    <div class="col-md box_collect">
        <a href="javascript:void(0)" class="btn-create-collection edit-product-collect add-product data-product" data-action="{{route('collections.create')}}">
            <i class="fas fas-create-collection fa-plus-circle img-redo mr-2 check"></i>
            <i class="fas fa-check fas-create-collection checked text-success"></i>
            <p class="mt-3">@lang('collections.add-product')</p>
        </a>
    </div>
</div>
@endsection
@section('modal')
<form action="{{route('collections.store')}}" method="post" class="form-collect">
@csrf
    @include('templates.popup.image-create-collection')
    @include('templates.popup.collection-info-popup',['edit' => false])
    {{-- @include('templates.popup.collection-product-popup',['edit' => false]) --}}
    @include('templates.popup.collection-product-create')

</form>
@endsection
@section('script')
<script src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script type="text/javascript">
        
        

        $('.edit-info-collect').click(function() {
            let url = $(this).attr('href');
            $('.edit-info-collection').addClass('show');
        });
        $('.edit-product-collect').click(function() {
            let url = $(this).attr('href');
            $('.edit-product-collection').addClass('show');
        });
        $('.edit-images-collect').click(function() {
            let url = $(this).attr('href');
            $('.edit-img-colls').addClass('show');
        });

        //Show image
        $('#input-file').change(function () {
            $('.label-image-add-col').before(`<img id="output" src="${window.URL.createObjectURL(this.files[0])}" width="120" height="120">'`);
        })

        $(document).on('click','.btn-search',function(){
            // $(this).parents('.modal-product').find('.all-product').removeClass('d-none');
            let brand_id = $('#brand_id').val();
            const url = $(this).data('action');
            const val = $('#search-product').val();
            $.get(url+'?search='+val,{brand_id: brand_id},function(data){
                $('#product-list').html(data)
            })
        })
        $(document).on('click','.add-product',function(){
            let brand_id = $('#brand_id').val();
            const url = $(this).data('action');
            const val = $('#search-product').val();
            $.get(url+'?search='+val,{brand_id: brand_id},function(data){
                $('#product-list').html(data)
            })
        })
        $(document).on('click','.btn_cancel',function(){
                $('.file-preview-item').remove();
                $('.selected-files').val('');
            })
        // $(document).on('change','.aiz-selectpicker',function(){
        //     console.log($(this).val())
        // })
        $(document).on('click','.btn-create-now',function(){
            var url = $(this).data('action');
            const parent = $(this).parents('body').find('.form-collect');
            $.ajax({
                url : url,
                data: parent.serialize(),
                type : "POST",
                statusCode : {
                    200 : () => {
                        $('.'+$(this).data('text')).addClass('done');
                        parent.find('.error').remove();
                    },
                    422 : ({responseJSON}) => {
                        parent.find('.error').remove();
                        const e = Object.entries(responseJSON.errors);
                        console.log(e);
                        const arr_infor = ['name','section','short_description','start_date','end_date']
                        const arr_img = ['id_files'];
                        // const arr_product = ['product'];
                        for (const [key,val] of e) {
                            if(arr_infor.includes(key)) {
                                $('.edit-info-collection').addClass('show');
                            }
                            if(arr_img.includes(key)) {
                                $('.edit-img-colls').addClass('show');
                            }
                            // if(arr_product.includes(key)) {
                            //     $('.edit-product-collection').addClass('show');
                            // }
                            if(key == 'section') {
                            parent.find(`[name="${key}"]`).parents('.dropdown').after(`
                            <div class="error">${val}</div>
                            `)
                            }else if(key == 'id_files') {
                                parent.find(`[name="${key}"]`).parents('.row').after(`
                                <div class="error">${val}</div>
                            `)
                            }else {
                                parent.find(`[name="${key}"]`).after(`
                                <div class="error">${val}</div>
                            `)
                            }
                            
                        }
                    }
                },
                success : function(res) {
                    $('.form-collect').submit();
                },
                error : function(err) {
                    console.log(err)
                }
            })
        })
        // $(document).on('click','#infor-reset1',function(){
        //         $('.edit-info-collection').removeClass('show');
        //         $('.form-reset')[0].reset();
        // })
        $(document).on('click','#infor-reset2',function(){
                $('.edit-product-collection').removeClass('show');
                $('.form-reset')[0].reset();
        })
        $(document).on('click','.btn_addproduct',function(){
            $('.'+$(this).data('text')).addClass('done');
        })
        $(document).on('click','.btn-addimg',function(){
            // $('.'+$(this).data('text')).addClass('done');
            var url = $(this).data('action');
            const parent = $(this).parents('form');
            $.ajax({
                url : url,
                data: parent.serialize(),
                type : "POST",
                statusCode : {
                    200 : () => {
                        $('.'+$(this).data('text')).addClass('done');
                        parent.find('.edit-img-colls .error').remove();
                    },
                    422 : ({responseJSON}) => {
                        parent.find('.edit-img-colls .error').remove();
                        const e = Object.entries(responseJSON.errors);
                        console.log(e);
                        for (const [key,val] of e) {
                                parent.find(`[name="${key}"]`).parents('.row').after(`
                                <div class="error">${val}</div>
                            `)
                            
                            
                        }
                    }
                },
                success : function(res) {
                    $('.edit-img-colls').removeClass('show');
                },
                error : function(err) {
                    console.log(err)
                }
            })

        })
        $('#start_date').daterangepicker({
            singleDatePicker: true,
            autoUpdateInput: false,
            showDropdowns: false,
            minDate: '1921/01/01',
            locale: {
            format: 'YYYY-MM-DD',
            applyLabel: "대다",
            cancelLabel: "취소",
                "monthNames":[
                    "일월" , // Tháng 1
                    "이월" , // Tháng 2
                    "삼월" , // Tháng 3
                    "사월" , // Tháng 4
                    "오월" , // Tháng 5
                    "유월" , // Tháng 6
                    "칠월" , // Tháng 7
                    "팔월" , // Tháng 8
                    "구월" , // Tháng 9
                    "시월" , // Tháng 10
                    "십일월" , // Tháng 11
                    "십이월" , // Tháng 12
                ],
                daysOfWeek:[
                    "월요일" ,// Thứ 2
                    "화요일" ,// Thứ 3
                    "수요일" ,// Thứ 4
                    "목요일" ,// Thứ 5
                    "금요일" ,// Thứ 6
                    "토요일" ,// Thứ 7
                    "일요일" ,//  Chủ nhật.
                ] 
            }
        });
        $('#start_date').on('apply.daterangepicker', function (ev, picker) {
            $(this).val(picker.startDate.format('YYYY/MM/DD'));
        });

        $('#start_date').on('cancel.daterangepicker', function (ev, picker) {
            $(this).val('');
        });

        // $('#start_date').val('{{request()->get('start_date')}}');
        $('#end_date').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            autoUpdateInput: false,
            minDate: '1921/01/01',
            locale: {
                format: 'YYYY-MM-DD',
            applyLabel: "대다",
            cancelLabel: "취소",
                "monthNames":[
                    "일월" , // Tháng 1
                    "이월" , // Tháng 2
                    "삼월" , // Tháng 3
                    "사월" , // Tháng 4
                    "오월" , // Tháng 5
                    "유월" , // Tháng 6
                    "칠월" , // Tháng 7
                    "팔월" , // Tháng 8
                    "구월" , // Tháng 9
                    "시월" , // Tháng 10
                    "십일월" , // Tháng 11
                    "십이월" , // Tháng 12
                ],
                daysOfWeek:[
                    "월요일" ,// Thứ 2
                    "화요일" ,// Thứ 3
                    "수요일" ,// Thứ 4
                    "목요일" ,// Thứ 5
                    "금요일" ,// Thứ 6
                    "토요일" ,// Thứ 7
                    "일요일" ,//  Chủ nhật.
                ] 
            }
        });

        $('#end_date').on('apply.daterangepicker', function (ev, picker) {
            $(this).val(picker.startDate.format('YYYY/MM/DD'));
        });

        $('#end_date').on('cancel.daterangepicker', function (ev, picker) {
            $(this).val('');
        });
        // $('#created_at').val('{{request()->get('created_at')}}');

        $(document).on('click','.btn-save-create',function(){
            var url = $(this).data('action');
            const parent = $(this).parents('form');
            $.ajax({
                url : url,
                data: parent.serialize(),
                type : "POST",
                statusCode : {
                    200 : () => {
                        $('.'+$(this).data('text')).addClass('done');
                        parent.find('.edit-info-collection .error').remove();
                    },
                    422 : ({responseJSON}) => {
                        parent.find('.edit-info-collection .error').remove();
                        const e = Object.entries(responseJSON.errors);
                        for (const [key,val] of e) {
                            if(key == 'section') {
                            parent.find(`[name="${key}"]`).parents('.dropdown').after(`
                            <div class="error">${val}</div>
                            `)
                            }else {
                                parent.find(`[name="${key}"]`).after(`
                                <div class="error">${val}</div>
                            `)
                            }
                            
                        }
                    }
                },
                success : function(res) {
                    $('.edit-info-collection').removeClass('show');
                },
                error : function(err) {
                    console.log(err)
                }
            })
        })


        
</script>

@endsection


