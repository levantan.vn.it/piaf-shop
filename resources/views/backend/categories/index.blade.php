@extends('backend.layouts.app')
@section('title')
@lang('categories.cate')
@endsection
@section('content')

<div class="aiz-titlebar text-left mt-2 mb-3">
    <div class="row align-items-center">
        <div class="col-md-6">
            <h1 class="h3 font-weight-bold">@lang('categories.cate')</h1>
        </div>
    </div>
    <div class="row mb-2 md-3">
        <form action="" class="col-md-9">
        <div >
            <input type="text" class="form-control" id="search" name="search" @isset($sort_search)
            value="{{ $sort_search }}" @endisset placeholder="@lang('categories.search-cate')">
        </div>
    </form>
    </div>
</div>

<div class="row gutters-10">
<div class="col">
    <div class="row-categories row m-0">
        <div>
            <h6 class="mb-0 fs-14">@lang('categories.pri-cate')</h6> <br>
        </div>
        <div>
            <div class="add-new-category">
                <button class="btn-add-categories btn-dark"><i class="fas fa-plus"></i></button>
            </div>
        </div>

    </div>
    <div>
        @if (!empty($categories) && count($categories))
            @foreach ($categories as $category)
                <div class="category  show-child-category-1 d-flex w-100 br-10 {{ $category->status == 2 ?'show-lock':''}} justify-content-between align-items-center">
                    <div class="d-flex justify-content-between align-items-center">
                        @php
                                $images = $category->images;
                        @endphp
                        @if(!empty($images)&& !empty($images[0]))
                        <img class="img-table-cate ml-2 " style="width:30px" src="{{ my_asset($category->images) }}" alt="">
                        @else
                        <img class="img-table-cate ml-2 " style="width:30px" src="/public/assets/img/placeholder.jpg" alt="">
                        @endif
                        <p class="mt-3 ml-2">{{ $category->title }}</p>
                    </div>
                    <div class="dropdown">
                        <button class="btn btn-drop" type="button" id="dropdownMenuButton" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-ellipsis-h"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item edit-categories" data-id_images="{{ $category->id_images }}" data-id_images-main="{{ $category->main_image_id }}" data-title='{{ $category->title }}'  data-images="{{ my_asset($category->images)}}" data-images-main="{{ my_asset($category->main_image)}}" href="{{ route('categories.editpri', $category->id) }}">@lang('categories.edit')</a>
                            <a class="dropdown-item lock-cate" data-status="{{ route('categories.status', $category) }}" data-statuss="{{ $category->status == 1 }}" href="#">{{ $category->status == 1 ?__('categories.lock'): __('categories.unlock')}}</a>
                            <form action="{{ route('categories.destroy',$category->id ) }}" method="GET" >
                            @csrf
                            <a class="dropdown-item categories-delete" data-delete="{{$category->id}}" href="#">@lang('categories.del')</a>
                        </form>
                        </div>

                    </div>
                    <div class="d-none list-child">
                    @if (!empty($category->childrenCategories) && count($category->childrenCategories))
                    @foreach ($category->childrenCategories as $child)
                        <div class="border-0 bg-white mb-1 d-flex w-100 br-10 justify-content-between align-items-center">
                            <div class="d-flex justify-content-between align-items-center">
                                <img class="img-table-cate ml-2" style="width:30px" src="{{ my_asset($child->images) }}" alt="">
                                <p class="mt-3 ml-2"> {{ $child->title }}</p>
                            </div>
                            <div class="dropdown">
                                <button class="btn btn-drop" type="button"  data-toggle="dropdown">
                                    <i class="fas fa-ellipsis-h"></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right" >
                                    <a class="dropdown-item show-cate" id="edit-child-categories" data-image-main="{{ my_asset($category->main_image)}}" data_image="{{ my_asset($category->images)}}" href="{{ route('categories.view',$child->id)}}">@lang('categories.edit')</a>
                                <form action="{{ route('categories.deletechild',$child->id ) }}" method="GET" >
                                    @csrf
                                    <a class="dropdown-item categories-child-delete" data-delete="{{$child->id}}" href="#">@lang('categories.del')</a>
                                </form>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
                    </div>
                </div>

            @endforeach
        @else
            <div class="text_nothing_found">
                <p>@lang('categories.nothing_found')</p>
            </div>
        @endif
    </div>
</div>
<div class="col-md-6">
    <div class="row-categories row m-0">
        <div>
            <h6 class="mb-0 fs-14">@lang('categories.child-cate')</h6> <br>
        </div>
        <div class="add-child-categories">
            <button class="btn-add-categories btn-dark"> <i class="fas fa-plus"></i></button>
        </div>
    </div>
    <div class="child-category">
    </div>
</div>

        <div class="aiz-pagination">
            {{ $categories->appends(request()->input())->links() }}
        </div>
    </div>
    {{-- <div class="edit-category popup-db">
    </div> --}}
@endsection


@section('modal')
    @include('modals.delete_modal')
    @include('templates.popup.new-category-popup')
    @include('templates.popup.edit-category-popup')
    @include('templates.popup.add-child-category')
    {{-- @include('templates.popup.category-edit-child') --}}
    <div class="edit-child-category popup-db">

    </div>
    @endsection


    @section('script')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script>
        function previewImg(data){
                return `<div class="d-flex justify-content-between align-items-center file-preview-item" data-id="${data.id_images}">
                    <div class="align-items-center align-self-stretch d-flex justify-content-center thumb">
                        <img src="${ data.imageUrl }" class="img-fit">
                    </div>
                    <div class="remove">
                    <button class="btn btn-sm btn-link remove-attachment" type="button"><i class="la la-close"></i></button>
                    </div>
                </div>`
        }

        function preview_Img(data){
            return `<div class="d-flex justify-content-between align-items-center file-preview-item" data-id-main="${data.main_image_id}">
                <div class="align-items-center align-self-stretch d-flex justify-content-center thumb">
                    <img src="${ data.imageUrlMain }" class="img-fit">
                </div>
                <div class="remove">
                <button class="btn btn-sm btn-link remove-attachment" type="button"><i class="la la-close"></i></button>
                </div>
            </div>`
        }

        function viewCate(url){
            $.ajax({
                url,
                method: "GET",
                dataType: "JSON",
                success: function(res){
                    const category = res.data;
                    console.log(category);
                    console.log(res);
                    $('.edit-child-category').html(res.view).addClass('show').find('.custom-kt-img .file-preview').append(previewImg(category));

                },
                error: function(err){
                    console.error(err);
                }
            })
        }


        $(document).on('click','.show-cate', function(){
            $(this).parents('.dropdown').find('.btn-drop').click();
            const url = $(this).attr('href');
            console.log(url);
            if(!url || !url.length) return false;
            viewCate(url);
            // $('.dropdown-child-cate').hide();
            return false;
    })

        $(function(){
            $('.category').click(function(){
                $('.child-category').html($(this).find('.list-child').html())
            })
        })

        $('.add-new-category').click(function() {
            let url = $(this).attr('href');
            $('.add-category').addClass('show');
            $('.file-preview').html('');
        });

        function loadCategory(id){

        }

        $('.edit-categories').click(function(e) {
            e.preventDefault()
            let url = $(this).attr('href');
            let name = $(this).attr('data-title');
            let images = $(this).attr('data-images');
            let id_images = $(this).attr('data-id_images');

            $('.edit-category .custom-image .file-preview.box.sm').html( previewImg({imageUrl:images, id_images}))
            $('.edit-category [name="title"]').val(name)
            $('.edit-category [name="id_files"]').val(id_images)
            $('.edit-category form').attr('action',url)
            $('.edit-category').addClass('show');
            let id = $(this).attr('data-id')
            $('.invalid-feedback').hide()
            $('.form-control').removeClass('is-invalid')
            loadCategory(id);
        });

        $('#edit-child-categories').click(function() {
            let url = $(this).attr('href');
            let images= $(this).attr('data-image');
            $('.edit-category .custom-kt-img .thumb img').attr('src',images);
            $('.edit-child-category').addClass('show');
        });



        $('.add-child-categories').click(function() {
            let url = $(this).attr('href');
            $('.add-child-category').addClass('show');
        });

        $('.show-child-category-1').click(function(e) {
            if (e.target !== this)
            return;
            $('.show-child-category-1').removeClass('show-hover-1');
            $(this).addClass('show-hover-1');
        });


        $(document).on('click', '.lock-cate', function (e) {
        e.preventDefault();
        const status = this.dataset.statuss;
        Swal.fire({
            title: status ==1?  '@lang('categories.unlock_pri')':'@lang('categories.lock_pri')' ,


            text: '@lang('categories.continue')',
            confirmButtonText: '@lang('categories.yes')',
            cancelButtonText: '@lang('categories.no')',
            showCancelButton: true,
            showCloseButton: true,
        })
        .then((res)=>{
                // $(this).text(res)
                if (res.isConfirmed) {
                    $.post(this.dataset.status).then((resspon)=>{
                        this.dataset.statuss = resspon.status;
                // console.log(resspon);
                        $(this).text(resspon.value)
                        // AIZ.plugins.notify(1, resspon.flash);
                        AIZ.plugins.notify('success', resspon.flash);
                        $(this).parents('.show-child-category-1').removeClass('show-hover-1').toggleClass('show-lock');
                    })
                }
            })
    });

        $(document).ready(function(){
            @error('CategoryRequest')
                document.querySelector('.add-category .add-child-category .edit-category .edit-child').classList.add('show');
            @enderror
	 	});

        // $(document).ready(function(){
        //     @error('CategoryRequest')
        //         document.querySelector('.add-child-category').classList.add('show');
        //     @enderror
	 	// });

        //  $(document).ready(function(){
        //     @error('EditCategoryRequest')
        //         document.querySelector('.edit-category').classList.add('show');
        //     @enderror
	 	// });
        //  $(document).ready(function(){
        //     @error('CategoryRequest')
        //         document.querySelector('.edit-child').classList.add('show');
        //     @enderror
	 	// });

         $(document).on('click','.check-form', function(){
            const parent = $(this).parents('form');
            console.log(parent.serialize());
            $('.error-popup').remove();
            $.ajax({
                url: "{{ route('categories.check-validate') }}",
                type: 'post',
                data: parent.serialize(),
                statusCode:{
                    422: ({responseJSON})=>{
                        parent.find('.is-invalid').removeClass('is-invalid');
                        parent.find('.invalid-feedback').remove();
                        const e = Object.entries(responseJSON.errors);
                        for (const [key,val] of e) {
                            parent.find(`[name="${key}"]`).addClass('is-invalid').after(`
                                <div class="invalid-feedback">${val}</div>
                            `)
                        }
                    }
                },
                success: function(res){
                    // console.log('res:', res);
                    $('.add-category').removeClass('show');
                    $('.edit-child').removeClass('show');
                    $('.edit-category').removeClass('show');
                    $('.add-child-category').removeClass('show');
                    parent.submit();
                },
                error:function(err){
                    console.error(err);
                }
            })

        });

        //Show image
        $('#input-file').change(function() {
            $('.label-image-add-cate').before(
                `<img id="output" src="${window.URL.createObjectURL(this.files[0])}" width="120" height="120">'`
            );
        })



    $(document).on('click', 'a.dropdown-item.categories-delete', function() {
        Swal.fire({
            title: '@lang('categories.delete_pri')',
            text: '@lang('categories.continue')',
            confirmButtonText: '@lang('categories.yes')',
            cancelButtonText: '@lang('categories.no')',
            showCancelButton: true,
            showCloseButton: true,
            })
            .then((result) => {
                if (result.isConfirmed) {
                    $(this).parent('form').submit()
                }
            })
    });

    $('.edit-primary').submit(function(){
        let id = $('.popup-products-edit .file-preview-item').attr('data-id');
        $(this).append(`<input type="hidden" name="id_file" value="${id}"/>`)
    });

    $(document).on('click', 'a.dropdown-item.categories-child-delete', function() {
        Swal.fire({
            title: '@lang('categories.delete_child')',
            text: '@lang('categories.continue')',
            confirmButtonText: '@lang('categories.yes')',
            cancelButtonText: '@lang('categories.no')',
            showCancelButton: true,
            showCloseButton: true,
            })
            .then((result) => {
                if (result.isConfirmed) {
                    $(this).parent('form').submit()
                }
            })
    });



    </script>
@endsection
