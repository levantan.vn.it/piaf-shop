<div class="supplier-add-imgs popup-db">
    <div class="popup-products-edit">
            <div class="popup-header">
                <a href="javascript:document.querySelector('.supplier-add-imgs').classList.remove('show')"
                    class="text-yl close-model">
                    <img src="{{ static_asset('assets/img/icons/x.svg') }}" class="" alt="X">
                </a>
            </div>
            <div class="parent_image_supplier">
                <div>
                    <div class="w-96 mb-3">
                        <b>@lang('suppliers.img-sup') &nbsp; <span class="text-danger" style="margin-top: -3px">*</span></b>
                    </div>
                    <div class="w-96 mb-3 col-12 children_image_supplier_1">
                        <div class="row" name="">
                            @include('backend.inc.upload_file',['values_file'=>old('id_files')])
                        </div>
                    </div>
                </div>
                @error('id_files')
                    <p class="error mt-1">{{ $message}}</p>
                @enderror
                <div>
                    <div class="w-96 mb-3">
                        <b>@lang('suppliers.main-img')</b>
                    </div>
                    <div class="w-96 mb-3 col-12 children_image_supplier_2">
                        <div class="row">
                            @include('backend.inc.upload_main_file',['values_file'=>'','type'=>'single'])
                        </div>
                    </div>
                </div>
                {{-- @error('id_main_files')
                    <p class="error mt-1">{{ $message}}</p>
                @enderror --}}
                <div class="all-btn-group-popup">
                    <a href="javascript:document.querySelector('.supplier-add-imgs').classList.remove('show')"class="mr-3 btn  br-10">@lang('suppliers.cancel')</a>
                    <a href="javascript:document.querySelector('.supplier-add-imgs').classList.remove('show')" class="btn btn-info br-10 click-upload-images">@lang('suppliers.save')</a>
                </div>
            </div>
    </div>

</div>
