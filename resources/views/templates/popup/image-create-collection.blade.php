<div class="edit-img-colls popup-db z-index-99">

    <div class="popup-products-edit">
            <input type="hidden" name="id" value="">
            <input type="hidden" name="entity" value="collection">
            <input type="hidden" name="route" value="collections.index">
            <div class="popup-header">
                <a href="javascript:document.querySelector('.edit-img-colls').classList.remove('show')"
                    class="text-yl close-model">
                    <img src="{{ static_asset('assets/img/icons/x.svg') }}" class="" alt="X">
  
                </a>
            </div>
  
            <div>
            <div>
                <div class="w-96 mb-3">
                    <b>@lang('collections.img-cl')</b>
                </div>
                <div class="w-96 mb-3 col-12">
                    <div class="row">
                        @include('backend.inc.upload_file',['values_file'=> '' ])
                    </div>
                </div>
            </div>
            <div>
                <div class="w-96 mb-3">
                    <b>@lang('collections.main-img')</b>
                </div>
                <div class="w-96 mb-3 col-12">
                    <div class="row">
                        @include('backend.inc.upload_main_file',['values_file'=> '','type'=>'single'])
                    </div>
                </div>
            </div>

            <div class="all-btn-group-popup">
                  <a href="javascript:document.querySelector('.edit-img-colls').classList.remove('show')"
                      class="mr-3 btn btn_cancel br-10">@lang('members.cancel')</a>
                  <a href="javascript:void(0)" data-action="{{route('collections.validate_image')}}"
                  data-text="data-image" class="btn btn-info br-10 btn-addimg">@lang('members.save')</a>
            </div>
            </div>
    </div>
  </div>
  