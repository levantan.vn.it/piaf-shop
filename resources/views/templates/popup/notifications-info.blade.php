@if($edit)
<form  id="form-mark-deceased" class="redirect-popup d-inline-block">
    @csrf
    @endif
    <div class="noti-infos popup-db">
        <div class="popup-products-edit">
            <div class="popup-header">
                <a href="javascript:document.querySelector('.noti-infos').classList.remove('show')" class="text-yl close-model">
                    <img src="{{ static_asset('assets/img/icons/x.svg') }}" class="" alt="X">

                </a>
            </div>
            <div class="row m-0 all-form-productid mb-3 ">
                @if($edit)
                <div class="w-48">
                    <p class="font-weight-800">@lang('notifications.id')</p>
                    <input type="text" class="form-control" id="" name="id" disabled value="#{{ optional($notification)->id }}">
                </div>
                @endif
                <div class="w-48">
                    <div class="wrapper">
                        <p class="font-weight-800">@lang('notifications.noti_title')<span class="required-icon"> *</span></p>
                        <input type="text" class="form-control" id="title" name="title" value="{{ $edit ?optional($notification)->title : old('title') }}">
                        @error('title')
                        <p class="error mt-1">{{ $message}}</p>
                        @enderror
                        <p class="error title_err"></p>
                    </div>
                </div>
            </div>
            <div class="row m-0 all-form-productid mb-3">
                <div class="w-48">
                    <div class="wrapper">
                        <p class="font-weight-800">@lang('notifications.type')<span class="required-icon"> *</span></p>
                        @if($edit)
                        <select class="form-control form-control-sm aiz-selectpicker mb-2 mb-md-0" id="type" name="type">
                            <option value="">{{ __('notifications.select_type') }}</option>
                            <option value="1" {{optional($notification)->type == '1' ? 'selected' : ''}} >{{ __('notifications.announcement') }}</option>
                            <option value="2" {{optional($notification)->type == '2' ? 'selected' : ''}}>{{ __('notifications.promotion') }}</option>
                            <option value="3" {{optional($notification)->type == '3' ? 'selected' : ''}}>{{ __('notifications.system') }}</option>
                        </select>
                        @else 
                        <select class="form-control form-control-sm aiz-selectpicker mb-2 mb-md-0" id="" name="type">
                            <option value="">{{ __('notifications.select_type') }}</option>
                            <option value="1"  >{{ __('notifications.announcement') }}</option>
                            <option value="2" >{{ __('notifications.promotion') }}</option>
                            <option value="3" >{{ __('notifications.system') }}</option>
                        </select>
                        @endif
                    </div>
                    @error('type')
                      <p class="error mt-1">{{ $message}}</p>
                    @enderror
                    <p class="error type_err"></p>
                </div>
                @if($edit)
                <div class="w-48">
                    <p class="font-weight-800">@lang('notifications.created')</p>
                    <input type="text" disabled value="{{date('Y-m-d',strtotime(optional($notification)->created_at))}}"
                     class="form-control mb-md-0" placeholder="YYYY/MM/DD" id="created_at" name="created_at">

                    @error('created_at')
                      <p class="error mt-1">{{ $message}}</p>
                    @enderror
                    <p class="error created_at_err"></p>
                </div>
                @endif
            </div>
            <div class="all-btn-group-popup">
                <a href="javascript:document.querySelector('.noti-infos').classList.remove('show')" class="mr-3 btn">@lang('notifications.btn_cancel')</a>
                @if($edit)
                <button type="button" class="btn btn-info btn_update_infor" data-action="{{route('notifications.infor',optional($notification)->id)}}">@lang('notifications.btn_save')</button>
                @else 
                <a href="javascript:void(0)" data-text="data-info" class="btn btn-info btn-create-info">@lang('notifications.btn_save')</a>
                @endif
            </div>
        </div>
    </div>
@if($edit)
</form>
@endif
