<div class="mark-add-deliveries z-index-99 popup-db">
    <div class="popup-products-edit">
        <div class="popup-header">
            <a href="javascript:document.querySelector('.mark-add-deliveries').classList.remove('show')" class="text-yl close-model">
                <img src="{{ static_asset('assets/img/icons/x.svg') }}" class="" alt="X">
            </a>
        </div>

        <div class="row m-0 all-form-productid mb-3">
            <div class="w-48">
                <p class="font-weight-800">@lang('orders.delivery_method') <span class="text-danger">*</span> </p>
                <select class="form-control form-control-sm aiz-selectpicker mb-2 mb-md-0" id="delivery_method" name="delivery_method">
                    <option value="">@lang('orders.nothing') </option>
                    @foreach ($order as $item)
                    <option value="{{$item->delivery_method}}" data-title="{{ $item->delivery_fee }}">{{$item->delivery_method}}</option>
                    @endforeach
                </select>
            </div>
            <div class="w-48">
                <p class="font-weight-800">@lang('orders.delivery_fee') <span class="text-danger">*</span> </p>
                <input readonly type="text" class="form-control" id="order_delivery_fee" value="{{optional($item->first())->delivery_fee}}" name="delivery_fee">
                @if($errors->has('delivery_fee'))
                <span class="errors">
                    <strong class="text-errors">{{ $errors->first('delivery_fee') }}</strong>
                </span>
                @endif
            </div>
        </div>
        <div class="all-btn-group-popup">
            <a href="javascript:document.querySelector('.mark-add-deliveries').classList.remove('show')" class="mr-3 btn  br-10">@lang('orders.cancel')</a>
            <a href="javascript:void(0)" class="mr-3 btn click-validateDelivery btn-info br-10" data-action="{{route('orders.validateDelivery')}}">@lang('orders.save')</a>
        </div>
    </div>
</div>