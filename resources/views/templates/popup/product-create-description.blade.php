<div class="product-addDescription z-index-9 popup-db">
   <div class="popup-products-edit br-10">

         <div class="popup-header">
            <a href="javascript:document.querySelector('.product-addDescription').classList.remove('show')"
               class="text-yl close-model">
               <img src="{{ static_asset('assets/img/icons/x.svg') }}" class="" alt="X">
            </a>
         </div> 
         <div class="w-96 mb-3 row m-0 all-form-productid">
            <b>@lang('products.pr-des')</b>
         </div>
         <div class="bg-popups">
            <div class="bg-popup-description">
               <textarea class="aiz-text-editor" id="description" name="description"></textarea>
            </div>
         </div>
            @error('description')
                      <p class="error mt-1">{{ $message}}</p>
            @enderror
         <div class="all-btn-group-popup">
            <a href="javascript:document.querySelector('.product-addDescription').classList.remove('show')"
               class="mr-3 btn br-10">@lang('suppliers.cancel')</a>
            <a href="javascript:void(0)" class="btn btn-info br-10 clickEditDescription" data-action="{{route('products.validateDescription')}}">@lang('suppliers.save')</a>
         </div>
   </div>
</div>
