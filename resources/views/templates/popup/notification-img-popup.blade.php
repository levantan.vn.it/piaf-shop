{{-- <form action="" method="post" id="form-mark-deceased" class="redirect-popup d-inline-block">
  @csrf
  <div class="mark-notifications-img popup-db">
    <div class="popup-products-edit">
      <div class="popup-header">
        <a href="javascript:document.querySelector('.mark-notifications-img').classList.remove('show')" class="text-yl close-model">
          <img src="{{ static_asset('assets/img/icons/x.svg') }}" class="" alt="X">

        </a>
      </div>


      <div class="w-48">
        <p class="font-weight-bold">@lang('notifications.noti-img') </p>
        <form action="/action_page.php">
          <div class="" id="upload-img">
            <label class="btn btn-add-image-notifications">
              <img id="test" src="/public/assets/img/cloud-upload-2.png" width="70" height="70">
              <input name="photo" type="file" accept="image/*" value="Upload Photo" id="input-file" style="width: 0px;height: 0px;overflow: hidden;" />
            </label>
          </div>

        </form>
      </div>
      <div class="all-btn-group-popup">
        <a href="javascript:document.querySelector('.mark-notifications-img').classList.remove('show')" class="mr-3 btn br-10">Cancel</a>

        <a href="javascripts:void(0)" class="btn btn-info br-10">Save</a>
      </div>
    </div>
  </div>
</form> --}}



<div class="mark-notifications-img popup-db">

  <div class="popup-products-edit">
      <form action="{{route('upload.update-image')}}" method="post" id="form-upload-image" class="redirect-popup">
          @csrf
          <input type="hidden" name="id" value="{{optional($notification)->id}}">
          <input type="hidden" name="entity" value="notification">
          <input type="hidden" name="route" value="notifications.detail">
          <div class="popup-header">
              <a href="javascript:document.querySelector('.mark-notifications-img').classList.remove('show')"
                  class="text-yl close-model">
                  <img src="{{ static_asset('assets/img/icons/x.svg') }}" class="" alt="X">

              </a>
          </div>

          <div>
              <div class="w-96 mb-3">
                <p class="font-weight-bold">@lang('notifications.noti-img') </p>
              </div>
              <div class="w-96 mb-3 col-12">
                  <div class="row">
                      @include('backend.inc.upload_file',['values_file'=> optional($notification)->id_images,'type' => 'single'])
                  </div>
              </div>
              <div class="all-btn-group-popup">
                <a href="javascript:document.querySelector('.mark-notifications-img').classList.remove('show')"
                    class="mr-3 btn  br-10">@lang('notifications.btn_cancel')</a>
                <a href="javascript:void(0)" class="btn btn-info br-10 btn_addimg" data-action="{{route('notifications.validate_image')}}">
                  @lang('notifications.btn_save')</a>
              </div>

          </div>
    </form>
  </div>
</div>
