<div class="members-changepw popup-db">
    <div class="popup-products-edit wrapper">
        <div class="popup-header">
            <a href="javascript:document.querySelector('.members-changepw').classList.remove('show')"
                class="text-yl close-model-mb">
                <img src="{{ static_asset('assets/img/icons/x.svg') }}" class="" alt="X">

            </a>
        </div>
        {{-- @dump($edit) --}}
        <h5 class="title mb-5 text-center">@lang('members.pw_title')</h5>
      <form action="{{route('members.change-password')}}" method="post" class="form-role">
            @csrf            
            <div class="form-group row">
                <label for="password" class="col-md-4 col-form-label text-md-right">@lang('members.current_pw') <span class="required-icon">*</span></label>

                <div class="col-md-6">
                    <input id="password" type="password" class="form-control" name="current_password" autocomplete="current-password">
                    @error('current_password')
                      <p class="error mt-1">{{ $message}}</p>
                    @enderror
                </div>
            </div>

            <div class="form-group row">
                <label for="password" class="col-md-4 col-form-label text-md-right">@lang('members.new_pw')<span class="required-icon">*</span></label>

                <div class="col-md-6">
                    <input id="new_password" type="password" class="form-control" name="new_password" autocomplete="current-password">
                    @error('new_password')
                      <p class="error mt-1">{{ $message}}</p>
                    @enderror
                </div>
            </div>

            <div class="form-group row">
                <label for="password" class="col-md-4 col-form-label text-md-right">@lang('members.confirm_pw')<span class="required-icon">*</span></label>

                <div class="col-md-6">
                    <input id="new_confirm_password" type="password" class="form-control" name="new_confirm_password" autocomplete="current-password">
                    @error('new_confirm_password')
                      <p class="error mt-1">{{ $message}}</p>
                    @enderror
                </div>
            </div>
            <div class="all-btn-group-popup">
                    <a href="javascript:document.querySelector('.members-changepw').classList.remove('show')"
                        class="mr-3 btn">@lang('members.cancel')</a>
                    <button type="submit" class="btn btn-info">@lang('members.save')</button>
                    
            </div>
        </form> 

    </div>
</div>
