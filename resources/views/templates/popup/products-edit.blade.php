@if($edit)
<form  method="POST" action="{{route('products.updateInfor',$products->id)}}">
    @csrf
@endif
<div class="mark-deceaseds popup-db">
    <div class="popup-products-edit popup-products-transform ">
        <div class="popup-header">
            <a href="javascript:document.querySelector('.mark-deceaseds').classList.remove('show')" class="text-yl close-model">
                <img src="{{ static_asset('assets/img/icons/x.svg') }}" class="" alt="X">
            </a>
        </div>
        <div class="custom-over custom-height">
            <div class="row m-0 all-form-productid mb-3 ">
                @if($edit)
                <div class="w-48">
                    <p class="fs-16">@lang('products.id')</p>
                    <input type="text" class="fs-16 form-control-value-number form-control" id="joined_date141" readonly value="#{{$products->id }}">
                </div>
                @endif
                <div class="w-48">
                    <p class="fs-16">@lang('products.title') <span class="text-danger">*</span></p>
                    <div>
                        <input type="text" class="fs-16 form-control-value-number form-control" id="joined_date2" name="name" value="{{$edit ? $products->name : old('title') }}">
                    </div>
                    @error('name')
                      <p class="error mt-1">{{ $message}}</p>
                    @enderror
                </div>
            </div>
            <div class="row m-0 all-form-productid mb-3">
                <div class="w-48 custom-category-dropdown">
                    <p class="fs-16">@lang('products.cate') <span class="text-danger">*</span></p>
                    <select class="fs-16 form-control form-control-sm aiz-selectpicker mb-2 mb-md-0" id="status3" name="category_id">
                        @foreach ($category as $item)
                        <option value="{{$item->id}}" @if($edit) @if (($products->category_id) == $item->id) selected @endif @endif >{{ optional($item)->title}}</option>
                        @endforeach
                    </select>
                    <!-- @if($errors->has('category_id'))
                        <span class="errors">
                            <strong class="text-errors">{{ $errors->first('category_id') }}</strong>
                        </span>
                    @endif -->
                    @error('category_id')
                      <p class="error mt-1">{{ $message}}</p>
                    @enderror
                </div>
                <div class="w-48 custom-supplier">
                    <p class="fs-16">@lang('products.sup') <span class="text-danger">*</span></p>
                    <select class="fs-16 form-control form-control-sm aiz-selectpicker mb-2 mb-md-0" id="supplier_id" name="supplier_id">
                        @foreach ($supplier as $item)
                        <option value="{{$item->id}}" @if($edit) @if (($products->supplier_id) == $item->id) selected @endif @endif >{{optional($item)->name}}</option>
                        @endforeach
                    </select>
                    @error('supplier_id')
                      <p class="error mt-1">{{ $message}}</p>
                    @enderror
                </div>
            </div>
            <div class="w-48 mb-3">
                <p class="fs-16">@lang('products.sku-format') <span class="text-danger">*</span></p>
                @php $skuFormat = "[상품ID]"; @endphp
                @if($edit)
                    @if( count($variants ?? []) )
                        <?php $variants = $products->parseVariant;  ?>
                        @foreach ($variants['title'] as $index=>$item)
                            @php $skuFormat .= "[".$item."]"; @endphp
                        @endforeach
                        <input type="text" class="fs-16 form-control mb-3" readonly value="{{$skuFormat}}">
                    @endif
                @else
                    <input type="text" class="fs-16 form-control mb-3" readonly value="{{$skuFormat}}">
                @endif
            </div>
            @if($edit)
            <div class="m-0 all-form-productid mb-3 ">
                <p class="fs-16">@lang('products.var') <span class="text-danger">*</span></p>
                <?php $variants = $products->parseVariant;
                ?>
                @if( count($variants ?? []) )
                <div class="row m-0 all-form-productid ">
                    <div class=" p-0  title-cusWidth " id="title-last-index" data-index="{{ count($variants['title']) }}">
                        @foreach ($variants['title'] as $index=>$item)
                            <div class="row m-0 count-row-variant">
                                <div class="col-md-4 row m-0 width-Variant p-0">
                                    <a href="javascript:void(0)" data-id="{{$index}}" class="btn-del btn-delete-now"><i class="fas fa-trash"></i></a>
                                    <input placeholder="@lang('products.value')" type="text" class="mb-3 fs-16 form-control-value form-control" name="variant[title][{{$index}}]" value="{{$item}}">
                                </div>
                                <div class="row m-0 col-md-8 p-0 variant-parent" id="variant-late{{$index}}" data-index="{{ count(($variants['value'][$index])?? []) }}">
                                    @foreach(($variants['value'][$index] ?? []) as $key=> $it)
                                    <div class="col-md-6 pr-0 mb-3 click-totalKey" data-key="{{$key}}">
                                        <input placeholder="@lang('products.value')" type="text" class="fs-16 form-control test4321" name="variant[value][{{$index}}][{{$key}}]" value="{{$it}}">
                                        <a href="javascript:void(0)" data-id="{{$index}}" class="btn-del btn-del-now"><i class="fas fa-trash"></i></a>
                                    </div>
                                    @endforeach
                                    @if($errors->has('variant'))
                                        <span class="errors">
                                            <strong class="text-errors">{{ $errors->first('variant') }}</strong>
                                        </span>
                                    @endif
                                    <div class="form-control-vari add-Column{{$index}}">
                                        <a href="javascript:void(0);" class="d-flex add-btnVariant mt-2" data-index="{{$index}}">
                                            <img src="{{ static_asset('assets/img/icons/plus1.png') }}" class="img-plus mr-2" alt="Search">
                                            <span class="fs-16">@lang('products.add-value')</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        @if($errors->has('variant[title][]'))
                            <span class="errors">
                                <strong class="text-errors">{{ $errors->first('variant[title][]') }}</strong>
                            </span>
                        @endif

                        
                    </div>
                </div>
                <div class="m-0 ">
                    <div class="form-control-vari2 add-Column3{{$index}}">
                        <a href="javascript:void(0);" class=" d-flex add-btnVariant3 mt-2 css-mr-2" data-index="{{$index}}">
                            <img src="{{ static_asset('assets/img/icons/plus1.png') }}" class="img-plus mr-2" alt="Search">
                            <span class="fs-16">@lang('products.add-variant')</span>
                        </a>
                    </div>
                </div>
                @error("variant.value.*.*")
                    <p class="error mt-1">{{ $message}}</p>
                @enderror
                @error("variant.title.*")
                    <p class="error mt-1">{{ $message}}</p>
                @enderror
            </div>
            @endif
            @else
            <div class="m-0 all-form-productid mb-3 row-my-product">
                <p class="fs-16">@lang('products.var') <span class="text-danger">*</span></p>
                <div class="row m-0 all-form-productid  ">
                    <div class=" p-0 " id="title-last-index-product" >
                        <div class="row m-0">
                            <div class="col-md-4 widthVariant  p-0">
                                <div>
                                     <input placeholder="@lang('products.value')" type="text" class="fs-16 form-control" name="variant[title][0]" value="">
                                </div>
                            </div>
                            <div class="row m-0 col-md-8 p-0 variant-parent " id="variant-late" >
                                    <div class="col-md-6 pr-0 mb-3 ">
                                        <div class="click-totalKey">
                                            <input placeholder="@lang('products.value')" type="text" class="fs-16 form-control value-variants" name="variant[value][0][0]" value="">
                                            <a href="javascript:void(0)"  class="btn-del btn-del-now"><i class="fas fa-trash"></i></a>
                                        </div>
                                    </div>
                                    <div class="form-control-vari add-Column">
                                                <a href="javascript:void(0);" class="d-flex add-btnVariant mt-2" >
                                                    <img src="{{ static_asset('assets/img/icons/plus1.png') }}" class="img-plus mr-2" alt="Search">
                                                    <span class="fs-16">@lang('products.add-value')</span>
                                                </a>
                                    </div>
                            </div>

                        </div>
                    </div>
                </div>
                @error("variant.value.*.*")
                    <p class="error mt-1">{{ $message}}</p>
                @enderror
                @error("variant.title.*")
                    <p class="error mt-1">{{ $message}}</p>
                @enderror
            </div>
            <div class="m-0 ">
                    <div class="form-control-vari2 add-Column39">
                        <a href="javascript:void(0);" class=" d-flex add-btnVariant37 mt-2 css-mr-2" ">
                            <img src="{{ static_asset('assets/img/icons/plus1.png') }}" class="img-plus mr-2" alt="Search">
                            <span class="fs-16">@lang('products.add-variant')</span>
                        </a>
                    </div>
            </div>
            @endif
            @if($edit)
            <div class="row m-0 all-form-productid mb-3 ">
                <div class="w-48">
                    <p class="fs-16">@lang('products.lasted') </p>
                    <input  type="text" readonly autocomplete="off" class="fs-16 form-control custom-placeholder"  name="updated_at" id="click_dateProductInfomation"  value="{{$edit? Date('Y/m/d', strtotime($products->updated_at)): old('updated_at') }}">
                    @error('updated_at')
                      <p class="error mt-1">{{ $message}}</p>
                    @enderror
                </div>
                <div class="w-48">
                    <p class="fs-16">@lang('products.created') </p>
                    <input  type="text" readonly autocomplete="off" class="fs-16 form-control custom-placeholder"  name="created_at" id="click_dateProInfomation"  value="{{$edit? Date('Y/m/d', strtotime($products->created_at)): old('created_at') }}">
                    @error('created_at')
                      <p class="error mt-1">{{ $message}}</p>
                    @enderror
                </div>
            </div>
            @endif
        </div>
        <div class="all-btn-group-popup">
            <a href="javascript:document.querySelector('.mark-deceaseds').classList.remove('show')" class="mr-3 btn ">@lang('banners.cancel')</a>
            @if($edit)
            <div class="redirect-popup d-inline-block">
                <button class="btn btn-info" type="submit">@lang('banners.save')</button>
            </div>
            @else
            <a href="javascript:void(0)" class="btn btn-info clickvalidateProductInfor" data-action="{{route('products.validateDetailProduct')}}">@lang('banners.save')</a>
            @endif
        </div>
    </div>
</div>
@if($edit)
</form>
@endif
