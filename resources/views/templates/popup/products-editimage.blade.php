<div class="mark-edit-imgs popup-db">
    <div class="popup-products-edit">
        <form action="{{route('products.updateImageProduct',$products->id)}}" method="post" class="redirect-popup">
            @csrf
            <input type="hidden" name="id" value="{{$products->id}}">
            <input type="hidden" name="entity" value="product">
            <input type="hidden" name="route" value="products.detail">
            <div class="popup-header">
                <a href="javascript:document.querySelector('.mark-edit-imgs').classList.remove('show')" class="text-yl close-model">
                    <img src="{{ static_asset('assets/img/icons/x.svg') }}" class="" alt="X">

                </a>
            </div>
            <div>
                <div class="w-96 mb-3">
                    <b>@lang('products.pr-img')</b>
                </div>
                <div class="w-96 mb-3 col-12">
                    <div class="row">
                        @include('backend.inc.upload_file',['values_file'=>$products->id_images])
                    </div>
                </div>
                @error('id_files')
                <p class="error mt-1">{{ $message}}</p>
                @enderror
                <div class="all-btn-group-popup">
                    <a href="javascript:document.querySelector('.mark-edit-imgs').classList.remove('show')" class="mr-3 btn br-10">@lang('banners.cancel')</a>
                    <button type="submit" class="btn btn-info br-10">@lang('banners.save')</button>
                </div>
            </div>
        </form>
    </div>
</div>