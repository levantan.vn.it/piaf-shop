<div class="edit-product-collection popup-db z-index-9">
    <div class="popup-products-edit">
        <div class="popup-header">
            <a href="javascript:document.querySelector('.edit-product-collection').classList.remove('show')"
                class="text-yl close-model close-model-product">
                <img src="{{ static_asset('assets/img/icons/x.svg') }}" class="" alt="X">

            </a>
        </div>
        <div class="modal-product">
            <div class="search-bar">
                <input type="text" class="form-control " id="search-product" name="search" placeholder="@lang('collections.search-pro')">
                <button type="button" class="btn-search" data-action="{{route('collections.create')}}">
                    <img width="20px" src="{{static_asset('assets/img/icons/search_icon.svg')}}" alt="">
                </button>
            </div>
            <div class="all-product">
                <form action="" method="post" >
                    @csrf
                    <div id="product-list">
                        {{-- <div class="list-product">
                            @if(count($allproduct) && !empty($allproduct))
                            @foreach ($allproduct as $produ)
                                <div class="overflow-auto mt-2">
                                    @if(!empty($produ->minPriceSku))
                                    <div class="d-flex bd-highlight product_items align-items-center" data-supplier={{$produ->supplier_id ? $produ->supplier_id : "" }}>
                                            <input type="checkbox" name="product[]"  class="mr-1" value="{{$produ->id}}" data-id="{{$produ->id}}">
                                            <div class="p-2 bd-highlight">
                                                @if($produ->images)
                                                    @php
                                                        $pro_img = explode(',',$produ->images);
                                                    @endphp
                                                    <img class="img-table mr-2" src="{{static_asset($pro_img[0])}}" alt="img" >
                                                @else 
                                                    <img class="img-table mr-2" src="{{static_asset('/assets/img/placeholder.jpg')}}" alt="img" >
                                                @endif
                                            </div>
                                            <div class="p-2 bd-highlight">
                                                <table>
                                                    <tbody>
                                                        <tr>
                                                            <td>SKU: <b>{{$produ->minPriceSku}}</b></td>
                                                        </tr>
                                                        <tr>
                                                            <td>{{$produ->name}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>{{format_price($produ->minPrice)}}원</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                    </div>
                                    @endif
                                </div>
                            @endforeach
                             @endif
                        </div>
                        <div class="btn-add d-flex justify-content-end mt-3 ">
                            <input type="reset" id="infor-reset2" class="btn-reset-infor mr-3 btn" value="{{__('collections.cancel')}}">
                            <a href="javascript:document.querySelector('.edit-product-collection').classList.remove('show')" data-text="data-product" class="btn btn-save btn-info btn_addproduct">{{__('collections.save')}}</a>
                        </div> --}}
                    </div>
                </form>
                
                
            </div>
            
        </div>

    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
