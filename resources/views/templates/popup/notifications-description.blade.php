@if($edit)
<form  id="form-mark-deceased" class="redirect-popup d-inline-block">
    @csrf
    @endif

    <div class="noti-Descriptions popup-db z-index-9">
        <div class="popup-products-edit">
            <div class="popup-header">
                <a href="javascript:document.querySelector('.noti-Descriptions').classList.remove('show')" class="text-yl close-model">
                    <img src="{{ static_asset('assets/img/icons/x.svg') }}" class="" alt="X">

                </a>
            </div>
            <div class="row m-0 all-form-productid mb-3 ">
                <div class="w-48">
                    <p class="font-weight-800 m-0">@lang('notifications.noti-des') <span class="required-icon"> *</span></p>
                </div>
            </div>
            <div class="">
                <textarea class="noti-PopupDes" name="content" id="content">{{$edit ? optional($notification)->content : old('content')}}</textarea>
                @error('content')
                      <p class="error mt-1">{{ $message}}</p>
                @enderror
                <p class="error content_err"></p>
            </div>

            <div class="all-btn-group-popup">
                <a href="javascript:document.querySelector('.noti-Descriptions').classList.remove('show')" class="mr-3 btn">@lang('notifications.btn_cancel')</a>
                @if($edit)
                <button type="button" class="btn btn-info btn_update_descript" data-action="{{route('notifications.descript',optional($notification)->id)}}">@lang('notifications.btn_save')</button>
                @else 
                <a href="javascript:void(0)"  data-text="data-role" class="mr-3 btn btn-info btn_adddescript">@lang('notifications.btn_save')</a>
                @endif
            </div>
        </div>
    </div>
@if($edit)
</form>
@endif