
<div class="supplier-add-information popup-db">
    <div class="popup-products-edit popup-supplier-add-information">
        <div class="popup-header">
            <a href="javascript:document.querySelector('.supplier-add-information').classList.remove('show')"
                class="text-yl close-model">
                <img src="{{ static_asset('assets/img/icons/x.svg') }}" class="" alt="X">
            </a>
        </div>
        <div class="row m-0 all-form-productid mb-3 ">
            <div class="w-48">
                <label class="font-weight-800"> @lang('suppliers.id')</label>
                <input type="text" class="form-control " readonly  placeholder="@lang('suppliers.id')">
            </div>
            <div class="w-48">
                <label class="font-weight-800 d-flex"> @lang('suppliers.sup-name')&nbsp; <span class="text-danger" style="margin-top: -3px">*</span></label>
                <input type="text" class="form-control supplier-col @error('name') is-invalid  @enderror" maxlength="255" id="name" name="name" value="{{ old('name') }}" placeholder="@lang('suppliers.add-sup-name')">
                @error('name')
                <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
        </div>
        <div class="row m-0 all-form-productid mb-3 ">
            <div class="w-48">
                <label class="font-weight-800 d-flex">  @lang('suppliers.sup-address')&nbsp; <span class="text-danger" style="margin-top: -3px">*</span></label>
                <input type="text" class="form-control supplier-col @error('address') is-invalid  @enderror" maxlength="255" id="address" name="address" value="{{ old('address') }}" placeholder="@lang('suppliers.add-sup-address')">
                @error('address')
                <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
            <div class="w-48">
                <label class="font-weight-800 d-flex">  @lang('suppliers.location')&nbsp; <span class="text-danger" style="margin-top: -3px">*</span></label>
                <input type="text" class="form-control supplier-col @error('location') is-invalid  @enderror"  maxlength="255" id="location" name="location" value="{{ old('location') }}" placeholder="@lang('suppliers.add-sup-loc')">
                @error('location')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
        </div>
        <div class="row m-0 all-form-productid mb-3 ">
            <div class="w-48">
                <label class="font-weight-800 d-flex">  @lang('suppliers.email')&nbsp; <span class="text-danger" style="margin-top: -3px">*</span></label>
                <input type="email" class="form-control supplier-col @error('email') is-invalid  @enderror" id="email"  name="email" value="{{ old('email') }}" placeholder="@lang('suppliers.add-sup-email')">
                @error('email')
                <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
            <div class="w-48">
                <label class="font-weight-800 d-flex">  @lang('suppliers.sup-phone')&nbsp; <span class="text-danger" style="margin-top: -3px">*</span></label>
                <input type="tel" class="form-control supplier-col @error('phone') is-invalid  @enderror" maxlength="20" id="phone" name="phone" value="{{ old('phone') }}" placeholder="@lang('suppliers.add-sup-phone')">
                @error('phone')
                <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
        </div>

        <div class="row m-0 all-form-productid mb-3 ">
            <div class="w-48">
                <label class="font-weight-800 d-flex"> @lang('suppliers.sup-rep')&nbsp; <span class="text-danger" style="margin-top: -3px">*</span></label>
                <input type="text" class="form-control supplier-col @error('representative') is-invalid  @enderror" maxlength="255" id="representative" name="representative" value="{{ old('representative') }}" placeholder="@lang("suppliers.add-sup-rep")">
                @error('representative')
                <div class="invalid-feedback">{{ $message }}</div>
                 @enderror
            </div>
            <div class="w-48">
                <label class="font-weight-800"> @lang('suppliers.j-date')</label>
                <input type="text"  class="form-control supplier-col" value="{{ date('Y-m-d') }}" readonly  placeholder="@lang('suppliers.j-date')">
            </div>
        </div>

        <div class="all-btn-group-popup">
            <a href="javascript:document.querySelector('.supplier-add-information').classList.remove('show')" class="mr-3 btn  br-10">@lang('orders.cancel')</a>
            <a href="javascript:void(0)" class="mr-3 btn click-validateDelivery btn-info br-10" data-action="{{route('suppliers.validateInfo')}}">@lang('orders.save')</a>
                <!-- <a href="javascript:document.querySelector('.supplier-add-information').classList.remove('show')" class="mr-3 btn  br-10" >@lang('suppliers.cancel')</a>
                 <a href="javascript:document.querySelector('.supplier-add-information').classList.remove('show')" class="btn btn-info br-10 check-form">@lang('suppliers.save')</button> -->
        </div>
    </div>
</div>
