<div class="edit-img-colls popup-db">

  <div class="popup-products-edit">
      @if($edit)
      <form action="{{route('upload.update-image')}}" method="post" id="form-image-upload" class="redirect-popup">
          @csrf
          <input type="hidden" name="id" value="{{$edit ? $data->id : ''}}">
          <input type="hidden" name="entity" value="collection">
          <input type="hidden" name="route" value="collections.information">
        @endif
          <div class="popup-header">
              <a href="javascript:document.querySelector('.edit-img-colls').classList.remove('show')"
                  class="text-yl close-model">
                  <img src="{{ static_asset('assets/img/icons/x.svg') }}" class="" alt="X">
              </a>
          </div>
          <div>
              <div>
                  <div class="w-96 mb-3">
                      <b>@lang('collections.img-cl')</b>
                  </div>
                  <div class="w-96 mb-3 col-12">
                      <div class="row">
                          @include('backend.inc.upload_file',['values_file'=> $edit ? $data->id_images : '' ])
                      </div>
                  </div>
              </div>
              <div>
                  <div class="w-96 mb-3">
                      <b>@lang('collections.main-img')</b>
                  </div>
                  <div class="w-96 mb-3 col-12">
                      <div class="row">
                          @include('backend.inc.upload_main_file',['values_file'=> $edit ? $data->main_image_id : '','type'=>'single' ])
                      </div>
                  </div>
              </div>
              @if($edit)
              <div class="all-btn-group-popup">
                <a href="javascript:document.querySelector('.edit-img-colls').classList.remove('show')"
                    class="mr-3 btn  br-10">@lang('suppliers.cancel')</a>
                <a href="javascript:void(0)" class="btn btn-info br-10 btn-addimg" data-action="{{route('collections.validate_image')}}">@lang('suppliers.save')</a>
              </div>
              @else
              <div class="all-btn-group-popup">
                <a href="javascript:document.querySelector('.edit-img-colls').classList.remove('show')"
                    class="mr-3 btn  br-10">@lang('suppliers.cancel')</a>
                <a href="javascript:document.querySelector('.edit-img-colls').classList.remove('show')"
                data-text="data-image" class="btn btn-info br-10 btn-addimg">@lang('suppliers.save')</a>
            </div>
            @endif
          </div>
      @if($edit)
    </form>
    @endif
  </div>
</div>
