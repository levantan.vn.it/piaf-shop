<div class="send-invoices popup-db">
    <div class="popup-products-edit br-10">
        <form action="{{route('orders.updateImgReason',$order->id)}}" enctype="multipart/form-data" method="post" id="form-send-invoices" class="">
            @csrf
            <input type="hidden" name="id" value="{{$order->id}}">
            <input type="hidden" name="entity" value="order">
            <input type="hidden" name="route" value="orders.detail">
            <div class="popup-header">
                <a href="javascript:document.querySelector('.send-invoices').classList.remove('show')"
                    class="text-yl close-model">
                    <img src="{{ static_asset('assets/img/icons/x.svg') }}" class="" alt="X">
                </a>
            </div>
            <div>
                <div class="row">
                    <div class="ml-3">
                        <div class="w-96 mb-3 css-p-10">
                            <b>@lang('orders.tax-invoice')</b>
                        </div>
                        <div class="w-96 mb-3 col-12">
                            <div class="row">
                                @include('backend.inc.upload_file_shipping',['values_file'=>$order->invoice_id_images])
                            </div>
                        </div>
                    </div>
                    <div class="ml-3">
                        <div class="w-96 mb-3 css-p-10">
                            <b>@lang('orders.shipping')</b>
                        </div>
                        <div class="w-96 mb-3 col-12">
                            <div class="row">
                                @include('backend.inc.upload_file_pdf',['values_file'=>$order->id_upload_file_pdf])
                            </div>
                        </div>
                    </div>
                </div>
                <div class="all-btn-group-popup">
                    <a href="javascript:document.querySelector('.send-invoices').classList.remove('show')"
                        class="mr-3 btn  br-10">@lang('suppliers.cancel')</a>
                    <button type="submit" class="btn btn-info br-10">@lang('suppliers.save')</button>
                </div>
            </div>
        </form>
    </div>
</div>