<div class="order-add-products z-index-98 popup-db">
    <div class="popup-products-edit popup-add-product">
        <div class="popup-header">
            <a href="javascript:document.querySelector('.order-add-products').classList.remove('show')" class="text-yl close-model close-model-product">
                <img src="{{ static_asset('assets/img/icons/x.svg') }}" class="" alt="X">
            </a>
        </div>
        <div class="modal-product">
            <div class="search-bar test">
                <input type="text" class="form-control " id="search-product" value="{{request()->get('search')}}" name="search" placeholder="@lang('collections.search-pro')">

                <button type="button" class="btn-search" data-action="{{route('orders.create')}}">
                    <img width="20px" src="{{static_asset('assets/img/icons/search_icon.svg')}}" alt="">
                </button>
            </div>
            <div class="all-product test">
                <form action="" method="post">
                    @csrf
                    <div id="product-list">
                        <div class=" custom-listProduct">
                            @if(count($allproduct) && !empty($allproduct))
                            @foreach ($allproduct as $produ)
                            <div class="overflow-auto mt-2">
                                <div class="d-flex bd-highlight align-items-center">
                                    <input type="checkbox" name="id[]"   class="mr-1 select-input" value="{{$produ->id}}" data-id="{{$produ->id}}">
                                    <div class="p-2 bd-highlight">
                                        @if(optional($produ->product)->images)
                                        @php
                                        $pro_img = explode(',',optional($produ->product)->images);
                                        @endphp
                                        <img class="img-table mr-2" src="{{static_asset($pro_img[0])}}" alt="img">
                                        @else
                                        <img class="img-table mr-2" src="{{static_asset('/assets/img/placeholder.jpg')}}" alt="img">
                                        @endif
                                    </div>
                                    <div class="p-2 bd-highlight">
                                        <table>
                                            <tbody>
                                                <tr>
                                                    <td>SKU: <b>{{$produ->sku}}</b></td>
                                                </tr>
                                                <tr>
                                                    <td>{{optional($produ->product)->name}}</td>
                                                </tr>
                                                <tr>
                                                    <td>{{format_price($produ->price)}}원</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
           
                                </div>
                            </div>
                            @endforeach
                            @endif
                        </div>
                        @error('id')
                            <p class="error mt-1">{{ $message}}</p>
                        @enderror
                    </div>
                        <div class="btn-add d-flex justify-content-end mt-3 ">
                            <a href="javascript:document.querySelector('.order-add-products').classList.remove('show')" class="mr-3 btn  br-10">@lang('orders.cancel')</a>
                            <a data-action="{{route('orders.validateProduct')}}" href="javascript:void(0)" class="btn btn-save btn-info clickAddProduct">@lang('orders.save')</a>
                        </div>
                </form>
            </div>


        </div>

    </div>
</div>
</div>
</div>
</div>
</div>