<form action="{{route ('orders.updateReasonCancel',$order->id)}}" method="post">
    @csrf
    <div class="mark-add-reason-cancel popup-db">
      <div class="popup-products-edit">
          <div class="popup-header">
              <a href="javascript:document.querySelector('.mark-add-reason-cancel').classList.remove('show')"
                  class="text-yl close-model">
                  <img src="{{ static_asset('assets/img/icons/x.svg') }}" class="" alt="X">
    
              </a>
          </div>
        
          <div class="row m-0 all-form-productid mb-3">

              <div class="w-48">
                <p class="font-weight-800">@lang('orders.reason_cancel') <span class="text-danger">*</span> </p>
                <input type="text" class="form-control" id="order_reason_cancel" value="{{optional($order)->reason_cancel}}" name="reason_cancel" >
                @if($errors->has('reason_cancel'))
                    <span class="errors">
                        <strong class="text-errors">{{$errors->first('reason_cancel')}}</strong>
                    </span>
                @endif
            </div>
          </div>
          <div class="all-btn-group-popup">
                <a href="javascript:document.querySelector('.mark-add-reason-cancel').classList.remove('show')"
                      class="mr-3 btn  br-10">@lang('orders.cancel')</a>
                <button type="submit" class="mr-3 btn btn-info br-10">@lang('orders.save')</a>
          </div>
      </div>
    </div>
</form>
