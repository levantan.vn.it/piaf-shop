<div class="mark-banners-img z-index-99 popup-db">
    <div class="popup-products-edit">

        <input type="hidden" name="id" value="">
        <input type="hidden" name="entity" value="banner">
        <input type="hidden" name="route" value="banners.index">
        <div class="popup-header">
            <a href="javascript:document.querySelector('.mark-banners-img').classList.remove('show')" class="text-yl close-model">
                <img src="{{ static_asset('assets/img/icons/x.svg') }}" class="" alt="X">
            </a>
        </div>

        <div>
            <b>@lang('banners.bn_img') </b>
        </div>
        <div class="banner-img-title mt-3">
            <div class="kt-img row">
                @include('backend.inc.upload_file',['values_file'=>'','type'=>'single'])
            </div>
            @error('id_files')
            <p class="error mt-1">{{$message}}</p>
            @enderror
        </div>
        <div class="all-btn-group-popup">
            <a href="javascript:document.querySelector('.mark-banners-img').classList.remove('show')" class="mr-3 btn br-10">@lang('banners.cancel')</a>
            <a href="javascript:void(0)" class="mr-3 btn br-10 btn-info clickImgAddBanner">@lang('banners.save')</a>
        </div>
        </form>
    </div>
</div>