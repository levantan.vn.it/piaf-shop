@if($edit)
<form action="{{route('banners.updateInfor',$banners->id)}}" id="change-banner" name="changebanner" method="POST">
    @csrf
@endif
    <div class="banner-infos popup-db">
        <div class="popup-products-edit">
            <div class="popup-header">
                <a href="javascript:document.querySelector('.banner-infos').classList.remove('show')"
                    class="text-yl close-model">
                    <img src="{{ static_asset('assets/img/icons/x.svg') }}" class="" alt="X">
    
                </a>
            </div>
            <div class="row m-0 all-form-productid mb-3 ">
                @if($edit)
                <div class="w-48">
                    <p class="font-weight-800">@lang('banners.id')</p>
                    <input type="text" class="form-control" id="joined_date13" readonly name="joined_date13" value="#{{$banners->id }}">
                </div>
                @endif
                <div class="w-48">
                    <p class="font-weight-800">@lang('banners.pos') <span class="text-danger">*</span></p>
                    <select class="form-control form-control-sm aiz-selectpicker mb-2 mb-md-0" id="status5" name="position">
                            @foreach(App\Banner::POSITIONS as $view => $value)
                            <option value="{{ $value }}">{{ $view }}</option>
                            @endforeach
                    </select>
                    @error('position')
                      <p class="error mt-1">{{ $message}}</p>
                    @enderror
                </div>
            </div>
            <div class="row m-0 all-form-productid mb-3">
                <div class="w-48">
                    <p class="font-weight-800">@lang('banners.start') <span class="text-danger">*</span></p>
                    <input type="text" autocomplete="off" class="form-control custom-placeholder"  name="start_date" id="status6"  value="{{ $edit ?Date('Y-m-d',strtotime($banners->start_date)): old('start_date') }}">

                    @error('start_date')
                      <p class="error mt-1">{{ $message}}</p>
                    @enderror
                </div>
                <div class="w-48">
                    <p class="font-weight-800">@lang('banners.end') <span class="text-danger">*</span></p>

                    <input type="text" autocomplete="off" class="form-control custom-placeholder"  name="end_date" id="status7"  value="{{ $edit?Date('Y-m-d',strtotime($banners->end_date)): old('end_date') }}">

        
                    @error('end_date')
                      <p class="error mt-1">{{ $message}}</p>
                    @enderror
                </div>
            </div>
            <div class="all-btn-group-popup">
                    <a href="javascript:document.querySelector('.banner-infos').classList.remove('show')" class="mr-3 btn ">@lang('banners.cancel')</a>
                    @if($edit)
                    <button type="submit" class="mr-3 btn btn-info">@lang('banners.save')</button>
                    @else
                    <!-- <button type="button" class="mr-3 btn btn-info checkInfoBanner ">@lang('banners.save')</button> -->
                    <a class="mr-3 btn btn-info clickvalidateBannerInfor" href="javascript:void(0)" data-action="{{route('banners.validateBannerInfor')}}">@lang('banners.save')</a>
                    @endif
            </div>
        </div>
    </div>
@if($edit)
</form>
@endif

