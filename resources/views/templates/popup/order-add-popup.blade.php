
    <div class="mark-add-orders popup-db">
      <div class="popup-products-edit">
          <div class="popup-header">
              <a href="javascript:document.querySelector('.mark-add-orders').classList.remove('show')"
                  class="text-yl close-model">
                  <img src="{{ static_asset('assets/img/icons/x.svg') }}" class="" alt="X">
              </a>
          </div>
        
          <div class="row m-0 all-form-productid mb-3">
              <div class="w-48 custom-select2">
                  <p class="font-weight-800">@lang('orders.us_id') <span class="text-danger">*</span> </p>
                  <!-- <input type="text" id="user_id" name="user_id" class="form-control form-control-sm aiz-selectpicker mb-2 mb-md-0" value=""> -->
                  <select class="form-control form-control-sm aiz-selectpicker mb-2 mb-md-0 js-example-basic-single"  id="order_userId3" name="user_id">
                    <!-- <option value="">{{ translate('선택 옵션') }}</option> -->
                      @foreach($users as $item)
                      <option value="{{$item->id}}" class="option-userid" data-title="{{$item->displayname}}">#{{$item->id}}</option>
                      @endforeach
                      @error('user_id')
                      <p class="error mt-1">{{ $message}}</p>
                        @enderror
                  </select>
              </div>
              <div class="w-48 click-get-delivevry">
                   <p class="font-weight-800">@lang('orders.delivery_bookk') <span class="text-danger">*</span>  </p>
                   <input type="text" hidden value="" class="get-value-address" name="delivery_address">
                  <!-- <select  class="form-control  form-control-sm aiz-selectpicker mb-2 mb-md-0" id="delivery_address" name="delivery_address">
                      <option value="" selected>@lang('orders.nothing')</option>
                  </select> -->
                  <select  class="form-control  form-control-sm aiz-selectpicker mb-2 mb-md-0" id="delivery_address" name="">
                      <option value="" selected>@lang('orders.nothing')</option>
                  </select>
                  @error('delivery_address')
                      <p class="error mt-1">{{ $message}}</p>
                  @enderror
              </div>
          </div>
          <div class="row m-0 all-form-productid mb-3 ">
            <div class="w-48">
                <p class="font-weight-800">@lang('orders.user_namee') <span class="text-danger">*</span>  </p>
                <input type="text" class="form-control" id="order-userName" value=""  name="displayname" placeholder=""  >
                @error('displayname')
                      <p class="error mt-1">{{ $message}}</p>
                @enderror
            </div>
            <div class="w-48">
              <p class="font-weight-800">@lang('orders.receiverr') <span class="text-danger">*</span> </p>
              <input type="text" class="form-control" id="receiver" name="receiver" >
              @error('receiver')
                      <p class="error mt-1">{{ $message}}</p>
            @enderror
            </div>
        </div>
        <div class="row m-0 all-form-productid mb-3 ">
          <div class="w-48">
             
          </div>
          <div class="w-48">
            <p class="font-weight-800">@lang('orders.phonee') <span class="text-danger">*</span> </p>
            <input type="number" class="form-control" id="phone"  name="phone" placeholder="">
            @error('phone')
                      <p class="error mt-1">{{ $message}}</p>
            @enderror
        </div>
      </div>
      
          <div class="all-btn-group-popup">
                  <a href="javascript:document.querySelector('.mark-add-orders').classList.remove('show')"
                      class="mr-3 btn  br-10">@lang('orders.cancel')</a>
                      <a href="javascript:void(0)"
                      class="mr-3 btn btn-info click-validate br-10" data-action="{{route('orders.validateOrder')}}">@lang('orders.save')</a>
          </div>
      </div>
    </div>
