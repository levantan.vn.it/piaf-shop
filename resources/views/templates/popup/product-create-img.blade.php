<div class="product-addImgs z-index-99 popup-db">
    <div class="popup-products-edit">
          <input type="hidden" name="id" value="">
          <input type="hidden" name="entity" value="product">
          <input type="hidden" name="route" value="products.index">
          <div class="popup-header">
              <a href="javascript:document.querySelector('.product-addImgs').classList.remove('show')"
                  class="text-yl close-model">
                  <img src="{{ static_asset('assets/img/icons/x.svg') }}" class="" alt="X">
              </a>
          </div>
          <div>
              <div class="w-96 mb-3">
                  <b>@lang('products.pr-img')</b>
              </div>
              <div class="w-96 mb-3 col-12">
                  <div class="row">
                      @include('backend.inc.upload_file',['values_file'=>'','type' =>'single'])
                  </div>
              </div>
              <div class="all-btn-group-popup">
                  <a href="javascript:document.querySelector('.product-addImgs').classList.remove('show')"
                      class="mr-3 btn br-10">@lang('banners.cancel')</a>
                  <a href="javascript:document.querySelector('.product-addImgs').classList.remove('show')" class="btn btn-info br-10">@lang('banners.save')</a>
              </div>
          </div>
  </div>
</div>
