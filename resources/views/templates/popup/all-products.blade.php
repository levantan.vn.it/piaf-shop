    <div class="list-product">
        @if(count($allproduct) && !empty($allproduct))
        @foreach ($allproduct as $produ)
            <div class="overflow-auto mt-2">
                <div class="d-flex bd-highlight align-items-center">
                        <input type="checkbox" name="product[]" class="mr-1"  value="{{$produ->id}}" data-id="{{$produ->id}}">
                        <div class="p-2 bd-highlight">
                            @if($produ->images)
                                @php
                                    $pro_img = explode(',',$produ->images);
                                @endphp
                                <img class="img-table mr-2" src="{{static_asset($pro_img[0])}}" alt="img" >
                            @else 
                                <img class="img-table mr-2" src="{{static_asset('/assets/img/placeholder.jpg')}}" alt="img" >
                            @endif
                        </div>
                        <div class="p-2 bd-highlight">
                            <table>
                                <tbody>
                                    <tr>
                                        <td>SKU: <b>{{$produ->minPriceSku}}</b></td>
                                    </tr>
                                    <tr>
                                        <td>{{$produ->name}}</td>
                                    </tr>
                                    <tr>
                                        <td>{{format_price($produ->skus->min('price'))}}원</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                </div>
            </div>
        @endforeach
        @else
        <div class="no_data">
            {{__('collections.no_data_product')}}
        </div>
        @endif
    </div>
    <div class="btn-add d-flex justify-content-end mt-3 ">
        <a href="javascript:document.querySelector('.edit-product-collection').classList.remove('show')" class="btn btn-cancel-product">{{__('collections.cancel')}}</a>
        <button type="submit" class="btn btn-info mr-2">{{__('collections.save')}}</button>
    </div>
