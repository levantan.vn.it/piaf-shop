<div class="add-category popup-db">
    <div class="popup-products-edit popup-add-category">
        <form action="{{ route('categories.createprimary') }}" method="post" class="redirect-popup d-inline-block w-100">
             @csrf
            <div class="popup-header">
            <a href="javascript:document.querySelector('.add-category').classList.remove('show')"
                class="text-yl close-model">
                <img src="{{ static_asset('assets/img/icons/x.svg') }}" class="" alt="X">
            </a>
        </div>
        <div class="row m-0 all-form-productid mb-3">
            <div class="w-100">
                <p class="font-weight-800 d-flex">@lang('categories.title-cate') &nbsp; <span class="text-danger" style="margin-top: -3px">*</span></p>
                <input type="text" name="title" class="form-control" placeholder="@lang('categories.title-cate')">
            </div>
        </div>
        <div class="row m-0 all-form-productid mb-3">
            <div class="w-100">
                <p class="font-weight-800">@lang('categories.type-cate')</p>
                <input type="text" class="form-control" readonly placeholder="@lang('categories.pri-cate')">
            </div>
        </div>
        <div class="w-100 mb-3 img_primary_cate">
            <p class="font-weight-800">@lang('categories.img-cate') &nbsp; <span class="text-danger" style="margin-top: -3px">*</span></p>
            <div class="banner-img-title mt-3">
                <div class="kt-img row">
                    @include('backend.inc.upload_file',['values_file'=>null,'type'=>'single'])
                </div>
            </div>
         </div>
        <div class="all-btn-group-popup">
                <a href="javascript:document.querySelector('.add-category').classList.remove('show')"
                    class="mr-3 btn  br-10">@lang('categories.cancel')</a>
                    <button type="button" href="" class="btn btn-info br-10 check-form">@lang('categories.save')</button>
                </div>
            </form>
    </div>
</div>

