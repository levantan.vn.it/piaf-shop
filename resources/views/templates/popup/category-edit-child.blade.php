    <div class="popup-products-edit edit-child">
        {{-- <form action="#" method="post" class="redirect-popup d-inline-block w-100"> --}}
            <form method="post" action="{{ route('categories.editchild', $category->id) }}" class="redirect-popup d-inline-block w-100">
            @csrf

            <div class="popup-header">
                <a href="javascript:document.querySelector('.edit-child-category').classList.remove('show')"
                    class="text-yl close-model">
                    <img src="{{ static_asset('assets/img/icons/x.svg') }}" class="" alt="X">

                </a>
            </div>
            <div class="row m-0 all-form-productid mb-3 ">
                <div class="w-48">
                    <p class="font-weight-800 d-flex">@lang('categories.title-cate') &nbsp;<span class="text-danger" style="margin-top: -3px">*</span></p>
                    <input type="text" name="title" class="form-control @error('title') is-invalid  @enderror" value="{{ $category->title }}" placeholder="@lang('categories.child-cate')">
                    @error('title')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
                </div>
            </div>
            <div class="row m-0 all-form-productid mb-3">
                <div class="w-48">
                    <p class="font-weight-800">@lang('categories.type-cate')</p>
                    <input type="text" class="form-control" readonly placeholder="@lang('categories.child-cate')">
                </div>
                <div class="w-48">
                    <p class="font-weight-800">@lang('categories.pri-cate')&nbsp; <span class="text-danger" style="margin-top: -3px">*</span></p>
                    <select class="form-control form-control-sm aiz-selectpicker mb-2 mb-md-0"
                        name="parent_id">
                        @foreach ( ($categories ?? []) as $item)
                            <option value="{{ $item->id  }}" {{ $item->id == $category->parent_id ? 'selected':'' }} placeholder="">{{ $item->title }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="w-96 mb-3">
                <p class="font-weight-800">@lang('categories.img-cate') </p>
                 <div class="banner-img-title mt-3">
                    <div class="kt-img row custom-kt-img">
                        @include('backend.inc.upload_file',['values_file'=>$category->id_images,'type'=>'single'])
                    </div>
                </div>
            </div>
            <div class="all-btn-group-popup">
                <a href="javascript:document.querySelector('.edit-child-category').classList.remove('show')"
                    class="mr-3 btn  br-10 cancel-edit-child">@lang('categories.cancel')</a>
                    <button type="button" href="" class="btn btn-info br-10 check-form">@lang('categories.save')</button>
            </div>
        </form>
    </div>
</div>
</div>

