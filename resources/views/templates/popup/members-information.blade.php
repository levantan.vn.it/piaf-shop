<div class="members-informations popup-db">
    <div class="popup-products-edit">
        <div class="popup-header">
            <a href="javascript:document.querySelector('.members-informations').classList.remove('show')"
                class="text-yl close-model">
                <img src="{{ static_asset('assets/img/icons/x.svg') }}" class="" alt="X">
            </a>
        </div>
        @if($edit) <form  class="form-infor">
            @csrf            
            @endif
            <div class="row m-0 all-form-productid mb-3 ">
                <div class="w-48 {{$edit ? '' : 'd-none'}}">
                    <p class="font-weight-800">@lang('members.member_id')</p>
                    <input type="text" class="form-control" id="" disabled name="id" value="#{{ $edit ? optional($member)->id : ''}}">
                </div>
                <div class="w-48">
                    <p class="font-weight-800">@lang('members.noti-name')<span class="required-icon"> *</span></p>
                    <input type="text" class="form-control" id="name"  name="name" value="{{$edit ? optional($member)->name : ''}}">
                    @error('name')
                      <p class="error mt-1">{{ $message}}</p>
                    @enderror
                    <p class="error name_err"></p>
                </div>
            </div>
            <div class="row m-0 all-form-productid mb-3 ">
                <div class="w-48">
                    <p class="font-weight-800">@lang('members.email')@if(!$edit)<span class="required-icon"> *</span> @endif</p>
                    <input type="text" class="form-control" {{$edit ? "disabled" : "" }} id="email" name="email" value="{{$edit ? optional($member)->email: ''}}">
                    @error('email')
                      <p class="error mt-1">{{ $message}}</p>
                    @enderror
                    <p class="error email_err"></p>
                </div>
                <div class="w-48">
                    <p class="font-weight-800">@lang('members.phone')<span class="required-icon"> *</span></p>
                    <input type="text" class="form-control" id="phone" name="phone"
                      value="{{$edit ? optional($member)->phone : ''}}">
                    @error('phone')
                      <p class="error mt-1">{{ $message}}</p>
                    @enderror
                    <p class="error phone_err"></p>
                </div>
            </div>
            <div class="row m-0 all-form-productid mb-3 ">
                <div class="w-48">
                    <p class="font-weight-800">@lang('members.noti-date')<span class="required-icon"> *</span></p>
                    <input type="text" autocomplete="off" class="form-control" 
                    name="birthday" id="birthday"  value="{{$edit ? date('Y-m-d', strtotime(optional($member)->birthday)) : ''}}">

                    @error('birthday')
                      <p class="error mt-1">{{ $message}}</p>
                    @enderror
                    <p class="error birthday_err"></p>
                </div>
                <div class="w-48">
                    <p class="font-weight-800">@lang('members.noti-address')<span class="required-icon"> *</span></p>
                    <input type="text" class="form-control" id="address" name="address"  value="{{$edit ? optional($member)->address : ''}}">
                    @error('address')
                      <p class="error mt-1">{{ $message}}</p>
                    @enderror
                    <p class="error address_err"></p>
                </div>
            </div>
            <div class="row m-0 all-form-productid mb-3 ">
                <div class="w-48">
                    <p class="font-weight-800">@lang('members.noti-id')<span class="required-icon"> *</span></p>
                    <input type="text" class="form-control" id="id_number" name="id_number"  value="{{$edit? optional($member)->id_number : ''}}">
                    @error('id_number')
                      <p class="error mt-1">{{ $message}}</p>
                    @enderror
                    <p class="error id_number_err"></p>
                </div>
                <div class="w-48">
                    <p class="font-weight-800">@lang('members.noti-date2')<span class="required-icon"> *</span></p>
                    <input type="text" autocomplete="off" class="form-control" 
                     name="date_of_issue" id="member-issue"  value="{{$edit ? date('Y-m-d', strtotime(optional($member)->date_of_issue)) : ''}}">

                    @error('date_of_issue')
                      <p class="error mt-1">{{ $message}}</p>
                    @enderror
                    <p class="error date_of_issue_err"></p>
                </div>
            </div>
            <div class="all-btn-group-popup">
                    @if($edit)
                    <a href="javascript:document.querySelector('.members-informations').classList.remove('show')"
                        class="mr-3 btn">@lang('members.cancel')</a>
                    <button type="button" data-action="{{route('members.updateInfor',optional($member)->id)}}" data-id="{{optional($member)->id}}" class="btn btn-info btn-update-infor">@lang('members.save')</button>
                    @else 
                    {{-- <a href="javascript:void(0)" class="btn-cancel-infor mr-3 btn ">Cancel</a> --}}
                    <input type="reset" id="infor-reset" class="btn-cancel-infor mr-3 btn" value="@lang('members.cancel')">
                    <a href="javascript:void(0)" class="btn-create-infor btn btn-info" data-text="data-info" data-action="{{route('members.validate-infor')}}">@lang('members.save')</a>
                    @endif
            </div>
            @if($edit)</form> @endif

    </div>
</div>
