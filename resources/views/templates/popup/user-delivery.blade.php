<div class="mark-deliverys popup-db">
   <div class="popup-products-edit br-10">
      <div class="popup-header">
         <a href="javascript:document.querySelector('.mark-deliverys').classList.remove('show')"
            class="text-yl close-model">
            <img src="{{ static_asset('assets/img/icons/x.svg') }}" class="" alt="X">
         </a>
      </div>
      <div class="overflow-auto show-like">
         <table class="table">
            <thead>
               <tr>
                  <th scope="col">@lang('users.location_name')</th>
                  <th scope="col">@lang('users.delivery_address')</th>
                  <th class="w-145" scope="col">@lang('users.receiver')</th>
                  <th class="w-135" scope="col">@lang('users.phone')</th>
                  <th class="w-98" scope="col">@lang('users.type')</th>
               </tr>
            </thead>
            <tbody>
               @if (count($customers->deliveryBooks))
                  @foreach ($customers->deliveryBooks as $deliveryBooks)
                  <tr>
                     <td scope="row">{{$deliveryBooks->location_name}}</td>
                     <td>
                        {{$deliveryBooks->address}}
                     </td>
                     <td>
                        {{$deliveryBooks->receiver}}
                     </td>
                     <td> 
                        <?php 
                           $number = $deliveryBooks->phone;
                           echo (substr($number, 0, 3) . "-" ) . (substr($number, 3, 4) . "-" ) . (substr($number, 7, 4));
                        ?>
                     </td>
                     <td>
                        {{$deliveryBooks->typeText()}}
                     </td>
                  </tr>
               @endforeach
               @else
                  <tr>
                  <td class="text-center pb-0" colspan="5">
                     <h5 class="mb-0 pt-3">@lang('users.nodata_delivery')</h5>
                  </td>
                  </tr>
               @endif
            </tbody>
         </table>
      </div>
   </div>
</div>
