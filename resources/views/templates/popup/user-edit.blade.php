<div class="mark-deceasedds popup-db">
  <div class="popup-products-edit br-10">
    <form action="{{ route('users.update', $customers->id) }}" id="form-mark-deceasedd" method="post" class="redirect-popup ">
        <div class="popup-header">
          <a href="javascript:document.querySelector('.mark-deceasedds').classList.remove('show')"
              class="text-yl close-model">
              <img src="{{ static_asset('assets/img/icons/x.svg') }}" class="" alt="X">
          </a>
      </div>
      <div class="row m-0 all-form-productid mb-3 ">
          <div class="w-48 ">
              <label class="font-weight-bold">@lang('users.us_id')</label>
              <input type="text" class="form-control" readonly value="#{{$customers->id}}">
          </div>
          <div class="w-48">
              <label class="font-weight-bold d-flex">@lang('users.display') <span class="red-validate" >*</span> </label>
              <input type="text" class="form-control @error('displayname') is-invalid  @enderror" maxlength="255" name="displayname"  value="{{$customers->displayname}}" >
              @error('displayname')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
          </div>
      </div>
      <div class="row m-0 all-form-productid mb-3 ">
            <div class="w-48 ">
                <label class="font-weight-bold ">@lang('users.us_name') <span ></span></label>
                <input type="text" class="form-control @error('fullname') is-invalid  @enderror" maxlength="255" name="fullname" value="{{$customers->fullname}}" >
                @error('fullname')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
            <div class="w-48">
                <label class="font-weight-bold d-flex">@lang('users.phone') <span class="red-validate" >*</span></label>
                <input type="text" id='maskpopupinfor' class="form-control @error('phone') is-invalid  @enderror" maxlength="255" name="phone" value="{{$customers->phone}}" >
                @error('phone')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
        </div>
        <div class="row m-0 all-form-productid mb-3 ">
            <div class="w-48 ">
                <label class="font-weight-bold d-flex">@lang('users.email') <span class="red-validate" >*</span></label>
                <input type="email" class="form-control @error('email') is-invalid  @enderror" maxlength="255" name="email" value="{{$customers->email}}" >
                @error('email')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
            <div class="w-48">
                <label class="font-weight-bold d-flex">@lang('users.joined_date') </label>
                <input type="text" class="form-control" readonly value="{{$customers->created_at->format('Y/m/d')}}">
            </div>
        </div>
      <div class="all-btn-group-popup">
            <a href="javascript:document.querySelector('.mark-deceasedds').classList.remove('show')" class="mr-3 btn  br-10">@lang('users.cancel')</a>
                {{csrf_field()}}
            <button type="submit" herf="javascript:void(0)" class="btn btn-info br-10">@lang('users.save')</button>
      </div>
    </form>
</div>
</div>
