<div class="mark-payments popup-db">
   <div class="popup-products-edit br-10">
      <div class="popup-header">
         <a href="javascript:document.querySelector('.mark-payments').classList.remove('show')"
            class="text-yl close-model">
            <img src="{{ static_asset('assets/img/icons/x.svg') }}" class="" alt="X">
         </a>
      </div>
      <div class="overflow-auto show-like">
         <table class="table">
            <thead>
               <tr>
                  <th scope="col">@lang('users.payment_method')</th>
                  <th scope="col">@lang('users.value')</th>
                  <th scope="col">@lang('users.receiver')</th>
                  <th scope="col">@lang('users.connected')</th>
                  <th scope="col">@lang('users.type')</th>
               </tr>
            </thead>
            <tbody>
               @if (count($customers->payments))
                  @foreach ($customers->payments as $userpayment)
                     <tr>
                        <td scope="row">{{ $userpayment->getCardBrand() }}</td>
                        <td> **** **** ****
                           <?php echo substr($userpayment->card_number, -4, 4); ?>
                        </td>
                        <td>{{ $userpayment->card_name }}</td>
                        <td>{{ $userpayment->expiry_date }} </td>
                        <td>{{ $userpayment->typeText() }}</td>
                     </tr>
                  @endforeach
               @else
                  <tr>
                     <td class="text-center pb-0" colspan="5">
                           <h5 class="mb-0 pt-3">@lang('users.nodata_payment')</h5>
                     </td>
                  </tr>
               @endif
            </tbody>
         </table>
      </div>
   </div>
</div>
