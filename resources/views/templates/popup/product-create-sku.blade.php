
    <div class="form-create-skus popup-db">
        <div class="popup-products-edit popup-products-transform">
            <div class="popup-header">
                <a href="javascript:document.querySelector('.form-create-skus').classList.remove('show')" class="text-yl close-model">
                    <img src="{{ static_asset('assets/img/icons/x.svg') }}" class="" alt="X">
                </a>
            </div>
            <div class="custom-productHeight">
                <div class="mb-3">
                    <p class="font-weight-800 text-danger" style="font-size: 24px">@lang('products.please_add_product_before_sku')</p>
                </div>

            </div>
        </div>
    </div>
