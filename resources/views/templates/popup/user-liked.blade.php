<div class="mark-likeds popup-db">
    <div class="popup-products-edit br-10">
        <div class="popup-header">
            <a href="javascript:document.querySelector('.mark-likeds').classList.remove('show')"
                class="text-yl close-model">
                <img src="{{ static_asset('assets/img/icons/x.svg') }}" class="" alt="X">
            </a>
        </div>
        <div class="overflow-auto show-like" >
            <div class="">
                @if (count($customers->likeProducts))
                    @foreach ($customers->likeProducts as $index => $likeProducts)
                    <?php 
                        $item = $likeProducts->minSkuItem; 
                        $photo = optional($likeProducts->photos)[0];
                    ?>
                    @if ($item)
                        <div class="d-flex mt-2">
                            <div >
                                <img class="img-table mr-4" src="{{ $photo ?? my_asset('assets/img/placeholder.jpg')}}" alt="{{ $likeProducts->name }}">
                            </div>
                            <div>
                                <p class="mb-1">
                                    @lang('users.sku') <b>{{ $item->sku }}</b>
                                </p>
                                <p class="mb-1">
                                    {{ $likeProducts->name }}
                                </p>
                                <p class="mb-1">
                                    <p>
                                        {{format_price($item->price)}} @lang('users.pri')
                                    </p>
                                </p>
                            </div>
                        </div>
                        <hr>
                    @endif
                @endforeach
                @else
                    <h5 class="mb-0 py-3 text-center">@lang('users.nodata_like')</h5>
                @endif
            </div>
        </div>
    </div>
</div>
