<div class="mark-deceaseddds popup-db">
  <div class="popup-products-edit br-10">
    <form action="{{ route('users.updateimg', $customers->id) }}" id="form-business" method="post" class="redirect-popup " enctype="multipart/form-data">
      <div class="popup-header">
          <a href="javascript:document.querySelector('.mark-deceaseddds').classList.remove('show')"
              class="text-yl close-model">
              <img src="{{ static_asset('assets/img/icons/x.svg') }}" class="" alt="X">
          </a>
      </div>
      <div class="row m-0 all-form-productid mb-3 ">
          <div class="w-48 ">
              <label class="font-weight-bold d-flex">@lang('users.business_name')<span class="red-validate">*</span></label>
              <input type="text" class="form-control @error('business_name') is-invalid  @enderror" maxlength="255"  name="business_name" value="{{$customers->business_name}}" >
               @error('business_name')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
          </div>
          <div class="w-48">
                <label class="font-weight-bold d-flex"> @lang('users.business_registration_number') <span class="red-validate" >*</span></label>
                <input type="text" id="maskpopupbusiness" class="form-control @error('tax_invoice') is-invalid  @enderror" maxlength="255"  name="tax_invoice" value="{{$customers->tax_invoice}}" >
                @error('tax_invoice')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
          </div>
      </div>
    <div class="row m-0 all-form-productid mb-1 ">
        <div class="w-48 ">
            <label class="font-weight-bold d-flex">@lang('users.representative')<span class="red-validate">*</span></label>
            <input type="text" class="form-control @error('representative') is-invalid  @enderror" maxlength="255"  name="representative" value="{{$customers->representative}}" >
            @error('representative')
                <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>
        <div class="w-48">
                {{-- @foreach ($customers as $customers)  --}}
                <label class="font-weight-bold">@lang('users.business_registration_document') </label>
                <div class="kt-img row">
                    @include('backend.inc.upload_file',['values_file'=>$customers->document_id])
                </div>
                {{-- @endforeach --}}
        </div>
    </div>
    <div class="row m-0 all-form-productid mb-3 ">
        <div class="w-48 ">
            <label class="font-weight-bold d-flex">@lang('users.store_name')<span class="red-validate" >*</span></label>
            <input type="text" class="form-control @error('store_name') is-invalid  @enderror" maxlength="255"  name="store_name" value="{{$customers->store_name}}" >
             @error('store_name')
                    <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>
        <div class="w-48">
            <label class="font-weight-bold d-flex">@lang('users.store_address')<span class="red-validate" >*</span> </label>
            <input type="text" class="form-control @error('store_address') is-invalid  @enderror" maxlength="255"  name="store_address" value="{{$customers->store_address}}" >
             @error('store_address')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
        </div>
    </div>
        <div class="all-btn-group-popup">
            <a href="javascript:document.querySelector('.mark-deceaseddds').classList.remove('show')" class="mr-3 btn br-10">@lang('users.cancel')</a>
                {{csrf_field()}}
            <button type="submit" herf="javascript:void(0)" class="btn btn-info br-10" value="Submit">@lang('users.save')</button>
        </div>
        </form>
  </div>
</div>
