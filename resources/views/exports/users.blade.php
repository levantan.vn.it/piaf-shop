<table>
    <thead>
        <tr>
            <th>ID</th>
            <th>{{__('users.displayname')}}</th>
            <th>{{__('users.fullname')}}</th>
            <th>{{__('users.email')}}</th>
            <th>{{__('users.phone')}}</th>
            <th>{{__('users.gender')}}</th>
            <th>{{__('users.birthday')}}</th>
            <th>{{__('users.bussiness_name')}}</th>
            <th>{{__('users.tax_invoice')}}</th>
            <th>{{__('users.representative')}}</th>
            <th>{{__('users.store_name')}}</th>
            <th>{{__('users.store_address')}}</th>
            <th>{{__('users.status')}}</th>
            <th>{{__('users.avatar')}}</th>
            <th>{{__('users.kakao_id')}}</th>
            <th>{{__('users.naver_id')}}</th>
            <th>{{__('users.join_date')}}</th>
        </tr>
    </thead>
    <tbody>
    @foreach($users as $user)
        <tr>
            <td>{{$user->id}}</td>
            <td>{{$user->displayname}}</td>
            <td>{{$user->fullname}}</td>
            <td>{{$user->email}}</td>
            <td>{{$user->phone}}</td>
            <td>{{$user->genderText()}}</td>
            <td>{{$user->birthday?date('Y.m.d', strtotime($user->birthday)):''}}</td>
            <td>{{$user->bussiness_name}}</td>
            <td>{{$user->tax_invoice}}</td>
            <td>{{$user->representative}}</td>
            <td>{{$user->store_name}}</td>
            <td>{{$user->store_address}}</td>
            <td>{{$user->statusText()}}</td>
            <td>{{ my_asset($user->avatar) }}</td>
            <td>{{$user->kakao_id}}</td>
            <td>{{$user->naver_id}}</td>
            <td>{{Date('Y.m.d', strtotime($user->created_at))}}</td>
        </tr>
    @endforeach
    </tbody>
</table>
