<table>
    <thead>
        <tr>
            <th class="">@lang('members.id')</th>
            <th class="">@lang('members.name')</th>
            <th class="">@lang('members.email')</th>
            <th class="">@lang('members.phone')</th>
            <th class="">@lang('members.role')</th>
            <th class="">@lang('members.joined')</th>
            <th class="">@lang('members.status')</th>
        </tr>
    </thead>
    <tbody>
    @foreach($members as $member)
        <tr>
            <td>{{optional($member)->id}}</td>
            <td>{{optional($member)->name}}</td>
            <td>{{optional($member)->email}}</td>
            <td>{{optional($member)->phone}}</td>
            <td>{{optional($member->role)->name}}</td>
            <td>{{date('Y-m-d',strtotime(optional($member)->created_at))}}</td>
            <td>{{optional($member)->statusText()}}</td>
        </tr>
    @endforeach
    </tbody>
</table>
