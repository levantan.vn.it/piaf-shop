<div class="col-12 col-md-4 info_user">
    <div class="box_avatar text-center bg-white">
        <div class="avatar">
            <img src="{{ static_asset('assets/img/avatar.png') }}" alt="">
        </div>
        <div class="name">
            <div class="user_name">
                <p>Robbie Williams</p>
                <span class="group">
                    <svg id="Group_839" data-name="Group 839" xmlns="http://www.w3.org/2000/svg" width="15" height="15"
                        viewBox="0 0 15 15">
                        <defs>
                            <style>
                                .cls-1 {
                                    fill: #43bbc3 !important;
                                    stroke: transparent !important;
                                }

                                .cls-2 {
                                    fill: #f2f2f2 !important;
                                }

                            </style>
                        </defs>
                        <path id="Path_491" data-name="Path 491" class="cls-1"
                            d="M-222.492,10.008a7.509,7.509,0,0,0-7.5-7.5,7.509,7.509,0,0,0-7.5,7.5,7.508,7.508,0,0,0,7.5,7.5A7.509,7.509,0,0,0-222.492,10.008Z"
                            transform="translate(237.492 -2.508)" />
                        <path id="Path_492" data-name="Path 492" class="cls-2"
                            d="M-217.569,18.628l.907,2.787a.454.454,0,0,0,.432.313h2.937a.453.453,0,0,1,.267.819L-215.4,24.27a.452.452,0,0,0-.165.507l.907,2.787a.454.454,0,0,1-.7.507l-2.376-1.722a.455.455,0,0,0-.534,0l-2.376,1.722a.454.454,0,0,1-.7-.507l.907-2.787a.452.452,0,0,0-.165-.507l-2.376-1.722a.453.453,0,0,1,.267-.819h2.937a.454.454,0,0,0,.432-.313l.907-2.787A.454.454,0,0,1-217.569,18.628Z"
                            transform="translate(225.501 -15.736)" />
                    </svg>
                </span>
            </div>
            <p class="full_name">Trần Thanh Ngọc</p>
        </div>
        <div class="info_point">
            <div class="point_item">
                <p class="number" id="point_number_user"></p>
                <span class="label">Điểm thưởng</span>
            </div>
            <hr width="1" size="500">
            <div class="point_item">
                <p class="number" id="total_order"></p>
                <span class="label">Đơn hàng</span>
            </div>
        </div>
    </div>

    <div class="box_info bg-white">
        <div class="title d-flex justify-content-between">
            <p>Tài khoản</p>
            <span class="down_arrow"><img src="{{ static_asset('assets/img/down_arrow.svg')}}" alt=""></span>
        </div>
        <div class="menu">
            <ul class="list-group">
                <li class="list-group-item">
                    <a href="{{ route('history_order') }}">
                        <span>
                            <svg id="user" xmlns="http://www.w3.org/2000/svg" width="10.541" height="10.541"
                                viewBox="0 0 10.541 10.541">
                                <g id="Group_2683" data-name="Group 2683" transform="translate(0)">
                                    <path id="Path_2065" data-name="Path 2065"
                                        d="M9,6.814a5.25,5.25,0,0,0-2-1.256,3.047,3.047,0,1,0-3.448,0A5.279,5.279,0,0,0,0,10.541H.824a4.447,4.447,0,1,1,8.894,0h.824A5.236,5.236,0,0,0,9,6.814ZM5.271,5.271A2.223,2.223,0,1,1,7.494,3.047,2.226,2.226,0,0,1,5.271,5.271Z"
                                        transform="translate(0)" />
                                </g>
                            </svg>
                        </span>
                        Lịch sử đặt hàng
                </li>
                <li class="list-group-item">
                     <a href="{{ route('my_editprofile') }}">
                        <span>
                            <svg id="user" xmlns="http://www.w3.org/2000/svg" width="10.541" height="10.541"
                                viewBox="0 0 10.541 10.541">
                                <g id="Group_2683" data-name="Group 2683" transform="translate(0)">
                                    <path id="Path_2065" data-name="Path 2065"
                                        d="M9,6.814a5.25,5.25,0,0,0-2-1.256,3.047,3.047,0,1,0-3.448,0A5.279,5.279,0,0,0,0,10.541H.824a4.447,4.447,0,1,1,8.894,0h.824A5.236,5.236,0,0,0,9,6.814ZM5.271,5.271A2.223,2.223,0,1,1,7.494,3.047,2.226,2.226,0,0,1,5.271,5.271Z"
                                        transform="translate(0)" />
                                </g>
                            </svg>
                        </span>
                       Chỉnh sửa thông tin cá nhân
                    </a></li>
                <li class="list-group-item"> 
                    <a href="javascript:void(0)">
                        <span class="address">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="10.121" viewBox="0 0 16 10.121">
                                <g id="delivery_1_" data-name="delivery (1)" transform="translate(0 0)">
                                  <g id="Group_2663" data-name="Group 2663">
                                    <g id="Group_2662" data-name="Group 2662" transform="translate(0 0)">
                                      <path id="Path_2054" data-name="Path 2054" d="M14.88,98.353,14.466,96.7a.251.251,0,0,0,.2-.245v-.267a1.053,1.053,0,0,0-1.052-1.052H11.724V94.58a.518.518,0,0,0-.518-.518H1.587a.518.518,0,0,0-.518.518v4.543a.251.251,0,0,0,.5,0V94.58a.017.017,0,0,1,.017-.017h9.62a.017.017,0,0,1,.017.017v4.543a.251.251,0,1,0,.5,0v-.284h2.957a.82.82,0,0,1,.778.568h-.779a.251.251,0,0,0-.251.251v.534a.786.786,0,0,0,.785.785H15.5v1.1h-.655a1.586,1.586,0,0,0-3,0h-.121v-1.887a.251.251,0,0,0-.5,0v1.887h-5.2a1.586,1.586,0,0,0-3,0H1.587a.017.017,0,0,1-.017-.017v-.551H2.656a.251.251,0,0,0,0-.5H.251a.251.251,0,0,0,0,.5h.818v.551a.518.518,0,0,0,.518.518H2.94c0,.006,0,.011,0,.017a1.587,1.587,0,0,0,3.173,0c0-.006,0-.011,0-.017h5.646c0,.006,0,.011,0,.017a1.587,1.587,0,0,0,3.173,0c0-.006,0-.011,0-.017h.819a.251.251,0,0,0,.251-.251V99.657A1.321,1.321,0,0,0,14.88,98.353Zm-3.156-2.721h1.887a.552.552,0,0,1,.551.551V96.2H11.724Zm0,2.706V96.7h2.226l.409,1.637Zm-7.2,5.344A1.086,1.086,0,1,1,5.612,102.6,1.087,1.087,0,0,1,4.526,103.682Zm8.818,0A1.086,1.086,0,1,1,14.43,102.6,1.087,1.087,0,0,1,13.344,103.682Zm2.155-3.207h-.284a.284.284,0,0,1-.284-.284v-.284H15.5v.568Z" transform="translate(0 -94.062)"/>
                                    </g>
                                  </g>
                                  <g id="Group_2665" data-name="Group 2665" transform="translate(4.008 8.017)">
                                    <g id="Group_2664" data-name="Group 2664">
                                      <path id="Path_2055" data-name="Path 2055" d="M128.785,350.6a.518.518,0,1,0,.518.518A.518.518,0,0,0,128.785,350.6Z" transform="translate(-128.267 -350.597)"/>
                                    </g>
                                  </g>
                                  <g id="Group_2667" data-name="Group 2667" transform="translate(12.827 8.017)">
                                    <g id="Group_2666" data-name="Group 2666">
                                      <path id="Path_2056" data-name="Path 2056" d="M410.973,350.6a.518.518,0,1,0,.518.518A.518.518,0,0,0,410.973,350.6Z" transform="translate(-410.455 -350.597)"/>
                                    </g>
                                  </g>
                                  <g id="Group_2669" data-name="Group 2669" transform="translate(6.413 6.948)">
                                    <g id="Group_2668" data-name="Group 2668" transform="translate(0 0)">
                                      <path id="Path_2057" data-name="Path 2057" d="M209.219,316.393h-3.741a.251.251,0,1,0,0,.5h3.741a.251.251,0,1,0,0-.5Z" transform="translate(-205.227 -316.393)"/>
                                    </g>
                                  </g>
                                  <g id="Group_2671" data-name="Group 2671" transform="translate(0.534 5.879)">
                                    <g id="Group_2670" data-name="Group 2670" transform="translate(0 0)">
                                      <path id="Path_2058" data-name="Path 2058" d="M20.559,282.188H17.353a.251.251,0,0,0,0,.5h3.207a.251.251,0,1,0,0-.5Z" transform="translate(-17.102 -282.188)"/>
                                    </g>
                                  </g>
                                </g>
                              </svg>
                              
                        </span>
                       Địa chỉ giao hàng
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <div class="box_info bg-white">
        <div class="title d-flex justify-content-between">
            <p>Sản phẩm</p>
            <span class="down_arrow"><img src="{{ static_asset('assets/img/down_arrow.svg')}}" alt=""></span>
        </div>
        <div class="menu">
            <ul class="list-group">
                <li class="list-group-item">
                    <a href="javascript:void(0)">
                        <span>
                            <svg id="Page-1" xmlns="http://www.w3.org/2000/svg" width="11.266" height="14.537" viewBox="0 0 11.266 14.537">
                                <defs>
                                  <style>
                                    .cls-1 {
                                      fill-rule: evenodd;
                                      fill: black;
                                    }
                                  </style>
                                </defs>
                                <g id="데스크탑-카테고리메인" transform="translate(0)">
                                  <g id="cart">
                                    <path id="Path_2080" data-name="Path 2080" class="cls-1" d="M13.992,9.011h2.155a.378.378,0,0,1,.377.346l.747,9.766a.343.343,0,0,1-.094.284.383.383,0,0,1-.276.12H6.387a.38.38,0,0,1-.377-.4l.755-9.766a.371.371,0,0,1,.37-.346H9.3V7.584a2.359,2.359,0,1,1,4.7,0Zm-.747,0V7.584a1.608,1.608,0,1,0-3.2,0V9.011ZM6.793,18.776h9.7l-.7-9.014H7.483Z" transform="translate(-6.009 -4.991)"/>
                                  </g>
                                </g>
                              </svg>
                        </span>
                        Giỏ hàng
                </li>
                <li class="list-group-item">
                     <a href="javascript:void(0)">
                        <span>
                            <svg id="user" xmlns="http://www.w3.org/2000/svg" width="10.541" height="10.541"
                                viewBox="0 0 10.541 10.541">
                                <g id="Group_2683" data-name="Group 2683" transform="translate(0)">
                                    <path id="Path_2065" data-name="Path 2065"
                                        d="M9,6.814a5.25,5.25,0,0,0-2-1.256,3.047,3.047,0,1,0-3.448,0A5.279,5.279,0,0,0,0,10.541H.824a4.447,4.447,0,1,1,8.894,0h.824A5.236,5.236,0,0,0,9,6.814ZM5.271,5.271A2.223,2.223,0,1,1,7.494,3.047,2.226,2.226,0,0,1,5.271,5.271Z"
                                        transform="translate(0)" />
                                </g>
                            </svg>
                        </span>
                        Sản phẩm đã xem
                    </a></li>
            </ul>
        </div>
    </div>

    <div class="box_info bg-white">
        <div class="title d-flex justify-content-between">
            <p>Thông tin</p>
            <span class="down_arrow"><img src="{{ static_asset('assets/img/down_arrow.svg')}}" alt=""></span>
        </div>
        <div class="menu">
            <ul class="list-group">
                <li class="list-group-item">
                    <a href="javascript:void(0)">
                        <span>
                            <svg id="Page-1" xmlns="http://www.w3.org/2000/svg" width="11.266" height="14.537" viewBox="0 0 11.266 14.537">
                                <defs>
                                  <style>
                                    .cls-1 {
                                      fill-rule: evenodd;
                                      fill: black;
                                    }
                                  </style>
                                </defs>
                                <g id="데스크탑-카테고리메인" transform="translate(0)">
                                  <g id="cart">
                                    <path id="Path_2080" data-name="Path 2080" class="cls-1" d="M13.992,9.011h2.155a.378.378,0,0,1,.377.346l.747,9.766a.343.343,0,0,1-.094.284.383.383,0,0,1-.276.12H6.387a.38.38,0,0,1-.377-.4l.755-9.766a.371.371,0,0,1,.37-.346H9.3V7.584a2.359,2.359,0,1,1,4.7,0Zm-.747,0V7.584a1.608,1.608,0,1,0-3.2,0V9.011ZM6.793,18.776h9.7l-.7-9.014H7.483Z" transform="translate(-6.009 -4.991)"/>
                                  </g>
                                </g>
                              </svg>
                        </span>
                        Thông tin về Fime Shop
                </li>
                <li class="list-group-item">
                     <a href="javascript:void(0)">
                        <span>
                            <svg id="user" xmlns="http://www.w3.org/2000/svg" width="10.541" height="10.541"
                                viewBox="0 0 10.541 10.541">
                                <g id="Group_2683" data-name="Group 2683" transform="translate(0)">
                                    <path id="Path_2065" data-name="Path 2065"
                                        d="M9,6.814a5.25,5.25,0,0,0-2-1.256,3.047,3.047,0,1,0-3.448,0A5.279,5.279,0,0,0,0,10.541H.824a4.447,4.447,0,1,1,8.894,0h.824A5.236,5.236,0,0,0,9,6.814ZM5.271,5.271A2.223,2.223,0,1,1,7.494,3.047,2.226,2.226,0,0,1,5.271,5.271Z"
                                        transform="translate(0)" />
                                </g>
                            </svg>
                        </span>
                        Giới thiệu bạn bè
                    </a>
                </li>
                <li class="list-group-item">
                    <a href="javascript:void(0)">
                       <span>
                           <svg id="user" xmlns="http://www.w3.org/2000/svg" width="10.541" height="10.541"
                               viewBox="0 0 10.541 10.541">
                               <g id="Group_2683" data-name="Group 2683" transform="translate(0)">
                                   <path id="Path_2065" data-name="Path 2065"
                                       d="M9,6.814a5.25,5.25,0,0,0-2-1.256,3.047,3.047,0,1,0-3.448,0A5.279,5.279,0,0,0,0,10.541H.824a4.447,4.447,0,1,1,8.894,0h.824A5.236,5.236,0,0,0,9,6.814ZM5.271,5.271A2.223,2.223,0,1,1,7.494,3.047,2.226,2.226,0,0,1,5.271,5.271Z"
                                       transform="translate(0)" />
                               </g>
                           </svg>
                       </span>
                       Gửi yêu cầu hỗ trợ
                   </a>
               </li>
               <li class="list-group-item">
                <a href="javascript:void(0)">
                   <span>
                       <svg id="user" xmlns="http://www.w3.org/2000/svg" width="10.541" height="10.541"
                           viewBox="0 0 10.541 10.541">
                           <g id="Group_2683" data-name="Group 2683" transform="translate(0)">
                               <path id="Path_2065" data-name="Path 2065"
                                   d="M9,6.814a5.25,5.25,0,0,0-2-1.256,3.047,3.047,0,1,0-3.448,0A5.279,5.279,0,0,0,0,10.541H.824a4.447,4.447,0,1,1,8.894,0h.824A5.236,5.236,0,0,0,9,6.814ZM5.271,5.271A2.223,2.223,0,1,1,7.494,3.047,2.226,2.226,0,0,1,5.271,5.271Z"
                                   transform="translate(0)" />
                           </g>
                       </svg>
                   </span>
                   Hotline: 18009797
               </a>
           </li>
            </ul>
        </div>
    </div>
</div>
<script type="text/javascript">
    document.addEventListener("DOMContentLoaded", function(event) { 
        $('.box_info > .title').click(function (e) { 
            e.preventDefault();
            $(this).next().slideToggle(1000);
            $(this).children('.down_arrow').toggleClass('active')
        });


        // ajax get point and total order
        $.ajax({
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "get",
            url: '/my-page/point-orders',
            dataType: "json",
            success: function (response) {
                console.log(response);
                $('#point_number_user').append(response.point);
                $('#total_order').append(response.order);
            }
        });
    });
</script>
