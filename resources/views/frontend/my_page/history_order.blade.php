@extends('frontend.my_page.master_mypage')
@section('content_my_page')
    <div class="col-12 col-md-8 info_order">
        <div class="menu_action bg-white">
            <ul class="nav">
                <li class="nav-item">
                    <a class="nav-link  active" id="link_all">Tất cả</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link btn-order-menu" href="collapseUnpaid">Chưa thanh toán</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link btn-order-menu" href="collapseProcess">Đã xác nhận</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link btn-order-menu" href="collapseShipping">Đang giao</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link btn-order-menu" href="collapseReceived">Đã giao</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link btn-order-menu" href="collapseDisable">Đã hủy</a>
                </li>
            </ul>
        </div>


        <div class="action_order" id="collapseReceived">
            @foreach ($orders as $order)
                @if ($order->status == 5)
                    <div class="card card-body">
                        <div class="title">
                            <p class="code_order">Mã đơn hàng : <span>#{{ $order->id }}</span></p>
                            <p class="status_order">Đã nhận hàng</p>
                        </div>
                        <div class="list_order">
                            <ul class="list-group list-group-flush">
                                @foreach ($order->OrderProduct->slice(0, 2) as $product)
                                    <li class="list-group-item">
                                        <div class="image_product">
                                            <img src="{{ static_asset('assets/img/obagi_salicylic_acid_2_a_5_clenziderm_md_pore_therapy_orchard_vn_2.png') }}"
                                                alt="">
                                        </div>
                                        <div class="name_product">
                                            <p>{{ $product->product->name }}</p>
                                            <span class="total">x{{ $product->qty }}</span>
                                        </div>
                                        <div class="price_product">
                                            {{ $product->subtotal }}
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                            <div class="total_price">
                                <div class="date_buy">
                                    <p>Ngày mua hàng: <span>{{ $order->created_at->format('d/m/Y') }}</span></p>
                                </div>
                                <div class="total_number">
                                    <p>Tổng số tiền: <span>{{ number_format($order->total) }}đ</span></p>
                                </div>
                            </div>
                            <div class="button_detail">
                                <button class="btn-detail">Xem chi tiết</button>
                            </div>
                        </div>
                    </div>
                @endif
            @endforeach

        </div>

        <div class="action_order " id="collapseDisable">
            @foreach ($orders as $order)
                @if ($order->status == 10)
                    <div class="card card-body">
                        <div class="title">
                            <p class="code_order">Mã đơn hàng : <span>#{{ $order->id }}</span></p>
                            <p class="status_order status_disable">Đã hủy</p>
                        </div>
                        <div class="list_order">
                            <ul class="list-group list-group-flush">
                                @foreach ($order->OrderProduct->slice(0, 2) as $product)
                                    <li class="list-group-item">
                                        <div class="image_product">
                                            <img src="{{ static_asset('assets/img/obagi_salicylic_acid_2_a_5_clenziderm_md_pore_therapy_orchard_vn_2.png') }}"
                                                alt="">
                                        </div>
                                        <div class="name_product">
                                            <p>{{ $product->product->name }}</p>
                                            <span class="total">x{{ $product->qty }}</span>
                                        </div>
                                        <div class="price_product">
                                            {{ $product->subtotal }}
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                            <div class="total_price">
                                <div class="date_buy">
                                    <p>Ngày mua hàng: <span>{{ $order->created_at->format('d/m/Y') }}</span></p>
                                </div>
                                <div class="total_number">
                                    <p>Tổng số tiền: <span>{{ number_format($order->total) }}đ</span></p>
                                </div>
                            </div>
                            <div class="button_detail">
                                <button class="btn-detail">Xem chi tiết</button>
                            </div>
                        </div>
                    </div>
                @endif
            @endforeach
        </div>

        <div class="action_order " id="collapseProcess">
            @foreach ($orders as $order)
                @if ($order->status == 2)
                    <div class="card card-body">
                        <div class="title">
                            <p class="code_order">Mã đơn hàng : <span>#{{ $order->id }}</span></p>
                            <p class="status_order status_process">Đã xác nhận</p>
                        </div>
                        <div class="list_order">
                            <ul class="list-group list-group-flush">
                                @foreach ($order->OrderProduct->slice(0, 2) as $product)
                                    <li class="list-group-item">
                                        <div class="image_product">
                                            <img src="{{ static_asset('assets/img/obagi_salicylic_acid_2_a_5_clenziderm_md_pore_therapy_orchard_vn_2.png') }}"
                                                alt="">
                                        </div>
                                        <div class="name_product">
                                            <p>{{ $product->product->name }}</p>
                                            <span class="total">x{{ $product->qty }}</span>
                                        </div>
                                        <div class="price_product">
                                            {{ $product->subtotal }}
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                            <div class="total_price">
                                <div class="date_buy">
                                    <p>Ngày mua hàng: <span>{{ $order->created_at->format('d/m/Y') }}</span></p>
                                </div>
                                <div class="total_number">
                                    <p>Tổng số tiền: <span>{{ number_format($order->total) }}đ</span></p>
                                </div>
                            </div>
                            <div class="button_detail">
                                <button class="btn-detail">Xem chi tiết</button>
                            </div>
                        </div>
                    </div>
                @endif
            @endforeach
        </div>

        <div class="action_order " id="collapseShipping">
            @foreach ($orders as $order)
                @if ($order->status == 4)
                    <div class="card card-body">
                        <div class="title">
                            <p class="code_order">Mã đơn hàng : <span>#{{ $order->id }}</span></p>
                            <p class="status_order status_shipping">Đang giao</p>
                        </div>
                        <div class="list_order">
                            <ul class="list-group list-group-flush">
                                @foreach ($order->OrderProduct->slice(0, 2) as $product)
                                    <li class="list-group-item">
                                        <div class="image_product">
                                            <img src="{{ static_asset('assets/img/obagi_salicylic_acid_2_a_5_clenziderm_md_pore_therapy_orchard_vn_2.png') }}"
                                                alt="">
                                        </div>
                                        <div class="name_product">
                                            <p>{{ $product->product->name }}</p>
                                            <span class="total">x{{ $product->qty }}</span>
                                        </div>
                                        <div class="price_product">
                                            {{ $product->subtotal }}
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                            <div class="total_price">
                                <div class="date_buy">
                                    <p>Ngày mua hàng: <span>{{ $order->created_at->format('d/m/Y') }}</span></p>
                                </div>
                                <div class="total_number">
                                    <p>Tổng số tiền: <span>{{ number_format($order->total) }}</span></p>
                                </div>
                            </div>
                            <div class="button_detail">
                                <button class="btn-detail">Xem chi tiết</button>
                            </div>
                        </div>
                    </div>
                @endif
            @endforeach
        </div>

        <div class="action_order " id="collapseUnpaid" style="display:none;">
            @foreach ($orders as $order)
                @if ($order->status == 2 || $order->status == 3 || $order->status == 4 || $order->status == 5)
                    <div class="card card-body">
                        <div class="title">
                            <p class="code_order">Mã đơn hàng : <span>#{{ $order->id }}</span></p>
                            <p class="status_order status_unpaid">Chưa thanh toán</p>
                        </div>
                        <div class="list_order">
                            <ul class="list-group list-group-flush">
                                @foreach ($order->OrderProduct->slice(0, 2) as $product)
                                    <li class="list-group-item">
                                        <div class="image_product">
                                            <img src="{{ static_asset('assets/img/obagi_salicylic_acid_2_a_5_clenziderm_md_pore_therapy_orchard_vn_2.png') }}"
                                                alt="">
                                        </div>
                                        <div class="name_product">
                                            <p>{{ $product->product->name }}</p>
                                            <span class="total">x{{ $product->qty }}</span>
                                        </div>
                                        <div class="price_product">
                                            {{ $product->subtotal }}
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                            <div class="total_price">
                                <div class="date_buy">
                                    <p>Ngày mua hàng: <span>{{ $order->created_at->format('d/m/Y') }}</span></p>
                                </div>
                                <div class="total_number">
                                    <p>Tổng số tiền: <span>{{ number_format($order->total) }}đ</span></p>
                                </div>
                            </div>
                            <div class="button_detail">
                                <button class="btn-detail">Xem chi tiết</button>
                            </div>
                        </div>
                    </div>
                @endif
            @endforeach
        </div>

    </div>


@endsection

@section('scripts')
    <script type="text/javascript">
        document.addEventListener("DOMContentLoaded", function(event) {
            $('#link_all').click(function(e) {
                e.preventDefault();
                $('.nav-link').removeClass('active');
                $(this).addClass('active');
                $('.action_order').each(function() {
                    $('#' + this.id).css('display', 'block');
                });
				$('#collapseUnpaid').css('display', 'none');
            });

            $('.btn-order-menu').click(function(e) {
                e.preventDefault();
                $('.nav-link').removeClass('active');
                $(this).addClass('active');
                var href = $(this).attr('href');
                $('.action_order').each(function() {
                    if (this.id != href) {
                        $('#' + this.id).css('display', 'none');
                    } else {
                        $('#' + this.id).css('display', 'block');

                    }
                });
            });
        });
    </script>
@endsection
