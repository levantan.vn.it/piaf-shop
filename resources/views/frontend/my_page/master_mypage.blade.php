@extends('frontend.layout.app')
@section('title')
    My page
@endsection
@section('content')
<div class="wapper_order">
    <div class="container mx-auto">
        <div class="row">
            @include('frontend.my_page.sidebar')
            @yield('content_my_page')
        </div>
    </div>
</div>
@endsection
