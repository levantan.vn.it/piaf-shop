@extends('frontend.my_page.master_mypage')
@section('content_my_page')
    <div class="col-12 col-md-8 edit-profile">
        <div class="title">
            <p class="m-0">Chỉnh sửa thông tin cá nhân</p>
        </div>
        <form class="form-edit-profile" action="{{ route('updateProfile', ['id'=>$user->id]) }}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group row">
                <label for="displayname" class="col-4 col-sm-6 col-md-3 col-form-label">Tên</label>
                <div class="col-8 col-sm-6 col-md-9 col-lg-7">
                    <input type="text" class="form-control-plaintext" name="displayname" id="displayname" value="{{ $user->displayname }}">
                </div>
            </div>
            <div class="form-group row">
                <label for="id_user" class="col-4 col-sm-6 col-md-3 col-form-label">ID</label>
                <div class="col-8 col-sm-6 col-md-9 col-lg-7">
                    <input type="text" class="form-control-plaintext" readonly id="id_user" value="{{ $user->id }}">
                </div>
            </div>
            <div class="form-group row">
                <label for="url" class="col-4 col-sm-6 col-md-3 col-form-label">Url</label>
                <div class="col-8 col-sm-6 col-md-9 col-lg-7">
                    <input type="text" class="form-control-plaintext"  name="Url"  id="Url" value="https://fime.vn/usr/rubypu">
                </div>
            </div>
            <div class="form-group row">
                <label for="email" class="col-4 col-sm-6 col-md-3 col-form-label">Email</label>
                <div class="col-8 col-sm-6 col-md-9 col-lg-7">
                    <input type="text" class="form-control-plaintext" name="email" id="email" value="{{ $user->email }}">
                </div>
            </div>
            <div class="form-group row   input-phone-row">
                <label for="phone" class="col-4 col-sm-6 col-md-3 col-form-label">Số điện thoại</label>
                <div class="col-8 col-sm-6 col-md-9 col-lg-7 d-flex align-items-center input_arrow">
                    <input type="text" class="form-control-plaintext input-phone"  name="phone" id="phone" value="{{ $user->phone }}">
                    <span class="arrow">
                        <svg id="Down_arrow" data-name="Down arrow" xmlns="http://www.w3.org/2000/svg" width="19"
                            height="10.802" viewBox="0 0 19 10.802">
                            <g id="XMLID_13_">
                                <path id="XMLID_17_"
                                    d="M59.184,257.027l-.868.91-2.077,2.178-2.511,2.633-2.17,2.275c-.351.368-.711.73-1.056,1.107l-.014.015h1.143l-.868-.91-2.077-2.178-2.511-2.633L44,258.149c-.351-.368-.7-.743-1.056-1.107l-.014-.015a.792.792,0,0,0-1.143,0,.892.892,0,0,0,0,1.2l.868.91,2.077,2.178,2.511,2.633,2.17,2.275c.351.368.7.743,1.056,1.107l.014.015a.793.793,0,0,0,1.143,0l.868-.91,2.077-2.178,2.511-2.633,2.17-2.275c.351-.368.709-.732,1.056-1.107l.014-.015a.888.888,0,0,0,0-1.2.8.8,0,0,0-1.143,0Z"
                                    transform="translate(-41.559 -256.784)" />
                            </g>
                        </svg>
                    </span>
                </div>
            </div>
            <div class="form-group row">
                <label for="gender_lable" class="col-4 col-sm-6 col-md-3 col-form-label">Giới tính</label>
                <div class="col-8 col-sm-6 col-md-9 col-lg-7 gender_checkbox">
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="gender" id="Male" value="1" 
                        @if ($user->gender == 1)
                            checked
                        @endif>
                        <label class="form-check-label" for="Male">
                            Nam
                        </label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="gender" id="female"  value="2"
                        @if ($user->gender == 2)
                        checked
                        @endif>
                        <label class="form-check-label" for="female">
                            Nữ
                        </label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="gender" id="other"  value="0" 
                        @if ($user->gender == 0)
                            checked
                        @endif>
                        <label class="form-check-label" for="other">
                            Khác
                        </label>
                    </div>
                </div>
                <div class="col-8 col-sm-6 col-md-9 col-lg-7 gender_checkbox_mb input_arrow">
                    <input type="text" name="gender" value="Nam" id="input_gender_mb">
                    <span class="arrow">
                        <svg id="Down_arrow" data-name="Down arrow" xmlns="http://www.w3.org/2000/svg" width="19"
                            height="10.802" viewBox="0 0 19 10.802">
                            <g id="XMLID_13_">
                                <path id="XMLID_17_"
                                    d="M59.184,257.027l-.868.91-2.077,2.178-2.511,2.633-2.17,2.275c-.351.368-.711.73-1.056,1.107l-.014.015h1.143l-.868-.91-2.077-2.178-2.511-2.633L44,258.149c-.351-.368-.7-.743-1.056-1.107l-.014-.015a.792.792,0,0,0-1.143,0,.892.892,0,0,0,0,1.2l.868.91,2.077,2.178,2.511,2.633,2.17,2.275c.351.368.7.743,1.056,1.107l.014.015a.793.793,0,0,0,1.143,0l.868-.91,2.077-2.178,2.511-2.633,2.17-2.275c.351-.368.709-.732,1.056-1.107l.014-.015a.888.888,0,0,0,0-1.2.8.8,0,0,0-1.143,0Z"
                                    transform="translate(-41.559 -256.784)" />
                            </g>
                        </svg>
                    </span>
                </div>

            </div>
            <div class="form-group row">
                <label for="phone" class="col-4 col-sm-6 col-md-3 col-form-label">Ngày sinh</label>
                <div class="col-8 col-sm-6 col-md-9 col-lg-7 birth_day">
                    <select name="day" id="day">
                        <option value="">Ngày</option>
                        
                        @for ($i = 1; $i <= 31; $i++)
                            <option value="{{ $i }}" {{ date('j', strtotime($user->birthday)) == $i ? 'selected' : '' }}>Ngày {{ $i }}</option>
                        @endfor
                    </select>
                    <select name="month" id="month">
                        <option value="">Tháng</option>
                        @for ($i = 1; $i <= 12; $i++)
                            <option value="{{ $i }}" {{ date('n', strtotime($user->birthday)) == $i ? 'selected' : '' }}>Tháng {{ $i }}</option>
                        @endfor
                    </select>
                    <select name="year" id="year">
                        <option value="">Năm</option>
                    </select>
                </div>
                <div class="col-8 col-sm-6 col-md-9 col-lg-7 birth_day_mb input_arrow">
                    <p>24/06/1996</p>
                    <span class="arrow">
                        <svg id="Down_arrow" data-name="Down arrow" xmlns="http://www.w3.org/2000/svg" width="19"
                            height="10.802" viewBox="0 0 19 10.802">
                            <g id="XMLID_13_">
                                <path id="XMLID_17_"
                                    d="M59.184,257.027l-.868.91-2.077,2.178-2.511,2.633-2.17,2.275c-.351.368-.711.73-1.056,1.107l-.014.015h1.143l-.868-.91-2.077-2.178-2.511-2.633L44,258.149c-.351-.368-.7-.743-1.056-1.107l-.014-.015a.792.792,0,0,0-1.143,0,.892.892,0,0,0,0,1.2l.868.91,2.077,2.178,2.511,2.633,2.17,2.275c.351.368.7.743,1.056,1.107l.014.015a.793.793,0,0,0,1.143,0l.868-.91,2.077-2.178,2.511-2.633,2.17-2.275c.351-.368.709-.732,1.056-1.107l.014-.015a.888.888,0,0,0,0-1.2.8.8,0,0,0-1.143,0Z"
                                    transform="translate(-41.559 -256.784)" />
                            </g>
                        </svg>
                    </span>
                </div>
            </div>
            <div class="form-group row button-save">
                <hr>
                <div class="col-12 col-sm-6 col-md-9 col-lg-7 offset-md-3 ">
                    <button class="btn-save" type="submit">Lưu</button>
                </div>
            </div>
        </form>
    </div>


    <!-- Modal edit phone-->
    <div class="modal fade" id="modal_edit_profile_phone" tabindex="-1" aria-labelledby="phoneModalMBLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="phoneModalMBLabel">Thay đổi số điện thoại</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="">Số điện thoại <span>*</span></label>
                            <input type="text" name="" id="" value="090 908 738" class="form-control" placeholder="" aria-describedby="helpId">
                            <span class="icon-delete-value">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="6" height="6" viewBox="0 0 6 6">
                                    <defs>
                                      <style>
                                        .cls-1 {
                                          fill: #fff;
                                          stroke: #707070;
                                        }
                                  
                                        .cls-2 {
                                          clip-path: url(#clip-path);
                                        }
                                  
                                        .cls-3 {
                                          fill: none;
                                          stroke: #000;
                                          stroke-linecap: round;
                                          stroke-linejoin: round;
                                          stroke-width: 0.5px;
                                        }
                                      </style>
                                      <clipPath id="clip-path">
                                        <rect id="Rectangle_2887" data-name="Rectangle 2887" class="cls-1" width="6" height="6" transform="translate(1148.32 458.32)"/>
                                      </clipPath>
                                    </defs>
                                    <g id="Mask_Group_723" data-name="Mask Group 723" class="cls-2" transform="translate(-1148.32 -458.32)">
                                      <g id="close" transform="translate(1149.328 459.328)">
                                        <path id="Union_1" data-name="Union 1" class="cls-3" d="M1.929,1.929,0,3.859,1.929,1.929,0,0,1.929,1.929,3.859,0,1.929,1.929,3.859,3.859Z"/>
                                      </g>
                                    </g>
                                  </svg>
                                  
                            </span>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn-save">Cập nhật</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Modal edit gender-->
    <div class="modal fade" id="modal_edit_profile_gender" tabindex="-1" aria-labelledby="genderModalMBLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <p class="m-0" data-dismiss="modal" aria-label="Close">Hủy</p>
                    <h5 class="modal-title" id="genderModalMBLabel">Chọn giới tính</h5>
                    <button type="button" class="m-0 done">Xong</button>
                </div>
                    <div class="modal-body">
                        <div class="form-check ">
                            <input class="form-check-input" type="radio" name="gender_modal"  id="male_modal">
                            <label class="form-check-label" for="male_modal">
                                Nam
                            </label>
                        </div>
                        <div class="form-check ">
                            <input class="form-check-input" type="radio" name="gender_modal"  id="female_modal">
                            <label class="form-check-label" for="female_modal">
                                Nữ
                            </label>
                        </div>
                        <div class="form-check active">
                            <input class="form-check-input" type="radio" name="gender_modal" id="other_modal">
                            <label class="form-check-label" for="other_modal">
                                Khác
                            </label>
                        </div>
                    </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript">
       document.addEventListener("DOMContentLoaded", function(event) {
            makeYear();
          
            // disable input gender when screen > 720
            var width = $(window).width();
            if ((width > 720.68)) {
                $('#input_gender_mb').attr('disabled', true);
            }else{
                $('#input_gender_mb').prop('disabled', false);
            }
           
            $('.input-phone-row').click(function(e) {
                e.preventDefault();
                var width = $(window).width();
                if ((width < 720.68)) {
                    $('#modal_edit_profile_phone').modal('show')
                } 
            });
            $('.gender_checkbox_mb').click(function(e) {
                e.preventDefault();
                var width = $(window).width();
                if ((width < 720.68)) {
                    $('#modal_edit_profile_gender').modal('show');
                }
            });
        });

        // make select year
        function makeYear() {
            var minOffset = 0,
                maxOffset = 100;
            var thisYear = new Date().getFullYear();

            var select = $('#year');
            var option = "";
            for (var i = minOffset; i <= maxOffset; i++) {
                var year = thisYear - i;
                if (year == {{date("Y", strtotime($user->birthday))}}) {
                    $('#year').append( '<option value='+year+' selected>'+year+'</option>');
                }else{
                    $('#year').append( '<option value='+year+'>'+year+'</option>');
                }
              
            }
        }

    </script>
@endsection
{{-- date("Y", strtotime($user->birthday)) == '+year+' ? "selected" : "" --}}