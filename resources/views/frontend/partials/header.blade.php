<div class="piaf_header" id="header">
    <div class="header-wrapper">
        <div class="piaf_nav">
            <a href="#" class="logo-piaf">
                <img src="{{static_asset('assets/img/logo.png')}}" alt="">
            </a>
            <div class="nav-right">
                <div class="search">
                    <input type="text">
                    <a href="" class="btn-search">Search</a>
                </div>
                <div class="search-mobile">
                    <div class="search-wrapper">
                        <a href="#" class="btn-search">
                            <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z" />
                          </svg>
                        </a>
                        <input type="text">
                        <div class="btn-close">
                            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
                              </svg>
                        </div>
                    </div>
                </div>
                <ul class="auth-user sign-in">
                    <li class="user-item login mobile">
                        <a href="" class="">Đăng nhập</a>
                    </li>
                    <li class="user-item logined d-none">
                        <div class="user-name">
                            <span>Dev Test</span>
                            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7" />
                            </svg>
                        </div>
                        <ul class="my-page">
                            <li>
                                <a href="">My page</a>
                            </li>
                            <li><a href="">Đăng xuất</a></li>
                        </ul>
                    </li>
                    <li class="user-item desktop">
                        <div class="search-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z" />
                              </svg>
                        </div>
                    </li>
                    <li class="user-item mobile">
                        <a href="" class="wishlist item"><img src="{{static_asset('assets/img/icons/favorite.svg')}}" alt="">
                            <span class="quantity">0</span>
                            </a>
                    </li>
                    <li class="user-item ">
                        <a href="" class="cart item"><img src="{{static_asset('assets/img/icons/cart.svg')}}" alt="">
                        <span class="quantity">0</span>
                        </a>
                        
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>