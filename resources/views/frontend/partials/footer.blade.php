<div class="piaf-footer" id="footer">
    <div class="footer_bar">
     <ul>
        <li>
        <a href="https://clubpiaf.vn/reviews"><img src="{{static_asset('assets/img/icons/review.png')}}" alt="Review">
            <span>Review</span></a></li>
         <li>
             <a href="https://clubpiaf.vn/tries"><img src="{{static_asset('assets/img/icons/try.png')}}" alt="Try">
                <span>Try</span></a></li>
          <li>
              <a href="https://clubpiaf.vn/tips"><img src="{{static_asset('assets/img/icons/tips.png')}}" alt="Tips">
                <span>Tips</span></a></li>
          <li class="active">
              <a href="/"><img class="img_active" src="{{static_asset('assets/img/icons/shopmb.png')}}" alt="Shop">
                <img class="img_noactive" src="{{static_asset('assets/img/icons/shop.png')}}" alt="Shop">
                <span>Shop</span></a></li>            
            <li class="more ">
                <img class="img_active" src="{{static_asset('assets/img/icons/more-black.png')}}" alt="More">
                <img class="img_noactive" src="{{static_asset('assets/img/icons/more.png')}}" alt="More"><span>More</span>
            </li>
      </ul>
    </div>
    <div class="footer-wrapper">
        <div class="footer-list">
            <div class="footer-item footer-left">
                <a href="" class="logo">
                    <img src="{{static_asset('assets/img/icons/logo-footer.png')}}" alt="">
                </a>
                <p class="company-name">
                    Công ty TNHH DM&C
                </p>
                <p class="company-address">
                    Địa chỉ: Tầng 10, 194 Golden Building, 473 Điện Biên Phủ, Phường 25, Quận Bình Thạnh, TP. Hồ Chí Minh
                </p>
                <div class="email-contact">
                    <ul>
                        <li>
                        <span>Email CSKH: </span><a href="mailto:cskh.clubpiafshop@dmnc.vn">cskh.clubpiafshop@dmnc.vn</a> </li>
                                               <li>
                        <span>Email LHHT: </span><a href="mailto:merchandise.clubpiafshop@dmnc.vn">merchandise.clubpiafshop@dmnc.vn</a>
                        </li>
                            <li><span>Mã số thuế: 0314724148</span></li>
                            <li>
                        <span>Số điện thoại: </span><a href="tel:0902-322-350">0902-322-350</a>
                        </li>
                                           </ul>
                </div>
            </div>
            <div class="footer-item footer-right">
                <div class="menu_footer_item">
                    <ul><li><a href="https://clubpiaf.vn/brands/all">Brand</a></li>
                        <li><a href="/">Shop</a></li>           
                        <li><a href="https://clubpiaf.vn/events">Event</a></li>       
                        <li><a href="/">Group Buying</a></li>
                        <li><a href="/layout/basic/contact.html">Contact</a></li>
                    </ul>
                </div>
                <div class="menu_footer_item">
                    <ul class="xans-element- xans-layout xans-layout-statelogon "><li><a href="/myshop/index.html">Tài khoản</a></li>
                        <li><a href="/exec/front/Member/logout/">Đăng xuất</a></li>
                        <li><a href="/member/privacy.html">Chính sách bảo mật</a></li>
                        <li><a href="/member/agreement.html">Điều khoản</a></li>
                        <li><a href="https://clubpiaf.vn/faqs">Chính sách thành viên</a></li>
                    </ul>
                </div>
                <div class="menu_footer_item">
                    <ul class="social"><li><a href="https://www.facebook.com/clubpiafshop">Facebook</a></li>
                        <li><a href="https://www.instagram.com/fistore.vn/">Instagram</a></li>
                        <li><a href="https://www.youtube.com/channel/UCEuW7adc9PvnrT4MiNig4FA">Youtube</a></li>
                    </ul><div class="label_payment">
                        <p>Thanh toán</p>
                         <ul><li><img src="{{static_asset('assets/img/icons/visa.jpg')}}" alt="Visa"></li>
                             <li><img src="{{static_asset('assets/img/icons/master_card.jpg')}}" alt="Master Card"></li>
                             <li><img src="{{static_asset('assets/img/icons/jcb.jpg')}}" alt="JCB"></li>
                             <li><img src="{{static_asset('assets/img/icons/paypal.jpg')}}" alt="Paypal"></li>
                        </ul><a class="register_mit" href="http://www.online.gov.vn/Home/WebDetails/44781"><img src="{{static_asset('assets/img/icons/bo-cong-thuong.png')}}" alt="FistoreBlueLabel"></a>
                    </div>
                    </div>
            </div>
        </div>
    </div>
    <div class="option">
        <a class="btn_avt icon-fixed" href="/myshop/myaccount.html">
            <span>My page</span>
            <img src="{{static_asset('assets/img/icons/img_member_default.gif')}}" alt="">
        </a>
        <a class="btn_sub btn_voucher icon-fixed" href="https://clubpiaf.vn/vouchers/shop/all">
            <span>Voucher</span>
            <img alt="Voucher" src="{{static_asset('assets/img/icons/voucher.png')}}">
        </a>
        <a class="btn_sub btn_fimer icon-fixed" href="https://clubpiaf.vn/piers/all">
            <span>Pier</span>
             <div class="icon">
                <img alt="Pier" src="{{static_asset('assets/img/icons/user_white.png')}}">
             </div>
        </a>
        <a class="btn_sub btn_event icon-fixed" href="https://clubpiaf.vn/events">
            <span>Event</span>
                <div class="icon">
                    <img alt="Event" src="{{static_asset('assets/img/icons/event.png')}}">
                </div>
        </a>
        <a class="btn_sub btn_brand icon-fixed" href="https://clubpiaf.vn/brands/all">
            <span>Brand</span>
                <div class="icon">
                    <img alt="Brand" src="{{static_asset('assets/img/icons/tag.png')}}">
                </div>
        </a>
        <div class="ft_more_menu icon-fixed" >
            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
              </svg>
        </div>
        <div id="overlay-more"></div>
    </div>
</div>