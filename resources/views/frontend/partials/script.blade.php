<script>
    $(document).ready(function(){
        hide = true;
        $('body').on("click", function(e) {
            if(hide) {
                $(".logined").removeClass("active");
            }
            hide = true;
        });
        $('body').on('click', '.logined', function () {

            var self = $(this);
            if (self.hasClass('active')) {
                $('.logined').removeClass('active');
                return false;
            }
            $('.logined').removeClass('active');
            self.toggleClass('active');
            hide = false;
        });
        $('.search-icon').click(function(){
            $('.search-mobile').addClass('show');
        })
        $('.nav-right .search-mobile .btn-close').click(function(){
            $('.search-mobile').removeClass('show');
        })
        var lastScrollTop = 0;
        $(window).scroll(function(event){
            var st = $(window).scrollTop();
            if (st > lastScrollTop){
                //downscroll code
                $('.footer_bar').addClass('down');
                $('.footer_bar').removeClass('up');
            } else {
                // upscroll code
                $('.footer_bar').addClass('up');
                $('.footer_bar').removeClass('down');
            }
            lastScrollTop = st;
            if($(window).scrollTop() + window.innerHeight >= $(document).height() - 100) {
                $('.footer_bar').removeClass('down');
            }
        });

        $('.footer_bar .more').click(function(){
            $('#footer .option').toggleClass('active');
            $(this).toggleClass('open');
        })
        $('#footer .ft_more_menu ').click(function(){
            $('#footer .option').removeClass('active');
            $('.footer_bar .more').removeClass('open');
        })
    })
</script>