@extends('frontend.layout.app')
<title>Homepage</title>
@section('css')
<link rel="stylesheet" href={{static_asset('assets/css/homepage.css')}}>
@endsection

@section('content')
    <div class="homepage" id="homepage">
        <div class="home-wrapper">
            <div class="section-banner">
                <div class="banner-image">
                    <div class="slider-banner">
                        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                              <li data-slide-to="0" data-target="#carouselExampleIndicators" class="active"></li>
                              <li data-slide-to="1" data-target="#carouselExampleIndicators"></li>
                              <li data-slide-to="2" data-target="#carouselExampleIndicators"></li>
                              <li data-slide-to="3" data-target="#carouselExampleIndicators"></li>
                            </ol>
                            <div class="carousel-inner">
                              <div class="carousel-item active">
                                <a href="#" class="banner-link">
                                  <img src="{{static_asset('assets/img/demo/banner_1.jpg')}}" class="d-block w-100" alt="...">
                                </a>
                              </div>
                              <div class="carousel-item">
                                <a href="#" class="banner-link">
                                  <img src="{{static_asset('assets/img/demo/banner_2.jpg')}}" class="d-block w-100" alt="...">
                                </a>
                              </div>
                              <div class="carousel-item">
                                <a href="#" class="banner-link">
                                  <img src="{{static_asset('assets/img/demo/banner_3.jpg')}}" class="d-block w-100" alt="...">
                                </a>
                              </div>
                              <div class="carousel-item">
                                <a href="#" class="banner-link">
                                  <img src="{{static_asset('assets/img/demo/banner_4.jpg')}}" class="d-block w-100" alt="...">
                                </a>
                              </div>
                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                              <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                              <span class="carousel-control-next-icon" aria-hidden="true"></span>
                              <span class="sr-only">Next</span>
                            </a>
                          </div>
                    </div>
                    <div class="banner">
                        <div class="image">
                            <a href=""><img src="{{static_asset('assets/img/demo/banner_right_1.jpg')}}" alt=""></a>
                        </div>
                        <div class="image">
                            <a href=""><img src="{{static_asset('assets/img/demo/banner_right_2.jpg')}}" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section-flashsale">
              <div class="flashsale-heading">
                <div class="title-heading">
                  <img src="{{static_asset('assets/img/icons/flash_sale.png')}}" alt="">
                  <h3 class="title">Flash sale</h3>
                </div>
                <div class="see-all">
                  <a href=""><span>Xem Tất Cả</span> 
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5l7 7-7 7" />
                    </svg>
                </a>
                </div>
              </div>
              <div class="list-flashsale">
                <div class="list-product">
                  <div class="item-product">
                    <div class="wrapper-product">
                      <div class="top-product">
                        <a href="" class="image-product">
                          <img src="{{static_asset('assets/img/demo/demo1.png')}}" alt="">
                        </a>
                        <div class="promotion">
                          <span class="discount">-35%</span>
                        </div>
                        <div class="add-cart">
                          <a href="">Thêm vào giỏ hàng</a>
                        </div>
                      </div>
                      <div class="bottom-product">
                        <a href="" class="name">Kem chống nắng Caryophy Smart Sunscreen 50ML</a>
                        <div class="price">
                          <span class="price-origin">207,000 đ</span>
                          <span class="price-sale">35,000 đ</span>
                        </div>
                        <div class="flashsale-time">
                          <div class="time-count">
                            <span class="text">còn lại </span>
                            <span class="time">01:09:20:30</span>
                          </div>
                          <div class="time-background"></div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="item-product">
                    <div class="wrapper-product">
                      <div class="top-product">
                        <a href="" class="image-product">
                          <img src="{{static_asset('assets/img/demo/demo3.jpg')}}" alt="">
                        </a>
                        <div class="promotion">
                          <span class="discount">-35%</span>
                        </div>
                        <div class="add-cart">
                          <a href="">Thêm vào giỏ hàng</a>
                        </div>
                      </div>
                      <div class="bottom-product">
                        <a href="" class="name">Kem chống nắng Caryophy Smart Sunscreen 50ML</a>
                        <div class="price">
                          <span class="price-origin">207,000 đ</span>
                          <span class="price-sale">35,000 đ</span>
                        </div>
                        <div class="flashsale-time">
                          <div class="time-count">
                            <span class="text">còn lại </span>
                            <span class="time">01:09:20:30</span>
                          </div>
                          <div class="time-background"></div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="item-product">
                    <div class="wrapper-product">
                      <div class="top-product">
                        <a href="" class="image-product">
                          <img src="{{static_asset('assets/img/demo/demo4.jpg')}}" alt="">
                        </a>
                        <div class="promotion">
                          <span class="discount">-35%</span>
                        </div>
                        <div class="add-cart">
                          <a href="">Thêm vào giỏ hàng</a>
                        </div>
                      </div>
                      <div class="bottom-product">
                        <a href="" class="name">Kem chống nắng Caryophy Smart Sunscreen 50ML</a>
                        <div class="price">
                          <span class="price-origin">207,000 đ</span>
                          <span class="price-sale">35,000 đ</span>
                        </div>
                        <div class="flashsale-time">
                          <div class="time-count">
                            <span class="text">còn lại </span>
                            <span class="time">01:09:20:30</span>
                          </div>
                          <div class="time-background"></div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="item-product">
                    <div class="wrapper-product">
                      <div class="top-product">
                        <a href="" class="image-product">
                          <img src="{{static_asset('assets/img/demo/demo5.png')}}" alt="">
                        </a>
                        <div class="promotion">
                          <span class="discount">-35%</span>
                        </div>
                        <div class="add-cart">
                          <a href="">Thêm vào giỏ hàng</a>
                        </div>
                      </div>
                      <div class="bottom-product">
                        <a href="" class="name">Kem chống nắng Caryophy Smart Sunscreen 50ML</a>
                        <div class="price">
                          <span class="price-origin">207,000 đ</span>
                          <span class="price-sale">35,000 đ</span>
                        </div>
                        <div class="flashsale-time">
                          <div class="time-count">
                            <span class="text">còn lại </span>
                            <span class="time">01:09:20:30</span>
                          </div>
                          <div class="time-background"></div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="item-product">
                    <div class="wrapper-product">
                      <div class="top-product">
                        <a href="" class="image-product">
                          <img src="{{static_asset('assets/img/demo/demo7.jpg')}}" alt="">
                        </a>
                        <div class="promotion">
                          <span class="discount">-35%</span>
                        </div>
                        <div class="add-cart">
                          <a href="">Thêm vào giỏ hàng</a>
                        </div>
                      </div>
                      <div class="bottom-product">
                        <a href="" class="name">Kem chống nắng Caryophy Smart Sunscreen 50ML</a>
                        <div class="price">
                          <span class="price-origin">207,000 đ</span>
                          <span class="price-sale">35,000 đ</span>
                        </div>
                        <div class="flashsale-time">
                          <div class="time-count">
                            <span class="text">còn lại </span>
                            <span class="time">01:09:20:30</span>
                          </div>
                          <div class="time-background"></div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="item-product">
                    <div class="wrapper-product">
                      <div class="top-product">
                        <a href="" class="image-product">
                          <img src="{{static_asset('assets/img/demo/demo2.jpg')}}" alt="">
                        </a>
                        <div class="promotion">
                          <span class="discount">-35%</span>
                        </div>
                        <div class="add-cart">
                          <a href="">Thêm vào giỏ hàng</a>
                        </div>
                      </div>
                      <div class="bottom-product">
                        <a href="" class="name">Kem chống nắng Caryophy Smart Sunscreen 50ML</a>
                        <div class="price">
                          <span class="price-origin">207,000 đ</span>
                          <span class="price-sale">35,000 đ</span>
                        </div>
                        <div class="flashsale-time">
                          <div class="time-count">
                            <span class="text">còn lại </span>
                            <span class="time">01:09:20:30</span>
                          </div>
                          <div class="time-background"></div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="item-product">
                    <div class="wrapper-product">
                      <div class="top-product">
                        <a href="" class="image-product">
                          <img src="{{static_asset('assets/img/demo/demo6.png')}}" alt="">
                        </a>
                        <div class="promotion">
                          <span class="discount">-35%</span>
                        </div>
                        <div class="add-cart">
                          <a href="">Thêm vào giỏ hàng</a>
                        </div>
                      </div>
                      <div class="bottom-product">
                        <a href="" class="name">Kem chống nắng Caryophy Smart Sunscreen 50ML</a>
                        <div class="price">
                          <span class="price-origin">207,000 đ</span>
                          <span class="price-sale">35,000 đ</span>
                        </div>
                        <div class="flashsale-time">
                          <div class="time-count">
                            <span class="text">còn lại </span>
                            <span class="time">01:09:20:30</span>
                          </div>
                          <div class="time-background"></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="section-banner-body">
              <a href="#" class="banner-body">
                <img src="{{static_asset('assets/img/demo/banner-body.jpeg')}}" alt="">
              </a>
            </div>
            <div class="section-category">
              <h3 class="title">Category</h3>
              <div class="list-category">
                <div class="category-item">
                  <a href="" class="category"><img src="{{static_asset('assets/img/icons/lips_text.png')}}" alt=""></a>
                </div>
                <div class="category-item">
                  <a href="" class="category"><img src="{{static_asset('assets/img/icons/eyes_text.png')}}" alt=""></a>
                </div>
                <div class="category-item">
                  <a href="" class="category"><img src="{{static_asset('assets/img/icons/skin_face_text.png')}}" alt=""></a>
                </div>
                <div class="category-item">
                  <a href="" class="category"><img src="{{static_asset('assets/img/icons/body_text.png')}}" alt=""></a>
                </div>
                <div class="category-item">
                  <a href="" class="category"><img src="{{static_asset('assets/img/icons/men_text.png')}}" alt=""></a>
                </div>
                <div class="category-item">
                  <a href="" class="category"><img src="{{static_asset('assets/img/icons/mom_baby_text.png')}}" alt=""></a>
                </div>
                <div class="category-item">
                  <a href="" class="category"><img src="{{static_asset('assets/img/icons/health_text.png')}}" alt=""></a>
                </div>
                <div class="category-item">
                  <a href="" class="category"><img src="{{static_asset('assets/img/icons/interior_text.png')}}" alt=""></a>
                </div>
              </div>
            </div>
            <div class="section-new-arrival">
              <div class="heading-product">
                <h3 class="title">
                  New Arrivals
                </h3>
                <div class="see-all">
                  <a href=""><span>Xem Tất Cả</span> 
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                      <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5l7 7-7 7" />
                      </svg>
                  </a>
                </div>
              </div>
              <div class="list-products list-global">
                <div class="product">
                  <div class="product-item">
                    <div class="top-product">
                      <a href="" class="image-product">
                        <img src="{{static_asset('assets/img/demo/demo3.jpg')}}" alt="">
                      </a>
                      <div class="promotion">
                        <span class="discount">-35%</span>
                      </div>
                      <a href="#" class="wishlist">
                        <img class="add" src="{{static_asset('assets/img/icons/wishlist.png')}}" alt="">
                        <img class="added" src="{{static_asset('assets/img/icons/wishlisted.png')}}" alt="">
                      </a>
                      <div class="add-cart">
                        <a href="">Thêm vào giỏ hàng</a>
                      </div>
                    </div>
                    <div class="bottom-product">
                      <a href="" class="name">Kem chống nắng Caryophy Smart Sunscreen 50ML</a>
                      <div class="price">
                        <span class="price-origin">207,000 đ</span>
                        <span class="price-sale">35,000 đ</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="product">
                  <div class="product-item">
                    <div class="top-product">
                      <a href="" class="image-product">
                        <img src="{{static_asset('assets/img/demo/demo1.png')}}" alt="">
                      </a>
                      <div class="promotion">
                        <span class="discount">-35%</span>
                      </div>
                      <a href="#" class="wishlist">
                        <img class="add" src="{{static_asset('assets/img/icons/wishlist.png')}}" alt="">
                        <img class="added" src="{{static_asset('assets/img/icons/wishlisted.png')}}" alt="">
                      </a>
                      <div class="add-cart">
                        <a href="">Thêm vào giỏ hàng</a>
                      </div>
                    </div>
                    <div class="bottom-product">
                      <a href="" class="name">Kem chống nắng Caryophy Smart Sunscreen 50ML</a>
                      <div class="price">
                        <span class="price-origin">207,000 đ</span>
                        <span class="price-sale">35,000 đ</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="product">
                  <div class="product-item">
                    <div class="top-product">
                      <a href="" class="image-product">
                        <img src="{{static_asset('assets/img/demo/demo1.png')}}" alt="">
                      </a>
                      <div class="promotion">
                        <span class="discount">-35%</span>
                      </div>
                      <a href="#" class="wishlist">
                        <img class="add" src="{{static_asset('assets/img/icons/wishlist.png')}}" alt="">
                        <img class="added" src="{{static_asset('assets/img/icons/wishlisted.png')}}" alt="">
                      </a>
                      <div class="add-cart">
                        <a href="">Thêm vào giỏ hàng</a>
                      </div>
                    </div>
                    <div class="bottom-product">
                      <a href="" class="name">Kem chống nắng Caryophy Smart Sunscreen 50ML</a>
                      <div class="price">
                        <span class="price-origin">207,000 đ</span>
                        <span class="price-sale">35,000 đ</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="product">
                  <div class="product-item">
                    <div class="top-product">
                      <a href="" class="image-product">
                        <img src="{{static_asset('assets/img/demo/demo1.png')}}" alt="">
                      </a>
                      <div class="promotion">
                        <span class="discount">-35%</span>
                      </div>
                      <a href="#" class="wishlist">
                        <img class="add" src="{{static_asset('assets/img/icons/wishlist.png')}}" alt="">
                        <img class="added" src="{{static_asset('assets/img/icons/wishlisted.png')}}" alt="">
                      </a>
                      <div class="add-cart">
                        <a href="">Thêm vào giỏ hàng</a>
                      </div>
                    </div>
                    <div class="bottom-product">
                      <a href="" class="name">Kem chống nắng Caryophy Smart Sunscreen 50ML</a>
                      <div class="price">
                        <span class="price-origin">207,000 đ</span>
                        <span class="price-sale">35,000 đ</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="product">
                  <div class="product-item">
                    <div class="top-product">
                      <a href="" class="image-product">
                        <img src="{{static_asset('assets/img/demo/demo1.png')}}" alt="">
                      </a>
                      <div class="promotion">
                        <span class="discount">-35%</span>
                      </div>
                      <a href="#" class="wishlist">
                        <img class="add" src="{{static_asset('assets/img/icons/wishlist.png')}}" alt="">
                        <img class="added" src="{{static_asset('assets/img/icons/wishlisted.png')}}" alt="">
                      </a>
                      <div class="add-cart">
                        <a href="">Thêm vào giỏ hàng</a>
                      </div>
                    </div>
                    <div class="bottom-product">
                      <a href="" class="name">Kem chống nắng Caryophy Smart Sunscreen 50ML</a>
                      <div class="price">
                        <span class="price-origin">207,000 đ</span>
                        <span class="price-sale">35,000 đ</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="product">
                  <div class="product-item">
                    <div class="top-product">
                      <a href="" class="image-product">
                        <img src="{{static_asset('assets/img/demo/demo1.png')}}" alt="">
                      </a>
                      <div class="promotion">
                        <span class="discount">-35%</span>
                      </div>
                      <a href="#" class="wishlist">
                        <img class="add" src="{{static_asset('assets/img/icons/wishlist.png')}}" alt="">
                        <img class="added" src="{{static_asset('assets/img/icons/wishlisted.png')}}" alt="">
                      </a>
                      <div class="add-cart">
                        <a href="">Thêm vào giỏ hàng</a>
                      </div>
                    </div>
                    <div class="bottom-product">
                      <a href="" class="name">Kem chống nắng Caryophy Smart Sunscreen 50ML</a>
                      <div class="price">
                        <span class="price-origin">207,000 đ</span>
                        <span class="price-sale">35,000 đ</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="product">
                  <div class="product-item">
                    <div class="top-product">
                      <a href="" class="image-product">
                        <img src="{{static_asset('assets/img/demo/demo1.png')}}" alt="">
                      </a>
                      <div class="promotion">
                        <span class="discount">-35%</span>
                      </div>
                      <a href="#" class="wishlist">
                        <img class="add" src="{{static_asset('assets/img/icons/wishlist.png')}}" alt="">
                        <img class="added" src="{{static_asset('assets/img/icons/wishlisted.png')}}" alt="">
                      </a>
                      <div class="add-cart">
                        <a href="">Thêm vào giỏ hàng</a>
                      </div>
                    </div>
                    <div class="bottom-product">
                      <a href="" class="name">Kem chống nắng Caryophy Smart Sunscreen 50ML</a>
                      <div class="price">
                        <span class="price-origin">207,000 đ</span>
                        <span class="price-sale">35,000 đ</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="section-best-sellers">
              <div class="heading-product">
                <h3 class="title">
                  Best Sellers
                </h3>
                <div class="see-all">
                  <a href=""><span>Xem Tất Cả</span> 
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                      <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5l7 7-7 7" />
                      </svg>
                  </a>
                </div>
              </div>
              <div class="list-sellers list-global">
                <div class="product">
                  <div class="product-item">
                    <div class="top-product">
                      <a href="" class="image-product">
                        <img src="{{static_asset('assets/img/demo/demo3.jpg')}}" alt="">
                      </a>
                      <div class="promotion">
                        <span class="discount">-35%</span>
                      </div>
                      <a href="#" class="wishlist">
                        <img class="add" src="{{static_asset('assets/img/icons/wishlist.png')}}" alt="">
                        <img class="added" src="{{static_asset('assets/img/icons/wishlisted.png')}}" alt="">
                      </a>
                      <div class="add-cart">
                        <a href="">Thêm vào giỏ hàng</a>
                      </div>
                    </div>
                    <div class="bottom-product">
                      <a href="" class="name">Kem chống nắng Caryophy Smart Sunscreen 50ML</a>
                      <div class="price">
                        <span class="price-origin">207,000 đ</span>
                        <span class="price-sale">35,000 đ</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="product">
                  <div class="product-item">
                    <div class="top-product">
                      <a href="" class="image-product">
                        <img src="{{static_asset('assets/img/demo/demo1.png')}}" alt="">
                      </a>
                      <div class="promotion">
                        <span class="discount">-35%</span>
                      </div>
                      <a href="#" class="wishlist">
                        <img class="add" src="{{static_asset('assets/img/icons/wishlist.png')}}" alt="">
                        <img class="added" src="{{static_asset('assets/img/icons/wishlisted.png')}}" alt="">
                      </a>
                      <div class="add-cart">
                        <a href="">Thêm vào giỏ hàng</a>
                      </div>
                    </div>
                    <div class="bottom-product">
                      <a href="" class="name">Kem chống nắng Caryophy Smart Sunscreen 50ML</a>
                      <div class="price">
                        <span class="price-origin">207,000 đ</span>
                        <span class="price-sale">35,000 đ</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="product">
                  <div class="product-item">
                    <div class="top-product">
                      <a href="" class="image-product">
                        <img src="{{static_asset('assets/img/demo/demo1.png')}}" alt="">
                      </a>
                      <div class="promotion">
                        <span class="discount">-35%</span>
                      </div>
                      <a href="#" class="wishlist">
                        <img class="add" src="{{static_asset('assets/img/icons/wishlist.png')}}" alt="">
                        <img class="added" src="{{static_asset('assets/img/icons/wishlisted.png')}}" alt="">
                      </a>
                      <div class="add-cart">
                        <a href="">Thêm vào giỏ hàng</a>
                      </div>
                    </div>
                    <div class="bottom-product">
                      <a href="" class="name">Kem chống nắng Caryophy Smart Sunscreen 50ML</a>
                      <div class="price">
                        <span class="price-origin">207,000 đ</span>
                        <span class="price-sale">35,000 đ</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="product">
                  <div class="product-item">
                    <div class="top-product">
                      <a href="" class="image-product">
                        <img src="{{static_asset('assets/img/demo/demo1.png')}}" alt="">
                      </a>
                      <div class="promotion">
                        <span class="discount">-35%</span>
                      </div>
                      <a href="#" class="wishlist">
                        <img class="add" src="{{static_asset('assets/img/icons/wishlist.png')}}" alt="">
                        <img class="added" src="{{static_asset('assets/img/icons/wishlisted.png')}}" alt="">
                      </a>
                      <div class="add-cart">
                        <a href="">Thêm vào giỏ hàng</a>
                      </div>
                    </div>
                    <div class="bottom-product">
                      <a href="" class="name">Kem chống nắng Caryophy Smart Sunscreen 50ML</a>
                      <div class="price">
                        <span class="price-origin">207,000 đ</span>
                        <span class="price-sale">35,000 đ</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="product">
                  <div class="product-item">
                    <div class="top-product">
                      <a href="" class="image-product">
                        <img src="{{static_asset('assets/img/demo/demo1.png')}}" alt="">
                      </a>
                      <div class="promotion">
                        <span class="discount">-35%</span>
                      </div>
                      <a href="#" class="wishlist">
                        <img class="add" src="{{static_asset('assets/img/icons/wishlist.png')}}" alt="">
                        <img class="added" src="{{static_asset('assets/img/icons/wishlisted.png')}}" alt="">
                      </a>
                      <div class="add-cart">
                        <a href="">Thêm vào giỏ hàng</a>
                      </div>
                    </div>
                    <div class="bottom-product">
                      <a href="" class="name">Kem chống nắng Caryophy Smart Sunscreen 50ML</a>
                      <div class="price">
                        <span class="price-origin">207,000 đ</span>
                        <span class="price-sale">35,000 đ</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="product">
                  <div class="product-item">
                    <div class="top-product">
                      <a href="" class="image-product">
                        <img src="{{static_asset('assets/img/demo/demo1.png')}}" alt="">
                      </a>
                      <div class="promotion">
                        <span class="discount">-35%</span>
                      </div>
                      <a href="#" class="wishlist">
                        <img class="add" src="{{static_asset('assets/img/icons/wishlist.png')}}" alt="">
                        <img class="added" src="{{static_asset('assets/img/icons/wishlisted.png')}}" alt="">
                      </a>
                      <div class="add-cart">
                        <a href="">Thêm vào giỏ hàng</a>
                      </div>
                    </div>
                    <div class="bottom-product">
                      <a href="" class="name">Kem chống nắng Caryophy Smart Sunscreen 50ML</a>
                      <div class="price">
                        <span class="price-origin">207,000 đ</span>
                        <span class="price-sale">35,000 đ</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="product">
                  <div class="product-item">
                    <div class="top-product">
                      <a href="" class="image-product">
                        <img src="{{static_asset('assets/img/demo/demo1.png')}}" alt="">
                      </a>
                      <div class="promotion">
                        <span class="discount">-35%</span>
                      </div>
                      <a href="#" class="wishlist">
                        <img class="add" src="{{static_asset('assets/img/icons/wishlist.png')}}" alt="">
                        <img class="added" src="{{static_asset('assets/img/icons/wishlisted.png')}}" alt="">
                      </a>
                      <div class="add-cart">
                        <a href="">Thêm vào giỏ hàng</a>
                      </div>
                    </div>
                    <div class="bottom-product">
                      <a href="" class="name">Kem chống nắng Caryophy Smart Sunscreen 50ML</a>
                      <div class="price">
                        <span class="price-origin">207,000 đ</span>
                        <span class="price-sale">35,000 đ</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="section-banner-body">
              <a href="#" class="banner-body">
                <img src="{{static_asset('assets/img/demo/banner-footer.jpeg')}}" alt="">
              </a>
            </div>
            <div class="section-brand">
              <div class="list-brands">
                <div class="brand">
                  <div class="brand-item">
                    <img src="{{static_asset('assets/img/brand/around-me.jpg')}}" alt="Around Me">
                  </div>
                </div>
                <div class="brand">
                  <div class="brand-item">
                    <img src="{{static_asset('assets/img/brand/atopalm.jpg')}}" alt="Atopalm">
                  </div>
                </div>
                <div class="brand">
                  <div class="brand-item">
                    <img src="{{static_asset('assets/img/brand/berrisom.jpg')}}" alt="berrisom">
                  </div>
                </div>
                <div class="brand">
                  <div class="brand-item">
                    <img src="{{static_asset('assets/img/brand/black-rouge.jpg')}}" alt="BlackRouge">
                  </div>
                </div>
                <div class="brand">
                  <div class="brand-item">
                    <img src="{{static_asset('assets/img/brand/cleaning-time.jpg')}}" alt="Cleaning Time">
                  </div>
                </div>
                <div class="brand">
                  <div class="brand-item">
                    <img src="{{static_asset('assets/img/brand/clubpiaf.jpg')}}" alt="Club Piaf">
                  </div>
                </div>
                <div class="brand">
                  <div class="brand-item">
                    <img src="{{static_asset('assets/img/brand/cocodor.jpg')}}" alt="Cocodor">
                  </div>
                </div>
                <div class="brand">
                  <div class="brand-item">
                    <img src="{{static_asset('assets/img/brand/cshop.jpg')}}" alt="C Shop">
                  </div>
                </div>
                <div class="brand">
                  <div class="brand-item">
                    <img src="{{static_asset('assets/img/brand/daily-aqua.jpg')}}" alt="Daily Aqua">
                  </div>
                </div>
                <div class="brand">
                  <div class="brand-item">
                    <img src="{{static_asset('assets/img/brand/derm-all-matrix.jpg')}}" alt="Derm all maxtrix">
                  </div>
                </div>
                <div class="brand">
                  <div class="brand-item">
                    <img src="{{static_asset('assets/img/brand/dewytree.jpg')}}" alt="Dewytree">
                  </div>
                </div>
                <div class="brand">
                  <div class="brand-item">
                    <img src="{{static_asset('assets/img/brand/drseed.jpg')}}" alt="Drseed">
                  </div>
                </div>
                <div class="brand">
                  <div class="brand-item">
                    <img src="{{static_asset('assets/img/brand/ferderma.jpg')}}" alt="Ferderma">
                  </div>
                </div>
                <div class="brand">
                  <div class="brand-item">
                    <img src="{{static_asset('assets/img/brand/frudia.jpg')}}" alt="Frudia">
                  </div>
                </div>
                <div class="brand">
                  <div class="brand-item">
                    <img src="{{static_asset('assets/img/brand/im-sorry-for-my-skin.jpg')}}" alt="Im sorry for my skin">
                  </div>
                </div>
                <div class="brand">
                  <div class="brand-item">
                    <img src="{{static_asset('assets/img/brand/javin.jpg')}}" alt="Javin">
                  </div>
                </div>
                <div class="brand">
                  <div class="brand-item">
                    <img src="{{static_asset('assets/img/brand/lalarecipe.jpg')}}" alt="Lalarecipe">
                  </div>
                </div>
                <div class="brand">
                  <div class="brand-item">
                    <img src="{{static_asset('assets/img/brand/lavouvee-biato.jpg')}}" alt="Lavouvee Biato">
                  </div>
                </div>
                <div class="brand">
                  <div class="brand-item">
                    <img src="{{static_asset('assets/img/brand/manneric.jpg')}}" alt="Manneric">
                  </div>
                </div>
                <div class="brand">
                  <div class="brand-item">
                    <img src="{{static_asset('assets/img/brand/nature-dream.jpg')}}" alt="Nature Dream">
                  </div>
                </div>
                <div class="brand">
                  <div class="brand-item">
                    <img src="{{static_asset('assets/img/brand/pedison.jpg')}}" alt="Pedison">
                  </div>
                </div>
                <div class="brand">
                  <div class="brand-item">
                    <img src="{{static_asset('assets/img/brand/manneric.jpg')}}" alt="Manneric">
                  </div>
                </div>
                <div class="brand">
                  <div class="brand-item">
                    <img src="{{static_asset('assets/img/brand/plan.jpg')}}" alt="Plan">
                  </div>
                </div>
                <div class="brand">
                  <div class="brand-item">
                    <img src="{{static_asset('assets/img/brand/seoul-rose.jpg')}}" alt="Seoul Rose">
                  </div>
                </div>
                <div class="brand">
                  <div class="brand-item">
                    <img src="{{static_asset('assets/img/brand/skinmiso.jpg')}}" alt="Skin Miso">
                  </div>
                </div>
                <div class="brand">
                  <div class="brand-item">
                    <img src="{{static_asset('assets/img/brand/vari-hope.jpg')}}" alt="Vari Hope">
                  </div>
                </div>
                
              </div>
            </div>
            <div class="section-sale-collection">
              <div class="heading-product">
                <h3 class="title">
                  Sale Collection
                </h3>
                <div class="see-all">
                  <a href=""><span>Xem Tất Cả</span> 
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                      <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5l7 7-7 7" />
                      </svg>
                  </a>
                </div>
              </div>
              <div class="list-sale-collections list-global">
                <div class="product">
                  <div class="product-item">
                    <div class="top-product">
                      <a href="" class="image-product">
                        <img src="{{static_asset('assets/img/demo/demo3.jpg')}}" alt="">
                      </a>
                      <div class="promotion">
                        <span class="discount">-35%</span>
                      </div>
                      <a href="#" class="wishlist">
                        <img class="add" src="{{static_asset('assets/img/icons/wishlist.png')}}" alt="">
                        <img class="added" src="{{static_asset('assets/img/icons/wishlisted.png')}}" alt="">
                      </a>
                      <div class="add-cart">
                        <a href="">Thêm vào giỏ hàng</a>
                      </div>
                    </div>
                    <div class="bottom-product">
                      <a href="" class="name">Kem chống nắng Caryophy Smart Sunscreen 50ML</a>
                      <div class="price">
                        <span class="price-origin">207,000 đ</span>
                        <span class="price-sale">35,000 đ</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="product">
                  <div class="product-item">
                    <div class="top-product">
                      <a href="" class="image-product">
                        <img src="{{static_asset('assets/img/demo/demo1.png')}}" alt="">
                      </a>
                      <div class="promotion">
                        <span class="discount">-35%</span>
                      </div>
                      <a href="#" class="wishlist">
                        <img class="add" src="{{static_asset('assets/img/icons/wishlist.png')}}" alt="">
                        <img class="added" src="{{static_asset('assets/img/icons/wishlisted.png')}}" alt="">
                      </a>
                      <div class="add-cart">
                        <a href="">Thêm vào giỏ hàng</a>
                      </div>
                    </div>
                    <div class="bottom-product">
                      <a href="" class="name">Kem chống nắng Caryophy Smart Sunscreen 50ML</a>
                      <div class="price">
                        <span class="price-origin">207,000 đ</span>
                        <span class="price-sale">35,000 đ</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="product">
                  <div class="product-item">
                    <div class="top-product">
                      <a href="" class="image-product">
                        <img src="{{static_asset('assets/img/demo/demo1.png')}}" alt="">
                      </a>
                      <div class="promotion">
                        <span class="discount">-35%</span>
                      </div>
                      <a href="#" class="wishlist">
                        <img class="add" src="{{static_asset('assets/img/icons/wishlist.png')}}" alt="">
                        <img class="added" src="{{static_asset('assets/img/icons/wishlisted.png')}}" alt="">
                      </a>
                      <div class="add-cart">
                        <a href="">Thêm vào giỏ hàng</a>
                      </div>
                    </div>
                    <div class="bottom-product">
                      <a href="" class="name">Kem chống nắng Caryophy Smart Sunscreen 50ML</a>
                      <div class="price">
                        <span class="price-origin">207,000 đ</span>
                        <span class="price-sale">35,000 đ</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="product">
                  <div class="product-item">
                    <div class="top-product">
                      <a href="" class="image-product">
                        <img src="{{static_asset('assets/img/demo/demo1.png')}}" alt="">
                      </a>
                      <div class="promotion">
                        <span class="discount">-35%</span>
                      </div>
                      <a href="#" class="wishlist">
                        <img class="add" src="{{static_asset('assets/img/icons/wishlist.png')}}" alt="">
                        <img class="added" src="{{static_asset('assets/img/icons/wishlisted.png')}}" alt="">
                      </a>
                      <div class="add-cart">
                        <a href="">Thêm vào giỏ hàng</a>
                      </div>
                    </div>
                    <div class="bottom-product">
                      <a href="" class="name">Kem chống nắng Caryophy Smart Sunscreen 50ML</a>
                      <div class="price">
                        <span class="price-origin">207,000 đ</span>
                        <span class="price-sale">35,000 đ</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="product">
                  <div class="product-item">
                    <div class="top-product">
                      <a href="" class="image-product">
                        <img src="{{static_asset('assets/img/demo/demo1.png')}}" alt="">
                      </a>
                      <div class="promotion">
                        <span class="discount">-35%</span>
                      </div>
                      <a href="#" class="wishlist">
                        <img class="add" src="{{static_asset('assets/img/icons/wishlist.png')}}" alt="">
                        <img class="added" src="{{static_asset('assets/img/icons/wishlisted.png')}}" alt="">
                      </a>
                      <div class="add-cart">
                        <a href="">Thêm vào giỏ hàng</a>
                      </div>
                    </div>
                    <div class="bottom-product">
                      <a href="" class="name">Kem chống nắng Caryophy Smart Sunscreen 50ML</a>
                      <div class="price">
                        <span class="price-origin">207,000 đ</span>
                        <span class="price-sale">35,000 đ</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="product">
                  <div class="product-item">
                    <div class="top-product">
                      <a href="" class="image-product">
                        <img src="{{static_asset('assets/img/demo/demo1.png')}}" alt="">
                      </a>
                      <div class="promotion">
                        <span class="discount">-35%</span>
                      </div>
                      <a href="#" class="wishlist">
                        <img class="add" src="{{static_asset('assets/img/icons/wishlist.png')}}" alt="">
                        <img class="added" src="{{static_asset('assets/img/icons/wishlisted.png')}}" alt="">
                      </a>
                      <div class="add-cart">
                        <a href="">Thêm vào giỏ hàng</a>
                      </div>
                    </div>
                    <div class="bottom-product">
                      <a href="" class="name">Kem chống nắng Caryophy Smart Sunscreen 50ML</a>
                      <div class="price">
                        <span class="price-origin">207,000 đ</span>
                        <span class="price-sale">35,000 đ</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="product">
                  <div class="product-item">
                    <div class="top-product">
                      <a href="" class="image-product">
                        <img src="{{static_asset('assets/img/demo/demo1.png')}}" alt="">
                      </a>
                      <div class="promotion">
                        <span class="discount">-35%</span>
                      </div>
                      <a href="#" class="wishlist">
                        <img class="add" src="{{static_asset('assets/img/icons/wishlist.png')}}" alt="">
                        <img class="added" src="{{static_asset('assets/img/icons/wishlisted.png')}}" alt="">
                      </a>
                      <div class="add-cart">
                        <a href="">Thêm vào giỏ hàng</a>
                      </div>
                    </div>
                    <div class="bottom-product">
                      <a href="" class="name">Kem chống nắng Caryophy Smart Sunscreen 50ML</a>
                      <div class="price">
                        <span class="price-origin">207,000 đ</span>
                        <span class="price-sale">35,000 đ</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="section-service">
              <div class="service-list row">
                <div class="service col-md-4">
                  <a href="" class="service-item">
                    <img src="{{static_asset('assets/img/demo/quality.png')}}" alt="Giao hàng nhanh chóng và uy tín">
                  </a>
                </div>
                <div class="service col-md-4">
                  <a href="" class="service-item">
                    <img src="{{static_asset('assets/img/demo/service.png')}}" alt="Dịch Vụ 24/7">
                  </a>
                </div>
                <div class="service col-md-4">
                  <a href="" class="service-item">
                    <img src="{{static_asset('assets/img/demo/delivery.png')}}" alt="Cam kết chất lượng">
                  </a>
                </div>
              </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
      $(document).ready(function(){
        $('.list-product').slick({
          dots: false,
          infinite: true,
          speed: 300,
          slidesToShow: 5,
          slidesToScroll: 1,
          swipeToSlide: true,
          nextArrow: '<button class="arrow_next item-arrow"><img src="{{static_asset('assets/img/icons/flash_next.png')}}" alt="Next"></button>',
          prevArrow: '<button class="arrow_prev item-arrow"><img src="{{static_asset('assets/img/icons/flash_prev.png')}}" alt="Prev"></button>',
          responsive: [
            {
              breakpoint: 1200,
              settings: {
                slidesToShow: 4,
                slidesToScroll: 1,
              
              }
            },
            {
              breakpoint: 991,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
              
              }
            },
            {
              breakpoint: 767,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 1
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 1
              }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
          ]
        });
        $('.list-products').slick({
          dots: false,
          infinite: false,
          speed: 300,
          slidesToShow: 5,
          swipeToSlide: true,
          slidesToScroll: 1,
          nextArrow: `<div class="arrow arrow-next">
            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
              <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5l7 7-7 7" />
            </svg>
          </div>`,
          prevArrow: `<div class="arrow arrow-prev">
            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
              <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 19l-7-7 7-7" />
            </svg>
          </div>`,
          responsive: [
            {
              breakpoint: 1200,
              settings: {
                slidesToShow: 4,
                slidesToScroll: 1,
              
              }
            },
            {
              breakpoint: 991,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
              
              }
            },
            {
              breakpoint: 767,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 1
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 1
              }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
          ]
        });
        $('.list-sellers').slick({
          dots: false,
          infinite: false,
          speed: 300,
          slidesToShow: 5,
          swipeToSlide: true,
          slidesToScroll: 1,
          nextArrow: `<div class="arrow arrow-next">
            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
              <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5l7 7-7 7" />
            </svg>
          </div>`,
          prevArrow: `<div class="arrow arrow-prev">
            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
              <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 19l-7-7 7-7" />
            </svg>
          </div>`,
          responsive: [
            {
              breakpoint: 1200,
              settings: {
                slidesToShow: 4,
                slidesToScroll: 1,
              
              }
            },
            {
              breakpoint: 991,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
              
              }
            },
            {
              breakpoint: 767,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 1
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 1
              }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
          ]
        });
        $('.list-category').slick({
          dots: false,
          infinite: false,
          speed: 300,
          slidesToShow: 8,
          slidesToScroll: 1,
          swipeToSlide: true,
          responsive: [
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 7,
                slidesToScroll: 1,
              
              }
            },
            {
              breakpoint: 767,
              settings: {
                slidesToShow: 6,
                slidesToScroll: 2
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 5,
                slidesToScroll: 1
              }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
          ]
        });
        $('.list-brands').slick({
          dots: false,
          infinite: false,
          speed: 300,
          slidesToShow: 5,
          slidesToScroll: 1,
          swipeToSlide: true,
          nextArrow: '<div class="arrow_next item-arrow"><img src="{{static_asset('assets/img/icons/flash_next.png')}}" alt="Next"></div>',
          prevArrow: '<div class="arrow_prev item-arrow"><img src="{{static_asset('assets/img/icons/flash_prev.png')}}" alt="Prev"></div>',
          responsive: [
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 4,
                slidesToScroll: 1,
              
              }
            },
            {
              breakpoint: 767,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 1
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 1
              }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
          ]
        });
        $('.list-sale-collections').slick({
          dots: false,
          infinite: false,
          speed: 300,
          slidesToShow: 5,
          slidesToScroll: 1,
          swipeToSlide: true,
          nextArrow: `<div class="arrow arrow-next">
            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
              <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5l7 7-7 7" />
            </svg>
          </div>`,
          prevArrow: `<div class="arrow arrow-prev">
            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
              <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 19l-7-7 7-7" />
            </svg>
          </div>`,
          responsive: [
            {
              breakpoint: 1200,
              settings: {
                slidesToShow: 4,
                slidesToScroll: 1,
              
              }
            },
            {
              breakpoint: 991,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
              
              }
            },
            {
              breakpoint: 767,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 1
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 1
              }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
          ]
        });
        $('.list-sale-collections .wishlist').click(function(e){
          e.preventDefault();
          $(this).toggleClass('active');
        })
        $('.section-best-sellers .wishlist').click(function(e){
          e.preventDefault();
          $(this).toggleClass('active');
        })
        $('.section-new-arrival .wishlist').click(function(e){
          e.preventDefault();
          $(this).toggleClass('active');
        })
      })
    </script>
@endsection