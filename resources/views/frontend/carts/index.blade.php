<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="slick/slick-theme.css"/>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <link rel="stylesheet" href="/public/assets/css/cart.css">
    <link rel="stylesheet" href="/public/assets/css/global.css">
    <title>Giỏ Hàng</title>
</head>
<body>
    @include('frontend.partials.header')
    <div class="main_content">
        <div class="cart_page">
            <div class="row container-cart">
                <div class="col-12">
                    <h2 class="title_page ">Giỏ hàng <span class="">(2 sản phẩm)</span></h2>
                </div>
                {{-- slide left --}}
                <div class="col-12 col-md-7 ">
                    <div class="card">
                    @if(count($products ?? []))
                    @foreach($products as $item)
                        <div class="card-header checkbox-wrap">
                            <div class="check-box">
                                <input type="checkbox" onclick="toggle(this)">
                                <span class="checkmark">Chọn tất cả sản phẩm</span>
                            </div>
                        </div>
                        <div class="card-body table-responsive">
                            <div class="">
                                <div class="d-flex bd-highlight mb-0">
                                    <div class="check-box2">
                                        <input type="checkbox" name="check1">
                                    </div>
                                    <div class="bd-highlight avatar-product">
                                        {{-- <img class="img-cart" src="/public/assets/img/obagi_salicylic_acid_2_a_5_clenziderm_md_pore_therapy_orchard_vn_2.png" alt=""> --}}
                                        
                                        @if($images && $images[0])
                                                <img src="{{static_asset($images[0]) }}" class="img-cart" alt="{{$item->name}}"> 
                                        @endif
                                        
                                    </div>
                                    <div class="bd-highlight dash-flex">
                                        <a href="#"> 
                                            <p class="mb-1 text-dark title-product font-text"> {{$item->name}}</p>
                                        </a>
                                        <p class="mb-1 volume-product font-text"> Size: {{$item->size}} </p>
                                        <p class="mb-1 color-product font-text">Color: Red Velvet </p>
                                    </div>
                                    <div class="ml-auto bd-highlight money-product">
                                        <label class="mb-0 money-sale"> <del>225,000 đ</del></label> <br>
                                        <label class="mb-0 money">225,000 đ </label>
                                    </div>
                                </div>
                                <div class="update-quantity">
                                    <div class="buttons_added">
                                        <input class="minus is-form" type="button" value="-">
                                        <input aria-label="quantity" class="input-qty" max="100" min="1" name="" type="number" value="1">
                                        <input class="plus is-form" type="button" value="+">
                                    </div>
                                    <div class="delete-product">
                                        <a class="" href="#"><p class="btn-del">Xóa</p></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                    @endforeach
                    @endif
                    </div>
                </div>
                {{-- slide right --}}
                <div class="col-12 col-md-5 res">
                    <div class="info-delirery">
                        <div class="delirery-body">
                            <div class="title-delivery">
                                <p class="delivery">Địa chỉ nhận hàng</p>
                                <a href="#"> <p class="edit-delivery">Thay đổi</p> </a>
                            </div>
                            <div class="content-delivery">
                                <p class="name-user">Nguyễn minh anh <span class="text-color">|</span> 0915123123</p>    
                                <p class="add-user">39 Lê thach, phường 12, quận 4, tpHCM</p>  
                            </div>   
                        </div>
                                            
                    </div>
                    <div class="info-sale">
                        <div class="sale-body">
                            <div class="title-sale">
                                <p class="code-sale">Mã giảm giá</p>
                            </div>
                            <div class="insert">
                                <input type="text" placeholder="Nhập mã giảm giá" class="insert-code">
                            </div>
                            <div class="content-sale">
                                <p class="content-code">Tặng băng đồ rửa mặt THe Auragins cho đơn hàng từ 500K - Nhập mã <strong>BANGDO12</strong></p>
                            </div>   
                        </div>
                    </div>
                    <div class="info-price">
                        <div class="price-body">
                            <div class="title-price">
                                <p class="pro-price">Tiền hàng</p>
                                <p class="price">1.500.000đ</p>
                            </div>
                            <div class="title-price">
                                <p class="pro-price">Giảm giá</p>
                                <p class="price">0đ</p>
                            </div>
                            <div class="title-price">
                                <p class="pro-price">Phí giao hàng</p>
                                <p class="price">35.000đ</p>
                            </div>
                        </div>
                        <hr>
                        <div class="price-body-total">
                            <div class="title-price-total">
                                <p class="pro-price">Tạm tính</p>
                                <p class="price-total">1.700.000đ</p>
                            </div>
                        </div>
                    </div>
                    <div class="submit-order text-center">
                        <button type="submit" class="button btn-order">Đặt hàng</button>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
    <div class="main-favorite">
        <div class="favorite-page">
            <div class="row container-favorite ">
                <div class="col-12">
                    <h2 class="title-favorite">Có thể bạn cũng thích</h2>
                </div>
                <div class="d-flex col-12 pl-0 pr-0 ">
                    <div class="col-1 col-sm">
                        <div class="back-favorite">
                            <input class="back-icon" type="image" src="/public/assets/img/icons/back-svgrepo-com.svg">
                        </div>
                    </div>
                    <div class="d-flex col-10 col-sm ">
                        <div class="card mr-20 ">
                            <div class="flast-sale ">
                                <p class="discount text-center">-30%</p>
                                <p class="new text-center">New</p>
                            </div>
                            <div class="flast-sale2 align-items-end d-flex">
                                <button class="product-favorite"> <img src="/public/assets/img/icons/alert.svg" alt="image"></button>
                            </div>
                            <div>
                                <div class="to-cart text-center ">
                                    <a href="#"> <p class="add-to-cart">Thêm vào giỏ hàng</p></a>
                                </div>
                                <img src="/public/assets/img/image_525.png" class="card-img" alt="image">
                            </div>
                            <div class="card-body title-name">
                              <a href="#"> <p class="name-product">Up the bulk of the card's content.</p> </a>
                            </div>
                            <div class="d-flex">
                                <div class="money-favorite">
                                    <p class="money-favorite1">225.000 đ</p>
                                </div>
                                <div class="">
                                    <p class="money-favorite2"> <del>456.000 đ</del></p>
                                </div>
                                <div class="ml-auto freeship">
                                    <img class="img-freeship" src="/public/assets/img/icons/free_delivery_1.svg" alt="image">
                                </div>
                              </div>
                        </div>
                        <div class="card mr-20 ">
                            <div class="flast-sale ">
                                <p class="discount text-center">-30%</p>
                                <p class="new text-center">New</p>
                            </div>
                            <div class="flast-sale2 align-items-end d-flex">
                                <button class="product-favorite"> <img src="/public/assets/img/icons/alert.svg" alt="image"></button>
                            </div>
                            <div>
                                <div class="to-cart text-center">
                                    <p class="add-to-cart">Thêm vào giỏ hàng</p>
                                </div>
                                <img src="/public/assets/img/image_525.png" class="card-img" alt="image">
                            </div>
                            <div class="card-body title-name">
                              <p class="name-product">Up the bulk of the card's content.</p>
                            </div>
                            <div class="d-flex">
                                <div class="money-favorite">
                                    <p class="money-favorite1">225.000 đ</p>
                                </div>
                                <div class="">
                                    <p class="money-favorite2"> <del>456.000 đ</del></p>
                                </div>
                                <div class="ml-auto freeship">
                                    <img class="img-freeship" src="/public/assets/img/icons/free_delivery_1.svg" alt="image">
                                </div>
                              </div>
                        </div>
                        <div class="card mr-20 ">
                            <div class="flast-sale ">
                            </div>
                            <div class="flast-sale2 align-items-end d-flex">
                                <button class="product-favorite"> <img src="/public/assets/img/icons/alert.svg" alt="image"></button>
                            </div>
                            <div>
                                <div class="to-cart text-center ">
                                    <p class="add-to-cart">Thêm vào giỏ hàng</p>
                                </div>
                                <img src="/public/assets/img/image_525.png" class="card-img" alt="image">
                            </div>
                            <div class="card-body title-name">
                              <p class="name-product">Up the bulk of the card's content.</p>
                            </div>
                            <div class="d-flex">
                                <div class="money-favorite">
                                    <p class="money-favorite1">225.000 đ</p>
                                </div>
                                <div class="">
                                    <p class="money-favorite2"> <del>456.000 đ</del></p>
                                </div>
                                <div class="ml-auto freeship">
                                    <img class="img-freeship" src="/public/assets/img/icons/free_delivery_1.svg" alt="image">
                                </div>
                              </div>
                        </div>
                        <div class="card mr-20 ">
                            <div class="flast-sale ">
                                <p class="new text-center">New</p>
                            </div>
                            <div class="flast-sale2 align-items-end d-flex">
                                <button class="product-favorite"> <img src="/public/assets/img/icons/alert.svg" alt="image"></button>
                            </div>
                            <div>
                                <div class="to-cart text-center">
                                    <p class="add-to-cart">Thêm vào giỏ hàng</p>
                                </div>
                                <img src="/public/assets/img/image_525.png" class="card-img" alt="image">
                            </div>
                            <div class="card-body title-name">
                              <p class="name-product">Up the bulk of the card's content.</p>
                            </div>
                            <div class="d-flex">
                                <div class="money-favorite">
                                    <p class="money-favorite1">225.000 đ</p>
                                </div>
                                <div class="">
                                    <p class="money-favorite2"> <del>456.000 đ</del></p>
                                </div>
                                <div class="ml-auto freeship">
                                    <img class="img-freeship" src="/public/assets/img/icons/free_delivery_1.svg" alt="image">
                                </div>
                              </div>
                        </div>
                        <div class="card ">
                            <div class="flast-sale ">
                            </div>
                            <div class="flast-sale2 align-items-end d-flex">
                                <button class="product-favorite"> <img src="/public/assets/img/icons/alert.svg" alt="image"></button>
                            </div>
                            <div>
                                <div class="to-cart text-center d-none">
                                    <p class="add-to-cart">Thêm vào giỏ hàng</p>
                                </div>
                                <img src="/public/assets/img/image_525.png" class="card-img" alt="image">
                            </div>
                            <div class="card-body title-name">
                              <p class="name-product">Up the bulk of the card's content.</p>
                            </div>
                            <div class="d-flex">
                                <div class="money-favorite">
                                    <p class="money-favorite1">225.000 đ</p>
                                </div>
                                <div class="">
                                    <p class="money-favorite2"> <del>456.000 đ</del></p>
                                </div>
                                <div class="ml-auto freeship">
                                    <img class="img-freeship" src="/public/assets/img/icons/free_delivery_1.svg" alt="image">
                                </div>
                              </div>
                        </div>
                    </div>
                    <div class="col-1 col-sm">
                        <div class="next-favorite">
                            <input class="next-icon" type="image" src="/public/assets/img/icons/next-svgrepo-com.svg">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>
    <script type="text/javascript">

    
    function toggle(source) {
        checkboxes = document.getElementsByName('check1');
        for(var i=0, n=checkboxes.length;i<n;i++) {
            checkboxes[i].checked = source.checked;
        }
    }

    $('input.input-qty').each(function() {
    var $this = $(this),
        qty = $this.parent().find('.is-form'),
        min = Number($this.attr('min')),
        max = Number($this.attr('max'))
    if (min == 0) {
        var d = 0
    } else d = min

    $(qty).on('click', function() {
        if ($(this).hasClass('minus')) {
        if (d > min) d += -1
        } else if ($(this).hasClass('plus')) {
        var x = Number($this.val()) + 1
        if (x <= max) d += 1
        }
        $this.attr('value', d).val(d)
        })
    })
    </script>

  
@include('frontend.partials.footer')  
</body>
</html>