@extends('frontend.layout.app')
<title>Product Detail</title>
@section('css')
<link rel="stylesheet" href={{static_asset('assets/css/homepage.css')}}>
<link rel="stylesheet" href={{static_asset('assets/css/product.css')}}>
<script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
<script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css"/>
<link
rel="stylesheet"
href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"
/>
{{-- ALERT --}}
    <!-- CSS -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/alertify.min.css"/>
    <!-- Default theme -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/default.min.css"/>
    <!-- Semantic UI theme -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/semantic.min.css"/>
    <!-- Bootstrap theme -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/bootstrap.min.css"/>
<style>
    
</style>
@endsection

@section('content')
{{-- MENU --}}
    <section>
        <nav class="navbar navbar-expand navbar-light bg-white ">
            <div class="nav navbar-nav">
                <a class="nav-item nav-item-text" href="#">Beauty <span class="sr-only"></span></a>
                <a class="nav-item nav-item-text" href="#">Healthcare</a>
                <a class="nav-item nav-item-text" href="#">Fashion</a>
                <a class="nav-item nav-item-text" href="#">Accessories</a>
                <a class="nav-item nav-item-text" href="#">Sports</a>
                <a class="nav-item nav-item-text" href="#">Lifestyle</a>
                <a class="nav-item nav-item-text" href="#">Food Voucher</a>
            </div>
        </nav>
    </section>
{{-- END MENu --}}

{{-- CARDS PRODUCT --}}
    <section>
        <div class="cards">
            <ul class="cards-title">
                <li class ="cards-title-li"><a href="">Trang Chủ </a>
                </li>
                <li class ="cards-title-li">/
                </li>
                <li class ="cards-title-li"><a href="">Beauty</a>
                </li>
                <li class ="cards-title-li">/
                </li>
                <li class ="cards-title-li"><a href="">T Kem Chống Nắng Skin1004 Chiết Xuất Rau Má SPF50+ PA++++ 50ml Madagascar Centella Air-Fit Suncream </a>
                </li>
            </ul>

            <div class="cards-content">
                {{-- Card Wrapper --}}
                <div class="card-content">
                    {{-- LEFT --}}
                    <div class="card-left">
                        <div class="card-image">
                            <a href="#" class="banner-link">
                                <img src="{{static_asset('assets/img/demo/demo3.jpg')}}" class="d-block w-100 w-100" alt="...">
                            </a>
                        </div>
                        <div class="card-slick">
                                <div class="carousel">
                                  <div class="carousel-inner"><img src="https://picsum.photos/300/200?random=1"></div>
                                  <div class="carousel-inner"><img src="https://picsum.photos/300/200?random=2"></div>
                                  <div class="carousel-inner"><img src="https://picsum.photos/300/200?random=3"></div>
                                  <div class="carousel-inner"><img src="https://picsum.photos/300/200?random=4"></div>
                                  <div class="carousel-inner"><img src="https://picsum.photos/300/200?random=5"></div>
                                  <div class="carousel-inner"><img src="https://picsum.photos/300/200?random=6"></div>
                                </div>
                        </div>
                    </div>
                    {{-- END LEFT --}}
                    {{-- RIGHT --}}
                    <div class="card-right">
                        <div class="card-right-content">
                            <div class="card-right-header">
                                <h3>Kem Chống Nắng Skin1004 Chiết Xuất Rau Má SPF50+ PA++++ 50ml Madagascar Centella Air-Fit Suncream</h3>
                            </div>
                            <div class="card-right-body">
                                <div class="card-right-start">
                                        <li class="start">
                                            <ion-icon name="star" class='gold'></ion-icon>
                                            <ion-icon name="star" class='gold'></ion-icon>
                                            <ion-icon name="star" class='gold'></ion-icon>
                                            <ion-icon name="star" class='gold'></ion-icon>
                                            <ion-icon name="star" class='gold'></ion-icon>
                                        </li>
                                        <li class="review">20 Đánh giá</li>
                                        <li class="sold">155 Đã bán</li>
                                </div>
                                <div class="card-right-price">
                                    <div class="card-price"><del>725,000đ</del></div>
                                    <div class="card-price-sale">225,000đ</div>
                                    <div class="card-sale animate__jello animate__animated animate__infinite	infinite">Giảm 30%</div>
                                </div>
                                <div class="card-right-transport">
                                    <div class="card-transport card-right-text">Vận chuyển</div>
                                    <div class="card-transport-method">
                                        <img src="{{static_asset('assets/img/icons/free_delivery_1.png')}}" alt=""> <p>Miễn phí vận chuyển</p>
                                    </div>
                                </div>
                                <div class="card-right-color">
                                    <div class="card-color-title card-right-text">Màu sắc</div>
                                    <div class="card-color-content">
                                        <select class="form-control" name="" id="">
                                            <option>Superstay - Gloplertroler</option>
                                            <option>Superstay - Gloplertroler</option>
                                            <option>Superstay - Gloplertroler</option>
                                        </select>
                                        <div class="card-color-list">
                                         <div class="card-color-item">
                                            <input type="radio" id="male-1" class="radio-input radio-input-1" name="gender" />
                                            <label for="male-1" class="radio-label  color-1"></label>
                                          </div>
                                          <div class="card-color-item">
                                            <input type="radio" id="male-2" class="radio-input radio-input-2" name="gender" />
                                            <label for="male-2" class="radio-label color-2"></label>
                                          </div>
                                          <div class="card-color-item">
                                            <input type="radio" id="male-3" class="radio-input radio-input-3" name="gender" />
                                            <label for="male-3" class="radio-label color-3"></label>
                                          </div>
                                          <div class="card-color-item">
                                            <input type="radio" id="male-4" class="radio-input radio-input-4" name="gender" />
                                            <label for="male-4" class="radio-label color-4"></label>
                                          </div>
                                          <div class="card-color-item">
                                            <input type="radio" id="male-5" class="radio-input radio-input-5" name="gender" />
                                            <label for="male-5" class="radio-label color-5"></label>
                                          </div>
                                          <div class="card-color-item">
                                            <input type="radio" id="male-6" class="radio-input radio-input-6" name="gender" />
                                            <label for="male-6" class="radio-label color-6"></label>
                                          </div>
                                        </div>
                                        
                                    </div>
                                </div>
                                <div class="card-right-size">
                                    <div class="card-size-title card-right-text">Kích thước</div>
                                    <div class="card-size-list">
                                        <div class="card-color-item">
                                            <input type="radio" id="size-1" value="30ml" class="radio-input size-radio-input-1" name="size" />
                                            <label for="size-1" class="size-radio-label checked-1">30ml</label>
                                        </div>
                                        <div class="card-color-item">
                                            <input type="radio" id="size-2"  value="60ml" class="radio-input size-radio-input-2" name="size" />
                                            <label for="size-2" class="size-radio-label checked-2">60ml</label>
                                        </div>
                                        <div class="card-color-item">
                                            <input type="radio" id="size-3" value="120ml"  class="radio-input size-radio-input-3" name="size" />
                                            <label for="size-3" class="size-radio-label checked-3">120ml</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-right-qty">
                                    <div class="card-qty-title card-right-text">Số Lượng</div>
                                    <div class="card-qty-content">
                                        <div class="qty-sub" onclick="subtract()">-</div>
                                        <input type="text" class="qty-number" min="0" max="100" id="qty-number" value="1">
                                        <div class="qty-add"  onclick="add()">+</div>
                                    </div>
                                </div>
                                </div>
                            <div class="card-right-footer">
                                <div class="card-button-add" onclick="addtocart()">Thêm vào giỏ hàng</div>
                                <div class="card-button-pay animate__animated animate__bounce animate__repeat-1">Mua ngay</div>
                                <div class="card-button-heart">
                                    <img src="{{static_asset('assets/img/icons/alert.svg')}}" alt=""> 
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- END RIGHT --}}
                </div>
                {{-- Detail Wrapper --}}
                
            </div>

        </div>
    </section>
{{-- END CARDS PRODUTC --}}

{{-- CARDS DETAIL --}}
    <section>
        <div class="cards">
            <div class="cards-detail">
                {{-- <div class="card"> --}}
                    <div class="detail-content">
                        <div class="detail-left ">
                            <div class="card-detail detail-bg">
                                <div class="detail-title">
                                    <h3>Chi tiết sản phẩm</h3>
                                </div>
                                <div class="detail-list">
                                    <ul class="detail-list-item">
                                        <li class="list-item-left">
                                            <a href="">Danh mục</a>
                                        </li>
                                        <li class="list-item-right list-item-category">
                                            <a href="#">Trang chủ </a>
                                            <a href=""><i class="fas fa-chevron-right"></i></a>
                                            <a href="#">Beauty</a>
                                            <a href=""><i class="fas fa-chevron-right"></i></a>
                                            <a href="#">Chăm sóc da</a>
                                        </li>
                                    </ul>
                                    <ul class="detail-list-item">
                                        <li class="list-item-left">
                                            <a href="">Thương hiệu</a>
                                        </li>
                                        <li class="list-item-right list-item-brand">
                                            <a href="#">Skin 1004</a>
                                        </li>
                                    </ul>
                                    <ul class="detail-list-item">
                                        <li class="list-item-left">
                                            <a href="">Dạng sản phẩm</a>
                                        </li>
                                        <li class="list-item-right list-item-material">
                                            <a href="#">Serum</a>
                                        </li>
                                    </ul>
                                    <ul class="detail-list-item">
                                        <li class="list-item-left">
                                            <a href="">Xuất xứ</a>
                                        </li>
                                        <li class="list-item-right list-item-origin">
                                            <a href="#">Hàn Quốc</a>
                                        </li>
                                    </ul>
                                    <ul class="detail-list-item">
                                        <li class="list-item-left">
                                            <a href="">Hạn sử dụng</a>
                                        </li>
                                        <li class="list-item-right list-item-expiry">
                                            <a href="#">12/12/2022</a>
                                        </li>
                                    </ul>
                                    <ul class="detail-list-item">
                                        <li class="list-item-left">
                                            <a href="">Kho hàng</a>
                                        </li>
                                        <li class="list-item-right list-item-warehouse">
                                            <a href="#">15</a>
                                        </li>
                                    </ul>

                                </div>
                            </div>
                            <div class="card-description detail-bg">
                                <div class="detail-title">
                                    <h3>Mô tả sản phẩm</h3>
                                </div>
                                <div class="detail-description">
                                       <h6> THÀNH PHẦN CỦA PHẤN NƯỚC MISSHA NEW EDITION M MAGIC CUSHION SPF 50+ PA +++ 15g:
                                        có các thành phần chính sau:
                                       </h6>
                                        - Titanium Dioxide, Octinoxate: chống nắng, bảo vệ làn da khỏi tác hại của tia cực tím.
                                        - Bột Silica – bead: giúp hấp thụ một cách hiệu quả mồ hôi và bã nhờn giúp lớp trang điểm được bền màu mà không bị xuống tông.
                                </div>
                                <div class="detail-image">
                                    <img src="{{static_asset('assets/img/demo/product1.png')}}" alt="" class="description-image">
                                </div>
                                <div class="detail-seemore">
                                    <a href="#boxnoidung" aria-expanded="false" data-toggle="collapse" onclick="changeSeemore()">Xem thêm
                                    <span><i class="fas fa-chevron-down" id="rotate"></i></span>
                                    </a>
                                </div>
                                <div class="collapse" id="boxnoidung">
                                    <div class="">
                                        <p class="card-text">
                                            <h6> CÁCH SỬ DỤNG PHẤN NƯỚC MISSHA NEW EDITION M MAGIC CUSHION :</h6>
                                            - Sau bước dưỡng da và kem chống nắng  (vào buổi sáng), sử dụng bông phấn kèm theo sản phẩm, lấy một lượng thích hợp và bắt đầu dặm lên mặt và cổ.
                                            - Tiếp tục dặm lại với một lượng sản phẩm thích hợp đối với những vùng da nhiều khuyết điểm hoặc cần che phế thêm.
                                            - Đóng chặt nắp sau khi sử dụng để tránh hoạt chất và độ ẩm bị bốc hơi.
                                            - Hoàn thiện nốt các bước trang điểm còn lại: má hồng, phấn phủ, son môi …
        
                                            <h6>LƯU Ý KHI SỬ DỤNG MISSHA NEW EDITION M MAGIC CUSHION SPF 50+ PA+++ (Phiên bản 2019)</h6>
                                            - Sử dụng bông phấn đúng cách bằng cách dặm (táp trên da mặt) đều tay, nhẹ nhàng trên da chứ không phải miết.
                                            - Chú ý vệ sinh bông phấn và khay đựng bông phấn đều đặn để tránh các vấn đề về da.
                                            - Sử dụng thêm che khuyết điểm để nâng cao hiệu quả che phế đối với da nhiều khuyết điểm.
                                            - Phủ một lớp phấn nhẹ sau lớp cushion để tạo sự khô thoáng cho làn da vào mùa hè đồng thời cố định lớp trang điểm (đặc biệt là các bạn da dầu, hỗn hợp).
        
                                            <h4>LƯU Ý KHI SỬ DỤNG MISSHA NEW EDITION M MAGIC CUSHION SPF 50+ PA+++ (Phiên bản 2019)</h6>
                                            <p><b> Ưu điểm:</b></p>
                                                Giá thành rẻ, dễ sử dụng.
                                                Khả năng che phủ ở mức chấp nhận được đối với da không có quá nhiều khuyết điểm.
                                                Mượt mà, không bị mốc hay khô da.
                                                Độ kiềm dầu ổn.
                                                Chỉ số chống nắng cao, kết thúc semi-matte chứ không ẩm như nhiều loại cushion khác.
            
                                            <p> <b>Nhược điểm:</b></p>
                                            Có quá ít tone màu, sẽ rất khó khăn cho các bạn có làn da không sáng, vì những màu này vốn được sản xuất cho các cô gái xứ Hàn có làn da như sứ sử dụng.
                                            Có lẽ sẽ ko phù hợp với bạn nào có làn da quá mẫn cảm hoặc dị ứng với hương liệu do trong kem mình thoáng nghe mùi hương hơi khó chịu chút.
        
        
                                            - Có hai tone màu:
                                            # 21 — Light beige ( trắng sáng dành cho da trắng).
                                            # 23 — Natural beige ( tự nhiên dành cho da trung bình — sáng).
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="card-review detail-bg">
                               <div class="card-review-top">
                                    <div class="detail-title">
                                        <h3>Đánh giá sản phẩm</h3>
                                    </div>
                                    <div class="card-comment-start">
                                        <li class="start">
                                            <ion-icon name="star" class='gold'></ion-icon>
                                            <ion-icon name="star" class='gold'></ion-icon>
                                            <ion-icon name="star" class='gold'></ion-icon>
                                            <ion-icon name="star" class='gold'></ion-icon>
                                            <ion-icon name="star" class='gold'></ion-icon>
                                        </li>
                                        <li class="review-number">4.9</li>
                                    </div>
                               </div>
                               <div class="card-review-bottom">
                                    <div class="card-review-image">
                                        <div class="review-image-title">
                                            <h3>Hình ảnh từ khách hàng</h3>
                                        </div>
                                        <div class="review-image-list">
                                            <li><img src="{{static_asset('assets/img/demo/demo3.jpg')}}" alt=""></li>
                                            <li><img src="{{static_asset('assets/img/demo/demo3.jpg')}}" alt=""></li>
                                            <li><img src="{{static_asset('assets/img/demo/demo3.jpg')}}" alt=""></li>
                                            <li><img src="{{static_asset('assets/img/demo/demo3.jpg')}}" alt=""></li>
                                            <li class="box"><img src="{{static_asset('assets/img/demo/demo3.jpg')}}" alt="">
                                                <div class="box-item">
                                                    <p>+50</p>
                                                </div>
                                            </li>
                                        </div>
                                    </div>
                                    <div class="card-review-write">
                                        <div class="review-write-header">
                                            <p>Đã từng dùng sản phẩm này ? </p>
                                        </div>
                                        <div class="review-write-body">
                                            <p>Satisfied with the product you bought?
                                                Share your opinion, let's start it!</p>
                                        </div>
                                        <div class="review-write-footer" data-toggle="modal" data-target="#exampleModalCenter">
                                            <p>Viết đánh giá </p>
                                        </div>
                                        <!-- Modal -->
                                        <style>
                                            .modal{
                                                animation: bounceInDown 1s ease;
                                            }
                                        </style>
                                        <div class="modal fade animate_animated animate__backInDown" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLongTitle">Đánh giá của bạn</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                                </div>
                                                <div class="modal-body">
                                                    <textarea name="review" id="review" class="modal-textarea">Nhập vào đây ý kiến của bạn</textarea>
                                                </div>
                                                <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="button" class="btn btn-primary">Submit</button>
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                               </div>
                            </div>
                            <div class="card-comment detail-bg">
                                <div class="card-comment-list">
                                    <div class="card-comment-user">
                                        <div class="card-user-avatar">
                                            <a href="#">
                                                <img src="{{static_asset('assets/img/avatar.png')}}" alt="avatar_user">
                                            </a>
                                        </div>
                                        <div class="card-user-info">
                                            <p  class="card-user-name"><a href="#">Robbie Williams</a></p>
                                            <p class="card-user-clock">8 hours ago</p>
                                        </div>
                                    </div>
                                    <div class="card-comment-review">
                                        <div class="card-comment-start">
                                            <li class="start">
                                                <ion-icon name="star" class='gold'></ion-icon>
                                                <ion-icon name="star" class='gold'></ion-icon>
                                                <ion-icon name="star" class='gold'></ion-icon>
                                                <ion-icon name="star" class='gold'></ion-icon>
                                                <ion-icon name="star-half-outline" class='gold'></ion-icon>                                        
                                            <li>
                                            <li class="review-number"><b>4.5</b></li>
                                        </div>
                                        <div class="card-comment-write">
                                            <p>
                                                Giá thành rẻ, dễ sử dụng.
                                                <br>
                                                Khả năng che phủ ở mức chấp nhận được đối với da không có quá nhiều khuyết điểm.
                                                <br>
                                                Mượt mà, không bị mốc hay khô da.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-comment-list">
                                    <div class="card-comment-user">
                                        <div class="card-user-avatar">
                                            <a href="">
                                                <img src="{{static_asset('assets/img/avatar.png')}}" alt="avatar_user">
                                            </a>
                                        </div>
                                        <div class="card-user-info">
                                            <p  class="card-user-name"><a href="#">nur hotimah05</a></p>
                                            <p class="card-user-clock">8 hours ago</p>
                                        </div>
                                    </div>
                                    <div class="card-comment-review">
                                        <div class="card-comment-start">
                                            <li class="start">
                                                <ion-icon name="star" class='gold'></ion-icon>
                                                <ion-icon name="star" class='gold'></ion-icon>
                                                <ion-icon name="star" class='gold'></ion-icon>
                                                <ion-icon name="star" class='gold'></ion-icon>
                                            <li>
                                            <li class="review-number"><b>4.7</b></li>
                                        </div>
                                        <div class="card-comment-write">
                                            <p>
                                                Có quá ít tone màu, sẽ rất khó khăn cho các bạn có làn da không sáng, vì những màu này vốn được sản xuất cho các cô gái xứ Hàn có làn da như sứ sử dụng.
                                                <br>
                                                Có lẽ sẽ ko phù hợp với bạn nào có làn da quá mẫn cảm hoặc dị ứng với hương liệu do trong kem mình thoáng nghe mùi hương hơi khó chịu chút.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-comment-list">
                                    <div class="card-comment-user">
                                        <div class="card-user-avatar">
                                           <a href="">
                                            <img src="{{static_asset('assets/img/avatar.png')}}" alt="avatar_user">
                                           </a>
                                        </div>
                                        <div class="card-user-info">
                                            <p  class="card-user-name"><a href="#">choerunnisa nisa </a></p>
                                            <p class="card-user-clock">8 hours ago</p>
                                        </div>
                                    </div>
                                    <div class="card-comment-review">
                                        <div class="card-comment-start">
                                            <li class="start">
                                                <ion-icon name="star" class='gold'></ion-icon>
                                                <ion-icon name="star" class='gold'></ion-icon>
                                                <ion-icon name="star" class='gold'></ion-icon>
                                                <ion-icon name="star" class='gold'></ion-icon>
                                                <ion-icon name="star-half-outline" class='gold'></ion-icon>                                        
                                            <li>
                                            <li class="review-number"><b>4.0</b></li>
                                        </div>
                                        <div class="card-comment-write">
                                            <p>
                                                Sử dụng thêm che khuyết điểm để nâng cao hiệu quả che phế đối với da nhiều khuyết điểm.
                                                <br>
                                                - Phủ một lớp phấn nhẹ sau lớp cushion để tạo sự khô thoáng cho làn da vào mùa hè đồng thời cố định lớp trang điểm (đặc biệt là các bạn da dầu, hỗn hợp).
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-comment-list">
                                    <div class="card-comment-user">
                                        <div class="card-user-avatar">
                                           <a href="">
                                            <img src="{{static_asset('assets/img/avatar.png')}}" alt="avatar_user">
                                           </a>
                                        </div>
                                        <div class="card-user-info">
                                            <p  class="card-user-name"><a href="#">Sharon Nathasya </a></p>
                                            <p class="card-user-clock">8 hours ago</p>
                                        </div>
                                    </div>
                                    <div class="card-comment-review">
                                        <div class="card-comment-start">
                                            <li class="start">
                                                <ion-icon name="star" class='gold'></ion-icon>
                                                <ion-icon name="star" class='gold'></ion-icon>
                                                <ion-icon name="star" class='gold'></ion-icon>
                                                <ion-icon name="star" class='gold'></ion-icon>
                                                <ion-icon name="star-half-outline" class='gold'></ion-icon>                                        
                                            <li>
                                            <li class="review-number"><b>3.5</b></li>
                                        </div>
                                        <div class="card-comment-write">
                                            <p>
                                                Có quá ít tone màu, sẽ rất khó khăn cho các bạn có làn da không sáng, vì những màu này vốn được sản xuất cho các cô gái xứ Hàn có làn da như sứ sử dụng.
                                                <br>
                                                Có lẽ sẽ ko phù hợp với bạn nào có làn da quá mẫn cảm hoặc dị ứng với hương liệu do trong kem mình thoáng nghe mùi hương hơi khó chịu chút.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-comment-list">
                                    <div class="card-comment-user">
                                        <div class="card-user-avatar">
                                           <a href="">
                                            <img src="{{static_asset('assets/img/avatar.png')}}" alt="avatar_user">
                                           </a>
                                        </div>
                                        <div class="card-user-info">
                                            <p  class="card-user-name"><a href="#">AyuMuyassaroh </a></p>
                                            <p class="card-user-clock">8 hours ago</p>
                                        </div>
                                    </div>
                                    <div class="card-comment-review">
                                        <div class="card-comment-start">
                                            <li class="start">
                                                <ion-icon name="star" class='gold'></ion-icon>
                                                <ion-icon name="star" class='gold'></ion-icon>
                                                <ion-icon name="star" class='gold'></ion-icon>
                                                <ion-icon name="star" class='gold'></ion-icon>
                                                <ion-icon name="star-half-outline" class='gold'></ion-icon>                                        
                                            <li>
                                            <li class="review-number"><b>4.5</b></li>
                                        </div>
                                        <div class="card-comment-write">
                                            <p>
                                                Sử dụng thêm che khuyết điểm để nâng cao hiệu quả che phế đối với da nhiều khuyết điểm.
                                                <br>
                                                - Phủ một lớp phấn nhẹ sau lớp cushion để tạo sự khô thoáng cho làn da vào mùa hè đồng thời cố định lớp trang điểm (đặc biệt là các bạn da dầu, hỗn hợp).

                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-comment-list justify-content-center">
                                    <div class="card-pagination">
                                        <a href="#">«</a>
                                        <a href="#" class="active" >1</a>
                                        <a href="#">2</a>
                                        <a href="#">3</a>
                                        <a href="#">4</a>
                                        <a href="#">5</a>
                                        <a>...</a>
                                        <a href="#">»</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="detail-right">
                            <div class="detail-selling">
                                <div class="card-selling-top">
                                    <p>Top bán chạy</p>
                                </div>
                                <div class="card-selling">
                                    <div class="card-selling-collum">
                                        <div class="card-selling-content">
                                            <div class="card-selling-img">
                                                <div class="card-selling-list">
                                                    <div class="card-selling-sale animate__animated  animate__infinite	infinite animate__heartBeat">-30%</div>
                                                    <div class="card-selling-new animate__animated  animate__infinite	infinite animate__heartBeat">New</div>
                                                </div>
                                                <img src="{{static_asset('assets/img/demo/demo1.png')}}" alt="">
                                            </div>
                                            <div class="card-selling-title">
                                                <p>Lorem ipsumdolor Lorem ipsum</p>
                                            </div>
                                            <div class="card-selling-price">
                                                <p class="card-selling-price-sale">225,000 đ</p>
                                                <del><p class="card-selling-price-origin">455,000 đ</p></del>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-selling-collum">
                                        <div class="card-selling-content">
                                            <div class="card-selling-img">
                                                <img src="{{static_asset('assets/img/demo/demo2.jpg')}}" alt="">
                                            </div>
                                            <div class="card-selling-title">
                                                <p>Lorem ipsumdolor Lorem ipsum</p>
                                            </div>
                                            <div class="card-selling-price">
                                                <p class="card-selling-price-sale">225,000 đ</p>
                                                <del><p class="card-selling-price-origin">455,000 đ</p></del>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-selling-collum">
                                        <div class="card-selling-content">
                                            <div class="card-selling-img">
                                                <img src="{{static_asset('assets/img/demo/product1.png')}}" alt="">
                                            </div>
                                            <div class="card-selling-title">
                                                <p>Lorem ipsumdolor Lorem ipsum</p>
                                            </div>
                                            <div class="card-selling-price">
                                                <p class="card-selling-price-sale">225,000 đ</p>
                                                <del><p class="card-selling-price-origin">455,000 đ</p></del>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                {{-- </div> --}}
            </div>
        </div>
    </section>
{{-- END CARDS DETAIL --}}

{{-- BEST SELLER --}}
    <section>
        <div class="cards">
            <div class="cards-seller">
                <div class="seller-header">
                    <div class="seller-header-left">
                        <a href="#">Best Seller</a>
                    </div>
                    <div class="seller-header-right">
                        <a href="#"> Xem tất cả </a>
                        <i class="fas fa-chevron-right"></i>
                    </div>
                </div>
                <div class="seller-body">
                    <div class="seller-content">
                        <div class="seller-list">
                            <div class="seller-item"> 
                                <a href="#">
                                    <div class="card-selling-list">
                                        <div class="card-selling-sale animate__animated  animate__infinite	infinite animate__heartBeat">-30%</div>
                                    </div>
                                    <div class="seller-img"> <img class="seller-img-product" src="{{static_asset('assets/img/demo/product1.png')}}" alt="" class="description-image"></div>
                                    <p class="seller-title">Lorem ipsumdolor Lorem ipsum</p>
                                    <div class="seller-detail">
                                    <p class = "seller-price">225,000 đ</p>
                                    <del><p class ="seller-price-sale">455,000 đ</p></del>
                                    <img src="{{static_asset('assets/img/icons/free_delivery_1.png')}}" alt="">
                                    </div>
                                </a>
                                <div class="add-to-cart animate__animated 	infinite animate__backInUp">
                                    <a href="" >Thêm vào giỏ hàng</a>
                                </div>
                            </div>
                            <div class="seller-item"> 
                                <a href="#">
                                    <div class="seller-img"> 
                                        <img class="seller-img-product" src="{{static_asset('assets/img/demo/product1.png')}}" alt="" class="description-image">
                                    </div>
                                    <p class="seller-title">Lorem ipsumdolor Lorem ipsum</p>
                                    <div class="seller-detail">
                                        <p class = "seller-price">225,000 đ</p>
                                        <del><p class ="seller-price-sale">455,000 đ</p></del>
                                        <img src="{{static_asset('assets/img/icons/free_delivery_1.png')}}" alt="">
                                    </div>
                                </a>
                                <div class="add-to-cart animate__animated 	infinite animate__backInUp">
                                    <a href="" >Thêm vào giỏ hàng</a>
                                </div>
                            </div>
                            <div class="seller-item"> 
                                <a href="#">
                                    <div class="card-selling-list">
                                        <div class="card-selling-new animate__animated  animate__infinite	infinite animate__heartBeat">New</div>
                                    </div>
                                    <div class="seller-img">
                                        <img class="seller-img-product" src="{{static_asset('assets/img/demo/demo4.jpg')}}" alt="" class="description-image">
                                    </div>
                                    <p class="seller-title">Lorem ipsumdolor Lorem ipsum</p>
                                    <div class="seller-detail">
                                        <p class = "seller-price">225,000 đ</p>
                                        <del><p class ="seller-price-sale">455,000 đ</p></del>
                                        <img src="{{static_asset('assets/img/icons/free_delivery_1.png')}}" alt="">
                                    </div>
                                </a>
                                <div class="add-to-cart animate__animated 	infinite animate__backInUp">
                                    <a href="" >Thêm vào giỏ hàng</a>
                                </div>
                            </div>
                            <div class="seller-item"> 
                                <a href="#">
                                    <div class="card-selling-list">
                                        <div class="card-selling-sale animate__animated  animate__infinite	infinite animate__heartBeat">-30%</div>
                                        <div class="card-selling-new animate__animated  animate__infinite	infinite animate__heartBeat">New</div>
                                    </div>
                                    <div class="seller-img">
                                        <img class="seller-img-product" src="{{static_asset('assets/img/demo/demo3.jpg')}}" alt="" class="description-image">
                                    </div>
                                    <p class="seller-title">Lorem ipsumdolor Lorem ipsum</p>
                                    <div class="seller-detail">
                                        <p class = "seller-price">225,000 đ</p>
                                        <del><p class ="seller-price-sale">455,000 đ</p></del>
                                        <img src="{{static_asset('assets/img/icons/free_delivery_1.png')}}" alt="">
                                    </div>
                                </a>
                                <div class="add-to-cart animate__animated 	infinite animate__backInUp">
                                    <a href="" >Thêm vào giỏ hàng</a>
                                </div>
                            </div>
                            <div class="seller-item"> 
                                <a href="#">
                                    <div class="seller-img">
                                        <img class="seller-img-product" src="{{static_asset('assets/img/demo/demo2.jpg')}}" alt="" class="description-image">
                                    </div>
                                    <p class="seller-title">Lorem ipsumdolor Lorem ipsum</p>
                                    <div class="seller-detail">
                                        <p class = "seller-price">225,000 đ</p>
                                        <del><p class ="seller-price-sale">455,000 đ</p></del>
                                        <img src="{{static_asset('assets/img/icons/free_delivery_1.png')}}" alt="">
                                    </div>
                                </a>
                                <div class="add-to-cart animate__animated 	infinite animate__backInUp">
                                    <a href="" >Thêm vào giỏ hàng</a>
                                </div>
                            </div>
                            <div class="seller-item"> 
                                <a href="#">
                                    <div class="seller-img">
                                        <img class="seller-img-product" src="{{static_asset('assets/img/demo/demo1.png')}}" alt="" class="description-image">
                                    </div>
                                    <p class="seller-title">Lorem ipsumdolor Lorem ipsum</p>
                                    <div class="seller-detail">
                                        <p class = "seller-price">225,000 đ</p>
                                        <del><p class ="seller-price-sale">455,000 đ</p></del>
                                        <img src="{{static_asset('assets/img/icons/free_delivery_1.png')}}" alt="">
                                    </div>
                                </a>
                                <div class="add-to-cart animate__animated 	infinite animate__backInUp">
                                    <a href="" >Thêm vào giỏ hàng</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="cards-seller-next">
                <div class="seller-header">
                    <div class="seller-header-left">
                        <a href="#">Sale collection</a>
                    </div>
                    <div class="seller-header-right">
                        <a href="#"> Xem tất cả </a>
                        <i class="fas fa-chevron-right"></i>
                    </div>
                </div>
                <div class="seller-body">
                    <div class="seller-content">
                        <div class="seller-list">
                            <div class="seller-item"> 
                                <a href="#">
                                    <div class="card-selling-list">
                                        <div class="card-selling-sale animate__animated  animate__infinite	infinite animate__heartBeat">-30%</div>
                                    </div>
                                    <div class="seller-img">
                                        <img class="seller-img-product" src="{{static_asset('assets/img/demo/product1.png')}}" alt="" class="description-image">
                                    </div>
                                    <p class="seller-title">Lorem ipsumdolor Lorem ipsum</p>
                                    <div class="seller-detail">
                                    <p class = "seller-price">225,000 đ</p>
                                    <del><p class ="seller-price-sale">455,000 đ</p></del>
                                    <img src="{{static_asset('assets/img/icons/free_delivery_1.png')}}" alt="">
                                    </div>
                                </a>
                                <div class="add-to-cart animate__animated 	infinite animate__backInUp">
                                    <a href="" >Thêm vào giỏ hàng</a>
                                </div>
                            </div>
                            <div class="seller-item"> 
                                <a href="#">
                                    <div class="seller-img">
                                        <img class="seller-img-product" src="{{static_asset('assets/img/demo/product1.png')}}" alt="" class="description-image">
                                    </div>
                                    <p class="seller-title">Lorem ipsumdolor Lorem ipsum</p>
                                    <div class="seller-detail">
                                        <p class = "seller-price">225,000 đ</p>
                                        <del><p class ="seller-price-sale">455,000 đ</p></del>
                                        <img src="{{static_asset('assets/img/icons/free_delivery_1.png')}}" alt="">
                                    </div>
                                </a>
                                <div class="add-to-cart animate__animated 	infinite animate__backInUp">
                                    <a href="" >Thêm vào giỏ hàng</a>
                                </div>
                            </div>
                            <div class="seller-item"> 
                                <a href="#">
                                    <div class="card-selling-list">
                                        <div class="card-selling-new animate__animated  animate__infinite	infinite animate__heartBeat">New</div>
                                    </div>
                                    <div class="seller-img">
                                        <img class="seller-img-product" src="{{static_asset('assets/img/demo/demo4.jpg')}}" alt="" class="description-image">
                                    </div>
                                    <p class="seller-title">Lorem ipsumdolor Lorem ipsum</p>
                                    <div class="seller-detail">
                                        <p class = "seller-price">225,000 đ</p>
                                        <del><p class ="seller-price-sale">455,000 đ</p></del>
                                        <img src="{{static_asset('assets/img/icons/free_delivery_1.png')}}" alt="">
                                    </div>
                                </a>
                                <div class="add-to-cart animate__animated 	infinite animate__backInUp">
                                    <a href="" >Thêm vào giỏ hàng</a>
                                </div>
                            </div>
                            <div class="seller-item"> 
                                <a href="#">
                                    <div class="card-selling-list">
                                        <div class="card-selling-sale animate__animated  animate__infinite	infinite animate__heartBeat">-30%</div>
                                        <div class="card-selling-new animate__animated  animate__infinite	infinite animate__heartBeat">New</div>
                                    </div>
                                    <div class="seller-img">
                                        <img class="seller-img-product" src="{{static_asset('assets/img/demo/demo3.jpg')}}" alt="" class="description-image">
                                    </div>
                                    <p class="seller-title">Lorem ipsumdolor Lorem ipsum</p>
                                    <div class="seller-detail">
                                        <p class = "seller-price">225,000 đ</p>
                                        <del><p class ="seller-price-sale">455,000 đ</p></del>
                                        <img src="{{static_asset('assets/img/icons/free_delivery_1.png')}}" alt="">
                                    </div>
                                </a>
                                <div class="add-to-cart animate__animated 	infinite animate__backInUp">
                                    <a href="" >Thêm vào giỏ hàng</a>
                                </div>
                            </div>
                            <div class="seller-item"> 
                                <a href="#">
                                    <div class="seller-img">
                                        <img class="seller-img-product" src="{{static_asset('assets/img/demo/demo2.jpg')}}" alt="" class="description-image">
                                    </div>
                                    <p class="seller-title">Lorem ipsumdolor Lorem ipsum</p>
                                    <div class="seller-detail">
                                        <p class = "seller-price">225,000 đ</p>
                                        <del><p class ="seller-price-sale">455,000 đ</p></del>
                                        <img src="{{static_asset('assets/img/icons/free_delivery_1.png')}}" alt="">
                                    </div>
                                </a>
                                <div class="add-to-cart animate__animated 	infinite animate__backInUp">
                                    <a href="" >Thêm vào giỏ hàng</a>
                                </div>
                            </div>
                            <div class="seller-item"> 
                                <a href="#">
                                    <div class="seller-img">
                                        <img class="seller-img-product" src="{{static_asset('assets/img/demo/demo1.png')}}" alt="" class="description-image">
                                    </div>
                                    <p class="seller-title">Lorem ipsumdolor Lorem ipsum</p>
                                    <div class="seller-detail">
                                        <p class = "seller-price">225,000 đ</p>
                                        <del><p class ="seller-price-sale">455,000 đ</p></del>
                                        <img src="{{static_asset('assets/img/icons/free_delivery_1.png')}}" alt="">
                                    </div>
                                </a>
                                <div class="add-to-cart animate__animated 	infinite animate__backInUp">
                                    <a href="" >Thêm vào giỏ hàng</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
{{-- END BEST SELER --}}

@endsection
@section('scripts')
<script src="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/alertify.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script>
    $(document).ready(function(){
    $('.carousel').slick({
    dots: true,
    infinite: true,
    speed: 300,
    slidesToShow: 3,
    slidesToScroll: 3,
    responsive: [
        {
        breakpoint: 1024,
        settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            infinite: true,
            dots: true
        }
        },
        {
        breakpoint: 600,
        settings: {
            slidesToShow: 2,
            slidesToScroll: 1
        }
        },
    ]
    });
    });
</script>
<script>
    $('.seller-list').slick({
    dots: true,
    infinite: false,
    speed: 300,
    slidesToShow: 5,
    slidesToScroll: 5,
    responsive: [
        {
        breakpoint: 1024,
        settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            dots: true
        }
        },
        {
        breakpoint: 750,
        settings: {
            slidesToShow: 2,
            slidesToScroll: 2
        }
        },
    ]
    });
</script>
<script>
    function changeSeemore() {
        var semester = document.getElementById("rotate");
        var rotate = semester.style.transform;
        // rotate = "rotate(0deg)";
        if(rotate == "rotate(0deg)"){
            var angle =  semester.style.transform="rotate(180deg)";
            var transi = semester.style.transition="transform 0.3s ease";
            // rotate = "rotate(180deg)";
        }
        if(rotate == "rotate(180deg)"){
            var angle =  semester.style.transform="rotate(0deg)";
            var transi = semester.style.transition="transform 0.3s ease";
            // rotate = "rotate(0deg)";
        }
    }
</script>
<script>
    function add(){
        var qty_number = parseInt(document.getElementById("qty-number").value );
            if(qty_number != null){
               var kq = qty_number += 1;
               var qty_number = parseInt(document.getElementById("qty-number").value = kq);
            }
    }
    function subtract(){
        var qty_number = parseInt(document.getElementById("qty-number").value);
            if(qty_number != null){
               if(qty_number <= 0){
                var qty_number = parseInt(document.getElementById("qty-number").value = 1);
               }
               var kq = qty_number -= 1;
               var qty_number = parseInt(document.getElementById("qty-number").value = kq);
            }
    }
</script>
<script>
    
    function addtocart(){
        alertify.success('Thêm vào giỏ hàng thành công');
    }
</script>
@endsection