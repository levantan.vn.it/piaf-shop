@extends('frontend.layout.app')
@section('title')
    Club Piaf
@endsection
@section('content')
    <div class="topbar-mb-category" id="topbar-mb-category-page">
        <div class="btn-form-search">
            <button class="button-search-top" id="btnSearchMB">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none"
                    viewBox="0 0 24 24" stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                        d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"></path>
                </svg>
            </button>
            <button class="button-search-top" id="btnSortMB">
                <svg xmlns="http://www.w3.org/2000/svg" width="17.265" height="19" viewBox="0 0 17.265 19">
                    <g id="Page-1" transform="translate(-0.042 -0.063)">
                      <g id="Category-Page---empty" transform="translate(0.042 0.063)">
                        <g id="Group-5" transform="translate(0 0)">
                          <g id="noun_Settings_943929" transform="translate(0 0)">
                            <g id="Group" transform="translate(0 0)">
                              <path id="Shape" d="M16.483,8.353H14.792A2.956,2.956,0,0,0,11.967,6.2,3.031,3.031,0,0,0,9.119,8.353H.865A.823.823,0,1,0,.865,10H9.119a2.929,2.929,0,0,0,5.651,0h1.691a.821.821,0,0,0,.823-.823A.789.789,0,0,0,16.483,8.353ZM11.967,10.51a1.338,1.338,0,0,1-1.335-1.334,1.335,1.335,0,0,1,2.67,0A1.339,1.339,0,0,1,11.967,10.51Z" transform="translate(-0.042 0.324)"/>
                              <path id="Shape-2" data-name="Shape" d="M.865,3.866H2.623a2.929,2.929,0,0,0,5.651,0h8.21a.821.821,0,0,0,.823-.823.807.807,0,0,0-.823-.823H8.274a2.929,2.929,0,0,0-5.651,0H.865a.821.821,0,0,0-.823.823A.807.807,0,0,0,.865,3.866ZM5.448,1.709A1.338,1.338,0,0,1,6.783,3.043,1.311,1.311,0,0,1,5.448,4.378,1.338,1.338,0,0,1,4.113,3.043,1.368,1.368,0,0,1,5.448,1.709Z" transform="translate(-0.042 -0.063)"/>
                              <path id="Shape-3" data-name="Shape" d="M16.483,14.485H8.274a2.929,2.929,0,0,0-5.651,0H.865a.821.821,0,0,0-.823.823.807.807,0,0,0,.823.823H2.623a2.929,2.929,0,0,0,5.651,0h8.21a.821.821,0,0,0,.823-.823A.808.808,0,0,0,16.483,14.485ZM5.448,16.643a1.338,1.338,0,0,1-1.335-1.334,1.335,1.335,0,0,1,2.67,0A1.338,1.338,0,0,1,5.448,16.643Z" transform="translate(-0.042 0.711)"/>
                            </g>
                          </g>
                        </g>
                      </g>
                    </g>
                  </svg>
            </button>
        </div>
        <div class="form-action-search">
            <form action="">
                <div class="button-back">
                    <button class="btn-back-action">
                       <img src="{{ asset('public/assets/img/icons/back.png') }}" alt="">
                    </button>
                </div>
                <input type="text" name="keyword" class="input-search-mb" placeholder="Nhập từ khóa cần tìm kiếm">
            </form>
        </div>
    </div>
    <div class="container-md category" id="section-categori-page">
        <div class="row bg-primary ads" id="ads_category">
            <div class="col-12 image-ads"
                style="background-image: url({{ asset('public/assets/img/category/ads_banner_1.jpeg') }})">
            </div>
            <div class="col-12 image-ads"
                style="background-image: url({{ asset('public/assets/img/category/ads_banner_2.jpeg') }})">
            </div>
            <div class="col-12 image-ads"
                style="background-image: url({{ asset('public/assets/img/category/ads_banner_3.jpeg') }})">
            </div>
        </div>
        <div class="row" id="breadcrumb_category">
            <nav aria-label="breadcrumb" class="nav_breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item" aria-current="page"><a href="#">Trang chủ</a></li>
                    <li class="breadcrumb-item"><a href="#">{{ $breadcrumbs['category'] }}</a></li>
                </ol>
            </nav>
        </div>
        <div class="row main-category" id="category-content-main">
            <div class="col-12 col-md-2 search-catalog">
                <div class="title-catalog bottom-cross">
                    <h4><span><i class="fa fa-list-ul" aria-hidden="true"></i></span> Tất cả danh mục</h4>
                </div>
                <div class="title-catalog-mb">
                    <p>Danh mục</p>
                </div>
                <div class="list-category">
                    <ul class="list-ul">
                        @foreach ($categories as $category)
                            <li class="list-item">
                                <div class="image-category">
                                    <img src="{{ asset("public/".$category->image) }}" class="" alt="">  
                                </div>
                                <a href="{{ route('get_category', ['id_category' => $category->id]) }}">{{ $category->title }}</a>
                                 
                            </li>
                        @endforeach
                    </ul>
                </div>
                <div class="title-catalog">
                    <h4>BỘ LỌC TÌM KIẾM</h4>
                </div>
                <div class="input_search">
                    <div class="title-mb active">
                        <p>Bộ lọc tìm kiếm</p>
                        <span id="btnBackSort">
                            <img src="{{ asset('public/assets/img/icons/mask_group_141.png') }}" alt="">
                        </span>
                    </div>
                    <form id="form-search-product" action="{{ route('get_category',['id_category' => $breadcrumbs['category_id']]) }}" method="GET">
                        @csrf
                        <p class="title-form">Thương hiệu 
                            <a id="view-all-brand" class="active">Xem tất cả <span><img src="{{ asset('public/assets/img/icons/right-arrow.png') }}" alt=""></span></a>
                            <a id="close-view-all" ><img src="{{ asset('public/assets/img/icons/mask_group_141.png') }}" alt=""></a>
                        </p>
                        <div class="input-group mb-3">
                            <input type="text" name="supplier" class="form-control" placeholder="Tìm kiếm..."
                                aria-label="Tìm kiếm..." aria-describedby="button-search" value="{{ old('supplier') }}">
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary" type="button" id="button-search" disabled>
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none"
                                        viewBox="0 0 24 24" stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                            d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"></path>
                                    </svg>
                                </button>
                            </div>
                        </div>


                        <div class="input-search-brand  bottom-cross">
                            <div class="keyword">
                                <button class="btn-keyword">0 - 9</button>
                                <button class="btn-keyword">A - F</button>
                                <button class="btn-keyword">G - L</button>
                                <button class="btn-keyword">M - R</button>
                                <button class="btn-keyword">S - Z</button>
                                <button class="btn-keyword">Ect</button>
                            </div>
                            @foreach ($supplier as $supp)
                                <div class="checkbox-group">
                                    <input class="" name=" supplierCheckbox[]" type="checkbox"
                                        value="{{ $supp->id }}" id="">
                                    <label class="" for="">
                                   {{ $supp->name }}
                                </label>
                            </div>
                             @endforeach
                                </div>

                                <p class="title-form">Khoảng giá</p>
                                <div class="input-price">
                                    <input id="from_price" type="text" name="from_price" placeholder="Từ">
                                    <span></span>
                                    <input id="to_price" type="text" name="to_price" placeholder="Đến">
                                </div>
                                <div class="checkbox-price">
                                    <div class="form-check">
                                        <input class="form-check-input checkbox_price" name="checkbox_price" type="radio"
                                            id="exampleRadios1" value="0">
                                        <label class="form-check-label" for="exampleRadios1">
                                            0-100k
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input checkbox_price" name="checkbox_price" type="radio"
                                            id="exampleRadios2" value="1">
                                        <label class="form-check-label" for="exampleRadios2">
                                            100-200k
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input checkbox_price" name="checkbox_price" type="radio"
                                            id="exampleRadios3" value="2">
                                        <label class="form-check-label" for="exampleRadios3">
                                            200-500k
                                        </label>
                                    </div>
                                </div>
                                <div class="button-action">
                                    <button class="btn-action btn-delete" id="delete_form">Xóa tất cả</button>
                                    <button class="btn-action btn-apply" id="apply_form" type="submit">Áp dụng</button>
                                </div>
                    </form>
                </div>
            </div>
            <div class="col-12 col-md-10" id="content-product">
                {{-- <div class="menu-sort">
                    <p class="title">Sắp xếp theo</p>
                    <button class="bg-dark text-white">Phổ biến</button>
                    <button>Mới nhất</button>
                    <button>Bán chạy</button>
                    <select class="" id="desc_price">
                    	<option value="">Giá</option>
                        <option value="desc">Cao đến thấp</option>
                        <option value="asc">Thấp đến cao</option>
                    </select>
                </div> --}}
                <div class="main-content-prodcut">
                    @foreach ($products as $product)
                        <div class="product-items">
                            <div class="product-image">
                                <img src="{{ asset('public/' . explode(',', $product->images)[0]) }}" alt="">
                            </div>
                            <div class="title">
                                {{ $product->name }}
                            </div>
                            <div class="price">
                                <span class="sale-price">{{ $product->purchase_price }} đ</span>
                                <span class="origin-price">{{ $product->unit_price }} đ</span>
                                <span class="free-ship-icon float-right">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="18" height="16.418"
                                        viewBox="0 0 18 16.418">
                                        <defs>
                                            <style>
                                                .cls-1 {
                                                    fill: #00bfa5;
                                                }

                                            </style>
                                        </defs>
                                        <g id="free-delivery1" transform="translate(0 -0.5)">
                                            <path id="Path_1018" data-name="Path 1018" class="cls-1"
                                                d="M53.6,4.892c.234,0,.469-.1.469-.288V3.054h.907c.2,0,.282-.192.282-.355,0-.192-.1-.367-.282-.367h-.907V1.317h1.556c.18,0,.282-.192.282-.415,0-.192-.084-.4-.282-.4H53.537c-.2,0-.409.1-.409.288V4.6c0,.192.234.288.469.288Zm0,0"
                                                transform="translate(-51.251)" />
                                            <path id="Path_1019" data-name="Path 1019" class="cls-1"
                                                d="M158.148,4.893c.234,0,.469-.1.469-.288V3.156h.4l.823,1.6a.32.32,0,0,0,.3.174.61.61,0,0,0,.589-.487.2.2,0,0,0-.03-.108l-.733-1.328a1.184,1.184,0,0,0,.727-1.22c0-.979-.655-1.286-1.478-1.286h-1.244a.28.28,0,0,0-.288.282V4.6c0,.192.234.288.469.288Zm.469-3.575h.595c.336,0,.541.138.541.559s-.2.559-.541.559h-.595Zm0,0"
                                                transform="translate(-152.142 0)" />
                                            <path id="Path_1020" data-name="Path 1020" class="cls-1"
                                                d="M274.369,4.892h2.139c.2,0,.282-.21.282-.4,0-.222-.1-.415-.282-.415H274.9V3.054h.9c.2,0,.282-.192.282-.355,0-.192-.1-.367-.282-.367h-.9V1.318h1.61c.18,0,.282-.192.282-.415,0-.192-.084-.4-.282-.4h-2.139c-.2,0-.409.1-.409.288V4.6c0,.192.2.288.409.288Zm0,0"
                                                transform="translate(-264.339 0)" />
                                            <path id="Path_1021" data-name="Path 1021" class="cls-1"
                                                d="M383,4.892h2.139c.2,0,.282-.21.282-.4,0-.222-.1-.415-.282-.415h-1.61V3.054h.9c.2,0,.282-.192.282-.355,0-.192-.1-.367-.282-.367h-.9V1.318h1.61c.18,0,.282-.192.282-.415,0-.192-.084-.4-.282-.4H383c-.2,0-.409.1-.409.288V4.6c0,.192.2.288.409.288Zm0,0"
                                                transform="translate(-369.167 0)" />
                                            <path id="Path_1022" data-name="Path 1022" class="cls-1"
                                                d="M124.436,405.5a1.111,1.111,0,1,0-1.111,1.115A1.113,1.113,0,0,0,124.436,405.5Zm0,0"
                                                transform="translate(-117.921 -389.696)" />
                                            <path id="Path_1023" data-name="Path 1023" class="cls-1"
                                                d="M397.1,405.5a1.111,1.111,0,1,0-1.111,1.115A1.113,1.113,0,0,0,397.1,405.5Zm0,0"
                                                transform="translate(-381.022 -389.696)" />
                                            <path id="Path_1024" data-name="Path 1024" class="cls-1"
                                                d="M87.158,187.18l-1.36-.56-1.147-2.579a1.08,1.08,0,0,0-.984-.643H73.52a.529.529,0,0,0-.527.53v1.021h.527a.53.53,0,0,1,0,1.06h-.527v1.06h1.827a.53.53,0,0,1,0,1.06H72.993v1.06h.527a.53.53,0,0,1,0,1.06h-.527v1.08a.528.528,0,0,0,.527.53h.2a2.154,2.154,0,0,1,4.216,0H83.3a2.154,2.154,0,0,1,4.216,0h.375a.529.529,0,0,0,.527-.53v-2.266a2.028,2.028,0,0,0-1.255-1.885ZM82.1,187.2a.528.528,0,0,1-.527-.53v-2.333h2.09c.009,0,1.256,2.788,1.256,2.788a.518.518,0,0,0,.041.075Zm0,0"
                                                transform="translate(-70.413 -176.521)" />
                                            <path id="Path_1025" data-name="Path 1025" class="cls-1"
                                                d="M32.522,347.277a.53.53,0,1,0,0,1.06h.919v-1.06Zm0,0"
                                                transform="translate(-30.861 -334.59)" />
                                            <path id="Path_1026" data-name="Path 1026" class="cls-1"
                                                d="M32.522,287.277a.53.53,0,0,0,0,1.06h.919v-1.06Zm0,0"
                                                transform="translate(-30.861 -276.698)" />
                                            <path id="Path_1027" data-name="Path 1027" class="cls-1"
                                                d="M.53,227.273a.53.53,0,0,0,0,1.06H2.58v-1.06Zm0,0"
                                                transform="translate(0 -218.803)" />
                                        </g>
                                    </svg>

                                </span>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $(document).ready(function() {

            // slide banner ads
            $('#ads_category').slick();
            //appent icon arrow in button slide banner
            $('#ads_category > button.slick-prev').html(`
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="32" height="360" viewBox="0 0 32 360">
                    <defs>
                    <style>
                    .cls-1 {
                        fill: #fff;
                    }

                    .cls-2 {
                        clip-path: url(#clip-path);
                    }

                    .cls-3 {
                        fill: none;
                        stroke: #fff;
                        stroke-linecap: round;
                        stroke-linejoin: round;
                    }
                    </style>
                    <clipPath id="clip-path">
                    <rect id="Rectangle_2393" data-name="Rectangle 2393" class="cls-1" width="32" height="360" transform="translate(-0.191 -100.634)"/>
                    </clipPath>
                    </defs>
                    <g id="Mask_Group_520" data-name="Mask Group 520" class="cls-2" transform="translate(0.191 100.634)">
                        <path id="Path_1017" data-name="Path 1017" class="cls-3" d="M0,0V18.2H18.479" transform="translate(17.762 65.933) rotate(45)"/>
                    </g>
                </svg>
            `);
            $('#ads_category > button.slick-next').html(`
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="32" height="360" viewBox="0 0 32 360">
                    <defs>
                        <style>
                        .cls-1 {
                            fill: #fff;
                        }

                        .cls-2 {
                            clip-path: url(#clip-path);
                        }

                        .cls-3 {
                            fill: none;
                            stroke: #fff;
                            stroke-linecap: round;
                            stroke-linejoin: round;
                        }
                        </style>
                        <clipPath id="clip-path">
                        <rect id="Rectangle_2393" data-name="Rectangle 2393" class="cls-1" width="32" height="360" transform="translate(-0.191 -100.634)"/>
                        </clipPath>
                    </defs>
                    <g id="Mask_Group_520" data-name="Mask Group 520" class="cls-2" transform="translate(0.191 100.634)">
                        <path id="Path_1017" data-name="Path 1017" class="cls-3" d="M0,0V18.2H18.479" transform="translate(17.762 65.933) rotate(45)"/>
                    </g>
                </svg>
            `);


            $('.checkbox_price').change(function(e) {
                e.preventDefault();
                var value = $(this).attr('value');
                if (value == 0) {
                    $('#from_price').val('0');
                    $('#to_price').val('100');
                } else if (value == 1) {
                    $('#from_price').val('100');
                    $('#to_price').val('200');
                } else {
                    $('#from_price').val('200');
                    $('#to_price').val('500');
                }
            });

			$('#delete_form').click(function (e) { 
				e.preventDefault();
				$('#form-search-product')[0].reset();
				window.location.href = `{{ route('get_category',['id_category' => $breadcrumbs['category_id']]) }}`;
			});

			$('#desc_price').change(function (e) { 
				e.preventDefault();
				var sort = this.value;
				var current_url = window.location.href;
                var new_url = "";
                if (!current_url.includes('sort_price')) {
                    new_url = current_url+ "?sort_price=" + sort;
                }else if (current_url.includes('sort_price=desc')) {
                     new_url = current_url.replace('desc', sort);
                } else {
                     new_url = current_url.replace('asc', sort);
                }
				window.location.href = new_url;
			});


            $('#btnSearchMB').click(function (e) { 
                e.preventDefault();
               $('.form-action-search').toggle('active');
               $('.btn-form-search').css('display','none');
            });
            
            $('.btn-back-action').click(function (e) { 
                e.preventDefault();
                $('.form-action-search').toggle('active');
               $('.btn-form-search').css('display','flex');
            });

            $('#btnSortMB').click(function (e) { 
                e.preventDefault();
               $('.input_search').toggle('active');
            });
            
            $('#btnBackSort').click(function (e) { 
                e.preventDefault();
                $('.input_search').toggle('active');
            });

            $('.title-form a').click(function (e) { 
                e.preventDefault();
                $('.input-search-brand').toggleClass('active');
                $('#close-view-all').toggleClass('active');
                $('#view-all-brand').toggleClass('active');
                $('.title-mb').toggleClass('active');
                $('form#form-search-product').toggleClass('active');
                
            });
        });
    </script>
@endsection
