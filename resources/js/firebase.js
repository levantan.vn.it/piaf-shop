import firebase from "firebase/app";
import "firebase/database";
require('firebase/auth');
import 'firebase/firestore';
const firebaseConfig = {
  apiKey: "AIzaSyDQkFQQBCT4cs27h6Q_LFvlSUo-aTD77pg",
  authDomain: "avocadoo-54172.firebaseapp.com",
  databaseURL: "https://avocadoo-54172.firebaseio.com",
  projectId: "avocadoo-54172",
  storageBucket: "avocadoo-54172.appspot.com",
  messagingSenderId: "17652245842",
  appId: "1:17652245842:web:3488c0fc70b8a03de9068b"
};
var fire = firebase.initializeApp(firebaseConfig);
export default fire;
