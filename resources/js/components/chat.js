import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import moment from 'moment';
import firebase from "../firebase";
import Autocomplete from 'react-autocomplete'
import queryString from 'query-string';
import { random } from 'lodash';
import ModalImage from 'react-modal-image';

const myFirestore = firebase.firestore();
const messagesRef = myFirestore.collection("conversations");

// get userid1 or userid2 in firestore
async function getIsUserId1OrisUserId2(UserID) {
    // const isUserId1 = messagesRef.where('user', "array-contains", UserID.toString()).orderBy("lastCreate_at", "desc").get();
    const isUserId1 = messagesRef.orderBy("last_created_at", "desc").get();
    const [isUserId1QuerySnapshot] = await Promise.all([
        isUserId1
    ]);
    return isUserId1QuerySnapshot.docs;
}

async function getMessage() {
    // const isUserId1 = messagesRef.where('user', "array-contains", UserID.toString()).orderBy("lastCreate_at", "desc").get();
    const isUserId1 = messagesRef.orderBy("last_created_at", "desc").get();
    const [isUserId1QuerySnapshot] = await Promise.all([
        isUserId1
    ]);
    return isUserId1QuerySnapshot.docs;
}

const default_avatar = '/public/assets/img/avatar-place.png';
const arrow_img = '/public/assets/img/icons/arrow-down.png';
const base_url = location.hostname;
export default class Chat extends Component {
    constructor() {
        super();
        this._isMounted = false;
        this.state = {
            listChannel: [],
            listChannelReslove: [],
            listMess: [],
            user: [],
            userID: [],
            inforChannelConnection: [],
            loadMess: false,
            inputMessChange: '',
            sending: false,
            listUser: [],
            inputsearchChannel: '',
            listChannelSearch: [],
            listChannelSearchReslove: [],
            product: '',
            show : false,
            visible: false,
            member:'',
            countShowChannel: 0,
            countShowChannelReslove: 0,
            search: false,
            image: ''

        };
        this.submitChat = this.submitChat.bind(this);
        this.searchChannel = this.searchChannel.bind(this);
        this.toggleClass = this.toggleClass.bind(this);
    }
    componentWillUnmount() {
        this.setState = (state, callback) => {
            return;
        };
    }
    componentDidMount() {
        firebase.auth().signInAnonymously().catch(function (error) {
            console.log(error);
        });
        axios.get(`/admin/listUser?search=`).then(res => {
            const listUser = [];
            res.data.map(user => {
                listUser.push(user);
            });
            this.setState({ listUser: listUser });
            this.getListChannel();
        });

    }
    toggleClass() {
        const temp = this.state.visible;
        this.setState({visible: !temp});
    }
    // get list channel (call getIsUserId1OrisUserId2)
    getListChannel() {
        // get user present and UserID of admin
        const url = '/admin/message/getUser';
        axios.get(url).then(async (response) => {
            let user = response.data.user;
            const userID = response.data.userID;
            const urlUser = '/admin/user/detail/';
            const { listUser } = this.state;

            // listen change list channel
            messagesRef.onSnapshot(data => {
                let { inforChannelConnection } = this.state;

                const listChannel = [];
                const listChannelReslove = [];

                getMessage().then((res) => {
                    res.map(async (doc) => {
                        const data = doc.data();
                        data.id = doc.id;
                        const idGet = data.user_id
                        const productId = data.product_id;
                        // const unreadMess = data.userID1 == userID.toString() ? (data.unreadmessagesUser1 ?? 0) : (data.unreadmessagesUser2 ?? 0)
                        const unreadMess = data.product_unread ?? 0;
                        listUser.map(us => {
                            if (us.id?.toString() == idGet?.toString()) {
                                if(data.reslove != undefined && data.reslove == 1){
                                    const channel = Object.assign({}, data, { detail: us });
                                    channel.product_unread = unreadMess;
                                    if(data.last_created_at) {
                                        listChannelReslove.push(channel);
                                    }
                                    if (inforChannelConnection && inforChannelConnection.id == channel.id) {
                                        inforChannelConnection = channel;
                                    }
                                    this.setState({
                                        listChannelReslove
                                    });
                                }else{
                                    const channel = Object.assign({}, data, { detail: us });
                                    channel.product_unread = unreadMess;
                                    if(data.last_created_at) {
                                        listChannel.push(channel);
                                    }
                                    if (inforChannelConnection && inforChannelConnection.id == channel.id) {
                                        inforChannelConnection = channel;
                                    }
                                    this.setState({
                                        listChannel
                                    });
                                }

                            }
                        });
                    });
                    this.setState({ inforChannelConnection });

                }).catch(function (error) {
                    console.log("Error getting documents: ", error);
                });
            });
            this.setState({ user, userID });
        });
    }

    // connect to channel
    conectToChannel(channel) {
        this.setState({ loadMess: true,show : true });
        axios.get(`/admin/products/detail/${channel.product_id}`).then((res) => {
            const data = res.data;
            this.setState({product: data});
        }).catch((err) => {
            console.log(err)
        })
        const { userID } = this.state;
        
        axios.get(`/admin/members/detail-member/${userID}`).then(res => {
            const member = res.data;
            this.setState({member: member});
        }).catch((err) => {
            console.log(err);
        })
        myFirestore.collection(`conversations/${channel.id}/events`).orderBy("created_at", "asc").onSnapshot(data => {
            const listMess = [];
            data.forEach(mess => {
                let message = mess.data();
                message.id = mess.id;
                message.detail = channel.detail;
                if (message.sender_id != userID?.toString()) {

                }
                listMess.push(message);
            });
            this.setState({ listMess, loadMess: false, inforChannelConnection: channel });
            this.scrollToBottom();
            this.readMessage();
            this.setState({});
        });
    }
    resolveToChannel(){
        const { inforChannelConnection, userID } = this.state;
        myFirestore.collection(`conversations`).doc(inforChannelConnection.id).update({
            reslove: 1
        }).then(() => {
            inforChannelConnection.reslove = 1;
            this.setState({ inforChannelConnection });
        });
        return;
    }
    timeToChannel(channel) {
        var msPerMinute = 60 * 1000;
        var msPerHour = msPerMinute * 60;
        var msPerDay = msPerHour * 24;
        var msPerWeek = msPerDay * 7;
        var msPerMonth = msPerDay * 30;
        var msPerYear = msPerDay * 365;
        var msPerManyYeat = msPerYear * 10;
        var current= new Date();
        var elapsed = current - new Date(channel.last_created_at);

        if (elapsed < msPerMinute) {
            return Math.round(elapsed/1000) + ' 초 전';
        }

        else if (elapsed < msPerHour) {
             return Math.round(elapsed/msPerMinute) + ' 분 전';
        }

        else if (elapsed < msPerDay ) {
             return Math.round(elapsed/msPerHour ) + ' 시간 전';
        }

        else if (elapsed < msPerWeek) {
            return Math.round(elapsed/msPerDay) + ' 일전';
        }
        else if (elapsed < msPerMonth) {
            return Math.round(elapsed/msPerWeek) + ' 주 전';
        }

        else if (elapsed < msPerYear) {
            return Math.round(elapsed/msPerMonth) + ' 개월 전';
        }
        else if (elapsed < msPerManyYeat) {
            return Math.round(elapsed/msPerYear ) + ' 년 전';
        } else {
            return "1 분 전";
        }
        return "1 min ago";
    }
    inputMessChange(value) {
        this.setState({ inputMessChange: value.target.value });
    }
    // send message
    submitChat(event) {
        event.preventDefault();
        const { listMess, inputMessChange, inforChannelConnection, userID } = this.state;
        var current= new Date();
        var ids= `EVENT-${Date.parse(current)}-${random(1000)}`;
        if (inputMessChange.length != 0) {
            this.setState({ sending: true });
            const newMess = {
                message: inputMessChange,
                created_at: moment().utcOffset('+0000').format(),
                type: 'TEXT',
                sender_is_user: false,
                sender_id: userID
            };

            if (inforChannelConnection.length != 0) {
                // send message
                myFirestore.collection(`conversations/${inforChannelConnection.id}/events`).doc(ids).set(newMess).then(() => {
                    let lastMess = [];
                    if (inforChannelConnection.userID1 != userID?.toString()) {
                        lastMess = {
                            last_message: newMess.message,
                            last_sender_is_user: newMess.sender_is_user,
                            last_created_at: newMess.created_at,
                            last_type: newMess.type,
                            product_unread: inforChannelConnection.product_unread ? inforChannelConnection.product_unread + 1 : 1,
                            user_unread: inforChannelConnection.user_unread ? inforChannelConnection.user_unread + 1 : 1,
                            user_id: inforChannelConnection.user_id,
                            sender_id: newMess.sender_id
                        };
                    } else {
                        lastMess = {
                            last_message: newMess.message,
                            last_sender_is_user: newMess.sender_is_user,
                            last_created_at: newMess.created_at,
                            last_type: newMess.type,
                            product_unread: inforChannelConnection.product_unread ? inforChannelConnection.product_unread + 1 : 1,
                            user_unread: inforChannelConnection.user_unread ? inforChannelConnection.user_unread + 1 : 1,
                        };
                    }
                    // update lastmessage
                    myFirestore.collection(`conversations`).doc(inforChannelConnection.id).update(lastMess).then(() => {
                        if (lastMess.product_unread) {
                            inforChannelConnection.product_unread = lastMess.product_unread;
                        } else {
                            inforChannelConnection.product_unread = lastMess.product_unread;
                        }
                        this.setState({ inputMessChange: '', sending: false, inforChannelConnection });
                    });
                    myFirestore.collection(`conversations`).doc(inforChannelConnection.id).update(lastMess).then(() => {
                        if (lastMess.product_unread) {
                            inforChannelConnection.user_unread = lastMess.user_unread;
                        } else {
                            inforChannelConnection.user_unread = lastMess.user_unread;
                        }
                        this.setState({ inputMessChange: '', sending: false, inforChannelConnection });
                    });
                    /*
                    if (inforChannelConnection.userID1 != userID.toString())
                        axios.post(`/admin/message/${inforChannelConnection.userID1}`, {
                            message: newMess.content
                        });
                    else {
                        axios.post(`/admin/message/${inforChannelConnection.userID2}`, {
                            message: newMess.content
                        });
                    }*/
                    const data ={
                        "conversation_id" :  inforChannelConnection.id,
                        "sender_id" : inforChannelConnection.sender_id,
                        "product_id": inforChannelConnection.product_id,
                        "message" : newMess.message,
                        "notify_enabled": inforChannelConnection.notify_enabled
                    }
                    axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
                    axios.defaults.headers.common['X-CSRF-TOKEN'] = window.Laravel.csrfToken;
                    axios.post(`/admin/message/${inforChannelConnection.user_id}`,data).then((res) => {console.log(res)}).catch((err) => {console.log(err)});
                });


            }
        }
        this.scrollToBottom();
    }
    // search in list channel
    searchChannel(e) {
        const { listChannel,listChannelReslove } = this.state;
        this.setState({ inputsearchChannel: e.target.value,search : true });
        const search = e.target.value?.toLowerCase() ?? '';

        console.log(search,'search');
        // filter channel have name or email
        const resultChannel = listChannel.filter(item => {
            if (item.detail.displayname?.toLowerCase().includes(search))
                return true
            else if (item.detail.email?.toLowerCase().includes(search)) {
                return true
            }
            return false;

        });
        const resultResolve = listChannelReslove.filter(item => {
            if (item.detail.displayname?.toLowerCase().includes(search))
                return true
            else if (item.detail.email?.toLowerCase().includes(search)) {
                return true
            }
            return false;

        });
        this.setState({countShowChannel: resultChannel.length});
        this.setState({countShowChannelReslove: resultResolve.length});
        this.setState({ listChannelSearch: resultChannel });
        this.setState({ listChannelSearchReslove: resultResolve });
    }
    scrollToBottom() {
        this.messagesEnd.scrollIntoView({ behavior: "smooth" });

    }


    readMessage() {
        const { inforChannelConnection, userID } = this.state;
        myFirestore.collection(`conversations`).doc(inforChannelConnection.id).update({
            product_unread: 0
        }).then(() => {
            inforChannelConnection.product_unread = 0;
            this.setState({ inforChannelConnection });
        });
        return;
        if (inforChannelConnection.userID1 == userID.toString()) {
            myFirestore.collection(`conversations`).doc(inforChannelConnection.id).update({
                unreadmessagesUser1: 0
            }).then(() => {
                inforChannelConnection.product_unread = 0;
                this.setState({ inforChannelConnection });
            });
        } else {
            myFirestore.collection(`conversations`).doc(inforChannelConnection.id).update({
                unreadmessagesUser2: 0
            }).then(() => {
                inforChannelConnection.product_unread = 0;
                this.setState({ inforChannelConnection });
            });
        }

    }

    userUrl(inforChannelConnection){
        const id = inforChannelConnection.detail.id;
        return "/admin/users/"+id+"/detail";
    }
    producturl(id) {
        return "/admin/products/"+id+"/detail";
    }
    
    render() {
        const { sending,show, user,visible,member,product, countShowChannel,countShowChannelReslove,image,
            listChannelSearch, listChannelSearchReslove,inputsearchChannel, listChannel,search,
            listChannelReslove, listUser, userID, inforChannelConnection, listMess, loadMess, inputMessChange } = this.state;
        const showChannel = [];
        const listMessage = [];
        const showChannelReslove = [];
        console.log('inforChannelConnection here',product);
        if (listChannel.length != 0)
            if (inputsearchChannel.length == 0) {

                listChannel.map(channel => {
                    showChannel.push(
                        <li key={channel.id} className={`contact  ${channel.id == inforChannelConnection.id ? 'active' : ''}`} onClick={() => this.conectToChannel(channel)}>
                            <div className="wrap">
                                <img src={channel.detail.avatar_url ?? default_avatar} alt={channel.detail.displayname} />
                                <div className="meta">
                                    <p className="name mb-0">{channel.detail.displayname ?? ''}<span className="time">{this.timeToChannel(channel)}</span></p>
                                    <p className="name mb-0">{channel.detail.phone ?? ''}</p>
                                </div>
                            </div>
                            <p className="preview mb-0">{channel.last_type == "IMAGE" ? `${channel.detail.displayname} 님이 사진을 보냈습니다.` : channel.last_message}
                                    {channel.unreadmessages > 0 ? '<span className="unreadmessages">` (${channel.unreadmessages})`</span>' : ''}
                                </p>
                        </li>
                    );

                });
            } else {
                // list channel search
                listChannelSearch.map(channel => {
                    showChannel.push(
                        <li key={channel.id} className={`contact  ${channel.id == inforChannelConnection.id ? 'active' : ''}`} onClick={() => this.conectToChannel(channel)}>
                            <div className="wrap">
                                <img src={channel.detail.avatar_url ?? default_avatar} alt={channel.detail.displayname} />
                                <div className="meta">
                                    <p className="name mb-0">{channel.detail.displayname ?? ''}<span className="time">{this.timeToChannel(channel)}</span></p>
                                    <p className="name mb-0">{channel.detail.phone ?? ''}</p>
                                </div>
                            </div>
                            <p className="preview mb-0">{channel.last_type == "IMAGE"? `${channel.detail.displayname} 님이 사진을 보냈습니다.` : channel.last_message}
                                {channel.unreadmessages > 0 ? '<span className="unreadmessages">` (${channel.unreadmessages})`</span>' : ''}

                            </p>
                        </li>
                    );

                });
            }
        if (listChannelReslove.length != 0)
            if (inputsearchChannel.length == 0) {
                listChannelReslove.map(channel => {
                    showChannelReslove.push(
                        <li key={channel.id} className={`contact border-bottom border-light  ${channel.id == inforChannelConnection.id ? 'active' : ''}`} onClick={() => this.conectToChannel(channel)}>
                            <div className="wrap">
                                <img src={channel.detail.avatar_url ?? default_avatar} alt={channel.detail.displayname} />
                                <div className="meta">
                                    <p className="name mb-0">{channel.detail.displayname ?? ''}<span className="time">{this.timeToChannel(channel)}</span></p>
                                    <p className="name mb-0">{channel.detail.phone ?? ''}</p>
                                </div>
                            </div>
                            <p className="preview mb-0">{channel.last_type == "IMAGE" ? `${channel.detail.displayname} 님이 사진을 보냈습니다.` : channel.last_message}
                                    {channel.unreadmessages > 0 ? '<span className="unreadmessages">` (${channel.unreadmessages})`</span>' : ''}
                                </p>
                        </li>
                    );

                });
            } else {
                // list channel search
                listChannelSearchReslove.map(channel => {
                    showChannelReslove.push(
                        <li key={channel.id} className={`contact border-bottom border-light  ${channel.id == inforChannelConnection.id ? 'active' : ''}`} onClick={() => this.conectToChannel(channel)}>
                            <div className="wrap">
                                <img src={channel.detail.avatar_url ?? default_avatar} alt={channel.detail.displayname} />
                                <div className="meta">
                                    <p className="name mb-0">{channel.detail.displayname ?? ''}<span className="time">{this.timeToChannel(channel)}</span></p>
                                    <p className="name mb-0">{channel.detail.phone ?? ''}</p>
                                </div>
                            </div>
                            <p className="preview mb-0">{channel.last_type == "IMAGE"? `${channel.detail.displayname} 님이 사진을 보냈습니다.` : channel.last_message}
                                {channel.unreadmessages > 0 ? '<span className="unreadmessages">` (${channel.unreadmessages})`</span>' : ''}

                            </p>
                        </li>
                    );

                });
            }
        if (listMess.length != 0 && inforChannelConnection.length != 0)
            listMess.map(mess =>
                {
                    listMessage.push(mess.sender_is_user ?
                    
                        mess.thumbnail ?
                        <li key={mess.id} className="sent">
                            <img src={inforChannelConnection.detail.avatar_url ?? default_avatar} alt={inforChannelConnection.detail.displayname} />
                            <div className="sent-detail">
                                <div className="user-detail">
                                    <p className="user-name"><strong>{inforChannelConnection.detail.displayname}</strong></p>
                                    <span>{moment(mess.created_at).valueOf() < moment().subtract('days',1).valueOf() ? moment(mess.created_at).format('YYYY-MM-DD HH:mm:ss') : moment(mess.created_at).format('HH:mm:ss') }</span>
                                </div>
                                <div className="image">
                                    {/* <img className="img_user" src={mess.thumbnail} alt="image" /> */}
                                    <ModalImage
                                        small={mess.thumbnail}
                                        large={mess.thumbnail}
                                        alt="사진을"
                                        hideDownload={true}
                                        hideZoom={true}
                                        className="modal-image"
                                    />
                                </div>
                            </div>
                        </li>
                        :
                        <li key={mess.id} className="sent">
                            <img src={inforChannelConnection.detail.avatar_url ?? default_avatar} alt={inforChannelConnection.detail.displayname} />
                            <div className="sent-detail">
                                <div className="user-detail">
                                    <p className="user-name"><strong>{inforChannelConnection.detail.displayname}</strong></p>
                                    <span>{moment(mess.created_at).valueOf() < moment().subtract('days',1).valueOf() ? moment(mess.created_at).format('YYYY-MM-DD HH:mm:ss') : moment(mess.created_at).format('HH:mm:ss') }</span>
                                </div>
                            <p>{mess.message}</p>
                            </div>
                        </li>
                        :
                        <li key={mess.id} className="replies">
                            <img src={member.avatar ?? default_avatar} alt={member.name} />
                            <div className="sent-detail">
                                <div className="user-detail">
                                    <span>{moment(mess.created_at).valueOf() < moment().subtract('days',1).valueOf() ? moment(mess.created_at).format('YYYY-MM-DD HH:mm:ss') : moment(mess.created_at).format('HH:mm:ss') }</span>
                                    <p className="user-name"><strong>{member.name}</strong></p>
                                </div>
                            <p>{mess.message}</p>
                            </div>
                        </li>
                    )}
            );
        return (
            <>
                {/* <div className="image_preview">
                    < img src={image} alt=""/>
                </div> */}
                <div id="sidepanel">
                    <div id="search">
                        <label htmlFor="search"><i className="fa fa-search" aria-hidden="true"></i></label>
                        <input className="search_input" autoComplete="off" onChange={this.searchChannel} type="text" placeholder=" 회원 ID..." />
                    </div>
                    <div id="contacts">
                        <div className="new-mess">
                            <div className="total_new">새 메시지({search ? countShowChannel : listChannel.length})</div>
                            <ul>
                                {showChannel}

                            </ul>
                        </div>
                        <div className="reslove-mess">
                            <div className="total_resolve">읽은 메시지({search ? countShowChannelReslove : listChannelReslove.length})</div>
                            <ul>
                                {showChannelReslove}

                            </ul>
                        </div>
                    </div>
                </div>
                <div className="content">
                    {
                        show ?
                        <div className="product_detail">
                            <div className="wrapper_product">
                                <a className="image" href={this.producturl(product?.product?.id)} target="_blank">
                                <img src={product?.product?.images} alt="" />
                                </a>
                                <div className="product_left">
                                    <p>SKU: <strong>{product?.product?.minPriceSku}</strong></p>
                                    <p>{product?.product?.name}</p>
                                    <p>{product?.price ?? 0} 원</p>
                                </div>
                            </div>
                            <div className={visible ? "arrow active" : "arrow"} onClick={this.toggleClass}><img src={arrow_img} alt="Arrow"/></div>
                            <div className={visible ? "list_sku active" : "list_sku"}>
                            <div className="box_weight row mb-2">
                                {
                                    product?.product_sku && product?.product_sku.map((item,index) => (
                                         
                                        <div className="box col-md-3" key={index}>
                                            <span>Box {item?.weight}kg</span>
                                        </div>
                                        
                                    ))

                                }
                            </div>
                            <div className="sku">
                                {
                                    product?.product_sku && product?.product_sku.map((item,index) => (
                                         
                                        <div className="sku_item" key={index}>
                                            <p>{item?.sku}</p>
                                            <p>{item?.qty}</p>
                                        </div>
                                        
                                    ))

                                }
                             </div>   
                            </div>

                        </div>
                        :
                        ''
                    }
                    {inforChannelConnection.length != 0 ? (
                        <div className="contact-profile">
                            <div className="profile_wrapper">
                            <img src={inforChannelConnection.detail.avatar_url ?? default_avatar} alt={inforChannelConnection.detail.displayname} />
                            <div className="meta">
                                <p>{inforChannelConnection.detail.displayname}</p>
                                <p><strong>{inforChannelConnection.detail.phone}</strong></p>
                            </div>
                            </div>
                            <div className="meta-right">
                                <a className="profile" target="_blank" href={this.userUrl(inforChannelConnection)}><i className="las la-user-circle"></i></a>
                                {
                                    inforChannelConnection.reslove == 1 ? 
                                    ''
                                    :
                                    <span className="resolve" onClick={() => this.resolveToChannel()}><i className="las la-check-circle"></i></span>
                                }
                            </div>
                        </div>) : ''}
                    <div className="messages">
                        <ul>
                            {loadMess ? '' : listMessage}
                            <li
                                style={{ float: "left", clear: "both" }}
                                ref={(el) => { this.messagesEnd = el; }}
                            />
                        </ul>
                    </div>
                    {inforChannelConnection.length != 0 ?
                        <div className="message-input">
                            <div className="wrap">
                                {sending ? <>
                                    <input type="text" onChange={(value) => this.inputMessChange(value)} value={inputMessChange} placeholder="메시지를 입력하세요 ..." />
                                    <button type="submit" className="submit"><i className="fas fa-spinner"></i></button>
                                </>
                                    : <form onSubmit={this.submitChat}>
                                        <input type="text" onChange={(value) => this.inputMessChange(value)} value={inputMessChange} placeholder="메시지를 입력하세요 ..." />
                                        <button type="submit" className="submit">전송</button>
                                    </form>}
                            </div>

                        </div>
                        : ''
                    }
                </div>
            </>
        );
    }
}
if (document.getElementById('message'))
    ReactDOM.render(<Chat />, document.getElementById('message'));
